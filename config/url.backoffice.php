<?php

    return [
      'backoffice' => '/backoffice',
      'filemanager' => '/backoffice/filemanager',
      'filemanager_doc' => '/backoffice/filemanager_doc',
      'language' => '/backoffice/language',
      'dashboard' => '/backoffice/dashboard',
      'users' => '/backoffice/users',
      'users_create' => '/backoffice/users/create' ,
      'profile' => '/backoffice/profile', 
      'articles' =>'/backoffice/articles',
      'articles_create' => '/backoffice/articles/create',
      'article_categories' => '/backoffice/article-categories',
      'article_categories_create' => '/backoffice/article-categories/create',
      'banners' =>'/backoffice/banners',
      'banners_create' => '/backoffice/banners/create',
      'banners_highlight' =>'/backoffice/banners-highlight',
      'banners_highlight_create' => '/backoffice/banners-highlight/create',
      'events' =>'/backoffice/events',
      'events_create' => '/backoffice/events/create',
      'exchange_programs' =>'/backoffice/exchange-programs',
      'exchange_programs_create' => '/backoffice/exchange-programs/create',
      'exchange_categories' => '/backoffice/exchange-categories',
      'exchange_categories_create' => '/backoffice/exchange-categories/create',
      'cooperatives' =>'/backoffice/cooperatives',
      'cooperatives_create' => '/backoffice/cooperatives/create',
      'student_portfolios' =>'/backoffice/student-portfolios',
      'student_portfolios_create' => '/backoffice/student-portfolios/create',
      'videos' =>'/backoffice/videos',
      'videos_create' => '/backoffice/videos/create',
      'ebochures' =>'/backoffice/ebochures',
      'ebochures_create' => '/backoffice/ebochures/create', 

      'admissions' =>'/backoffice/admissions',
      'admissions_create' => '/backoffice/admissions/create',
      'admission_categories' => '/backoffice/admission-categories',
      'admission_categories_create' => '/backoffice/admission-categories/create',

      'finances' =>'/backoffice/finances',
      'finances_create' => '/backoffice/finances/create',


    ];

