# Cancer Fighting Web 

ระบบนี้พัฒนาด้วย Laravel version 5.4 ก่อนการติดตั้ง ที่เครื่องจำเป็นต้องติดตั้ง composer และ npm ก่อน

## ขั้นตอนการติดตั้ง


## 1. Clone this repository

## 2. เปลี่ยนชื่อไฟล์ .env-example เป็น .env

## 3. แก้ไขตัวแปรที่จำเป็นใน .env ดังนี้

    APP_URL

    APP_DEFAULT_LANGUAGE : th หรือ en หรือ jp

    APP_DEFAULT_LANGUAGE_BACKOFFICE : th หรือ en หรือ jp

    DIR_IMAGE=เช่น: F:/xampp/htdocs/local/laravel/cancer_fighting/public/storage/image/

    DIR_DOCUMENT=เช่น: F:/xampp/htdocs/local/laravel/cancer_fighting/public/storage/

    DB_*

    
## 4. สร้างโฟลเดอร์ตามนี้ พร้อมกำหนด permission ให้ read/write ได้

    /public/storage/image/catalog
    /public/storage/document

## 5. Copy ไฟล์ทั้งหมดที่อยู่ใน /public/image/ ไปไว้ที่ /public/storage/image/ พร้อมกำหนด permission ให้ read/write ได้
    

## 6. ติดตั้งแพ็คเกจต่าง ๆ ที่จำเป็นตามไฟล์ composer.json และ package.json โดยใช้คำสั่งดังนี้

    `composer update`

    `npm install`


## 7. สร้างฐานข้อมูลตามชื่อตัวแปร DB_DATABASE ในไฟล์ .env จากนั้นรันคำสั่งดังนี้

    ไปสร้าง database ชื่อ cancer_fighting เซต collation เป็น utf8_unicode_ci

    สร้างตาราง: `php artisan migrate`

    จำลองข้อมูลตัวอย่าง: `php artisan db:seed `

## 8. เพิ่มไฟล์รองรับภาษาไทย สำหรับ หน้า login 

    ที่ vendor/acacha/admin-lte-template-laravel/resource/lang/
    copy โฟเดอร์ en แล้วเปลี่ยนมาเป็น th

## 9. แก้ค่า e-mail ทีใช้รับข้อความและค่าคอนฟิคต่างๆ : 
    querysend_ajax.blade.php : ตัวแปร $email_reciever
    และคอนฟิคการส่งอีเมล์ที่ : lib/sendmail.class.php

## 10. สุดท้ายเข้าเว็บตาม URL: http://localhost/<PROJECT_NAME>




# DATABASE / MIGRATION

## การเพิ่ม/แก้ไข ข้อมูลตัวอย่างที่จำเป็น เข้าไปที่ /database/seeds/

1. User Roles : เมื่อมีการสร้าง Module เพิ่มเติมต้องไปเพิ่มสิทธิ์การใช้งานที่ RolesTableSeeder จากนั้นรันคำสั่งดังนี้
    `php artisan migrate:refresh --seed`

2. ระบบได้สร้างผู้ใช้สูงสุดเริ่มต้นไว้ 1 คน ต้องการเพิ่มให้เข้าไปแก้ไขที่ UsersTableSeeder

3. เพิ่ม/แก้ไข ข้อมูลภาษา LanguagesTableSeeder



# เมื่อ Deploy to Production ให้แก้ไขตัวแปรใน .env ดังนี้

    APP_ENV=production
    APP_DEBUG=false
    APP_URL
    DIR_IMAGE
    DB_*



## ER-DIAGRAM
อยู่ใน /database/ER-DIAGRAM.mwb
การออกแบบตารางเพิ่มเติมให้ออกแบบใน ER-DIAGRAM ก่อน จากนั้นนำที่ออกแบบไว้ มาเขียนที่ migration
