<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ArticleDescription extends Model
{
    private $table_main = 'articles'; //ตารางที่เก็บข้อมูลหลัก
    private $table_to_categories = 'article_to_categories'; //ตารางที่เก็บข้อมูล many to many
    
    public $timestamps = false; // Disable Laravel's Eloquent timestamps
    
    protected $fillable = [
        'article_id', 'language_id', 'name', 'description', 'tag', 'meta_title', 'meta_description', 'meta_keyword',
    ];

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function scopeLanguage($query, $language_id)
    {
        return $query->where('language_id', $language_id);
    }
    
    /**
     * ค้นหารตามชื่อ
     */
    public function scopeSearchKeyword($query, $name)
    {
        $query->where('name', 'LIKE', '%' . $name . '%');

        return $query;
    }

    /**
     * ค้นหาตามหมวดหมู่
     */
    public function scopeSearchCategory($query, $article_category_id)
    {
        $arr_article_id = ArticleToCategory::where('article_category_id', $article_category_id)->pluck('article_id')->toArray();
        return $query->whereIn('article_id', $arr_article_id);
    }

    /**
     * เรียงตามวันที่อัปเดทล่าสุด
     */
    public function scopeOrder($query)
    {
        return $query->join($this->table_main, $this->table_main . '.id', 'article_id')->orderBy($this->table_main . '.updated_at', 'DESC');
    }

    /**
     * สำหรับดึงข้อมูลที่ publish ของ Front-End
     */
    public function scopePublishWeb($query)
    {
        return $query->join($this->table_main, $this->table_main . '.id', '=', 'article_id')
            ->where($this->table_main . '.status', 1)
            ->where('publish_start', '<=', Carbon::now())
            ->where(function ($query) {
                $query->where('publish_stop', '>=', Carbon::now())
                    ->orWhere('publish_stop', '=', null);
            })
            //->orderBy($this->table_main . '.publish_start', 'DESC');
            ->orderBy($this->table_main . '.publish_start', 'DESC');
    }

    public function scopePopular($query)
    {
        return $query->join($this->table_main, $this->table_main . '.id', '=', 'article_id')
            ->where($this->table_main . '.status', 1)
            ->where('publish_start', '<=', Carbon::now())
            ->where(function ($query) {
                $query->where('publish_stop', '>=', Carbon::now())
                    ->orWhere('publish_stop', '=', null);
            })
            ->orderBy($this->table_main . '.viewed', 'DESC');
    }
    
    //_หาข่าวที่เกี่ยว (โดยเอามาจาก category เดียวกัน)
    public function scopeSearchRelatedNews($query, $article_category_id ,$except_article_id)
    {
        $arr_article_id = ArticleToCategory::where('article_category_id', $article_category_id)
                            ->where('article_id', '!=' , $except_article_id) //_ไม่ดึงข่าวที่เรากำลังดูอยู่
                            ->pluck('article_id')->toArray();

        return $query->whereIn('article_id', $arr_article_id);
    }

    public function scopePublishEvent($query)
    {
        return $query->join($this->table_main, $this->table_main . '.id', '=', 'article_id')
            ->where($this->table_main . '.status', 1)
            ->where('publish_start', '<=', Carbon::now())
            ->where(function ($query) {
                $query->where('publish_stop', '>=', Carbon::now())
                    ->orWhere('publish_stop', '=', null);
            })
            //->orderBy($this->table_main . '.publish_start', 'DESC');
            ->orderBy($this->table_main . '.publish_stop', 'ASC');

    }

}
