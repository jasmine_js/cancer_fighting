<?php
/**
* Nov 9, 2018, 9:16 AM
* Developed by Korn <kornthebkk@gmail.com>
*/

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmissionCategoryDescription extends Model
{
    private $table_main = 'admission_categories'; //ตารางที่เก็บข้อมูลหลัก

    public $timestamps = false; // Disable Laravel's Eloquent timestamps

    protected $fillable = [
        'language_id', 'name', 'description', 'meta_title', 'meta_description', 'meta_keyword',
    ];

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function admissionCategory()
    {
        return $this->belongsTo(AdmissionCategory::class);
    }

    public function scopeLanguage($query, $language_id)
    {
        return $query->where('language_id', $language_id);
    }

    public function scopeOrder($query)
    {
        return $query->join($this->table_main, $this->table_main . '.id', '=', 'admission_category_id')->orderBy($this->table_main . '.sort_order', 'asc');
    }

    public function scopeSearch($query, $find)
    {
        return $query->where('name', 'LIKE', '%' . $find . '%');
    }
}
