<?php
/**
* Nov 13, 2018, 11:12 AM
* Developed by Korn <kornthebkk@gmail.com>
*/

namespace App;

use Config;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as IImage;

class Image extends Model
{
    public static function resize($filename, $width, $height)
    {
        if (!is_file(Config::get('app.dir_image') . $filename) || substr(str_replace('\\', '/', realpath(Config::get('app.dir_image') . $filename)), 0, strlen(Config::get('app.dir_image'))) != str_replace('\\', '/', Config::get('app.dir_image'))) {
            return;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $image_old = $filename;
        $image_new = 'cache/' . substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if (!is_file(Config::get('app.dir_image') . $image_new) || (filemtime(Config::get('app.dir_image') . $image_old) > filemtime(Config::get('app.dir_image') . $image_new))) {
            list($width_orig, $height_orig, $image_type) = getimagesize(Config::get('app.dir_image') . $image_old);

            if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) {
                return Config::get('app.dir_image') . $image_old;
            }

            $path = '';

            $directories = explode('/', dirname($image_new));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir(Config::get('app.dir_image') . $path)) {
                    @mkdir(Config::get('app.dir_image') . $path, 0777);
                }
            }

            if ($width_orig != $width || $height_orig != $height) {

                //แบบไม่คำนึงถึงสัดส่วน
                // $image = IImage::make(Config::get('app.dir_image') . $image_old);
                // $image->resize($width, $height);
                // $image->save(Config::get('app.dir_image') . $image_new);


                //แบบคำนึงถึงสัดส่วน ถ้ารูปไม่ใช่จัตุรัสจะให้รูปอยู่กึ่งกลาง ด้านข้างให้เป็นขอบขาว
                $image = IImage::make(Config::get('app.dir_image') . $image_old);
                
                $canvas_ratio = $width / $height;

                $img_w = $image->width();
                $img_h = $image->height();
                $img_ratio = $img_w / $img_h;

                if ($img_ratio >= $canvas_ratio) {
                    $dest_w = $width;
                    $dest_h = $width * $canvas_ratio;
                } else {
                    $dest_w = $height * $canvas_ratio;
                    $dest_h = $height;
                }

                $image->resize($dest_w, $dest_h, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $image->resizeCanvas($width, $height, 'center', false);
                $image->save(Config::get('app.dir_image') . $image_new);


            } else {
                copy(Config::get('app.dir_image') . $image_old, Config::get('app.dir_image') . $image_new);
            }
        }

        return Config::get('app.url') . 'storage/image/' . $image_new;
    }
}
