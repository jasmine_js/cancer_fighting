<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    private $table_images = 'article_images';
    
    private $table_description = 'article_descriptions';

    protected $guarded = ['id'];

    public function articleDescriptions()
    {
        return $this->hasMany(ArticleDescription::class);
    }

    public function articleImages()
    {
        return $this->hasMany(ArticleImage::class);
    }

    public function articleCategories()
    {
        return $this->belongsToMany(ArticleCategory::class, 'article_to_categories');
    }

    public function scopeLanguage($query, $language_id)
    {
        return $this->join($this->table_description, $this->table_description . '.article_id', 'id')->where('language_id', $language_id);
    }

    public function scopeOrder($query)
    {
        return $this->orderBy('updated_at', 'DESC');
    }

    // public function user()
    // {
    //     return $this->belongsTo('App\User','foreign_key');
    // }

}
