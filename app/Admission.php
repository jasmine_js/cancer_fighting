<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admission extends Model
{
    private $table_images = 'admission_images';
    
    private $table_description = 'admission_descriptions';

    protected $guarded = ['id'];

    public function admissionDescriptions()
    {
        return $this->hasMany(AdmissionDescription::class);
    }

    public function admissionImages()
    {
        return $this->hasMany(AdmissionImage::class);
    }

    public function admissionCategories()
    {
        return $this->belongsToMany(AdmissionCategory::class, 'admission_to_categories');
    }

    public function scopeLanguage($query, $language_id)
    {
        return $this->join($this->table_description, $this->table_description . '.admission_id', 'id')->where('language_id', $language_id);
    }

    public function scopeOrder($query)
    {
        return $this->orderBy('updated_at', 'DESC');
    }

    // public function user()
    // {
    //     return $this->belongsTo('App\User','foreign_key');
    // }

}
