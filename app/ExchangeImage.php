<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeImage extends Model
{
    public $timestamps = false; // Disable Laravel's Eloquent timestamps

    protected $fillable = [
        'exchange_program_id', 'image', 'sort_order',
    ];

    public function exchangeProgram()
    {
        return $this->belongsTo(ExchangeProgram::class);
    }

    public function scopeOrder($query)
    {
        return $query->orderBy('sort_order', 'ASC');
    }

}