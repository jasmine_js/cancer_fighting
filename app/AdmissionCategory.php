<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmissionCategory extends Model
{
    private $table_description = 'admission_category_descriptions'; //ตารางที่เก็บข้อมูลภาษา

    
    public function admissionCategoryDescriptions()
    {
        return $this->hasMany(AdmissionCategoryDescription::class);
    }

    public function admissions()
    {
        return $this->belongsToMany(Admission::class);
    }

}
