<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleToCategory extends Model
{
    protected $table = 'article_to_categories';   

    public $timestamps = false; // Disable Laravel's Eloquent timestamps

    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }

    public function articleCategories()
    {
        return $this->belongsToMany(ArticleCategory::class);
    }
}
