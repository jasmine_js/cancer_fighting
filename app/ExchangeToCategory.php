<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeToCategory extends Model
{
    protected $table = 'exchange_to_categories';   

    public $timestamps = false; // Disable Laravel's Eloquent timestamps

    public function exchangePrograms()
    {
        return $this->belongsToMany(ExchangeProgram::class);
    }

    public function exchangeCategories()
    {
        return $this->belongsToMany(ExchangeCategory::class);
    }
}
