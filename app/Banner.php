<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Banner extends Model
{
    protected $guarded = ['id'];

    public function scopeSearch($query, $find)
    {
        return $query->where('name', 'LIKE', '%' . $find . '%');
    }

     public function scopeOrder($query)
     {
         return $query->orderBy('sort_order', 'ASC');
     }

    /**
     * สำหรับดึงข้อมูลที่ publish ของ Front-End
     */
    public function scopePublishWeb($query)
    {
        return $query->where('status', 1)
            ->where('publish_start', '<=', Carbon::now())
            ->where(function ($query) {
                $query->where('publish_stop', '>=', Carbon::now())
                    ->orWhere('publish_stop', '=', null);
             })->orderBy('sort_order', 'ASC');
    }
}
