<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'role_id', 'user_id'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function scopeOrder($query)
    {
        return $query->orderBy('sort_order', 'ASC');
    }
}
