<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    private $table_description = 'article_category_descriptions'; //ตารางที่เก็บข้อมูลภาษา

    
    public function articleCategoryDescriptions()
    {
        return $this->hasMany(ArticleCategoryDescription::class);
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }

}
