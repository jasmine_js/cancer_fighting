<?php
/**
* Nov 9, 2018, 9:16 AM
* Developed by Korn <kornthebkk@gmail.com>
*/

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App;
use Config;
use App\Language;

class BackofficeController extends Controller
{
    protected $languages; // ข้อมูลภาษาทั้งหมด

    protected $language; // Object เก็บภาษาปัจจุบัน นำไปใช้ เช่น ใช้ในการดึงข้อมูลในตาราง *_description ที่ relation กับตารางหลัก

    protected $data_common = []; // สำหรับเก็บข้อมูลที่ใช้ร่วมกันที่จะส่งไปใช้ที่ View, child view ที่นำไปใช้ห้ามใส่ค่าแบบทับตัวแปร ให้ใช้ merge ข้อมูล

    protected $language_key = 'language_code'; // cookie name key

    protected $cookie_expired = 86400; // 24 hours

    protected $per_page = 10;
    

    public function __construct()
    {
        $this->languages = Language::order()->get();

        //ตรวจสอบ cookie ภาษาปัจจุบันที่เครื่อง และต้องตรงกับรหัสภาษาที่มีใน DB
        if(isset($_COOKIE[$this->language_key]) && in_array($_COOKIE[$this->language_key], $this->languages->pluck('code')->toArray())){
            //update current language
            $this->language = Language::where('code', $_COOKIE[$this->language_key])->first();

        }else{
            //ถ้าไม่มีข้อมูล cookie ให้ดึงจากค่าเริ่มต้น
            $this->language = Language::where('code', Config::get('app.default_language_backoffice'))->first();

            setcookie($this->language_key, $this->language->code, time() + $this->cookie_expired);
        }

        // Initial current language
        App::setLocale($this->language->code);

        $this->data_common = [
            'languages' => $this->languages,
            'language' => $this->language,
        ];

    }

    protected function view($view)
    {
        return view($view, $this->data_common);
    }

}
