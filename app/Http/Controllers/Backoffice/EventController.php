<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use App\Event;
use App\EventDescription;
use App\Http\Requests\Backoffice\EventRequest;
use Config;
use App\Image;




class EventController extends BackofficeController
{
    public function __construct()
    {
        BackofficeController::__construct();

        //initial data
        $this->data_common += ['event_page_active' => 'active'];
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles('event/access');

        $find = $request->get('find');
        //$article_category_id = $request->get('article_category_id');

        $items = EventDescription::with('event')->language($this->language->id);

        
        if ($find) {
            $items = $items->searchKeyword($find);
        }
        // dd($items->toSql());

        $items = $items->order()->paginate($this->per_page);
        $Eventdesc = EventDescription::where([]);  //model

        $items->setCollection(
            $items->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image && !preg_match('/^http:\/\//', $item->image) && !preg_match('/^https:\/\//', $item->image)) { //ต้องมีข้อมูล และต้องไม่ขึ้นต้นด้วย http://
                        $item->image = Image::resize($item->image, 40, 40); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache
                    } else {
                        $item->image = Image::resize('no_image.png', 40, 40);
                    }

                    return $item;
                })
        );

        $this->data_common += [
            'items' => $items,
           
            'find' => $find,
            'breadcrumb' => [
                trans('backoffice/events.text_event_lists') => ['url' => '', 'active' => 'active'],
            ],
        ];
       
        return $this->view('backoffice.events.list');
    }

   
    public function create(Request $request)
    {
        $request->user()->authorizeRoles('event/modify');

        $this->data_common += [
            'breadcrumb' => [
                trans('backoffice/events.text_event_lists') => ['url' => Config::get('url.backoffice.events'), 'active' => ''],
                trans('backoffice/events.text_create_event') => ['', 'active' => 'active'],
            ],
        ];

        return $this->view('backoffice.events.create');
    }

    
    public function store(Request $req,EventRequest $request)
    {
        $req->user()->authorizeRoles('event/modify');

        // บันทึกเข้าตารางหลัก events
        $item = new Event();
        $item->image = $request->image;
        $item->event_date = $request->event_date;  //event_date
        $item->sort_order = (int) $request->sort_order;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        $item->status = $request->status;
        $item->save();
        
        $event_id = $item->id;

        // บันทึกเข้าตาราง event descriptions
        foreach ($request->event_descriptions as $language_id => $value) {
            if($value['name'] != '')
            {
            $item_description = new EventDescription();
            $item_description->event_id = $event_id;
            $item_description->language_id = $language_id;
            $item_description->name = $value['name'];  //ชื่อ
            $item_description->description = $value['description']; //รายละเอียด
            
            if(trim($value['meta_title']) != ''){
                $item_description->meta_title = $value['meta_title'];
            }else{
                $item_description->meta_title = $value['name'];
            }
            
            $item_description->meta_description = $value['meta_description'];
            $item_description->meta_keyword = $value['meta_keyword'];
            $item_description->tag = $value['tag'];
            $item_description->save();

            unset($item_description); // clear memory
        }
    }


        return redirect(Config::get('url.backoffice.events'))
            ->with('success', trans('backoffice/common.text_save_successful'));

    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id, Request $request)
    {
        $request->user()->authorizeRoles('event/modify');
        $item = Event::with('eventDescriptions') //function in event model 
        ->findOrFail($id);

        if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        }

        //Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            $item->thumb = Image::resize($item->image, 100, 100);
        } else {
            $item->thumb = Image::resize('no_image.png', 100, 100);
        }


        $description = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        foreach ($item->eventDescriptions as $dest) {
            $description[$dest->language_id] = $dest;
        }
        $item->description = $description;
       
        $this->data_common += [
            'item' => $item,
           
            'breadcrumb' => [
                trans('backoffice/events.text_event_lists') => ['url' => Config::get('url.backoffice.events'), 'active' => ''],
                trans('backoffice/events.text_edit_event') => ['', 'active' => 'active'],
            ],
        ];
        // dd($this->data_common);

        return $this->view('backoffice.events.edit');
    }
    public function update(Request $req,EventRequest $request, $event_id)
    {
        $req->user()->authorizeRoles('event/modify');

        // บันทึกเข้าตารางหลัก
        $item = Event::findOrFail($event_id);

        $item->image = $request->image;
        $item->event_date = $request->event_date;  //event_date
        $item->sort_order = (int) $request->sort_order;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        
        $item->status = $request->status;
        $item->save();

        EventDescription::where('event_id', $event_id)->delete();

        // บันทึกเข้าตาราง event descriptions
        foreach ($request->event_descriptions as $language_id => $value) {
            if($value['name'] != '')
            {
            $item_description = new EventDescription();
            $item_description->event_id = $event_id;
            $item_description->language_id = $language_id;
            $item_description->name = $value['name'];  //ชื่อ
            $item_description->description = $value['description']; //รายละเอียด
            
            //$item_description->meta_title = $value['meta_title'];
            if(trim($value['meta_title']) != ''){
                $item_description->meta_title = $value['meta_title'];
            }else{
                $item_description->meta_title = $value['name'];
            }

            $item_description->meta_description = $value['meta_description'];
            $item_description->meta_keyword = $value['meta_keyword'];
            $item_description->tag = $value['tag'];
            $item_description->save();

            unset($item_description); // clear memory
        }
    }


        return redirect(Config::get('url.backoffice.events'))
            ->with('success', trans('backoffice/common.text_save_successful'));

    }

  
    public function destroy(Request $req, $id)
    {
        $req->user()->authorizeRoles('event/modify');

        $item = Event::findOrFail($id);

        $item->delete();

        return redirect(Config::get('url.backoffice.events'))
            ->with('success', trans('backoffice/common.text_delete_successful'));
    }

    /**
     * อัปเดทวันที่ปัจจุบันที่ updated_at
     */
    public function touch(Request $request, $id)
    {
        Event::findOrFail($id)->touch();

      

        return redirect(Config::get('url.backoffice.events') )
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

}
