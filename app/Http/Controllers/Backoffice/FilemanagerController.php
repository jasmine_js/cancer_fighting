<?php
/**
* Nov 13, 2018, 11:12 AM
* Developed by Korn <kornthebkk@gmail.com>
*/

/**
 * แปลงมาจาก opencart 3.x filemanager controller มาเป็น Laravel controller
 * โดยใช้ Intervention Image Library เป็นตัวจัดการการ resize รูป (ใช้ใน App\Image Model)
 */

namespace App\Http\Controllers\Backoffice;

use App\Image;
use App\Pagination;
use Config;
use Illuminate\Http\Request;

class FilemanagerController extends BackofficeController
{

    protected $per_page = 16; // จำนวนรูปหรือโฟลเดอร์ที่แสดงต่อหน้า

    private $text_unauthorized = 'You are not authorized to use this function.';
    private $document_ext = ['doc', 'docx', 'pdf', 'odt'];


    public function index(Request $request)
    {
        if(!$request->user()->authorizeRoles('filemanager/access', false)){
            return '<div class="filemanager-unauthorized alert alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ' . $this->text_unauthorized . '</div>';
        }

        // Find which protocol to use to pass the full image link back
        $server = Config::get('app.url') . 'backoffice/';
        //$server =  "../../backoffice/";
        // die($server);

        if (isset($request->filter_name)) {
            $filter_name = rtrim(str_replace(array('*', '/', '\\'), '', $request->filter_name), '/');
        } else {
            $filter_name = '';
        }

        // Make sure we have the correct directory
        if (isset($request->directory)) {
            $directory = rtrim(Config::get('app.dir_image') . 'catalog/' . str_replace('*', '', $request->directory), '/');
        } else {
            $directory = Config::get('app.dir_image') . 'catalog';
        }

        if (isset($request->page)) {
            $page = $request->page;
        } else {
            $page = 1;
        }

        $directories = array();
        $files = array();

        $data['images'] = array();

        if (substr(str_replace('\\', '/', realpath($directory) . '/' . $filter_name), 0, strlen(Config::get('app.dir_image') . 'catalog')) == str_replace('\\', '/', Config::get('app.dir_image') . 'catalog')) {
            // Get directories
            $directories = glob($directory . '/' . $filter_name . '*', GLOB_ONLYDIR);

            if (!$directories) {
                $directories = array();
            }

            // Get files
            $files = glob($directory . '/' . $filter_name . '*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}', GLOB_BRACE);

            if (!$files) {
                $files = array();
            }
        }

        // Merge directories and files
        $images = array_merge($directories, $files);

        // Get total number of files and directories
        $image_total = count($images);

        // Split the array based on current page number and max number of items per page of 10
        $images = array_splice($images, ($page - 1) * $this->per_page, $this->per_page);

        foreach ($images as $image) {
            //$name = str_split(basename($image), 14);
            $name = basename($image);

            if (is_dir($image)) {
                $url = '';

                if (isset($request->target)) {
                    $url .= '&target=' . $request->target;
                }

                if (isset($request->thumb)) {
                    $url .= '&thumb=' . $request->thumb;
                }

                $data['images'][] = array(
                    'thumb' => '',
                    //'name' => implode(' ', $name),
                    'name' => $name,
                    'type' => 'directory',
                    'path' => substr($image, strlen(Config::get('app.dir_image'))),
                    'href' => Config::get('app.url') . 'backoffice/filemanager/?directory=' . urlencode(substr($image, strlen((Config::get('app.dir_image') . 'catalog/')))) . $url,
                );
            } elseif (is_file($image)) {
                $data['images'][] = array(
                    'thumb' => Image::resize(substr($image, strlen(Config::get('app.dir_image'))), 100, 100),
                    //'name' => implode(' ', $name),
                    'name' => $name,
                    'type' => 'image',
                    'path' => substr($image, strlen(Config::get('app.dir_image'))),
                    'href' => $server . 'image/' . substr($image, strlen(Config::get('app.dir_image'))),
                );

                //Image::resize(substr($image, strlen(Config::get('app.dir_image'))), 500, 500);
            }
        }
        

        if (isset($request->directory)) {
            $data['directory'] = urlencode($request->directory);
        } else {
            $data['directory'] = '';
        }

        if (isset($request->filter_name)) {
            $data['filter_name'] = $request->filter_name;
        } else {
            $data['filter_name'] = '';
        }

        // Return the target ID for the file manager to set the value
        if (isset($request->target)) {
            $data['target'] = $request->target;
        } else {
            $data['target'] = '';
        }

        // Return the thumbnail for the file manager to show a thumbnail
        if (isset($request->thumb)) {
            $data['thumb'] = $request->thumb;
        } else {
            $data['thumb'] = '';
        }

        // Parent
        $url = '';

        if (isset($request->directory)) {
            $pos = strrpos($request->directory, '/');

            if ($pos) {
                $url .= '&directory=' . urlencode(substr($request->directory, 0, $pos));
            }
        }

        if (isset($request->target)) {
            $url .= '&target=' . $request->target;
        }

        if (isset($request->thumb)) {
            $url .= '&thumb=' . $request->thumb;
        }

        $data['parent'] = Config::get('app.url') . 'backoffice/filemanager/?' . $url;

        // Refresh
        $url = '';

        if (isset($request->directory)) {
            $url .= '&directory=' . urlencode($request->directory);
        }

        if (isset($request->target)) {
            $url .= '&target=' . $request->target;
        }

        if (isset($request->thumb)) {
            $url .= '&thumb=' . $request->thumb;
        }

        $data['refresh'] = Config::get('app.url') . 'backoffice/filemanager/?' . $url;

        $url = '';

        if (isset($request->directory)) {
            $url .= '&directory=' . urlencode(html_entity_decode($request->directory, ENT_QUOTES, 'UTF-8'));
        }

        if (isset($request->filter_name)) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($request->filter_name, ENT_QUOTES, 'UTF-8'));
        }

        if (isset($request->target)) {
            $url .= '&target=' . $request->target;
        }

        if (isset($request->thumb)) {
            $url .= '&thumb=' . $request->thumb;
        }

        $pagination = new Pagination();
        $pagination->total = $image_total;
        $pagination->page = $page;
        $pagination->limit = $this->per_page;
        $pagination->url = Config::get('app.url') . 'backoffice/filemanager/?' . $url . '&page={page}';
        $data['pagination'] = $pagination->render();
        
        return view('backoffice.filemanager.filemanager', $data);
    }

    public function upload(Request $request)
    {
        if(!$request->user()->authorizeRoles('filemanager/modify', false)){
            return $this->text_unauthorized;
        }

        $json = array();

        // Make sure we have the correct directory
        if (isset($request->directory)) {
            $directory = rtrim(Config::get('app.dir_image') . 'catalog/' . $request->directory, '/');
        } else {
            $directory = Config::get('app.dir_image') . 'catalog';
        }

        // Check its a directory
        if (!is_dir($directory) || substr(str_replace('\\', '/', realpath($directory)), 0, strlen(Config::get('app.dir_image') . 'catalog')) != str_replace('\\', '/', Config::get('app.dir_image') . 'catalog')) {
            $json['error'] = 'error_directory';
        }

        if (!$json) {
            // Check if multiple files are uploaded or just one
            $files = array();

            // if (!empty($request->files['file']['name']) && is_array($this->request->files['file']['name'])) {
            if (!empty($_FILES['file']['name']) && is_array($_FILES['file']['name'])) {
                // if (!empty($request->file('file')) && is_array($request->file('file'))) {
                foreach (array_keys($request->file('file')) as $key) {
                    $files[] = array(
                        'name' => $_FILES['file']['name'][$key],
                        'type' => $_FILES['file']['type'][$key],
                        'tmp_name' => $_FILES['file']['tmp_name'][$key],
                        'error' => $_FILES['file']['error'][$key],
                        'size' => $_FILES['file']['size'][$key],
                    );
                }
            }

            foreach ($files as $file) {
                if (is_file($file['tmp_name'])) {
                    // Sanitize the filename
                    $filename = basename(html_entity_decode($file['name'], ENT_QUOTES, 'UTF-8'));

                    // Validate the filename length
                    if ((strlen($filename) < 3) || (strlen($filename) > 255)) {
                        $json['error'] = 'error_filename';
                    }

                    // Allowed file extension types
                    $allowed = array(
                        'jpg',
                        'jpeg',
                        'gif',
                        'png',
                    );

                    if (!in_array(strtolower(substr(strrchr($filename, '.'), 1)), $allowed)) {
                        $json['error'] = 'error_filetype';
                    }

                    // Allowed file mime types
                    $allowed = array(
                        'image/jpeg',
                        'image/pjpeg',
                        'image/png',
                        'image/x-png',
                        'image/gif',
                    );

                    if (!in_array($file['type'], $allowed)) {
                        $json['error'] = 'error_filetype';
                    }

                    // Return any upload error
                    if ($file['error'] != UPLOAD_ERR_OK) {
                        $json['error'] = 'error_upload_' . $file['error'];
                    }
                } else {
                    $json['error'] = 'error_upload';
                }

                if (!$json) {
                    move_uploaded_file($file['tmp_name'], $directory . '/' . $filename);
                }
            }
        }

        if (!$json) {
            $json['success'] = 'text_uploaded';
        }

        return $json;
    }

    public function delete(Request $request)
    {
        if(!$request->user()->authorizeRoles('filemanager/modify', false)){
            return $this->text_unauthorized;
        }


        $json = array();

        if (isset($request->path)) {
            $paths = $request->path;
        } else {
            $paths = array();
        }

        // Loop through each path to run validations
        foreach ($paths as $path) {
            // Check path exsists
            if ($path == Config::get('app.dir_image') . 'catalog' || substr(str_replace('\\', '/', realpath(Config::get('app.dir_image') . $path)), 0, strlen(Config::get('app.dir_image') . 'catalog')) != str_replace('\\', '/', Config::get('app.dir_image') . 'catalog')) {
                $json['error'] = 'error_delete';

                break;
            }
        }

        if (!$json) {
            // Loop through each path
            foreach ($paths as $path) {
                $path = rtrim(Config::get('app.dir_image') . $path, '/');

                // If path is just a file delete it
                if (is_file($path)) {
                    unlink($path);
                    // $json['path'] = $path;

                    // If path is a directory beging deleting each file and sub folder
                } elseif (is_dir($path)) {
                    $files = array();

                    // Make path into an array
                    $path = array($path);

                    // While the path array is still populated keep looping through
                    while (count($path) != 0) {
                        $next = array_shift($path);

                        foreach (glob($next) as $file) {
                            // If directory add to path array
                            if (is_dir($file)) {
                                $path[] = $file . '/*';
                            }

                            // Add the file to the files to be deleted array
                            $files[] = $file;
                        }
                    }

                    // Reverse sort the file array
                    rsort($files);

                    foreach ($files as $file) {
                        // If file just delete
                        if (is_file($file)) {
                            // $json['debug'] = $file;
                            unlink($file);

                            // If directory use the remove directory function
                        } elseif (is_dir($file)) {
                            rmdir($file);
                        }
                    }
                }
            }

            $json['success'] = 'text_delete_success';
        }

        return $json;
    }

    public function folder(Request $request)
    {
        if(!$request->user()->authorizeRoles('filemanager/modify', false)){
            return $this->text_unauthorized;
        }

        $json = array();

        // Make sure we have the correct directory
        if (isset($request->directory)) {
            $directory = rtrim(Config::get('app.dir_image') . 'catalog/' . $request->directory, '/');
        } else {
            $directory = Config::get('app.dir_image') . 'catalog';
        }
        
        // Check its a directory
        if (!is_dir($directory) || substr(str_replace('\\', '/', realpath($directory)), 0, strlen(Config::get('app.dir_image') . 'catalog')) != str_replace('\\', '/', Config::get('app.dir_image') . 'catalog')) {
            $json['error'] = 'error_directory';
        }

        //if ( preg_match('/\s/',$request->folder) ){
        //_check if have white space (\s) ,bracket :\),\( 
        if ( preg_match('/\s|\)|\(/' , $request->folder) ){
            //echo "yes $string contain whitespace";  
            $json['error'] = 'error_folder';
        }

        //if ($request->server['REQUEST_METHOD'] == 'POST') {
        //_Sanitize the folder name
        $folder = basename(html_entity_decode($request->folder, ENT_QUOTES, 'UTF-8'));

        // Validate the filename length
        if ((strlen($folder) < 3) || (strlen($folder) > 128)) {
            $json['error'] = 'error_folder';
        }

        // Check if directory already exists or not
        if (is_dir($directory . '/' . $folder)) {
            $json['error'] = 'error_exists';
        }
        //}

        if (!isset($json['error'])) {
            mkdir($directory . '/' . $folder, 0777);
            chmod($directory . '/' . $folder, 0777);

            @touch($directory . '/' . $folder . '/' . 'index.html');

            $json['success'] = 'text_directory_success';
        }

        return $json;
    }
}
