<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use App\Ebochure;
use App\EbochureDescription;
use App\Http\Requests\Backoffice\ EbochureRequest;
use Config;
use App\Image;
use Illuminate\Support\Facades\Auth;
use App\User;

class EbochureController extends BackofficeController
{
    public function __construct()
    {
        BackofficeController::__construct();

        //initial data
        $this->data_common += ['ebochure_page_active' => 'active'];
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles('ebochure/access');

        $find = $request->get('find');
        //$article_category_id = $request->get('article_category_id');

        $items = EbochureDescription::with('ebochure')->language($this->language->id);

        
        if ($find) {
            $items = $items->searchKeyword($find);
        }
        // dd($items->toSql());

        $items = $items->order()->paginate($this->per_page);
        //$exchangedesc = ExchangeProgramDescription::where([]);  //model

        $items->setCollection(
            $items->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image && !preg_match('/^http:\/\//', $item->image) && !preg_match('/^https:\/\//', $item->image))  { //ต้องมีข้อมูล และต้องไม่ขึ้นต้นด้วย http://
                        $item->image = Image::resize($item->image, 40, 40);//สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache
                        $item->image_highlight = Image::resize($item->image_highlight, 40, 40); 
                    } else {
                        $item->image = Image::resize('no_image.png', 40, 40);
                        $item->image_highlight = Image::resize('no_image.png', 40, 40);
                    }

                    return $item;
                })
        );
         //_get ชื่อ user ที่สร้างและแก้ไขข่าว
         if(count($items)){
            foreach($items as $value)
            {
                $username[] = [
                    'created_by'=>User::find($value->created_by,['name']),
                    'updated_by'=>User::find($value->updated_by,['name'])
                ];
            }
        }else{
            $username = array();
        }
        

        $this->data_common += [
            'items' => $items,
           
            'find' => $find,
            'breadcrumb' => [
                trans('backoffice/ebochures.text_ebochure_lists') => ['url' => '', 'active' => 'active'],
            ], 
            'username'=>$username
        ];
       
       
       
        return $this->view('backoffice.ebochures.list');
    }

   
    public function create(Request $request)
    {
        $request->user()->authorizeRoles('ebochure/modify');

        $this->data_common += [
            'breadcrumb' => [
                trans('backoffice/ebochures.text_ebochure_lists') => ['url' => Config::get('url.backoffice.ebochures'), 'active' => ''],
                trans('backoffice/ebochures.text_create_ebochure') => ['', 'active' => 'active'],
            ],
        ];

        return $this->view('backoffice.ebochures.create');
    }

    
    public function store(Request $req,EbochureRequest $request)//EventRequest $request
    {
        $req->user()->authorizeRoles('ebochure/modify');
        
        // บันทึกเข้าตารางหลัก events
        $item = new Ebochure();

        $item->image = $request->image;
        $item->image_highlight = $request->image_highlight;
        $item->pdf_link = $request->pdf_file;
        // $item->pdf_link = $request->pdf_file;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        $item->status = $request->status;
        $item->created_by = Auth::id();
        $item->save();

        $ebochure_id = $item->id;

        // บันทึกเข้าตาราง event descriptions
        foreach ($request->ebochure_descriptions as $language_id => $value) {
            if($value['name'] != '')
            {
            $item_description = new EbochureDescription();
            $item_description->ebochure_id = $ebochure_id;
            $item_description->language_id = $language_id;
            $item_description->name = $value['name'];  //ชื่อ
            $item_description->description = $value['description']; //รายละเอียด
            $item_description->link = $value['link']; //link
           //$item_description->meta_title = $value['meta_title'];
            if(trim($value['meta_title']) != ''){
                $item_description->meta_title = $value['meta_title'];
            }else{
                $item_description->meta_title = $value['name'];
            }
            $item_description->meta_description = $value['meta_description'];
            $item_description->meta_keyword = $value['meta_keyword'];
            $item_description->tag = $value['tag'];
            $item_description->save();

            unset($item_description); // clear memory
        }
    }


        return redirect(Config::get('url.backoffice.ebochures'))
            ->with('success', trans('backoffice/common.text_save_successful'));

    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id, Request $request)
    {
        $request->user()->authorizeRoles('ebochure/modify');
        $item = Ebochure::with('ebochureDescriptions') //function in ebochure model 
        ->findOrFail($id);


        if (($item->image) && ($item->image_highlight)) {
            $item->image = $item->image;
            $item->image_highlight = $item->image_highlight;
        } else {
            $item->image = '';
            $item->image_highlight = '';
        }

        //Prepare thumb field for View (สำหรับแสดง)
        if (($item->image) && ($item->image_highlight)) {
            $item->thumb = Image::resize($item->image, 100, 100);
            $item->thumb1 = Image::resize($item->image_highlight, 100, 100);
        } else {
            $item->thumb = Image::resize('no_image.png', 100, 100);
        }

        

        $description = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        foreach ($item->ebochureDescriptions as $dest) {
            $description[$dest->language_id] = $dest;
        }
        $item->description = $description;
       
        $this->data_common += [
            'item' => $item,
           
            'breadcrumb' => [
                trans('backoffice/ebochures.text_ebochure_lists') => ['url' => Config::get('url.backoffice.ebochures'), 'active' => ''],
                trans('backoffice/ebochures.text_edit_ebochure') => ['', 'active' => 'active'],
            ],
        ];
        // dd($this->data_common);

        return $this->view('backoffice.ebochures.edit');
    }
    public function update(Request $req, EbochureRequest $request,$ebochure_id)
    {
        $req->user()->authorizeRoles('ebochure/modify');

        // บันทึกเข้าตารางหลัก
        $item = Ebochure::findOrFail($ebochure_id);
       
        $item->image = $request->image;
        $item->image_highlight = $request->image_highlight;
        $item->pdf_link = $request->pdf_file;
       
        // $item->pdf_link = $request->pdf_file;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        $item->status = $request->status;
        $item->updated_by = Auth::id();
        $item->save();

      

        EbochureDescription::where('ebochure_id', $ebochure_id)->delete();

        // บันทึกเข้าตาราง event descriptions
        foreach ($request->ebochure_descriptions as $language_id => $value) {
            if($value['name'] != '')
            {
            $item_description = new  EbochureDescription();
            $item_description->ebochure_id = $ebochure_id;
            $item_description->language_id = $language_id;
            $item_description->name = $value['name'];  //ชื่อ
            $item_description->description = $value['description']; //รายละเอียด
            $item_description->link = $value['link']; //link
            //$item_description->meta_title = $value['meta_title'];
            if(trim($value['meta_title']) != ''){
                $item_description->meta_title = $value['meta_title'];
            }else{
                $item_description->meta_title = $value['name'];
            }
            $item_description->meta_description = $value['meta_description'];
            $item_description->meta_keyword = $value['meta_keyword'];
            $item_description->tag = $value['tag'];
            $item_description->save();

            unset($item_description); // clear memory
        }
    }


        return redirect(Config::get('url.backoffice.ebochures'))
            ->with('success', trans('backoffice/common.text_save_successful'));

    }

  
    public function destroy(Request $req, $id)
    {
        $req->user()->authorizeRoles('ebochure/modify');

        $item = Ebochure::findOrFail($id);

        $item->delete();

        return redirect(Config::get('url.backoffice.ebochures'))
            ->with('success', trans('backoffice/common.text_delete_successful'));
    }


    // public function uploadFilePost(Request $request){
    //     $request->validate([
    //         'fileToUpload' => 'required|file|max:1024',
    //     ]);

    //     $request->fileToUpload->store('logos');

    //     return back()
    //         ->with('success','You have successfully upload image.');

    // }

    public function uploadfile(Request $request)
    {
        $validator = Validator::make($request->all(), ['file' => 'required|mimes:pdf|max:102400'],
        [
            'file.mimes' => 'ไฟล์เอกสารต้องเป็น PDF เท่านั้น',
            'file.required' => 'ระบุไฟล์เพื่ออัพโหลด',
            'file.max' => 'ขนาดไฟล์ใหญ่เกินไป ห้ามเกิน 100 MB.'
        ]);
        $status = 'success';
        $message = 'อัพโหลดเสร็จสมบูรณ์';
        $data = null;
        if ($validator->fails()) {
            $status = 'error';
            $message = $validator->errors();
        } else {
            $data = $request->file('file')->store('download');

        }

        return redirect(Config::get('url.backoffice.ebochures'))
        ->with('success', trans('backoffice/common.text_delete_successful'));
    }

    /**
     * อัปเดทวันที่ปัจจุบันที่ updated_at
     */
    public function touch(Request $request, $id)
    {
        Ebochure::findOrFail($id)->touch();

      

        return redirect(Config::get('url.backoffice.ebochures') )
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

}
