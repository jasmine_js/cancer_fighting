<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App\Http\Controllers\Backoffice;

use App\Http\Requests;
use Illuminate\Http\Request;
use Config;
// use App\Article;
// use App\ArticleCategory;
// use App\ArticleCategoryDescription;
// use App\ArticleDescription;
// use App\ArticleImage;
// use App\ArticleToCategory;
// use App\Banner;
// use App\CoOperative;
// use App\CoOperativeDescription;
// use App\Ebochure;
// use App\EbochureDescription;
// use App\Event;
// use App\EventDescription;
// use App\ExchangeProgram;
// use App\ExchangeProgramDescription;
// use App\StudentPortfolio;
// use App\StudentPortfolioDescription;
// use App\Video;
// use App\VideoDescription;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        // $obj = Article :: All();
        // $obj1 = ArticleCategory :: All();
        // $obj2 = ArticleCategoryDescription :: All();
        // $obj3 = ArticleDescription :: All();
        // $items = Banner::where([]);
        // $obj5 = Event :: All();
        // $obj6 = ExchangeProgram :: All();
        // $obj7 = Ebochure :: All();
        // $obj8 = StudentPortfolio :: All();
        // $obj9 = Video :: All();
        // $obj10 = CoOperative :: All();



        // $objs = Banner::all();
        // $data['objs'] = $objs; 
       
       // return view('/homepage',$data);



        return redirect(Config::get('url.backoffice.dashboard'));
        //return view('vendor/adminlte/home',$data);
    }
}