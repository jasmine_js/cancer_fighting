<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App\Http\Controllers\Backoffice;
use App\User;


use App\ArticleDescription;
//use App\Article;

use App\Banner;
use App\EventDescription;
use App\ExchangeProgramDescription;
use App\CoOperativeDescription;
use App\StudentPortfolioDescription;
use App\VideoDescription;
use App\EbochureDescription;
use App\AdmissionDescription;
use App\BannerHighlight;
use App\FinanceDescription;
//use App\Admission;

class DashboardController extends BackofficeController
{
    public function __construct()
    {
        BackofficeController::__construct();

        //initial data
        $this->data_common += [
            'dashboar_page_active' => 'active',
        ];
    }

    public function index()
    {

        $data = [
            $this->data_common += $this->count_admission(),
            $this->data_common += $this->count_article(),
            $this->data_common += $this->count_banner(),
            $this->data_common += $this->count_event(),
            $this->data_common += $this->count_exchange_program(),
            $this->data_common += $this->count_co_operative(),
            $this->data_common += $this->count_student_portfolio(),
            $this->data_common += $this->count_video(),
            $this->data_common += $this->count_ebochure(),
            $this->data_common += $this->count_banner_highlight(),
            $this->data_common += $this->count_finance(),
        ];
        $this->data_common += $data;

        return $this->view('adminlte::home');
    }

    private function count_admission()
    {
        $number = AdmissionDescription::with('admission')
                        ->language($this->language->id)
                        ->count(); 
                        //->paginate();
        return [
            'admission_number' => $number
        ];
    }

    private function count_article()
    {
        $number = ArticleDescription::with('article')
                        ->language($this->language->id)
                        ->count(); 
                        //->paginate();
        return [
            'article_number' => $number
        ];
    }

    private function count_banner()
    {
        $number = Banner::all()
                        ->count(); 
        return [
            'banner_number' => $number
        ];
    }

    private function count_event()
    {
        $number = EventDescription::with('event')
                        ->language($this->language->id)
                        ->count();

        return [
            'event_number' => $number
        ];
    }

    private function count_exchange_program()
    {
        $number = ExchangeProgramDescription::with('exchangeProgram')
                        ->language($this->language->id)
                        ->count();

        return [
            'exchange_program_number' => $number
        ];
    }

    private function count_co_operative()
    {
        $number = CoOperativeDescription::with('coOperative')
                        ->language($this->language->id)
                        ->count();

        return [
            'co_operative_number' => $number
        ];
    }

    private function count_student_portfolio()
    {
        $number = StudentPortfolioDescription::with('studentPortfolio')
                        ->language($this->language->id)
                        ->count();

        return [
            'student_portfolio_number' => $number
        ];
    }

    private function count_video()
    {
        $number = VideoDescription::with('video')
                        ->language($this->language->id)
                        ->count();

        return [
            'video_number' => $number
        ];
    }

    private function count_ebochure()
    {
        $number = EbochureDescription::with('ebochure')
                        ->language($this->language->id)
                        ->count();

        return [
            'ebochure_number' => $number
        ];
    }

    private function count_banner_highlight()
    {
       $number = BannerHighlight::all()
                        ->count(); 
        return [
            'banner_highlight_number' => $number
        ];
    }

    private function count_finance()
    {
        $number = FinanceDescription::with('finance')
                        ->language($this->language->id)
                        ->count();

        return [
            'finance_number' => $number
        ];
    }
}
