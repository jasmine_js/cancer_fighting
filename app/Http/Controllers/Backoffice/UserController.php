<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App\Http\Controllers\Backoffice;

use Config;
use App\Image;
use App\Role;
use App\RoleUser;
use App\User;

use Illuminate\Http\Request;
use App\Http\Requests\Backoffice\UserRequest;


class UserController extends BackofficeController
{

    public function __construct()
    {
        BackofficeController::__construct();

        //initial data
        $this->data_common += [
            'user_page_active' => 'active',
            'placeholder' => Image::resize('no_image.png', 100, 100), // placeholder image
        ];
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles('user/access');
        
        //receive vars
        $find = trim($request->get('find'));
        $orderBy = $request->get('orderBy');
        $asc = (int) $request->get('asc');

        $allowOrderBy = ['id', 'name', 'email', 'updated_at', 'last_login'];

        if (empty($orderBy) || !in_array($orderBy, $allowOrderBy)) {
            $orderBy = 'last_login';
        }

        $items = User::where([]);

        $filter = [];

        if ($find) {
            $items = $items->where('name', 'LIKE', '%' . $find . '%')
                ->orWhere('email', 'LIKE', '%' . $find . '%');
        }

        //  die($items->toSql());
        if ($orderBy) {
            if ($asc) {
                $items = $items->orderBy($orderBy, 'ASC');
            } else {
                $items = $items->orderBy($orderBy, 'DESC');
            }

        }

        $items = $items->paginate($this->per_page);

        // Map image fields
        $items->setCollection(
            $items->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 40, 40); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache
                    } else {
                        $item->image = Image::resize('no_image.png', 40, 40);
                    }
                    return $item;
                })
        );

        $params = '?find=' . $find . '&asc=' . !$asc;// นำไปใช้ตอนกดจัดลำดับที่หัวตารางใน View

        $this->data_common += [
            'items' => $items,
            'find' => $find,
            'asc' => $asc,
            'orderBy' => $orderBy,
            'params' => $params,
            'breadcrumb' => [
                trans('backoffice/common.text_users') => ['url' => '', 'active' => 'active'],
            ],
        ];

        return $this->view('backoffice.users.list');
    }

    public function create(Request $request)
    {
        if(! $request->user()->authorizeRoles('user/modify', false)){
            return redirect(Config::get('url.backoffice.users'))
                ->with('error', trans('backoffice/common.error_unauthorized'));
        }

        $this->data_common += [
            'roles' => Role::order()->get(),
            'roleUserData' => [],
            'breadcrumb' => [
                trans('backoffice/common.text_users') => ['url' => Config::get('url.backoffice.users'), 'active' => ''],
                trans('backoffice/common.text_create_user') => ['url' => '', 'active' => 'active'],
            ],
        ];

        return $this->view('backoffice.users.create');
    }

    public function store(Request $request, UserRequest $userRequest)
    {
        $request->user()->authorizeRoles(['user/modify']);

        if ($userRequest->file('user_photo')) {
            $this->validate($userRequest, [
                'user_photo' => 'mimes:jpeg,jpg,png,gif|max:' . $this->user_photo_size,
            ]);
        }

        $user = new User();

        $user->name = $userRequest->name;
        $user->email = $userRequest->email;
        $user->password = bcrypt($userRequest->password);
        $user->status = $userRequest->status;
        $user->image = $userRequest->image;
        $user->save();

        if ($userRequest->roles) {
            $user->roles()->sync($userRequest->roles);
        }

        return redirect(Config::get('url.backoffice.users'))
            ->with('success', trans('backoffice/common.text_save_successful') );
    }

    public function show(Request $request, $id)
    {
        $request->user()->authorizeRoles('user/access');
    }

    public function edit(Request $request, $id)
    {
        if(! $request->user()->authorizeRoles('user/modify', false)){
            return redirect(Config::get('url.backoffice.users'))
                ->with('error', trans('backoffice/common.error_unauthorized'));
        }

        $item = User::findOrFail($id);

        //Prepare image field for hidden input (สำหรับเก็บลงฐานข้อมูล)
        if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        }

        //Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            $item->thumb = Image::resize($item->image, 100, 100);
        } else {
            $item->thumb = Image::resize('no_image.png', 100, 100);
        }
                
        $roleUser = RoleUser::where('user_id', $id)->pluck('role_id');
        
        $roleUserData = array();
        foreach ($roleUser as $role_id) {
            $roleUserData[] = $role_id;
        }

        $this->data_common += [
            'roles' => Role::order()->get(),
            'item' => $item,
            'roleUserData' => $roleUserData,
            'breadcrumb' => [
                trans('backoffice/common.text_users') => ['url' => Config::get('url.backoffice.users'), 'active' => ''],
                trans('backoffice/users.text_edit_user') => ['url' => '', 'active' => 'active'],
            ],
        ];

        return $this->view('backoffice.users.edit');
    }

    public function update(Request $request, $id)
    {
        $request->user()->authorizeRoles('user/modify');

        // General vars validate
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
        ],[
            'name.required' => 'กรุณาใส่ชื่อ',
            'email.required' => 'กรุณาใส่อีเมล์',
            'email.email' => 'กรุณาใส่อีเมล์ให้ถูกต้อง',
        ]);

        $user = User::findOrFail($id);
        // dd($user);

        // validate password
        if ($request->password) {
            $this->validate($request, [
                'password' => 'required|min:6|confirmed',
            ],[
                'password.required' => 'กรุณาใส่หัสผ่าน',
                'password.min' => 'กรุณาใส่รหัสผ่านขั้นต่ำ 6 ตัวอักษร',
                'password.confirmed' => 'ยืนยันรหัสผ่านไม่ตรงกัน',
            ]);

            $user->password = bcrypt($request->password);
        }

        //validate email if it new change
        if ($request->email != $user->email) {
            $this->validate($request, [
                'email' => 'unique:users,email,' . $user->id,
            ],[
                'email.unique' => 'มีผู้ใช้อีเมล์นี้แล้ว กรุณาเปลี่ยนอีเมล์ใหม่',
            ]);

            $user->email = $request->email;
        }

        $user->name = $request->name;
        $user->status = $request->status;
        $user->image = $request->image;
        $user->save();

        //manage user roles
        $roles = $request->roles;
        if (!$roles) {
            $user->roles()->detach();
        } else {
            $user->roles()->sync($roles);
        }

        return redirect(Config::get('url.backoffice.users'))
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

    public function destroy(Request $request, $id)
    {
        if(! $request->user()->authorizeRoles('user/modify', false)){
            return redirect(Config::get('url.backoffice.users'))
                 ->with('error', trans('backoffice/common.error_unauthorized'));
        }

        User::findOrFail($id)->delete();

        return redirect(Config::get('url.backoffice.users'))
            ->with('success', trans('backoffice/common.text_delete_successful'));
    }
}
