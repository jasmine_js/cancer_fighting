<?php
/**
 * Nov 19, 2018, 11:52 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App\Http\Controllers\Backoffice;

use App;
use App\Language;
use Illuminate\Http\Request;

/**
 * ตัวควบคุมการเปลี่ยนภาษา
 */
class LanguageController extends BackofficeController
{
    public function __construct()
    {
        BackofficeController::__construct();
    }

    public function index(Request $request)
    {
        $code = $request->code;

        if ($code && in_array($code, Language::pluck('code')->toArray())) {
            $this->language = Language::where('code', $code)->enable()->first();
            App::setLocale($code);
            setcookie($this->language_key, $code, time() + $this->cookie_expired);
        }

        return redirect()->back();
    }
}
