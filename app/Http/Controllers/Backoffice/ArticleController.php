<?php
/**
 * Nov 14, 2018, 3:47 PM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App\Http\Controllers\Backoffice;

use Config;
use App\Image;
use App\Article;
use App\ArticleCategoryDescription;
use App\ArticleDescription;
use App\ArticleImage;
use Illuminate\Support\Facades\Auth;
use App\User;


use App\Http\Requests\Backoffice\ArticleRequest;
use Illuminate\Http\Request;


class ArticleController extends BackofficeController
{

    public function __construct()
    {
        BackofficeController::__construct();

        //initial data
        $this->data_common += [
            'article_page_active' => 'active',
            'placeholder' => Image::resize('no_image.png', 100, 100), // placeholder image
        ];
        
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles('article/access');

        $find = $request->get('find');
        $article_category_id = $request->get('article_category_id');
        // $items1 = ArticleDescription::with('user');
        // $items1 = Article::where('updated_by')->get();
        // $items1 = User::find(1)->articles;

        $items = ArticleDescription::with('article')->language($this->language->id);
    
        if ($article_category_id) {
            $items = $items->searchCategory($article_category_id);
        }

        if ($find) {
            $items = $items->searchKeyword($find);
        }

        
        $items = $items->order()->paginate($this->per_page);
        
        // Map image fields
        $items->setCollection(
            $items->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image && !preg_match('/^http:\/\//', $item->image) && !preg_match('/^https:\/\//', $item->image)) { //ต้องมีข้อมูล และต้องไม่ขึ้นต้นด้วย http://
                        $item->image = Image::resize($item->image, 40, 40); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache
                    } else {
                        $item->image = Image::resize('no_image.png', 40, 40);
                    }

                    return $item;
                })
        );

        //ดึงหมวดหมู่
        $categories = ArticleCategoryDescription::order()->language($this->language->id)->get();

        //_get ชื่อ user ที่สร้างและแก้ไขข่าว
        if(count($items)){
            foreach($items as $value)
            {
                $username[] = [
                    'created_by'=>User::find($value->created_by,['name']),
                    'updated_by'=>User::find($value->updated_by,['name'])
                ];
            }
        }else{
            $username = array();
        }
        
        $this->data_common += [
            'items' => $items,
            
            'categories' => $categories,
            'find' => $find,
            'article_category_id' => $article_category_id,
            'breadcrumb' => [
                trans('backoffice/articles.text_news_lists') => ['url' => '', 'active' => 'active'],
            ],
            'username'=>$username
        ];
        // print_r($this->data_common); die;

        return $this->view('backoffice.articles.list');
    }

    public function create(Request $request)
    {
        $request->user()->authorizeRoles('article/modify');

        $article_categories = ArticleCategoryDescription::order()->language($this->language->id)->get();
        // dd($article_categories);

        $this->data_common += [
            'article_categories' => $article_categories,
            'breadcrumb' => [
                trans('backoffice/articles.text_news_lists') => ['url' => Config::get('url.backoffice.articles'), 'active' => ''],
                trans('backoffice/articles.text_create_news') => ['', 'active' => 'active'],
            ],
        ];
        // dd($this->data_common);

        return $this->view('backoffice.articles.create');
    }

    public function store(Request $req, ArticleRequest $request)
    {
        $request->user()->authorizeRoles('article/modify');

        // บันทึกเข้าตารางหลัก
        $item = new Article();
        $item->image = $request->image;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        $item->status = $request->status;
        $item->created_by = Auth::id();
        $item->save();

        $article_id = $item->id;

        // บันทึกเข้าตารางภาษา
        foreach ($request->article_descriptions as $language_id => $value) {
            if(trim($value['name']) != '')
            {
            $item_description = new ArticleDescription();
            $item_description->article_id = $article_id;
            $item_description->language_id = $language_id;
            $item_description->name = $value['name'];
            $item_description->description = $value['description'];
            $item_description->link = $value['link']; //link
            
            //$item_description->meta_title = $value['meta_title'];
            if(trim($value['meta_title']) != ''){
                $item_description->meta_title = $value['meta_title'];
            }else{
                $item_description->meta_title = $value['name'];
            }
            
            $item_description->meta_description = $value['meta_description'];
            $item_description->meta_keyword = $value['meta_keyword'];
            $item_description->tag = $value['tag'];
            $item_description->save();

            unset($item_description); // clear memory
        }
    }

        // บันทึกรูปเพิ่มเติมเข้าตาราง article_images
        if ($request->article_image) {
            foreach ($request->article_image as $image) {
                $article_image = new ArticleImage();
                $article_image->article_id = $article_id;
                $article_image->image = $image['image'];
                $article_image->sort_order = (int) $image['sort_order'];
                $article_image->save();

                unset($article_image); //clear memory
            }
        }

        // dd($request->article_categories);
        // บันทึกเข้าตารางหมวดหมู่(many to many relationship)
        if ($request->article_categories) {
            $item->articleCategories()->sync($request->article_categories);
        }

        return redirect(Config::get('url.backoffice.articles'))
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id, Request $request)
    {
        $request->user()->authorizeRoles('article/modify');

        $item = Article::with('articleDescriptions')
            ->with('articleImages')
            ->with('articleCategories')
            ->findOrFail($id);
        // dd($item);

        //Prepare image field for hidden input (สำหรับเก็บลงฐานข้อมูล)
        if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        }

        //Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            $item->thumb = Image::resize($item->image, 100, 100);
        } else {
            $item->thumb = Image::resize('no_image.png', 100, 100);
        }

        // เตรียมข้อมูลแต่ละภาษาสำหรับจะไปแสดงใน View
        $description = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        foreach ($item->articleDescriptions as $dest) {
            $description[$dest->language_id] = $dest;
        }
        $item->description = $description;

        // เตรียมข้อมูลรูปภาพเพิ่มเติม พร้อม mapping image field ที่จะไปใช้ใน View
        $additional_image = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลรูปภาพเพิ่มเติม
        foreach ($item->articleImages as $image) {
            if ($image['image']) {
                $additional_image[] = [
                    'thumb' => Image::resize($image['image'], 100, 100), // สำหรับแสดง
                    'image' => $image['image'], // สำหรับเก็บลง hidden input คือข้อมูลจริงของเดิม สำหรับบันทึกลงฐานข้อมูล
                    'sort_order' => $image['sort_order'],
                ];
            }
        }
        $item->additional_image = $additional_image;

        // เตรียมข้อมูลหมวดหมู่ที่ถูกเลือกไว้
        $article_categories_selected = [];  // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลหมวดหมู่ที่เลือกไว้้
        foreach ($item->articleCategories as $category) {
            $article_categories_selected[] = $category->id;
        }
        $item->article_categories_selected = $article_categories_selected;
        // dd($item);

        $article_categories = ArticleCategoryDescription::order()->language($this->language->id)->get();

        $this->data_common += [
            'item' => $item,
            'article_categories' => $article_categories,
            'breadcrumb' => [
                trans('backoffice/articles.text_news_lists') => ['url' => Config::get('url.backoffice.articles'), 'active' => ''],
                trans('backoffice/articles.text_edit_news') => ['', 'active' => 'active'],
            ],
        ];
        // dd($this->data_common);

        return $this->view('backoffice.articles.edit');
    }

    public function update(Request $req, ArticleRequest $request, $article_id)
    {
        $req->user()->authorizeRoles('article/modify');

        // บันทึกเข้าตารางหลัก
        $item = Article::findOrFail($article_id);

        $item->image = $request->image;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        $item->status = $request->status;
        $item->updated_by = Auth::id();
        $item->save();

        // บันทึกเข้าตารางภาษา
        // ลบของเดิมในตาราง article_descriptions ก่อน ตาม article_id
        ArticleDescription::where('article_id', $article_id)->delete();

        //แทรกข้อมูลภาาษาใหม่
        foreach ($request->article_descriptions as $language_id => $value) {
            if( trim($value['name']) != '')
            {
                $item_description = new ArticleDescription();
                $item_description->article_id = $article_id;
                $item_description->language_id = $language_id;
                $item_description->name = $value['name'];
                $item_description->description = $value['description'];
                $item_description->link = $value['link']; //link
                //$item_description->meta_title = $value['meta_title'];
                if(trim($value['meta_title']) != ''){
                    $item_description->meta_title = $value['meta_title'];
                }else{
                    $item_description->meta_title = $value['name'];
                }
                
                $item_description->meta_description = $value['meta_description'];
                $item_description->meta_keyword = $value['meta_keyword'];
                $item_description->tag = $value['tag'];
                $item_description->save();

                unset($item_description); // clear memory
            }
    }

        // บันทึกรูปเพิ่มเติมเข้าตาราง article_images
        // ลบข้อมูลของเดิมตาม article_id เพื่อเตรียมการเพิ่มเข้าไปใหม่
        ArticleImage::where('article_id', $article_id)->delete();

        if ($request->article_image) {
            foreach ($request->article_image as $image) {
                $article_image = new ArticleImage();
                $article_image->article_id = $article_id;
                $article_image->image = $image['image'];
                $article_image->sort_order = (int) $image['sort_order'];
                $article_image->save();

                unset($article_image); //clear memory
            }
        }

        // บันทึกเข้าตารางหมวดหมู่(many to many relationship)
        if ($request->article_categories) {
            $item->articleCategories()->sync($request->article_categories);
        }else{
            //ถ้าไม่มีการเลือก ให้ลบข้อมูล
            $item->articleCategories()->detach();
        }

        return redirect(Config::get('url.backoffice.articles'))
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

    public function destroy(Request $req, $article_id)
    {
        $req->user()->authorizeRoles('article/modify');

        $item = Article::findOrFail($article_id); // ลบที่ตารางหลักที่เดียว ตารางที่ relation one-to-many กับตารางหลักจะถูกลบโดยอัตโนมัติ

        $item->delete();

        return redirect(Config::get('url.backoffice.articles'))
            ->with('success', trans('backoffice/common.text_delete_successful'));
    }

    /**
     * อัปเดทวันที่ปัจจุบันที่ updated_at
     */
    public function touch(Request $request, $id)
    {
        Article::findOrFail($id)->touch();

        $appends_url = '';

        if ($request->article_category_id) {
            $appends_url .= '&article_category_id=' . $request->article_category_id;
        }


        return redirect(Config::get('url.backoffice.articles') . $appends_url)
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

}
