<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App\Http\Controllers\Backoffice;

use App\AdmissionCategory;
use App\AdmissionCategoryDescription;
use App\Http\Requests\Backoffice\AdmissionCategoryRequest;
use App\Image;
use Config;
use Illuminate\Http\Request;

class AdmissionCategoryController extends BackofficeController
{

    public function __construct()
    {
        BackofficeController::__construct();

        //initial data
        $this->data_common += ['admission_page_active' => 'active'];
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles('admission_category/access');

        $find = $request->get('find');
        $this->data_common += ['find' => $find];

        $items = AdmissionCategoryDescription::with('admissionCategory')
            ->language($this->language->id);

        if ($find) {
            $items = $items->search($find);
        }

        $items = $items->order()->paginate($this->per_page);
        // dd($items);

        // Map image fields
        $items->setCollection(
            $items->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 40, 40); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache
                    } else {
                        $item->image = Image::resize('no_image.png', 40, 40);
                    }

                    return $item;
                })
        );

        $this->data_common += ['items' => $items];

        $this->data_common += ['breadcrumb' => [
            trans('backoffice/admission-categories.text_admission_category_lists') => ['url' => '', 'active' => 'active'],
        ]];

        return $this->view('backoffice.admission-categories.list');
    }

    public function create(Request $request)
    {
        $request->user()->authorizeRoles('admission_category/modify');

        $this->data_common += [
            'breadcrumb' => [
                trans('backoffice/admission-categories.text_admission_category_lists') => ['url' => Config::get('url.backoffice.admission_categories'), 'active' => ''],
                trans('backoffice/admission-categories.text_create_admission_category') => ['', 'active' => 'active'],
            ],
        ];

        return $this->view('backoffice.admission-categories.create');
    }

    public function store(Request $req, AdmissionCategoryRequest $request)
    {
        $req->user()->authorizeRoles('admission_category/modify');

        // dd($request->all());
        // dd($request->admission_category_descriptions);

        // บันทึกเข้าตารางหลัก
        $category = new AdmissionCategory();
        $category->image = $request->image;
        $category->sort_order = (int) $request->sort_order;
        $category->status = $request->status;
        $category->save();

        $admission_category_id = $category->id;
        unset($category);

        // บันทึกเข้าตารางภาษา
        foreach ($request->admission_category_descriptions as $language_id => $value) {
            if($value['name'] != '')
            {
            $category_description = new AdmissionCategoryDescription();
            $category_description->admission_category_id = $admission_category_id;
            $category_description->language_id = $language_id;
            $category_description->name = $value['name'];
            $category_description->description = $value['description'];

            if(trim($value['meta_title']) != ''){
                $category_description->meta_title   = $value['meta_title'];
            }else{
                $category_description->meta_title   = $value['name'];
            }
            
            $category_description->meta_description = $value['meta_description'];
            $category_description->meta_keyword = $value['meta_keyword'];
            $category_description->save();

            unset($category_description); // clear memory
        }
    }

        return redirect(Config::get('url.backoffice.admission_categories'))
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id, Request $req)
    {
        $req->user()->authorizeRoles('admission_category/modify');

        $item = AdmissionCategory::with('admissionCategoryDescriptions')->findOrFail($id);
        // dd($item);

        //Prepare image field for hidden input (สำหรับเก็บลงฐานข้อมูล)
        if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        }

        //Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            $item->thumb = Image::resize($item->image, 100, 100);
        } else {
            $item->thumb = Image::resize('no_image.png', 100, 100);
        }

        // เตรียมข้อมูลแต่ละภาษาสำหรับจะไปแสดงใน View
        $description = []; //สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        foreach ($item->admissionCategoryDescriptions as $dest) {
            $description[$dest->language_id] = $dest;
        }
        $item->description = $description;
        // dd($item);

        $data = [
            'item' => $item,
            'breadcrumb' => [
                trans('backoffice/admission-categories.text_admission_category_lists') => ['url' => Config::get('url.backoffice.admission_categories'), 'active' => ''],
                trans('backoffice/admission-categories.text_edit_admission_category') => ['', 'active' => 'active'],
            ],
        ];

        $this->data_common += $data;

        return $this->view('backoffice.admission-categories.edit');
    }

    public function update(Request $req, AdmissionCategoryRequest $request, $admission_category_id)
    {
        $req->user()->authorizeRoles('admission_category/modify');

        // dd($request->all());

        // บันทึกเข้าตารางหลัก
        $item = AdmissionCategory::findOrFail($admission_category_id);

        $item->image = $request->image;
        $item->sort_order = (int) $request->sort_order;
        $item->status = $request->status;
        $item->save();

        unset($item);

        // บันทึกเข้าตารางภาษา
        // ลบของเดิมในตาราง admission_category_descriptions ก่อน ตาม admission_category_id
        AdmissionCategoryDescription::where('admission_category_id', $admission_category_id)->delete();

        //แทรกข้อมูลภาาษาใหม่
        foreach ($request->admission_category_descriptions as $language_id => $value) {
            if($value['name'] != '')
            {
            $category_description = new AdmissionCategoryDescription();
            $category_description->admission_category_id = $admission_category_id;
            $category_description->language_id = $language_id;
            $category_description->name = $value['name'];
            $category_description->description = $value['description'];

            if(trim($value['meta_title']) != ''){
                $category_description->meta_title   = $value['meta_title'];
            }else{
                $category_description->meta_title   = $value['name'];
            }

            $category_description->meta_description = $value['meta_description'];
            $category_description->meta_keyword = $value['meta_keyword'];
            $category_description->save();

            unset($category_description); // clear memory
        }
    }

        return redirect(Config::get('url.backoffice.admission_categories'))
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

    public function destroy(Request $req, $admission_category_id)
    {
        $req->user()->authorizeRoles('admission_category/modify');

        $item = AdmissionCategory::findOrFail($admission_category_id);

        $item->delete();

        return redirect(Config::get('url.backoffice.admission_categories'))
            ->with('success', trans('backoffice/common.text_delete_successful'));
    }

}
