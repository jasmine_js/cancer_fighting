<?php

namespace App\Http\Controllers\Backoffice;

use Config;
use App\Image;
use App\ExchangeProgram;
use App\ExchangeProgramDescription;
use App\ExchangeCategoryDescription;
use App\ExchangeImage;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Requests\Backoffice\ ExchangeProgramRequest;
use Illuminate\Http\Request;




class ExchangeProgramController extends BackofficeController
{
    public function __construct()
    {
        BackofficeController::__construct();

        //initial data
        $this->data_common += [
            'exchange_program_page_active' => 'active',
            
            'placeholder' => Image::resize('no_image.png', 100, 100), // placeholder image
        ];
        
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles('exchange_program/access');

        $find = $request->get('find');
        $exchange_category_id = $request->get('exchange_category_id');

        //$article_category_id = $request->get('article_category_id');

        $items = ExchangeProgramDescription::with('exchangeProgram')->language($this->language->id);

        if ($exchange_category_id) {
            $items = $items->searchCategory($exchange_category_id);
        }

        if ($find) {
            $items = $items->searchKeyword($find);
        }
        // dd($items->toSql());

        $items = $items->order()->paginate($this->per_page);
        //$exchangedesc = ExchangeProgramDescription::where([]);  //model

        $items->setCollection(
            $items->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image && !preg_match('/^http:\/\//', $item->image) && !preg_match('/^https:\/\//', $item->image)) { //ต้องมีข้อมูล และต้องไม่ขึ้นต้นด้วย http://
                        $item->image = Image::resize($item->image, 40, 40); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache
                    } else {
                        $item->image = Image::resize('no_image.png', 40, 40);
                    }

                    return $item;
                })
        );
        
        //ดึงหมวดหมู่
        $categories = ExchangeCategoryDescription::order()->language($this->language->id)->get();
        // dd($categories);

        //_get ชื่อ user ที่สร้างและแก้ไขข่าว
        if(count($items)){
            foreach($items as $value)
            {
                $username[] = [
                    'created_by'=>User::find($value->created_by,['name']),
                    'updated_by'=>User::find($value->updated_by,['name'])
                ];
            }
        }else{
            $username = array();
        }

        $this->data_common += [
            'items' => $items,
            'categories' => $categories,
            'find' => $find,
            'exchange_category_id' => $exchange_category_id,
            'breadcrumb' => [
                trans('backoffice/exchange-programs.text_exchange_program_lists') => ['url' => '', 'active' => 'active'],
            ],
            'username'=>$username
        ];
       
       
       
        return $this->view('backoffice.exchange-programs.list');
    }

   
    public function create(Request $request)
    {
        $request->user()->authorizeRoles('exchange_program/modify');
        $exchange_categories = ExchangeCategoryDescription::order()->language($this->language->id)->get();
        // dd($article_categories);

        $this->data_common += [
            'exchange_categories' => $exchange_categories,
            'breadcrumb' => [
                trans('backoffice/exchange-programs.text_exchange_program_lists') => ['url' => Config::get('url.backoffice.exchange_programs'), 'active' => ''],
                trans('backoffice/exchange-programs.text_create_exchange_program') => ['', 'active' => 'active'],
            ],
        ];

        return $this->view('backoffice.exchange-programs.create');
    }

    
    public function store(Request $req,ExchangeProgramRequest $request)//EventRequest $request
    {
        $req->user()->authorizeRoles('exchange_program/modify');

        // บันทึกเข้าตารางหลัก events
        $item = new ExchangeProgram();
        $item->image = $request->image;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        $item->status = $request->status;
        $item->created_by = Auth::id();
        $item->save();

        $exchange_program_id = $item->id;

        // บันทึกเข้าตาราง event descriptions
        foreach ($request->exchange_program_descriptions as $language_id => $value) {
            if( trim($value['name']) != '')
            {
                $item_description = new ExchangeProgramDescription();
                $item_description->exchange_program_id = $exchange_program_id;
                $item_description->language_id = $language_id;
                $item_description->name = $value['name'];  //ชื่อ
                $item_description->description = $value['description']; //รายละเอียด
                $item_description->link = $value['link']; //link
                //$item_description->meta_title = $value['meta_title'];
                if(trim($value['meta_title']) != ''){
                    $item_description->meta_title = $value['meta_title'];
                }else{
                    $item_description->meta_title = $value['name'];
                }

                $item_description->meta_description = $value['meta_description'];
                $item_description->meta_keyword = $value['meta_keyword'];
                $item_description->tag = $value['tag'];
                $item_description->save();

                unset($item_description); // clear memory
            }
    }

            // บันทึกรูปเพิ่มเติมเข้าตาราง article_images
            if ($request->exchange_image) {
                foreach ($request->exchange_image as $image) {
                    $exchange_image = new ExchangeImage();
                    $exchange_image->exchange_program_id = $exchange_program_id;
                    $exchange_image->image = $image['image'];
                    $exchange_image->sort_order = (int) $image['sort_order'];
                    $exchange_image->save();

                    unset($exchange_image); //clear memory
                }
            }

            // dd($request->article_categories);
            // บันทึกเข้าตารางหมวดหมู่(many to many relationship)
            if ($request->exchange_categories) {
                $item->exchangeCategories()->sync($request->exchange_categories);
            }


        return redirect(Config::get('url.backoffice.exchange_programs'))
            ->with('success', trans('backoffice/common.text_save_successful'));

    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id, Request $request)
    {
        $request->user()->authorizeRoles('exchange_program/modify');
        $item = ExchangeProgram::with('exchangeprogramDescriptions')
        ->with('exchangeImages')
        ->with('exchangeCategories') //function in event model 
        ->findOrFail($id);


        if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        }

        //Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            $item->thumb = Image::resize($item->image, 100, 100);
        } else {
            $item->thumb = Image::resize('no_image.png', 100, 100);
        }

        

        $description = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        foreach ($item->exchangeprogramDescriptions as $dest) {
            $description[$dest->language_id] = $dest;
        }
        $item->description = $description;

         // เตรียมข้อมูลรูปภาพเพิ่มเติม พร้อม mapping image field ที่จะไปใช้ใน View
         $additional_image = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลรูปภาพเพิ่มเติม
         foreach ($item->exchangeImages as $image) {
             if ($image['image']) {
                 $additional_image[] = [
                     'thumb' => Image::resize($image['image'], 100, 100), // สำหรับแสดง
                     'image' => $image['image'], // สำหรับเก็บลง hidden input คือข้อมูลจริงของเดิม สำหรับบันทึกลงฐานข้อมูล
                     'sort_order' => $image['sort_order'],
                 ];
             }
         }
         $item->additional_image = $additional_image;
 
         // เตรียมข้อมูลหมวดหมู่ที่ถูกเลือกไว้
         $exchange_categories_selected = [];  // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลหมวดหมู่ที่เลือกไว้้
         foreach ($item->exchangeCategories as $category) {
             $exchange_categories_selected[] = $category->id;
         }
         $item->exchange_categories_selected = $exchange_categories_selected;
         // dd($item);
 
         $exchange_categories = ExchangeCategoryDescription::order()->language($this->language->id)->get();
 
       
        $this->data_common += [
            'item' => $item,
            'exchange_categories' => $exchange_categories,
            'breadcrumb' => [
                trans('backoffice/exchange-programs.text_exchange_program_lists') => ['url' => Config::get('url.backoffice.exchange_programs'), 'active' => ''],
                trans('backoffice/exchange-programs.text_edit_exchange_program') => ['', 'active' => 'active'],
            ],
        ];
        // dd($this->data_common);

        return $this->view('backoffice.exchange-programs.edit');
    }

    
    public function update(Request $req,ExchangeProgramRequest $request, $exchange_program_id)
    {
        $request->user()->authorizeRoles('exchange_program/modify');

        // บันทึกเข้าตารางหลัก
        $item = ExchangeProgram::findOrFail($exchange_program_id);

        $item->image = $request->image;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        //$item->event_date = $request->event_date;  //event_date
        $item->status = $request->status;
        $item->updated_by = Auth::id();
        $item->save();

        ExchangeProgramDescription::where('exchange_program_id', $exchange_program_id)->delete();
       
        // บันทึกเข้าตาราง event descriptions
        foreach ($request->exchange_program_descriptions as $language_id => $value) {
            if(trim($value['name']) != '')
            {
    
            $item_description = new  ExchangeProgramDescription();
            $item_description->exchange_program_id = $exchange_program_id;
            $item_description->language_id = $language_id;
            $item_description->name = $value['name'];  //ชื่อ
            $item_description->description = $value['description']; //รายละเอียด
            $item_description->link = $value['link']; //link
            //$item_description->meta_title = $value['meta_title'];
            if(trim($value['meta_title']) != ''){
                $item_description->meta_title = $value['meta_title'];
            }else{
                $item_description->meta_title = $value['name'];
            }

            $item_description->meta_description = $value['meta_description'];
            $item_description->meta_keyword = $value['meta_keyword'];
            $item_description->tag = $value['tag'];
            $item_description->save();

            unset($item_description); // clear memory
        }
    }

      // บันทึกรูปเพิ่มเติมเข้าตาราง article_images
        // ลบข้อมูลของเดิมตาม article_id เพื่อเตรียมการเพิ่มเข้าไปใหม่
        ExchangeImage::where('exchange_program_id', $exchange_program_id)->delete();

        if ($request->exchange_image) {
            foreach ($request->exchange_image as $image) {
                $exchange_image = new ExchangeImage();
                $exchange_image->exchange_program_id = $exchange_program_id;
                $exchange_image->image = $image['image'];
                $exchange_image->sort_order = (int) $image['sort_order'];
                $exchange_image->save();

                unset($exchange_image); //clear memory
            }
        }

        // บันทึกเข้าตารางหมวดหมู่(many to many relationship)
        if ($request->exchange_categories) {
            $item->exchangeCategories()->sync($request->exchange_categories);
        }else{
            //ถ้าไม่มีการเลือก ให้ลบข้อมูล
            $item->exchangeCategories()->detach();
        }


        return redirect(Config::get('url.backoffice.exchange_programs'))
            ->with('success', trans('backoffice/common.text_save_successful'));

    }

  
    public function destroy(Request $req, $exchange_program_id)
    {
        $req->user()->authorizeRoles('exchange_program/modify');
       
        $item = ExchangeProgram::findOrFail($exchange_program_id);

        $item->delete();

        return redirect(Config::get('url.backoffice.exchange_programs'))
            ->with('success', trans('backoffice/common.text_delete_successful'));
    }

    /**
     * อัปเดทวันที่ปัจจุบันที่ updated_at
     */
    public function touch(Request $request, $id)
    {
        ExchangeProgram::findOrFail($id)->touch();
        $appends_url = '';

        if ($request->exchange_category_id) {
            $appends_url .= '&exchange_category_id=' . $request->exchange_category_id;
        }
      

        return redirect(Config::get('url.backoffice.exchange_programs') . $appends_url )
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

}