<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Http\Request;
use App\Video;
use App\VideoDescription;
use App\Http\Requests\Backoffice\ VideoRequest;
use Config;
use App\Image;
use Illuminate\Support\Facades\Auth;
use App\User;

class VideoController extends BackofficeController
{
    public function __construct()
    {
        BackofficeController::__construct();

        //initial data
        $this->data_common += ['video_page_active' => 'active'];
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles('video/access');

        $find = $request->get('find');
        //$article_category_id = $request->get('article_category_id');

        $items = VideoDescription::with('video')->language($this->language->id);

        
        if ($find) {
            $items = $items->searchKeyword($find);
        }
        // dd($items->toSql());

        $items = $items->order()->paginate($this->per_page);
        //$exchangedesc = ExchangeProgramDescription::where([]);  //model

        $items->setCollection(
            $items->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image && !preg_match('/^http:\/\//', $item->image) && !preg_match('/^https:\/\//', $item->image)) { //ต้องมีข้อมูล และต้องไม่ขึ้นต้นด้วย http://
                        $item->image = Image::resize($item->image, 40, 40); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache
                    } else {
                        $item->image = Image::resize('no_image.png', 40, 40);
                    }

                    return $item;
                })
        );
        //_get ชื่อ user ที่สร้างและแก้ไขข่าว
        if(count($items)){
            foreach($items as $value)
            {
                $username[] = [
                    'created_by'=>User::find($value->created_by,['name']),
                    'updated_by'=>User::find($value->updated_by,['name'])
                ];
            }
        }else{
            $username = array();
        }
        

        $this->data_common += [
            'items' => $items,
           
            'find' => $find,
            'breadcrumb' => [
                trans('backoffice/videos.text_video_lists') => ['url' => '', 'active' => 'active'],
            ],
            'username'=>$username
        ];
       
       
       
        return $this->view('backoffice.videos.list');
    }

   
    public function create(Request $request)
    {
        $request->user()->authorizeRoles('video/modify');

        $this->data_common += [
            'breadcrumb' => [
                trans('backoffice/videos.text_video_lists') => ['url' => Config::get('url.backoffice.videos'), 'active' => ''],
                trans('backoffice/videos.text_create_video') => ['', 'active' => 'active'],
            ],
        ];

        return $this->view('backoffice.videos.create');
    }

    
    public function store(Request $req,VideoRequest $request)//EventRequest $request
    {
        $req->user()->authorizeRoles('video/modify');

        // บันทึกเข้าตารางหลัก events
        $item = new Video();
        $item->image = $request->image;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        $item->status = $request->status;
        $item->created_by = Auth::id();
        $item->save();

        $video_id = $item->id;

        // บันทึกเข้าตาราง event descriptions
        foreach ($request->video_descriptions as $language_id => $value) {
            if($value['name'] != '')
            {
            $item_description = new VideoDescription();
            $item_description->video_id = $video_id;
            $item_description->language_id = $language_id;
            $item_description->name = $value['name'];  //ชื่อ
            $item_description->description = $value['description']; //รายละเอียด
            $item_description->video_link = $value['link']; //link
            //$item_description->meta_title = $value['meta_title'];
            if(trim($value['meta_title']) != ''){
                $item_description->meta_title = $value['meta_title'];
            }else{
                $item_description->meta_title = $value['name'];
            }
            $item_description->meta_description = $value['meta_description'];
            $item_description->meta_keyword = $value['meta_keyword'];
            $item_description->tag = $value['tag'];
            $item_description->save();

            unset($item_description); // clear memory
        }
    }


        return redirect(Config::get('url.backoffice.videos'))
            ->with('success', trans('backoffice/common.text_save_successful'));

    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id, Request $request)
    {
        $request->user()->authorizeRoles('video/modify');
        $item = Video::with('videoDescriptions') //function in event model 
        ->findOrFail($id);


        if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        }

        //Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            $item->thumb = Image::resize($item->image, 100, 100);
        } else {
            $item->thumb = Image::resize('no_image.png', 100, 100);
        }

        

        $description = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        foreach ($item->videoDescriptions as $dest) {
            $description[$dest->language_id] = $dest;
        }
        $item->description = $description;
       
        $this->data_common += [
            'item' => $item,
           
            'breadcrumb' => [
                trans('backoffice/videos.text_video_lists') => ['url' => Config::get('url.backoffice.videos'), 'active' => ''],
                trans('backoffice/videos.text_edit_video') => ['', 'active' => 'active'],
            ],
        ];
        // dd($this->data_common);

        return $this->view('backoffice.videos.edit');
    }
    public function update(Request $req, VideoRequest $request,$video_id)
    {
        $req->user()->authorizeRoles('video/modify');

        // บันทึกเข้าตารางหลัก
        $item = Video::findOrFail($video_id);

        $item->image = $request->image;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        //$item->event_date = $request->event_date;  //event_date
        $item->status = $request->status;
        $item->updated_by = Auth::id();
        $item->save();

        VideoDescription::where('video_id', $video_id)->delete();

        // บันทึกเข้าตาราง event descriptions
        foreach ($request->video_descriptions as $language_id => $value) {
            if($value['name'] != '')
            {
            $item_description = new  VideoDescription();
            $item_description->video_id = $video_id;
            $item_description->language_id = $language_id;
            $item_description->name = $value['name'];  //ชื่อ
            $item_description->description = $value['description']; //รายละเอียด
            $item_description->video_link = $value['link']; //link
            //$item_description->meta_title = $value['meta_title'];
            if(trim($value['meta_title']) != ''){
                $item_description->meta_title = $value['meta_title'];
            }else{
                $item_description->meta_title = $value['name'];
            }
            $item_description->meta_description = $value['meta_description'];
            $item_description->meta_keyword = $value['meta_keyword'];
            $item_description->tag = $value['tag'];
            $item_description->save();

            unset($item_description); // clear memory
        }
    }


        return redirect(Config::get('url.backoffice.videos'))
            ->with('success', trans('backoffice/common.text_save_successful'));

    }

  
    public function destroy(Request $req, $id)
    {
        $req->user()->authorizeRoles('video/modify');

        $item = Video::findOrFail($id);

        $item->delete();

        return redirect(Config::get('url.backoffice.videos'))
            ->with('success', trans('backoffice/common.text_delete_successful'));
    }

    /**
     * อัปเดทวันที่ปัจจุบันที่ updated_at
     */
    public function touch(Request $request, $id)
    {
        Video::findOrFail($id)->touch();

      

        return redirect(Config::get('url.backoffice.videos') )
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

}