<?php
/**
 * Nov 14, 2018, 3:47 PM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App\Http\Controllers\Backoffice;

use Config;
use App\Image;
use App\Admission;
use App\AdmissionCategoryDescription;
use App\AdmissionDescription;
use App\AdmissionImage;
use Illuminate\Support\Facades\Auth;
use App\User;


use App\Http\Requests\Backoffice\AdmissionRequest;
use Illuminate\Http\Request;


class AdmissionController extends BackofficeController
{

    public function __construct()
    {
        BackofficeController::__construct();

        //initial data
        $this->data_common += [
            'admission_page_active' => 'active',
            'placeholder' => Image::resize('no_image.png', 100, 100), // placeholder image
        ];
        
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles('admission/access');

        $find = $request->get('find');
        $admission_category_id = $request->get('admission_category_id');
        // $items1 = ArticleDescription::with('user');
        // $items1 = Article::where('updated_by')->get();
        // $items1 = User::find(1)->articles;

        $items = AdmissionDescription::with('admission')->language($this->language->id);
    
        if ($admission_category_id) {
            $items = $items->searchCategory($admission_category_id);
        }

        if ($find) {
            $items = $items->searchKeyword($find);
        }

        
        $items = $items->order()->paginate($this->per_page);
        
        // Map image fields
        $items->setCollection(
            $items->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image && !preg_match('/^http:\/\//', $item->image) && !preg_match('/^https:\/\//', $item->image)) { //ต้องมีข้อมูล และต้องไม่ขึ้นต้นด้วย http://
                        $item->image = Image::resize($item->image, 40, 40); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache
                    } else {
                        $item->image = Image::resize('no_image.png', 40, 40);
                    }

                    return $item;
                })
        );

        //ดึงหมวดหมู่
        $categories = AdmissionCategoryDescription::order()->language($this->language->id)->get();

        //_get ชื่อ user ที่สร้างและแก้ไขข่าว
        if(count($items)){
            foreach($items as $value)
            {
                $username[] = [
                    'created_by'=>User::find($value->created_by,['name']),
                    'updated_by'=>User::find($value->updated_by,['name'])
                ];
            }
        }else{
            $username = array();
        }
        
        $this->data_common += [
            'items' => $items,
            
            'categories' => $categories,
            'find' => $find,
            'admission_category_id' => $admission_category_id,
            'breadcrumb' => [
                trans('backoffice/admissions.text_admission_lists') => ['url' => '', 'active' => 'active'],
            ],
            'username'=>$username
        ];
        // print_r($this->data_common); die;

        return $this->view('backoffice.admissions.list');
    }

    public function create(Request $request)
    {
        $request->user()->authorizeRoles('admission/modify');

        $admission_categories = AdmissionCategoryDescription::order()->language($this->language->id)->get();
        // dd($article_categories);

        $this->data_common += [
            'admission_categories' => $admission_categories,
            'breadcrumb' => [
                trans('backoffice/admissions.text_admission_lists') => ['url' => Config::get('url.backoffice.admissions'), 'active' => ''],
                trans('backoffice/admissions.text_create_admission') => ['', 'active' => 'active'],
            ],
        ];
        // dd($this->data_common);

        return $this->view('backoffice.admissions.create');
    }

    public function store(Request $req, AdmissionRequest $request)
    {
        $request->user()->authorizeRoles('admission/modify');

        // บันทึกเข้าตารางหลัก
        $item = new Admission();
        $item->image = $request->image;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        $item->status = $request->status;
        $item->created_by = Auth::id();
        $item->save();

        $admission_id = $item->id;

        // บันทึกเข้าตารางภาษา
        foreach ($request->admission_descriptions as $language_id => $value) {
            if(trim($value['name']) != '')
            {
            $item_description = new AdmissionDescription();
            $item_description->admission_id = $admission_id;
            $item_description->language_id = $language_id;
            $item_description->name = $value['name'];
            $item_description->description = $value['description'];
            $item_description->link = $value['link']; //link
            
            //$item_description->meta_title = $value['meta_title'];
            if(trim($value['meta_title']) != ''){
                $item_description->meta_title = $value['meta_title'];
            }else{
                $item_description->meta_title = $value['name'];
            }
            
            $item_description->meta_description = $value['meta_description'];
            $item_description->meta_keyword = $value['meta_keyword'];
            $item_description->tag = $value['tag'];
            $item_description->save();

            unset($item_description); // clear memory
        }
    }

        // บันทึกรูปเพิ่มเติมเข้าตาราง admission_images
        if ($request->admission_image) {
            foreach ($request->admission_image as $image) {
                $admission_image = new AdmissionImage();
                $admission_image->admission_id = $admission_id;
                $admission_image->image = $image['image'];
                $admission_image->sort_order = (int) $image['sort_order'];
                $admission_image->save();

                unset($admission_image); //clear memory
            }
        }

        // dd($request->article_categories);
        // บันทึกเข้าตารางหมวดหมู่(many to many relationship)
        if ($request->admission_categories) {
            $item->admissionCategories()->sync($request->admission_categories);
        }

        return redirect(Config::get('url.backoffice.admissions'))
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id, Request $request)
    {
        $request->user()->authorizeRoles('admission/modify');

        $item = Admission::with('admissionDescriptions')
            ->with('admissionImages')
            ->with('admissionCategories')
            ->findOrFail($id);
        // dd($item);

        //Prepare image field for hidden input (สำหรับเก็บลงฐานข้อมูล)
        if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        }

        //Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            $item->thumb = Image::resize($item->image, 100, 100);
        } else {
            $item->thumb = Image::resize('no_image.png', 100, 100);
        }

        // เตรียมข้อมูลแต่ละภาษาสำหรับจะไปแสดงใน View
        $description = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        foreach ($item->admissionDescriptions as $dest) {
            $description[$dest->language_id] = $dest;
        }
        $item->description = $description;

        // เตรียมข้อมูลรูปภาพเพิ่มเติม พร้อม mapping image field ที่จะไปใช้ใน View
        $additional_image = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลรูปภาพเพิ่มเติม
        foreach ($item->admissionImages as $image) {
            if ($image['image']) {
                $additional_image[] = [
                    'thumb' => Image::resize($image['image'], 100, 100), // สำหรับแสดง
                    'image' => $image['image'], // สำหรับเก็บลง hidden input คือข้อมูลจริงของเดิม สำหรับบันทึกลงฐานข้อมูล
                    'sort_order' => $image['sort_order'],
                ];
            }
        }
        $item->additional_image = $additional_image;

        // เตรียมข้อมูลหมวดหมู่ที่ถูกเลือกไว้
        $admission_categories_selected = [];  // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลหมวดหมู่ที่เลือกไว้้
        foreach ($item->admissionCategories as $category) {
            $admission_categories_selected[] = $category->id;
        }
        $item->admission_categories_selected = $admission_categories_selected;
        // dd($item);

        $admission_categories = AdmissionCategoryDescription::order()->language($this->language->id)->get();

        $this->data_common += [
            'item' => $item,
            'admission_categories' => $admission_categories,
            'breadcrumb' => [
                trans('backoffice/admissions.text_admission_lists') => ['url' => Config::get('url.backoffice.admissions'), 'active' => ''],
                trans('backoffice/admissions.text_edit_admission') => ['', 'active' => 'active'],
            ],
        ];
        // dd($this->data_common);

        return $this->view('backoffice.admissions.edit');
    }

    public function update(Request $req, AdmissionRequest $request, $admission_id)
    {
        $req->user()->authorizeRoles('admission/modify');

        // บันทึกเข้าตารางหลัก
        $item = Admission::findOrFail($admission_id);

        $item->image = $request->image;
        $item->publish_start = $request->publish_start;
        $item->publish_stop = $request->publish_stop;
        $item->status = $request->status;
        $item->updated_by = Auth::id();
        $item->save();

        // บันทึกเข้าตารางภาษา
        // ลบของเดิมในตาราง article_descriptions ก่อน ตาม article_id
        AdmissionDescription::where('admission_id', $admission_id)->delete();

        //แทรกข้อมูลภาาษาใหม่
        foreach ($request->admission_descriptions as $language_id => $value) {
            if( trim($value['name']) != '')
            {
                $item_description = new AdmissionDescription();
                $item_description->admission_id = $admission_id;
                $item_description->language_id = $language_id;
                $item_description->name = $value['name'];
                $item_description->description = $value['description'];
                $item_description->link = $value['link']; //link
                //$item_description->meta_title = $value['meta_title'];
                if(trim($value['meta_title']) != ''){
                    $item_description->meta_title = $value['meta_title'];
                }else{
                    $item_description->meta_title = $value['name'];
                }
                
                $item_description->meta_description = $value['meta_description'];
                $item_description->meta_keyword = $value['meta_keyword'];
                $item_description->tag = $value['tag'];
                $item_description->save();

                unset($item_description); // clear memory
            }
    }

        // บันทึกรูปเพิ่มเติมเข้าตาราง admission_images
        // ลบข้อมูลของเดิมตาม article_id เพื่อเตรียมการเพิ่มเข้าไปใหม่
        AdmissionImage::where('admission_id', $admission_id)->delete();

        if ($request->admission_image) {
            foreach ($request->admission_image as $image) {
                $admission_image = new AdmissionImage();
                $admission_image->admission_id = $admission_id;
                $admission_image->image = $image['image'];
                $admission_image->sort_order = (int) $image['sort_order'];
                $admission_image->save();

                unset($admission_image); //clear memory
            }
        }

        // บันทึกเข้าตารางหมวดหมู่(many to many relationship)
        if ($request->admission_categories) {
            $item->admissionCategories()->sync($request->admission_categories);
        }else{
            //ถ้าไม่มีการเลือก ให้ลบข้อมูล
            $item->admissionCategories()->detach();
        }

        return redirect(Config::get('url.backoffice.admissions'))
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

    public function destroy(Request $req, $admission_id)
    {
        $req->user()->authorizeRoles('admission/modify');

        $item = Admission::findOrFail($admission_id); // ลบที่ตารางหลักที่เดียว ตารางที่ relation one-to-many กับตารางหลักจะถูกลบโดยอัตโนมัติ

        $item->delete();

        return redirect(Config::get('url.backoffice.admissions'))
            ->with('success', trans('backoffice/common.text_delete_successful'));
    }

    /**
     * อัปเดทวันที่ปัจจุบันที่ updated_at
     */
    public function touch(Request $request, $id)
    {
        Admission::findOrFail($id)->touch();

        $appends_url = '';

        if ($request->admission_category_id) {
            $appends_url .= '&admission_category_id=' . $request->admission_category_id;
        }


        return redirect(Config::get('url.backoffice.admissions') . $appends_url)
            ->with('success', trans('backoffice/common.text_save_successful'));
    }

}
