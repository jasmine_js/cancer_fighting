<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App\Http\Controllers\Backoffice;

use Auth;
use Config;
use App\Image;
use App\User;
use Illuminate\Http\Request;


class ProfileController extends BackofficeController
{

    public function __construct(Request $request)
    {
        BackofficeController::__construct();

         //initial data
         $this->data_common += [
            'profile_active' => 'active',
        ];
    }

    public function edit()
    {
        $item = Auth::user();

        //Prepare image field for hidden input (สำหรับเก็บลงฐานข้อมูล)
        if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        }

        //Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            $item->thumb = Image::resize($item->image, 100, 100);
        } else {
            $item->thumb = Image::resize('no_image.png', 100, 100);
        }
        
        $this->data_common += [
            'item' => $item,
            'breadcrumb' => [
                trans('backoffice/common.text_profile') => ['url' => '', 'active' => 'active'],
            ],
        ];

        return $this->view('backoffice.profile.edit');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
        ], [
            'name.required' => 'กรุณาใส่ชื่อ',
            'email.required' => 'กรุณาใส่อีเมล์',
            'email.email' => 'กรุณาใส่อีเมล์ให้ถูกต้อง',
        ]);

        $user = User::findOrFail(Auth::user()->id);
        $user->name = $request->name;
        $user->image = $request->image;


        if ($request->password) {
            $this->validate($request, [
                'password' => 'required|min:6|confirmed',
            ], [
                'password.required' => 'กรุณาใส่หัสผ่าน',
                'password.min' => 'กรุณาใส่รหัสผ่านขั้นต่ำ 6 ตัวอักษร',
                'password.confirmed' => 'ยืนยันรหัสผ่านไม่ตรงกัน',
            ]);
            $user->password = bcrypt($request->password);
        }

        if ($request->email != $user->email) {
            $this->validate($request, [
                'email' => 'email|max:255|unique:users,email,' . $user->id,
            ],[
                'email.unique' => 'มีผู้ใช้อีเมล์นี้แล้ว กรุณาเปลี่ยนอีเมล์ใหม่',
            ]);
            $user->email = $request->email;
        }

        $user->save();
        
        return redirect(Config::get('url.backoffice.profile'))
            ->with('success', trans('backoffice/common.text_save_successful'));
    }
}
