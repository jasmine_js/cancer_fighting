<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use Config;

class HotlineController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }

    public function index()
    {
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/footer.text_direct_chancellor') => ['', 'active' => 'active'],
            ],
        ];
        return $this->view('main.hotline.index');
    }

}
