<?php
/**
 * Nov 19, 2018, 11:52 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App\Http\Controllers\Main;

use App;
use App\Language;
use Illuminate\Http\Request;

use Config;

/**
 * ตัวควบคุมการเปลี่ยนภาษา
 */
class LanguageController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }

    public function index(Request $request)
    {


        $code = $request->code; // code คือค่าภาษาที่เลือก code=en,th,jp

        if(isset($request->page)) // check การส่งค่าภาษามาเพื่อใช้ทำลิงก์จากเว็บ inter มายังหน้า exchange program ในภาษาอังกฤษ
            {
                $target_page = $request->page; // page คือค่าหน้าที่เราส่งมา ในที่นีใช้แค่ ex แทน exchange program
            }
        else
            {
                $target_page = ""; // ถ้าไม่ได้รับค่า page มา  page = ""
            }

        if ($code && in_array($code, Language::enable()->pluck('code')->toArray())) {
            $this->language = Language::where('code', $code)->enable()->first();
            App::setLocale($code);
            setcookie($this->language_key, $code, time() + $this->cookie_expired);
        }

        if($target_page == "ex"){   // ถ้าค่า page ที่ส่งมา page = ex คือให้ไปหน้า exchange_programs    // url ที่ส่งมาคือ url/language?code=en&page=ex
            return redirect(Config::get('url.main.exchange_programs'));
        }
        else{
            return redirect()->back();  // ถ้าไม่มีค่า page จะแสดงผลไปหน้า home ปกติ 
        }
      
       

    }
}
