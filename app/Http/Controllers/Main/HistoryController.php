<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Banner;
use App\Image;
use Config;
//use App\Http\Controllers\Controller;

class HistoryController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }
    public function index()
    {    // home hero banner mockup
        $this->data_common += $this->load_banner_image();
        $this->data_common += $this->load_thumb_image();
        $this->data_common += $this->load_latest_video();
        $this->data_common += $this->load_illustration_image();
        
        $this->data_common += [
            //'item'       => $item ,
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/history.text_intro') => ['', 'active' => 'active'],
            ],
        ];
       
        //dd( $this->data_common);
        return $this->view('main.history');
    }

    private function load_home_hero_banner()
    {
        //prepare mockup data // เปลี่ยนจากการดึงข้อมูลจริงจาก backoffice ให้มาแก้ที่นี่
       /*  $home_hero_banner = (object) [
            (object) ['image' => url('img/mockup/mockup-4.png'), 'background_color' => '#d81638', 'link' => 'http://www.tni.ac.th/tni2016/main/index.php?option=contents&category=49&id=30', 'title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'],
            (object) ['image' => url('img/mockup/mockup-5.png'), 'background_color' => '#4b73a7', 'link' => '', 'title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'],
            (object) ['image' => url('img/mockup/mockup-1.png'), 'background_color' => '#002a52', 'link' => '', 'title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'],
            (object) ['image' => url('img/mockup/mockup-2.png'), 'background_color' => '#ff5138', 'link' => '', 'title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'],
            (object) ['image' => url('img/mockup/mockup-3.png'), 'background_color' => '#28416a', 'link' => '', 'title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'],
        ]; */

        $home_hero_banner = Banner::publishWeb()->paginate(20);
        // dd($home_hero_banner);

        // Map image and background_color fields
        $home_hero_banner->setCollection(
            $home_hero_banner->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 1140, 450); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                    } else {
                        $item->image = Image::resize('no_image.png', 1140, 450);
                    }

                    $item->background_color = '#' . $item->background_color;

                    return $item;
                })
        );
        // dd($home_hero_banner->toArray());

        return ['home_hero_banner' => $home_hero_banner];
    }

    private function load_latest_video()
    {
        /**
         * prepare mockup data. เมื่อหลังบ้านเสร็จแล้วให้แก้โปรแกรมตรงนี้ เปลี่ยนจากข้อมูลตัวอย่าง เป็นดึงจาก db แทน
         */
        $data = [
            'id' => 1,
            'name' => 'Lorem Ipsum is simply dummy text of the printing.',
            'image' => url('img/mockup/tni-channel.jpg'), //ใช้อัตราส่วน 16:9
        ];

        return ['latest_video' => (object) $data];
    }

    private function load_thumb_image()
    {
        //_รูปใช้อัตราส่วน 16:9
        /* $data = (object)[
            (object)['id' => 1, 'image' => url('img/history/img1.jpg')], 
            (object)['id' => 2, 'image' => url('img/history/img2.jpg')], 
        ];
        return ['thumb_image' => (object) $data] */;

        $data = [
            ['id' => 1, 'image' => url('img/history/img1.jpg')], 
            ['id' => 2, 'image' => url('img/history/img2.jpg')], 
        ];
        return ['thumb_image' => $data];
        
        
    }

    private function load_banner_image()
    {   
        //_#cbcbcb = grey
        $data = (object)[
            (object) ['image' => url('img/history/banner1.png'), 'background_color' => '#cbcbcb', 'link' => '', 'title' => ''],
            (object) ['image' => url('img/history/banner4.jpg'), 'background_color' => '#cbcbcb', 'link' => '', 'title' => ''],
            (object) ['image' => url('img/history/banner3.png'), 'background_color' => '#cbcbcb', 'link' => '', 'title' => ''],
        ];

        return ['banner_image' => (object) $data];
    }

    
    public function bord()
    {       
        $this->data_common += $this->load_board_image();
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/history.text_board_and_executives') => ['', 'active' => 'active'],
            ],
        ];
    
        return $this->view('main.board');

    } 

    private function load_board_image()
    {
        
        $data = [                  

            ['id' => 1, 'image' => url('img/board/b1.jpg'), 
                'name' => trans('main/history.text_name1'),
                'position'=>trans('main/history.text_position1'),        
                'graduate' =>trans('main/history.text_graduate1'),     
            ], 

            ['id' => 2, 'image' => url('img/board/b2.jpg'), 
                'name' => trans('main/history.text_name2'),
                'position'=>trans('main/history.text_position2'),        
                'graduate' =>trans('main/history.text_graduate2'),     
            ], 

            ['id' => 3, 'image' => url('img/board/b3.jpg'),
                'name' => trans('main/history.text_name3'),
                'position'=>trans('main/history.text_position3'),        
                'graduate' =>trans('main/history.text_graduate3'),     
            ],

            ['id' => 4, 'image' => url('img/board/b4.jpg'),
            'name' => trans('main/history.text_name4'),
            'position'=>trans('main/history.text_position4'),        
            'graduate' =>trans('main/history.text_graduate4'),     
            ],


            // ['id' => 4, 'image' => url('img/board/b4.jpg'),
            //     'name' => trans('main/history.text_name4'),
            //     'position'=>trans('main/history.text_position4'),        
            //     'graduate' =>trans('main/history.text_graduate4'),     
            // ], 

            ['id' => 5, 'image' => url('img/board/b5.jpg'),
                'name' => trans('main/history.text_name5'),
                'position'=>trans('main/history.text_position5'),        
                'graduate' =>trans('main/history.text_graduate5'),     
            ], 

            // ['id' => 5, 'image' => url('img/board/b5.jpg'),
            //     'name' => trans('main/history.text_name5'),
            //     'position'=>trans('main/history.text_position5'),        
            //     'graduate' =>trans('main/history.text_graduate5'),     
            // ],

            ['id' => 6, 'image' => url('img/board/b6.jpg'),
                'name' => trans('main/history.text_name6'),
                'position'=>trans('main/history.text_position6'),        
                'graduate' =>trans('main/history.text_graduate6'),     
            ],

            ['id' => 7, 'image' => url('img/board/b7.jpg'),
                'name' => trans('main/history.text_name7'),
                'position'=>trans('main/history.text_position7'),        
                'graduate' =>trans('main/history.text_graduate7'),     
            ],

            ['id' => 8, 'image' => url('img/board/b8.jpg'),
                'name' => trans('main/history.text_name8'),
                'position'=>trans('main/history.text_position8'),        
                'graduate' =>trans('main/history.text_graduate8'),     
            ], 

            ['id' => 9, 'image' => url('img/board/b9.jpg'),
                'name' => trans('main/history.text_name9'),
                'position'=>trans('main/history.text_position9'),        
                'graduate' =>trans('main/history.text_graduate9'),     
            ],

            ['id' => 10, 'image' => url('img/board/b10.jpg'),
                'name' => trans('main/history.text_name10'),
                'position'=>trans('main/history.text_position10'),        
                'graduate' =>trans('main/history.text_graduate10'),     
            ],

            ['id' => 11, 'image' => url('img/board/b11.jpg'),
                'name' => trans('main/history.text_name11'),
                'position'=>trans('main/history.text_position11'),        
                'graduate' =>trans('main/history.text_graduate11'),     
            ], 

            ['id' => 12, 'image' => url('img/board/b12.jpg'),
                'name' => trans('main/history.text_name12'),
                'position'=>trans('main/history.text_position12'),        
                'graduate' =>trans('main/history.text_graduate12'),     
            ], 

            ['id' => 13, 'image' => url('img/board/b13.jpg'),
                'name' => trans('main/history.text_name13'),
                'position'=>trans('main/history.text_position13'),        
                'graduate' =>trans('main/history.text_graduate13'),     
            ], 

            ['id' => 14, 'image' => url('img/board/b14.jpg'),
                'name' => trans('main/history.text_name14'),
                'position'=>trans('main/history.text_position14'),        
                'graduate' =>trans('main/history.text_graduate14'),     
            ], 

            ['id' => 15, 'image' => url('img/board/b15.jpg'),
                'name' => trans('main/history.text_name15'),
                'position'=>trans('main/history.text_position15'),        
                'graduate' =>trans('main/history.text_graduate15'),     
            ], 

            // ['id' => 16, 'image' => url('img/board/b16.jpg'),
            //     'name' => trans('main/history.text_name16'),
            //     'position'=>trans('main/history.text_position16'),
            //     'graduate' =>trans('main/history.text_graduate16'),     
            // ], 

        ];
        return ['board_image' => $data];
      
    }

    public function executive()
    {       
        $this->data_common += $this->load_executive_image();
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/history.text_board_and_executives') => ['', 'active' => 'active'],
            ],
        ];
    
        return $this->view('main.executive');

    } 

    private function load_executive_image()
    {
        
        $data = [                  

            ['id' => 1, 'image' => url('img/executive/ex1.jpg'), 
                'name' =>  trans('main/history.text_ename1'),
                'position'=>trans('main/history.text_eposition1'),        
                'graduate' =>trans('main/history.text_egraduate1'),     
            ], 

            ['id' => 2, 'image' => url('img/executive/ex2.jpg'), 
                'name' => trans('main/history.text_ename2'),
                'position'=>trans('main/history.text_eposition2'),        
                'graduate' =>trans('main/history.text_egraduate2'),     
            ], 
        
            ['id' => 3, 'image' => url('img/executive/ex6.jpg'),
                'name' => trans('main/history.text_ename3'),
                'position'=>trans('main/history.text_eposition3'),        
                'graduate' =>trans('main/history.text_egraduate3'),     
            ],

            ['id' => 4, 'image' => url('img/executive/ex5.jpg'),
                'name' => trans('main/history.text_ename4'),
                'position'=>trans('main/history.text_eposition4'),        
                'graduate' =>trans('main/history.text_egraduate4'),     
            ], 

            ['id' => 5, 'image' => url('img/executive/ex3.jpg'),
                'name' => trans('main/history.text_ename5'),
                'position'=>trans('main/history.text_eposition5'),        
                'graduate' =>trans('main/history.text_egraduate5'),     
            ],

            ['id' => 6, 'image' => url('img/executive/ex13.jpg'),
                'name' => trans('main/history.text_ename6'),
                'position'=>trans('main/history.text_eposition6'),        
                'graduate' =>trans('main/history.text_egraduate6'),     
            ],
            
            ['id' => 7, 'image' => url('img/executive/ex4.jpg'),
                'name' => trans('main/history.text_ename7'),
                'position'=>trans('main/history.text_eposition7'),        
                'graduate' =>trans('main/history.text_egraduate7'),     
            ], 

            ['id' => 8, 'image' => url('img/executive/ex7.jpg'),
                'name' => trans('main/history.text_ename8'),
                'position'=>trans('main/history.text_eposition8'),        
                'graduate' =>trans('main/history.text_egraduate8'),     
            ],

            ['id' => 9, 'image' => url('img/executive/ex8.jpg'),
                'name' => trans('main/history.text_ename9'),
                'position'=>trans('main/history.text_eposition9'),        
                'graduate' =>trans('main/history.text_egraduate9'),     
            ],

            ['id' => 10, 'image' => url('img/executive/ex9.jpg'),
                'name' => trans('main/history.text_ename10'),
                'position'=>trans('main/history.text_eposition10'),        
                'graduate' =>trans('main/history.text_egraduate10'),     
            ], 

            ['id' => 11, 'image' => url('img/executive/ex10.png'),
                'name' => trans('main/history.text_ename11'),
                'position'=>trans('main/history.text_eposition11'),        
                'graduate' =>trans('main/history.text_egraduate11'),     
            ],

            ['id' => 12, 'image' => url('img/executive/ex11.jpg'),
                'name' => trans('main/history.text_ename12'),
                'position'=>trans('main/history.text_eposition12'),        
                'graduate' =>trans('main/history.text_egraduate12'),     
            ],

            // ['id' => 13, 'image' => url('img/executive/ex11.jpg'),
            // 'name' => trans('main/history.text_ename12'),
            // 'position'=>trans('main/history.text_eposition12'),        
            // 'graduate' =>trans('main/history.text_egraduate12'),     
            // ],

            ['id' => 13, 'image' => url('img/executive/ex12.png'),
                'name' => trans('main/history.text_ename13'),
                'position'=>trans('main/history.text_eposition13'),        
                'graduate' =>trans('main/history.text_egraduate13'),     
            ], 
           

            
        ];
        return ['executive_image' => $data];
      
    }

    public function teacher()
    {       
      
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/history.text_teacher') => ['', 'active' => 'active'],
            ],
        ];
    
        return $this->view('main.teacher');

    } 


    public function facilities()
    {       
      
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/history.text_facilities') => ['', 'active' => 'active'],
            ],
        ];
    
        return $this->view('main.facilities');

    } 

    public function ba_classroom()
    {       
      
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/history.text_facilities') => ['url' => Config::get('url.main.facilities'), 'active' => ''],
                trans('main/history.text_ba_classroom') => ['', 'active' => 'active'],
            ],
        ];
    
        return $this->view('main.ba_classroom');

    } 

    public function cgel_classroom()
    {       
      
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/history.text_facilities') => ['url' => Config::get('url.main.facilities'), 'active' => ''],
                trans('main/history.text_cgel_classroom') => ['', 'active' => 'active'],
            ],
        ];
    
        return $this->view('main.cgel_classroom');

    } 


    private function load_illustration_image()
    {
        $item = [
            ['id'=> 1 , 'image'=> url('img/history/logo.gif') ] //_tni logo
            ,['id'=> 2 , 'image'=> url('img/history/principal.jpg') ]//_พระประธานประจำสถาบัน 
            ,['id'=> 3 , 'image'=> url('img/history/tree.jpg') ]//_ต้นไม้ประจำ
            ,['id'=> 4 , 'image'=> url('img/history/tree_2.jpg') ]//_ต้นไม้ประจำ2
            ,['id'=> 5 , 'image'=> url('img/history/founder.jpg') ]//_อนุสาวรีย์ผู้ก่อตั้ง
        ];
        return ['illustration_image' => $item];
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
