<?php

namespace App\Http\Controllers\Main;

use App\Video;
use App\VideoDescription;
use Config;
use App\Image;
use App\Language;
use Illuminate\Http\Request;

class VideoController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }

    public function index()
    {
        $this->data_common += $this->load_video();

        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/home.text_tni_channel') => ['', 'active' => 'active'],
            ],
        ];
        
        return $this->view('main.video.index');
    }    

    private function load_video()
    {
        $videos =VideoDescription::with('video')
                ->language($this->language->id)
                ->publishWeb()
                ->paginate(12); ///แสดงหน้าละ 12
        
            $videos->setCollection(
                $videos->getCollection()
                        ->map(function ($item, $key) {
    
                        if ($item->image) {
                            $item->image = Image::resize($item->image, 235, 165); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                        } else {
                            $item->image = Image::resize('no_image.png', 235, 165);
                        }
    
                        return $item;
                    })
            );         
    
            
        return [
            'videos' => $videos,
        ];
    }

    public function detail($id , Request $request)
    {
        
        //_increst field:Viewd number when click to see news detail
        // CoOperative::findOrFail($id)->increment('viewed');
           //CoOperative::findOrFail($id)->increment('viewed');
        $item_viewed = Video::findOrFail($id);
        $item_viewed->timestamps = false; //_disable update field:updated_at
        $item_viewed->increment('viewed');
        unset($item_viewed); //_clear memory

        $item = Video::with('videoDescriptions') //function in event model 
                    ->findOrFail($id);   
       
        if(is_null($item->image)){
            $item->image = '';
        }

        //_Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            $item->thumb = Image::resize($item->image, 235, 165);
        } else {
            //$item->thumb = Image::resize('no_image.png', 235, 165);
            $item->thumb = '';
        }
        //dd($item);

        $description = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        foreach ($item->videoDescriptions as $dest) {
            $description[$dest->language_id] = $dest;
        }
        $item->description = $description;

        $this->data_common += [
            'item' => $item,
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/home.text_tni_channel') => ['url' => Config::get('url.main.tni_channel'), 'active' => ''],
                $item->description[$this->language->id]->name => ['', 'active' => 'active'],
            ],
        ];
        
       
        return $this->view('main.video.detail');
    }
}