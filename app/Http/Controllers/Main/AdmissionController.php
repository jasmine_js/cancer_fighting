<?php

namespace App\Http\Controllers\Main;

use App\AdmissionDescription;
use App\AdmissionImage;
use App\Admission;
use App\Image;
use App\AdmissionToCategory;

use App\AdmissionCategory;
use App\AdmissionCategoryDescription;

use Config;
use App\Language;
use Illuminate\Http\Request;


class AdmissionController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }

    public function index()
    {
        /* $admission_cat = AdmissionCategoryDescription::with('admissionCategory')
                            ->language($this->language->id)
                            ->get();
                               
        $this->data_common += ['admission_cat' => $admission_cat];//_for show in tab */

       // $this->data_common += ['admissions_bycat_1' => $this->load_admissions_by_category(1)];//_รับสมัคร
        $this->data_common += ['admissions_bycat_1' => $this->load_admissions_by_category(1)];//_ผลงาน นศ
        $this->data_common += ['admissions_bycat_2' => $this->load_admissions_by_category(2)];//_สหกิจ
        $this->data_common += ['admissions_bycat_7' => $this->load_admissions_by_category(7)];//_สหกิจ
      

        $this->data_common += $this->load_admissions();//_all news
      

        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/admission.text_intro_admission') => ['', 'active' => 'active'],
            ],
        ];
      
        
        return $this->view('main.admissions.index');
    }

    //_ดึงข่าวมาแสดงแยกตามประเภท
    private function load_admissions_by_category($admission_cat_id)
    {
        $admissions = AdmissionDescription::with('admission')
                    ->language($this->language->id)
                    ->searchCategory($admission_cat_id)
                    ->publishWeb()
                  
                    ->paginate(12 ,['*'] ,'page'.$admission_cat_id) //_ทำให้ paginate เป็นอิสระจากแต่ละ tab 
                    ->appends( ['category'=>$admission_cat_id] ); //_send category=x like this : news?catgory=4&page4=2
                    //->paginate(4 , [*] ,'pages');

        $admissions->setCollection(
                $admissions->getCollection()
                    ->map(function ($item, $key) {
    
                        if ($item->image) {
                            //_สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache 
                            //__ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                            $item->image = Image::resize($item->image, 235, 165); 
                        } else {
                            $item->image = Image::resize('no_image.png', 235, 165);
                        }
    
                        return $item;
                    })
            );
    
            // ดึงรูปเพิ่มเติมสำหรับข่าวลำดับที่ 1
            /* $first_admission_images = AdmissionImage::where('admission_id', @$admissions[0]->admission_id)->order()->paginate(100);
        
            if (count($first_admission_images) > 0) {
                // Map image fields
                $first_admission_images->setCollection(
                    $first_admission_images->getCollection()
                        ->map(function ($item, $key) {
    
                            if ($item->image) {
                                $item->image = Image::resize($item->image, 975, 475); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                            } else {
                                $item->image = Image::resize('no_image.png', 975, 475);
                            }
    
                            return $item;
                        })
                );
            } */
           
            return [
                'admissions' => $admissions,
                /* 'first_admission_images' => $first_admission_images,
                'first_admission_images_blank' => Image::resize('no_image.png', 975, 475), */
            ];    
    }

    private function load_admissions()
    {
        $admissions = AdmissionDescription::with('admission')
            ->language($this->language->id)
            ->publishWeb()
            ->paginate(12); ///แสดงหน้าละ 12
        
        $catagory = AdmissionToCategory::All();

            // $find = $request->get('find');
            // if ($find) {
            //     $admissions = $admissions->searchKeyword($find);
            // }    
           
            // $admissions = $admissions->order()->paginate($this->per_page);
        
        $admissions->setCollection(
            $admissions->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 235, 165); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                    } else {
                        $item->image = Image::resize('no_image.png', 235, 165);
                    }

                    return $item;
                })
        );

        // ดึงรูปเพิ่มเติมสำหรับข่าวลำดับที่ 1
        $first_admission_images = AdmissionImage::where('admission_id', @$admissions[0]->admission_id)->order()->paginate(100);
        // dd($first_admission_images);
        if (count($first_admission_images) > 0) {
            // Map image fields
            $first_admission_images->setCollection(
                $first_admission_images->getCollection()
                    ->map(function ($item, $key) {

                        if ($item->image) {
                            $item->image = Image::resize($item->image, 975, 475); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                        } else {
                            $item->image = Image::resize('no_image.png', 975, 475);
                        }

                        return $item;
                    })
            );
        }
       
        return [
           
            'admissions' => $admissions,
            'catagory' =>$catagory,
            'first_admission_images' => $first_admission_images,
            'first_admission_images_blank' => Image::resize('no_image.png', 975, 475),
          
        ];
    }




    




    //_ดึงข่าวที่เกียวข้อง
    private function load_related_admissions($admission_id)
    {
        //_get admission_category_id ของข่าวที่กำลังดูอยู่
        $item_cat = AdmissionToCategory::where('admission_id',$admission_id)
                    ->take(1) //_limit 1
                    ->get();

        //_if has Category ID
        if(count($item_cat) > 0)
        {
            //_ดึงข่าวหมวดเดี่ยวกันขึ้นมาแสดง
            $admissions = AdmissionDescription::with('admission')
                    ->language($this->language->id)
                    ->publishWeb()
                    ->searchRelatedNews($item_cat[0]->admission_category_id , $admission_id)
                    ->paginate(4);
        }else{
            //_ไม่ระบุหมวดให้ข่าวนั้นข่าวโน้นข่าวนี้
            /* $admissions = Admission::with('admissionDescriptions') //function in event model                    
                    ->articlaName($this->language->id)
                    ->findOrFail($admission_id)
                    ->paginate(4); */
            $admissions = array();
        }
        
        //dd($admissions);           
        /* $admissions = AdmissionDescription::with('admission')
                    ->language($this->language->id)
                    ->publishWeb()
                    ->searchRelatedNews($item_cat[0]->admission_category_id , $admission_id)
                    ->paginate(4); */
        if(count($admissions) > 0 ){
            $admissions->setCollection(
                $admissions->getCollection()
                    ->map(function ($item, $key) {

                        if ($item->image) {
                            //_สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache 
                            //__ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                            $item->image = Image::resize($item->image, 235, 165); 
                        } else {
                            $item->image = Image::resize('no_image.png', 235, 165);
                        }

                        return $item;
                    })
            );
        }

        return [
            'admissions_related'=>$admissions
        ];
        
        //dd($admission_related);
    } 

    public function detail($id , Request $request)
    {   
        //_increst field:Viewed number when click to see detail
        //Admission::findOrFail($id)->increment('viewed');
        $item_viewed = Admission::findOrFail($id);
        $item_viewed->timestamps = false; //_disable update field:updated_at
        $item_viewed->increment('viewed');
        unset($item_viewed); //_clear memory
        
        //_ดึงข่าวที่มียอดวิวสูงสุด 5 อันดับ 
        $populars = AdmissionDescription::with('admission') 
        ->language($this->language->id)
        ->popular()     // มาจาก model admissionsDescription
        ->paginate(5);
        
        $populars->setCollection(
            $populars->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 235, 165); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                    } else {
                        $item->image = Image::resize('no_image.png', 235, 165);
                    }

                    return $item;
                })
        );

        //_ดึงข่าวที่ update ล่าสุด 5 อันดับ
        $lastest_posts = AdmissionDescription::with('admission') 
                        ->language($this->language->id)
                        ->publishWeb()     // มาจาก model admissionsDescription
                        ->paginate(5);
     
        $lastest_posts->setCollection(
            $lastest_posts->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 235, 165); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                    } else {
                        $item->image = Image::resize('no_image.png', 235, 165);
                    }
                    return $item;
                })
        );

        //_ดึงข่าวที่คลิกเข้ามาดู
        $item = Admission::with('admissionDescriptions') //function in event model 
                    ->findOrFail($id);
                    
        if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        }
        
         //_Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            //$item->thumb = Image::resize($item->image, 235, 165);
            $item->thumb = Image::resize($item->image, 975, 685); 
        } else {
            //$item->thumb = Image::resize('no_image.png', 235, 165);
            $item->thumb = "";
        }

        $description = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        foreach ($item->admissionDescriptions as $dest) {
            $description[$dest->language_id] = $dest;
        }
        $item->description = $description;
        
        //_get news illustration images
        $admissionImgae = AdmissionImage::where('admission_id',$id)->order()->get();
        
        if(count($admissionImgae) > 0){
            foreach($admissionImgae as $item_admissionImage){
                //$item_admissionImage->image = Image::resize($item_admissionImage->image, 975, 475); 
                $item_admissionImage->image = Image::resize($item_admissionImage->image, 975, 685); 
            }
        }

        //dd($admissionImgae);

        //_ข่าวที่เกี่ยวข้อง
        $this->data_common += $this->load_related_admissions($id);

        $this->data_common += [

            'populars' => $populars,
            'lastest_posts' =>$lastest_posts,
            'item' => $item,
            'admission_image' => $admissionImgae,
            'admission_images_blank' => Image::resize('no_image.png', 235, 165),

            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/admission.text_intro_admission') => ['url' => Config::get('url.main.admissions'), 'active' => ''],
                $item->description[$this->language->id]->name => ['', 'active' => 'active'],
            ],
        ];


        return $this->view('main.admissions.detail');

    }



    public function tag()
    {   
        $tags = $_REQUEST["tag"];


        $admissions = AdmissionDescription::where('name','LIKE','%'.$tags.'%') // เลือกที่ ชื่อข่าวมีคำว่า  $tags ตามที่ส่งมา 
       
            ->orWhere('tag','LIKE','%'.$tags.'%')
            ->language($this->language->id)
            ->publishWeb()
            ->paginate(10,['*'],'page')///แสดงหน้าละ 10
            ->appends( ['tag'=>$tags] ); // ส่งตัวแปร เหมือน tag=วิจัย&page=1 | page = 1 มาจาก 3,['*'],'page' ที่ page
            // tag=วิจัย
        $catagory = AdmissionToCategory::All();

       
        $this->data_common += [

            // 'items'=>$items,
            'admissions' => $admissions,
            'catagory' =>$catagory,
            'tags'=>$tags,
        

            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/allnews.text_news_activity') => ['url' => Config::get('url.main.admissions'), 'active' => ''],
                trans('main/allnews.text_tags') => ['', 'active' => 'active'],
            ],
        ];


        return $this->view('main.admissions.search_tag');

    }

    public function scholarship(){
    
        $this->data_common += ['admissions_bycat_3' => $this->load_admissions_by_category(3)];//_ทุนการศึกษา
      

        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/admission.text_intro_admission') => ['url' => Config::get('url.main.admissions'), 'active' => ''],
                trans('main/admission.text_scholarship') => ['', 'active' => 'active'],
            ],
        ];
      
    
        return $this->view('main.admissions.scholarship');

    }

    public function fee(){
    
        $this->data_common += ['admissions_bycat_4' => $this->load_admissions_by_category(4)];//_ทุนการศึกษา
      

        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/admission.text_intro_admission') => ['url' => Config::get('url.main.admissions'), 'active' => ''],
                trans('main/admission.text_fee') => ['', 'active' => 'active'],
            ],
        ];
      
    
        return $this->view('main.admissions.fee');

    }
    public function privilege(){
    
        $this->data_common += ['admissions_bycat_5' => $this->load_admissions_by_category(5)];//_ทุนการศึกษา
        
     

        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/admission.text_intro_admission') => ['url' => Config::get('url.main.admissions'), 'active' => ''],
                
                trans('main/admission.text_privilege') => ['', 'active' => 'active'],
            ],
        ];
      
    
        return $this->view('main.admissions.privilege');

    }

    public function confirmation(){
    
        $this->data_common += ['admissions_bycat_6' => $this->load_admissions_by_category(6)];//_ทุนการศึกษา
        
     

        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/admission.text_intro_admission') => ['url' => Config::get('url.main.admissions'), 'active' => ''],
                
                trans('main/admission.text_regist_stdnew') => ['', 'active' => 'active'],
            ],
        ];
      
    
        return $this->view('main.admissions.enrollment_confirmation');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
