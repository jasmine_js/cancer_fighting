<?php
/**
 * Nov 20, 2018, 9:14 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App\Http\Controllers\Main;

use App\ArticleDescription;
use App\EventDescription;
use App\EbochureDescription;
use App\VideoDescription;
use App\ArticleImage;
use App\Image;
use App\Banner;
use App\BannerHighlight;
use App\Language;
use Config;
use Hamcrest\Core\IsNull;

class HomeController extends MainController
{

    public function __construct()
    {
        MainController::__construct();
    }

    public function index()
    {
        // home hero banner mockup
        $this->data_common += $this->load_home_hero_banner();

        // home banner highlight
        $this->data_common += $this->load_banner_highlight();

        // ดึงข่าวล่าสุด 5 อันดับแรก และดึงรูปเพิ่มเติมสำหรับข่าวลำดับที่ 1
        $this->data_common += $this->load_articles();

        // load event udpate
        // $this->data_common += $this->load_event_update();

        //Load middle section navigator
        $this->data_common += $this->load_nav_pec();

        // Load latest first video
        $this->data_common += $this->load_latest_video();

        // Load latest first e-bochure
        $this->data_common += $this->load_latest_bochure();

        $this->data_common += $this->load_events_by_category(6);//event_update

        // $this->data_common += $this->tnipresentation()();

        $this->data_common += $this->load_banner_image();
      

        return $this->view('main.home');
    }

    private function load_home_hero_banner()
    {
        //prepare mockup data // เปลี่ยนจากการดึงข้อมูลจริงจาก backoffice ให้มาแก้ที่นี่
       /*  $home_hero_banner = (object) [
            (object) ['image' => url('img/mockup/mockup-4.png'), 'background_color' => '#d81638', 'link' => 'http://www.tni.ac.th/tni2016/main/index.php?option=contents&category=49&id=30', 'title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'],
            (object) ['image' => url('img/mockup/mockup-5.png'), 'background_color' => '#4b73a7', 'link' => '', 'title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'],
            (object) ['image' => url('img/mockup/mockup-1.png'), 'background_color' => '#002a52', 'link' => '', 'title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'],
            (object) ['image' => url('img/mockup/mockup-2.png'), 'background_color' => '#ff5138', 'link' => '', 'title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'],
            (object) ['image' => url('img/mockup/mockup-3.png'), 'background_color' => '#28416a', 'link' => '', 'title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'],
        ]; */

        $home_hero_banner = Banner::publishWeb()->paginate(20);
        // dd($home_hero_banner);

        // Map image and background_color fields
        $home_hero_banner->setCollection(
            $home_hero_banner->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 1140, 450); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                    } else {
                        $item->image = Image::resize('no_image.png', 1140, 450);
                    }

                    $item->background_color = '#' . $item->background_color;

                    return $item;
                })
        );
        // dd($home_hero_banner->toArray());

        return ['home_hero_banner' => $home_hero_banner];
    }

    private function load_banner_highlight()
    {
    
        $banner_highlight = BannerHighlight::publishWeb()->paginate(20);
        // dd($home_hero_banner);

        // Map image and background_color fields
        $banner_highlight->setCollection(
            $banner_highlight->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 975, 475); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                    } else {
                        $item->image = Image::resize('no_image.png', 975, 475);
                    }

                    // $item->background_color = '#' . $item->background_color;

                    return $item;
                })
        );
        // dd($home_hero_banner->toArray());

        return [
            'banner_highlight' => $banner_highlight,
            'first_article_images_blank' => Image::resize('no_image.png', 975, 475),
        ];
    }




    private function load_articles()
    {
        // ดึงข่าวล่าสุด 5 อันดับแรก
        $articles = ArticleDescription::with('article')
            ->language($this->language->id)
            ->publishWeb()
            ->paginate(8);
       
        // Map image fields
        $articles->setCollection(
            $articles->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 235, 165); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                    } else {
                        $item->image = Image::resize('no_image.png', 235, 165);
                    }

                    return $item;
                })
        );

        // ดึงรูปเพิ่มเติมสำหรับข่าวลำดับที่ 1
        $first_article_images = ArticleImage::where('article_id', @$articles[0]->article_id)->order()->paginate(100);
        // dd($first_article_images);
        if (count($first_article_images) > 0) {
            // Map image fields
            $first_article_images->setCollection(
                $first_article_images->getCollection()
                    ->map(function ($item, $key) {

                        if ($item->image) {
                            $item->image = Image::resize($item->image, 975, 475); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                        } else {
                            $item->image = Image::resize('no_image.png', 975, 475);
                        }

                        return $item;
                    })
            );
        }

        return [
            'articles' => $articles,
            'first_article_images' => $first_article_images,
            'first_article_images_blank' => Image::resize('no_image.png', 975, 475),
          
        ];
    }

    private function load_events_by_category($articel_cat_id)
    {
        $rawdata = ArticleDescription::with('article')
                    ->language($this->language->id)
                    ->searchCategory($articel_cat_id)
                    ->publishEvent()
                    ->paginate(4);//_ดึง 4 ข่าวล่่าสุด
       
   
        if(count($rawdata)){
            foreach($rawdata as $item)
            {
                $data[] = (object) [
                    'id' => $item->id,
                    'name' => $item->name,
                    'day' => date('d',strtotime($item->publish_stop) ),
                    'month' => date('M Y',strtotime($item->publish_stop) ),
                    'link' => $item->link,
                ];
            // }
            }
        }else{ 

            $data = array();
        }
       
        return['event_update'=>$data];
        
        /* $data = [];
        //_prepare mockup data // เมื่อ backoffice เสร็จให้แก้ดึงข้อมูลจริงที่นี่
         for ($i = 1; $i <= 4; $i++) {
            $data[] = (object) [
                'id' => $i,
                'name' => 'Lorem Ipsum is simply dummy text of the printing.',
                'day' => 10 + $i,
                'month' => 'Dec 2018',
            ];
        } 
        return ['event_update' => (object) $data]; */    
    }



    /**
     * Load navigation section for student portfolio(P), exchange programe(E), co-operative(C)
     */
    private function load_nav_pec()
    {
        $data = [];

        $data[] = (object) [
            'name' => trans('main/home.text_student_portfolio'),
            'image' => url('img/pec2.jpg'),
            'link' => url(Config::get('url.main.student_portfolios')),
        ];

        $data[] = (object) [
            'name' => trans('main/home.text_exchange_program'),
            'image' => url('img/pec1.jpg'),
            'link' => url(Config::get('url.main.exchange_programs')),
        ];

        $data[] = (object) [
            'name' => trans('main/home.text_co_operative'),
            'image' => url('img/pec3.jpg'),
            'link' => url(Config::get('url.main.cooperatives')),
        ];

        return ['nav_pec' => (object) $data];
    }

    private function load_latest_video()
    {
        /**
         * prepare mockup data. เมื่อหลังบ้านเสร็จแล้วให้แก้โปรแกรมตรงนี้ เปลี่ยนจากข้อมูลตัวอย่าง เป็นดึงจาก db แทน
         */
        // $data = [
        //     'id' => 1,
        //     'name' => 'Lorem Ipsum is simply dummy text of the printing.',
        //     'image' => url('img/mockup/tni-channel.jpg'), //ใช้อัตราส่วน 16:9
        // ];

        $rawdata = VideoDescription::with('video')
        ->language($this->language->id)
        ->publishWeb()
        ->paginate(1);//_ดึง 4 ข่าวล่่าสุด

            foreach($rawdata as $item)
            {
                if ($item->image) {
                    $item->image = Image::resize($item->image, 1920, 1080); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                } else {
                    $item->image = Image::resize('no_image.png', 1920, 1080); 
                }

                $data[] = (object) [
                    'id' => '$item->id',
                    'name' => $item->name,
                    'image' => $item->image,
                    'link' => $item->video_link,                          
                ];
            }


        return ['latest_video' => $data];
    }

    private function load_latest_bochure()
    {
        /**
         * prepare mockup data. เมื่อหลังบ้านเสร็จแล้วให้แก้โปรแกรมตรงนี้ เปลี่ยนจากข้อมูลตัวอย่าง เป็นดึงจาก db แทน
         */
                //     $data = [
                //         'id' => 1,
                //         'name' => 'Lorem Ipsum is simply dummy text of the printing.',
                //         'image_highlight' => url('img/mockup/e-bochure.jpg'), //ใช้อัตราส่วน 16:9
                //     ];

                //     return ['latest_bochure' => (object) $data];
                // }

                $rawdata = EbochureDescription::with('ebochure')
                ->language($this->language->id)
                ->publishWeb()
                ->paginate(1);//_ดึง 4 ข่าวล่่าสุด
   
                    foreach($rawdata as $item)
                    {
                        if ($item->image_highlight) {
                            $item->image_highlight = Image::resize($item->image_highlight, 1920, 1080); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                        } else {
                            $item->image_highlight = Image::resize('no_image.png', 1920, 1080); 
                        }

                        $data[] = (object) [
                            'id' => '$item->id',
                            'name' => $item->name,
                            'image' => $item->image_highlight,
                            'link' => $item->link,                          
                        ];
                    }
    
          
            return['ebrochures'=>$data];
    }

    /* 
        Preview text (limit lenght)
	    for Thai lang (include Eng)
    */
    /* private function preview_text($input , $len_limit=0)
    {	
        if(mb_strlen($input ,'utf-8') > ($len_limit+1)){
            $output = mb_substr($input , 0 , $len_limit , 'utf-8').'...';
        }else{
            $output = $input ;
        }
        return $output;
    } */

    public function careers()
    {       
      
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/home.text_career_tni') => ['', 'active' => 'active'],
            ],
        ];
    
        return $this->view('main.careers');

    }

    public function weblink()
    {       
       // $this->data_common += $this->load_weblink();
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/footer.text_web_link') => ['', 'active' => 'active'],
            ],
        ];
    
        return $this->view('main.weblink');

    }

        // private function load_weblink() // โครงสร้างหลักสูตร 
        // {
            
        //     for($i=1; $i<3; $i++)
        //     {
        //         $arr_temp = [
        //             'id' => $i, 
        //             'name' => trans("main/weblink.text_name$i"), 
        //             'link' => trans("main/weblink.text_link$i"),
        //         ];

        //         $data[] = $arr_temp;
        //     }
        //     return ['weblink' => $data];
        
        // }



    public function tnipresentations()
    {       
      
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/home.text_tni_presentation') => ['', 'active' => 'active'],
            ],
        ];
    
        return $this->view('main.tnipresentations');

    }

    public function privacy_policy()
    {       
        $this->data_common += $this->load_privacy_detail();
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/home.text_privacy_policy') => ['', 'active' => 'active'],
            ],
        ];
    
        return $this->view('main.privacy_policy');

    }
    private function load_privacy_detail() // ชื่อปริญญาและสาขาวิชา 
        {
            
            for($i=1; $i<6; $i++)
            {
                $arr_temp = [
                    'id' => $i, 
                    'list' => trans("main/policy.text_list$i"), 
                    'detail' => trans("main/policy.text_list_detail$i"), 
                ];

                $data[] = $arr_temp;
            }
            return ['policy_detail' => $data];
        
        }


    public function cookies()
    {       
        $this->data_common += $this->load_cookies_detail();
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/home.text_cookies_policy') => ['', 'active' => 'active'],
            ],
        ];
    
        return $this->view('main.cookies');

    }

    private function load_cookies_detail() // ชื่อปริญญาและสาขาวิชา 
    {
        
        for($i=1; $i<4; $i++)
        {
            $arr_temp = [
                'id' => $i, 
                'list' => trans("main/policy.text_list_c$i"), 
                'detail' => trans("main/policy.text_list_detail_c$i"), 
            ];

            $data[] = $arr_temp;
        }
        return ['cookies_detail' => $data];
    
    }


    public function ajax_cookies_set(){
        return $this->view('main.layouts.partials.cookies_set');
    }


    private function load_banner_image()
    {   
        //_#cbcbcb = grey
        $data = (object)[
            (object) ['image' => url('img/history/banner1.png'), 'background_color' => '#cbcbcb', 'link' => '', 'title' => ''],
            (object) ['image' => url('img/history/banner4.jpg'), 'background_color' => '#cbcbcb', 'link' => '', 'title' => ''],
            (object) ['image' => url('img/history/banner3.png'), 'background_color' => '#cbcbcb', 'link' => '', 'title' => ''],
        ];

        return ['banner_image' => (object) $data];
    }





}
