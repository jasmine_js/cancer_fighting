<?php

namespace App\Http\Controllers\Main;

use App\Ebochure;
use App\EbochureDescription;
use Config;
use App\Image;
use App\Language;
use Illuminate\Http\Request;

class EbrochureController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }
    public function index()
    {
        $this->data_common += $this->load_ebrochure();

        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/home.text_e_bochure') => ['', 'active' => 'active'],
            ],
        ];
        
        return $this->view('main.ebrochures.index');
    }

    private function load_ebrochure()
    {
        // ดึงข่าวล่าสุด 5 อันดับแรก
     
        $ebrochures =EbochureDescription::with('ebochure')
                ->language($this->language->id)
                ->publishWeb()
                ->paginate(12); ///แสดงหน้าละ 12
        
            $ebrochures->setCollection(
                $ebrochures->getCollection()
                        ->map(function ($item, $key) {
    
                        if ($item->image) {
                            $item->image = Image::resize($item->image, 235, 165); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                        } else {
                            $item->image = Image::resize('no_image.png', 235, 165);
                        }
    
                        return $item;
                    })
            );
    
            
        return [
            'ebrochures' => $ebrochures, 
        ];
        
    }

    // public function detail($id , Request $request)
    // {
        
    //     //_increst field:Viewd number when click to see news detail
    //     Ebochure::findOrFail($id)->increment('viewed');

    //     $item = Ebochure::with('ebochureDescriptions') //function in event model 
    //                 ->findOrFail($id);   

    //   if(is_null($item->image)){
    //         $item->image = '';
    //     }

    //     //_Prepare thumb field for View (สำหรับแสดง)
    //     if ($item->image) {
    //         $item->thumb = Image::resize($item->image, 975, 685); 
    //     } else {
    //         //$item->thumb = Image::resize('no_image.png', 235, 165);
    //         $item->thumb = Image::resize($item->image, 235, 165);
    //     }

    //     $description = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
    //     foreach ($item->ebochureDescriptions as $dest) {
    //         $description[$dest->language_id] = $dest;
    //     }
    //     $item->description = $description;

    //     $this->data_common += [
    //         'item' => $item,
    //         'breadcrumb' => [
    //             trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
    //             trans('main/ebrochure.text_ebrochure') => ['url' => Config::get('url.main.ebrochures'), 'active' => ''],
    //             $item->description[$this->language->id]->name => ['', 'active' => 'active'],
    //         ],
    //     ];
        
       
    //     return $this->view('main.ebrochures.detail');
    // }
}
