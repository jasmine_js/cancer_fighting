<?php

namespace App\Http\Controllers\Main;

use App\ArticleDescription;
use App\ArticleImage;
use App\Article;
use App\Image;
use App\ArticleToCategory;
use Config;
use App\Language;
use Illuminate\Http\Request;


class EventController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }

    public function index()
    {
     
        $this->data_common += $this->load_articles_by_category(6);//event
      
        $this->data_common += $this->load_articles();//_all news

        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/event.text_event') => ['', 'active' => 'active'],
            ],
        ];
      
        
        return $this->view('main.events.index');
    }

    //_ดึงข่าวมาแสดงแยกตามประเภท
    private function load_articles_by_category($articel_cat_id)
    {
        $articles = ArticleDescription::with('article')
                    ->language($this->language->id)
                    ->searchCategory($articel_cat_id)
                    ->publishWeb()
                    ->paginate(12) //_ทำให้ paginate เป็นอิสระจากแต่ละ tab 
                    ->appends( ['category'=>$articel_cat_id] ); //_send category=x like this : news?catgory=4&page4=2
                    //->paginate(4 , [*] ,'pages');

        $articles->setCollection(
                $articles->getCollection()
                    ->map(function ($item, $key) {
    
                        if ($item->image) {
                            //_สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache 
                            //__ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                            $item->image = Image::resize($item->image, 235, 165); 
                        } else {
                            $item->image = Image::resize('no_image.png', 235, 165);
                        }
    
                        return $item;
                    })
            );
    
        
            return [
                'articles' => $articles,
         
            ];    
    }

    private function load_articles()
    {
        $articles = ArticleDescription::with('article')
            ->language($this->language->id)
            ->publishWeb()
            ->paginate(12); ///แสดงหน้าละ 12
        
        $catagory = ArticleToCategory::All();

     
        
        $articles->setCollection(
            $articles->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 235, 165); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                    } else {
                        $item->image = Image::resize('no_image.png', 235, 165);
                    }

                    return $item;
                })
        );

        // ดึงรูปเพิ่มเติมสำหรับข่าวลำดับที่ 1
        $first_article_images = ArticleImage::where('article_id', @$articles[0]->article_id)->order()->paginate(100);
        // dd($first_article_images);
        if (count($first_article_images) > 0) {
            // Map image fields
            $first_article_images->setCollection(
                $first_article_images->getCollection()
                    ->map(function ($item, $key) {

                        if ($item->image) {
                            $item->image = Image::resize($item->image, 975, 475); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                        } else {
                            $item->image = Image::resize('no_image.png', 975, 475);
                        }

                        return $item;
                    })
            );
        }
       
        return [
           
            'articles' => $articles,
            'catagory' =>$catagory,
            'first_article_images' => $first_article_images,
            'first_article_images_blank' => Image::resize('no_image.png', 975, 475),
          
        ];
    }

    //_ดึงข่าวที่เกียวข้อง
    private function load_related_articles($articel_id)
    {
        //_get articel_category_id ของข่าวที่กำลังดูอยู่
        $item_cat = ArticleToCategory::where('article_id',$articel_id)
                    ->take(1) //_limit 1
                    ->get();

        //_if has Category ID
        if(count($item_cat) > 0)
        {
            $articles = ArticleDescription::with('article')
                    ->language($this->language->id)
                    ->publishWeb()
                    ->searchRelatedNews($item_cat[0]->article_category_id , $articel_id)
                    ->paginate(4);
        }else{
            $articles = Article::with('articleDescriptions') //function in event model 
                    ->findOrFail($articel_id)
                    ->paginate(4);
        }
                    
    

        $articles->setCollection(
            $articles->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        //_สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache 
                        //__ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                        $item->image = Image::resize($item->image, 235, 165); 
                    } else {
                        $item->image = Image::resize('no_image.png', 235, 165);
                    }

                    return $item;
                })
        );
        
        return [
            'articels_related'=>$articles
        ];
        
        //dd($articel_related);
    } 

    public function detail($id , Request $request)
    {   
        //_increst field:Viewed number when click to see detail
        //Article::findOrFail($id)->increment('viewed');
        $item_viewed = Article::findOrFail($id);
        $item_viewed->timestamps = false; //_disable update field:updated_at
        $item_viewed->increment('viewed');
        unset($item_viewed); //_clear memory
        
        //_ดึงข่าวที่มียอดวิวสูงสุด 5 อันดับ 
        $populars = ArticleDescription::with('article') 
        ->language($this->language->id)
        ->popular()     // มาจาก model articlesDescription
        ->paginate(5);
        
        $populars->setCollection(
            $populars->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 235, 165); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                    } else {
                        $item->image = Image::resize('no_image.png', 235, 165);
                    }

                    return $item;
                })
        );

        //_ดึงข่าวที่ update ล่าสุด 5 อันดับ
        $lastest_posts = ArticleDescription::with('article') 
                        ->language($this->language->id)
                        ->publishWeb()     // มาจาก model articlesDescription
                        ->paginate(5);
     
        $lastest_posts->setCollection(
            $lastest_posts->getCollection()
                ->map(function ($item, $key) {

                    if ($item->image) {
                        $item->image = Image::resize($item->image, 235, 165); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                    } else {
                        $item->image = Image::resize('no_image.png', 235, 165);
                    }
                    return $item;
                })
        );

        //_ดึงข่าวที่คลิกเข้ามาดู
        $item = Article::with('articleDescriptions') //function in event model 
                    ->findOrFail($id);
                    
        if ($item->image) {
            $item->image = $item->image;
        } else {
            $item->image = '';
        }
        
         //_Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
            //$item->thumb = Image::resize($item->image, 235, 165);
            $item->thumb = Image::resize($item->image, 975, 685); 
        } else {
            //$item->thumb = Image::resize('no_image.png', 235, 165);
            $item->thumb = "";
        }

        $description = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        foreach ($item->articleDescriptions as $dest) {
            $description[$dest->language_id] = $dest;
        }
        $item->description = $description;
        
        //_get news illustration images
        $articleImgae = ArticleImage::where('article_id',$id)->order()->get();
        
        if(count($articleImgae) > 0){
            foreach($articleImgae as $item_articleImage){
                //$item_articleImage->image = Image::resize($item_articleImage->image, 975, 475); 
                $item_articleImage->image = Image::resize($item_articleImage->image, 975, 685); 
            }
        }

        //dd($articleImgae);

        //_ข่าวที่เกี่ยวข้อง
        $this->data_common += $this->load_related_articles($id);

        $this->data_common += [

            'populars' => $populars,
            'lastest_posts' =>$lastest_posts,
            'item' => $item,
            'article_imgae' => $articleImgae,
            'article_images_blank' => Image::resize('no_image.png', 235, 165),

            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/event.text_event') => ['url' => Config::get('url.main.events'), 'active' => ''],
                $item->description[$this->language->id]->name => ['', 'active' => 'active'],
            ],
        ];

        return $this->view('main.events.detail');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
