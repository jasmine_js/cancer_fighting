<?php

namespace App\Http\Controllers\Main;

use App\ExchangeProgram;
use App\ExchangeProgramDescription;
use App\ExchangeToCategory;
use App\ExchangeImage;
use Config;
use App\Image;
use App\Language;
use Illuminate\Http\Request;


//use App\Http\Controllers\Controller;

class ExchangeProgramController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }

    public function index()
    {
        //dd('gg');
        
        $this->data_common += ['exchange_bycat_2' => $this->load_exchange_by_category(2)];//_โครงการ
        $this->data_common += ['exchange_bycat_1' => $this->load_exchange_by_category(1)];//รายละเอียด
        $this->data_common += ['exchange_bycat_3' => $this->load_exchange_by_category(3)];//_บันทึกนศ.
        $this->data_common += ['exchange_bycat_4' => $this->load_exchange_by_category(4)];//_บันทึกอาจารย์
        $this->data_common += ['exchange_bycat_5' => $this->load_exchange_by_category(5)];//_รายชื่อมหาวิทยาลัย

        $this->data_common += $this->load_exchange_program();

        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/home.text_exchange_program') => ['', 'active' => 'active'],
            ],
        ];
       // dd($this->data_common);
        return $this->view('main.exchange_programs.index');
    }  
    
     //_ดึงข่าวมาแสดงแยกตามประเภท
     private function load_exchange_by_category($exchange_cat_id)
     {
         $exchange_cat = ExchangeProgramDescription::with('exchangeProgram')
                     ->language($this->language->id)
                     ->searchCategory($exchange_cat_id)
                     ->publishWeb()
                     ->paginate(12 ,['*'] ,'page'.$exchange_cat_id) //_ทำให้ paginate เป็นอิสระจากแต่ละ tab 
                     ->appends( ['category'=>$exchange_cat_id] ); //_send category=x like this : news?catgory=4&page4=2
                     //->paginate(4 , [*] ,'pages');
 
         $exchange_cat->setCollection(
                 $exchange_cat->getCollection()
                     ->map(function ($item, $key) {
     
                         if ($item->image) {
                             //_สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache 
                             //__ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                             $item->image = Image::resize($item->image, 235, 165); 
                         } else {
                             $item->image = Image::resize('no_image.png', 235, 165);
                         }
     
                         return $item;
                     })
             );
     
             // ดึงรูปเพิ่มเติมสำหรับข่าวลำดับที่ 1
             /* $first_article_images = ArticleImage::where('article_id', @$articles[0]->article_id)->order()->paginate(100);
         
             if (count($first_article_images) > 0) {
                 // Map image fields
                 $first_article_images->setCollection(
                     $first_article_images->getCollection()
                         ->map(function ($item, $key) {
     
                             if ($item->image) {
                                 $item->image = Image::resize($item->image, 975, 475); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                             } else {
                                 $item->image = Image::resize('no_image.png', 975, 475);
                             }
     
                             return $item;
                         })
                 );
             } */
            
             return [
                 'exchange_cat' => $exchange_cat,
                 /* 'first_article_images' => $first_article_images,
                 'first_article_images_blank' => Image::resize('no_image.png', 975, 475), */
             ];    
     }
     

    private function load_exchange_program()
    {
        $exchange_programs =ExchangeProgramDescription::with('exchangeProgram')
                ->language($this->language->id)
                ->publishWeb()
                ->paginate(12); ///แสดงหน้าละ 12

            $catagory = ExchangeToCategory::All();


            $exchange_programs->setCollection(
                $exchange_programs->getCollection()
                        ->map(function ($item, $key) {
    
                        if ($item->image) {
                            $item->image = Image::resize($item->image, 235, 165); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                        } else {
                            $item->image = Image::resize('no_image.png', 235, 165);
                        }
    
                        return $item;
                    })
            );   
            
            // ดึงรูปเพิ่มเติมสำหรับข่าวลำดับที่ 1
        $first_exchange_program_images = ExchangeImage::where('exchange_program_id', @$exchange_programs[0]->exchange_program_id)->order()->paginate(100);
        // dd($first_article_images);
        if (count($first_exchange_program_images) > 0) {
            // Map image fields
            $first_exchange_program_images->setCollection(
                $first_exchange_program_images->getCollection()
                    ->map(function ($item, $key) {

                        if ($item->image) {
                            $item->image = Image::resize($item->image, 975, 475); //สร้าง cache image ปรับขนาดได้ตามต้องการที่นี่ ไฟล์จะเก็บไว้ที่ public/storage/image/cache ถ้ามีไฟล์อยู่โปรแกรมจะไม่สร้างให้ใหม่
                        } else {
                            $item->image = Image::resize('no_image.png', 975, 475);
                        }

                        return $item;
                    })
            );
        }
    
            
        return [
            'catagory' =>$catagory,
            'exchange_programs' => $exchange_programs,
            'first_exchange_program_images' => $first_exchange_program_images,
            'first_exchange_program_images_blank' => Image::resize('no_image.png', 975, 475),
        ];
    }

    public function detail($id , Request $request)
    {
        
        //_increst field:Viewd number when click to see news detail
        //ExchangeProgram::findOrFail($id)->increment('viewed');
        $item_viewed = ExchangeProgram::findOrFail($id);
        $item_viewed->timestamps = false; //_disable update field:updated_at
        $item_viewed->increment('viewed');
        unset($item_viewed); //_clear memory

         //_ดึงข่าวที่คลิกเข้ามาดู
         $item = ExchangeProgram::with('exchangeprogramDescriptions') //function in event model 
         ->findOrFail($id);
         
        if ($item->image) {
        $item->image = $item->image;
        } else {
        $item->image = '';
        }

        //_Prepare thumb field for View (สำหรับแสดง)
        if ($item->image) {
        //$item->thumb = Image::resize($item->image, 235, 165);
        $item->thumb = Image::resize($item->image, 975, 685); 
        } else {
        //$item->thumb = Image::resize('no_image.png', 235, 165);
        $item->thumb = "";
        }

        $description = []; // สร้างฟิลด์ใหม่เพื่อแทรกเข้าในตัวแปร $item สำหรับเก็บข้อมูลแต่ละภาษา โดยใช้ array index เป็น language_id
        foreach ($item->exchangeprogramDescriptions as $dest) {
        $description[$dest->language_id] = $dest;
        }
        $item->description = $description;

        //_get news illustration images
        $exchangeImage = ExchangeImage::where('exchange_program_id',$id)->order()->get();

        if(count($exchangeImage) > 0){
        foreach($exchangeImage as $item_exchangeImage){
            //$item_articleImage->image = Image::resize($item_articleImage->image, 975, 475); 
            $item_exchangeImage->image = Image::resize($item_exchangeImage->image, 975, 685); 
        }
        }

        $this->data_common += [
            'exchange_image' => $exchangeImage,
            'exchange_images_blank' => Image::resize('no_image.png', 235, 165),
            'item' => $item,
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/home.text_exchange_program') => ['url' => Config::get('url.main.exchange_programs'), 'active' => ''],
                $item->description[$this->language->id]->name => ['', 'active' => 'active'],
            ],
        ];
        
       
        return $this->view('main.exchange_programs.detail');
    }
}
