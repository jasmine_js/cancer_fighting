<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
use Config;

class FacultyController extends MainController
{
    public function __construct()
    {
        MainController::__construct();
    }
    public function index()
    {
        $this->data_common += [
            'breadcrumb' => [
                trans('main/home.text_home') => ['url' => Config::get('url.main.home'), 'active' => ''],
                trans('main/navbar.text_faculty') => ['', 'active' => 'active'],
            ],
        ];
        return $this->view('main.faculty');

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //_เรียกผ่าน ajax ในหน้า (เมนู) คณะ
    public function fac_ajax(){
        return view('main.fac_info_ajax');
    }

    //_เรียกผ่าน ajax ในหน้า (เมนู) คณะ
    public function level_ajax(){
        return view('main.level_info_ajax');
    }

    
}
