<?php

namespace App\Http\Requests\Backoffice;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class FinanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $name_blank_numbers = 0;
        $link_blank_numbers = 0;
        $arr_alert_input    = [];

        //dd($request); 
        foreach($request->finance_descriptions as $language_id => $value)
        {
            //_count numbers of blank name
            if( trim($value['name']) == ""){
                $name_blank_numbers++;
            }
            
            //_count numbers of blank link
           /*  if( trim($value['link']) == ""){
                $link_blank_numbers++;
            } */
        }
        
        $arr_alert_input += [
            'publish_start' => 'required',
        ];

        //_เช็ค 3 หมายถึง ถ้าทั้ง 3 ภาษาว่าง แสดงว่าไม่ได้กรอกอะไรเลย
        if($name_blank_numbers == 3){
            $arr_alert_input['finance_descriptions.1.name'] = 'required';
        }
        /* if($link_blank_numbers == 3){
            $arr_alert_input['ebochure_descriptions.1.link'] = 'required';
        }  */
        
        return $arr_alert_input;

        
        /* return [
            // 'cooperative_descriptions.*.name' => 'required',
            // 'cooperative_descriptions.*.meta_title' => 'required',
            'publish_start' => 'required',
        ]; */
    }

    public function messages()
    {
        return [
            'required' => 'กรุณากรอกข้อมูล',
        ];
    }
}