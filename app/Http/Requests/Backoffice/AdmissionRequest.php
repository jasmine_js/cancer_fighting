<?php

namespace App\Http\Requests\Backoffice;

use Illuminate\Foundation\Http\FormRequest;

class AdmissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /* 'admission_descriptions.*.name' => 'required',
            'admission_descriptions.*.meta_title' => 'required',
            'admission_descriptions.*.link' => 'required', */
            // 'admission_descriptions.1.name' => 'required',
            // 'admission_descriptions.1.meta_title' => 'required',
            // 'admission_descriptions.1.link' => 'required',
            'publish_start' => 'required', 
        ];
    }
    

    public function messages()
    {
        return [
            'required'=>'กรุณากรอกข้อมูล'
        ];
    }
}
