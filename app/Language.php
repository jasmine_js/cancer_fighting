<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = [
       'id',
    ];

    public function scopeOrder($query)
    {
        return $query->orderBy('sort_order', 'ASC');
    }

    public function scopeGetCodeByID($query, $language_id)
    {
        return $query->where('id', $language_id)->pluck('code')->first();
    }

    public function scopeGetIdByCode($query, $language_code)
    {
        return $query->where('code', $language_code)->pluck('id')->first();
    }

    public function scopeEnable($query)
    {
        return $query->where('status', 1);
    }
}
