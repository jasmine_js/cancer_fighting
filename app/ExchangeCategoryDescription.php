<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeCategoryDescription extends Model
{
    private $table_main = 'exchange_categories'; //ตารางที่เก็บข้อมูลหลัก

    public $timestamps = false; // Disable Laravel's Eloquent timestamps

    protected $fillable = [
        'language_id', 'name', 'description', 'meta_title', 'meta_description', 'meta_keyword',
    ];

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function exchangeCategory()
    {
        return $this->belongsTo(ExchangeCategory::class);
    }

    public function scopeLanguage($query, $language_id)
    {
        return $query->where('language_id', $language_id);
    }

    public function scopeOrder($query)
    {
        return $query->join($this->table_main, $this->table_main . '.id', '=', 'exchange_category_id')->orderBy($this->table_main . '.sort_order', 'asc');
    }

    public function scopeSearch($query, $find)
    {
        return $query->where('name', 'LIKE', '%' . $find . '%');
    }
}

