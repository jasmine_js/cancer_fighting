<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleImage extends Model
{
    public $timestamps = false; // Disable Laravel's Eloquent timestamps

    protected $fillable = [
        'article_id', 'image', 'sort_order',
    ];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function scopeOrder($query)
    {
        return $query->orderBy('sort_order', 'ASC');
    }

}
