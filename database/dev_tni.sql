-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 28, 2018 at 10:49 AM
-- Server version: 10.1.37-MariaDB-0+deb9u1
-- PHP Version: 7.1.24-1+0~20181112093455.10+stretch~1.gbp09a4fd

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_tni`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publish_start` datetime NOT NULL,
  `publish_stop` datetime DEFAULT NULL,
  `viewed` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `image`, `publish_start`, `publish_stop`, `viewed`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, '2018-11-27 16:31:08', NULL, 9144, 1, '2018-11-27 09:31:08', '2018-11-27 09:31:08'),
(2, NULL, '2018-11-27 16:31:08', NULL, 4661, 1, '2018-11-27 09:31:08', '2018-11-27 09:31:08'),
(3, NULL, '2018-11-27 16:31:08', NULL, 1628, 1, '2018-11-27 09:31:08', '2018-11-27 09:31:08'),
(4, NULL, '2018-11-27 16:31:08', NULL, 2320, 1, '2018-11-27 09:31:08', '2018-11-27 09:31:08'),
(5, NULL, '2018-11-27 16:31:08', NULL, 4402, 1, '2018-11-27 09:31:08', '2018-11-27 09:31:08'),
(6, NULL, '2018-11-27 16:31:08', NULL, 5420, 1, '2018-11-27 09:31:08', '2018-11-27 09:31:08'),
(7, NULL, '2018-11-27 16:31:08', NULL, 2889, 1, '2018-11-27 09:31:08', '2018-11-27 09:31:08'),
(8, NULL, '2018-11-27 16:31:08', NULL, 2625, 1, '2018-11-27 09:31:08', '2018-11-27 09:31:08'),
(9, NULL, '2018-11-27 16:31:09', NULL, 5519, 1, '2018-11-27 09:31:09', '2018-11-27 09:31:09'),
(10, NULL, '2018-11-27 16:31:09', NULL, 9897, 1, '2018-11-27 09:31:09', '2018-11-27 09:31:09'),
(11, NULL, '2018-11-27 16:31:09', NULL, 1297, 1, '2018-11-27 09:31:09', '2018-11-27 09:31:09'),
(12, 'catalog/mockup/mockup-2.png', '2018-11-27 16:31:09', NULL, 4429, 1, '2018-11-27 09:31:09', '2018-11-27 09:54:06'),
(13, 'catalog/mockup/mockup-3.png', '2018-11-27 16:31:09', NULL, 5009, 1, '2018-11-27 09:31:09', '2018-11-27 09:54:15'),
(14, 'catalog/mockup/mockup-4.png', '2018-11-27 16:31:09', NULL, 8268, 1, '2018-11-27 09:31:09', '2018-11-27 09:54:24'),
(15, 'catalog/mockup/mockup-2.png', '2018-11-27 16:31:09', NULL, 831, 1, '2018-11-27 09:31:09', '2018-11-27 09:54:38'),
(16, NULL, '2018-11-27 16:31:09', NULL, 9875, 1, '2018-11-27 09:31:09', '2018-11-27 09:31:09'),
(17, NULL, '2018-11-27 16:31:09', NULL, 5640, 1, '2018-11-27 09:31:09', '2018-11-27 09:31:09'),
(18, NULL, '2018-11-27 16:31:09', NULL, 7396, 1, '2018-11-27 09:31:09', '2018-11-27 09:31:09'),
(19, NULL, '2018-11-27 16:31:09', NULL, 5804, 1, '2018-11-27 09:31:09', '2018-11-27 09:31:09'),
(20, 'catalog/mockup/mockup-1.png', '2018-11-27 16:31:09', NULL, 1295, 1, '2018-11-27 09:31:09', '2018-11-27 09:54:41');

-- --------------------------------------------------------

--
-- Table structure for table `article_categories`
--

CREATE TABLE `article_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article_categories`
--

INSERT INTO `article_categories` (`id`, `image`, `sort_order`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 1, '2018-11-27 09:31:05', '2018-11-27 09:31:05'),
(2, NULL, 2, 1, '2018-11-27 09:31:05', '2018-11-27 09:31:05'),
(3, NULL, 3, 1, '2018-11-27 09:31:06', '2018-11-27 09:31:06'),
(4, NULL, 4, 1, '2018-11-27 09:31:06', '2018-11-27 09:31:06'),
(5, NULL, 5, 1, '2018-11-27 09:31:06', '2018-11-27 09:31:06'),
(6, NULL, 6, 1, '2018-11-27 09:31:06', '2018-11-27 09:31:06'),
(7, NULL, 7, 1, '2018-11-27 09:31:06', '2018-11-27 09:31:06'),
(8, NULL, 8, 1, '2018-11-27 09:31:06', '2018-11-27 09:31:06'),
(9, NULL, 9, 1, '2018-11-27 09:31:06', '2018-11-27 09:31:06'),
(10, NULL, 10, 1, '2018-11-27 09:31:07', '2018-11-27 09:31:07'),
(11, NULL, 11, 1, '2018-11-27 09:31:07', '2018-11-27 09:31:07'),
(12, NULL, 12, 1, '2018-11-27 09:31:07', '2018-11-27 09:31:07'),
(13, NULL, 13, 1, '2018-11-27 09:31:07', '2018-11-27 09:31:07'),
(14, NULL, 14, 1, '2018-11-27 09:31:07', '2018-11-27 09:31:07'),
(15, NULL, 15, 1, '2018-11-27 09:31:07', '2018-11-27 09:31:07'),
(16, NULL, 16, 1, '2018-11-27 09:31:07', '2018-11-27 09:31:07'),
(17, NULL, 17, 1, '2018-11-27 09:31:07', '2018-11-27 09:31:07'),
(18, NULL, 18, 1, '2018-11-27 09:31:07', '2018-11-27 09:31:07'),
(19, NULL, 19, 1, '2018-11-27 09:31:07', '2018-11-27 09:31:07'),
(20, NULL, 20, 1, '2018-11-27 09:31:08', '2018-11-27 09:31:08');

-- --------------------------------------------------------

--
-- Table structure for table `article_category_descriptions`
--

CREATE TABLE `article_category_descriptions` (
  `article_category_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article_category_descriptions`
--

INSERT INTO `article_category_descriptions` (`article_category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 1, '(th)Hic ut iste pariatur qui et sit.', '(th)Aliquid ducimus magni sunt nihil nobis.', '(th)Hic ut iste pariatur qui et sit.', '(th)Aliquid ducimus magni sunt nihil nobis.', NULL),
(1, 2, '(en)Aut labore ut consequuntur et possimus minima.', '(en)Totam est fuga omnis aut ea sint.', '(en)Aut labore ut consequuntur et possimus minima.', '(en)Totam est fuga omnis aut ea sint.', NULL),
(1, 3, '(jp)Et cumque iure quam non expedita voluptates.', '(jp)Recusandae et deleniti quo tempora autem quo ex.', '(jp)Et cumque iure quam non expedita voluptates.', '(jp)Recusandae et deleniti quo tempora autem quo ex.', NULL),
(2, 1, '(th)Est velit dolorem hic quas rerum.', '(th)Dignissimos ea voluptatem et aut qui illo.', '(th)Est velit dolorem hic quas rerum.', '(th)Dignissimos ea voluptatem et aut qui illo.', NULL),
(2, 2, '(en)Sed cupiditate doloribus quidem non.', '(en)Odit nulla dolor est molestias voluptatem in occaecati esse.', '(en)Sed cupiditate doloribus quidem non.', '(en)Odit nulla dolor est molestias voluptatem in occaecati esse.', NULL),
(2, 3, '(jp)Reiciendis et velit consequatur voluptas ea quidem voluptatem.', '(jp)Dolore voluptatem doloribus totam velit expedita sapiente voluptas.', '(jp)Reiciendis et velit consequatur voluptas ea quidem voluptatem.', '(jp)Dolore voluptatem doloribus totam velit expedita sapiente voluptas.', NULL),
(3, 1, '(th)Qui libero eos illum culpa.', '(th)Aut necessitatibus aspernatur repellendus sed ipsa quo aliquam quas.', '(th)Qui libero eos illum culpa.', '(th)Aut necessitatibus aspernatur repellendus sed ipsa quo aliquam quas.', NULL),
(3, 2, '(en)Quo a est et eum.', '(en)Voluptas dolore vel nesciunt quam voluptatibus.', '(en)Quo a est et eum.', '(en)Voluptas dolore vel nesciunt quam voluptatibus.', NULL),
(3, 3, '(jp)Corrupti et consequuntur autem non molestiae id cum.', '(jp)Et natus enim vel.', '(jp)Corrupti et consequuntur autem non molestiae id cum.', '(jp)Et natus enim vel.', NULL),
(4, 1, '(th)Dolor ipsam voluptatem consequuntur explicabo.', '(th)Quas alias qui suscipit rerum omnis qui hic quibusdam.', '(th)Dolor ipsam voluptatem consequuntur explicabo.', '(th)Quas alias qui suscipit rerum omnis qui hic quibusdam.', NULL),
(4, 2, '(en)Pariatur ullam et veritatis.', '(en)Dolor quae et eos vero aut vel et veritatis.', '(en)Pariatur ullam et veritatis.', '(en)Dolor quae et eos vero aut vel et veritatis.', NULL),
(4, 3, '(jp)Qui et repellendus qui inventore a debitis.', '(jp)Aut est magni eligendi rerum dolor dolores pariatur.', '(jp)Qui et repellendus qui inventore a debitis.', '(jp)Aut est magni eligendi rerum dolor dolores pariatur.', NULL),
(5, 1, '(th)Ut voluptatem nihil ut laudantium incidunt est et.', '(th)Laboriosam id et libero.', '(th)Ut voluptatem nihil ut laudantium incidunt est et.', '(th)Laboriosam id et libero.', NULL),
(5, 2, '(en)Quis nulla maxime dolor.', '(en)Sed consequatur cupiditate quod.', '(en)Quis nulla maxime dolor.', '(en)Sed consequatur cupiditate quod.', NULL),
(5, 3, '(jp)Veniam alias laboriosam omnis repellendus.', '(jp)Et sit dolores temporibus ut quis.', '(jp)Veniam alias laboriosam omnis repellendus.', '(jp)Et sit dolores temporibus ut quis.', NULL),
(6, 1, '(th)Sed molestiae pariatur reprehenderit quod.', '(th)Consectetur enim fugiat dolorum veritatis.', '(th)Sed molestiae pariatur reprehenderit quod.', '(th)Consectetur enim fugiat dolorum veritatis.', NULL),
(6, 2, '(en)Eaque ut porro quasi nobis autem sequi porro.', '(en)Dolorum a temporibus provident.', '(en)Eaque ut porro quasi nobis autem sequi porro.', '(en)Dolorum a temporibus provident.', NULL),
(6, 3, '(jp)Id nam est sed incidunt ut qui.', '(jp)Voluptate enim omnis amet doloremque recusandae error aut.', '(jp)Id nam est sed incidunt ut qui.', '(jp)Voluptate enim omnis amet doloremque recusandae error aut.', NULL),
(7, 1, '(th)Est consequatur nobis non harum qui eum consequatur.', '(th)Aperiam quo tempora numquam quia delectus voluptatem quibusdam.', '(th)Est consequatur nobis non harum qui eum consequatur.', '(th)Aperiam quo tempora numquam quia delectus voluptatem quibusdam.', NULL),
(7, 2, '(en)Explicabo qui aut rem animi qui.', '(en)Aut laborum et id ipsa quia.', '(en)Explicabo qui aut rem animi qui.', '(en)Aut laborum et id ipsa quia.', NULL),
(7, 3, '(jp)Et voluptates assumenda laboriosam sunt temporibus.', '(jp)Maxime quisquam omnis sit aspernatur.', '(jp)Et voluptates assumenda laboriosam sunt temporibus.', '(jp)Maxime quisquam omnis sit aspernatur.', NULL),
(8, 1, '(th)Nostrum magnam rerum saepe veniam.', '(th)Fuga itaque facilis consequatur eos deserunt.', '(th)Nostrum magnam rerum saepe veniam.', '(th)Fuga itaque facilis consequatur eos deserunt.', NULL),
(8, 2, '(en)Et a beatae qui suscipit.', '(en)Commodi reprehenderit aut in nulla deserunt ipsa doloremque.', '(en)Et a beatae qui suscipit.', '(en)Commodi reprehenderit aut in nulla deserunt ipsa doloremque.', NULL),
(8, 3, '(jp)Est enim corrupti iste accusamus vel.', '(jp)Nam consequatur placeat ab qui minima.', '(jp)Est enim corrupti iste accusamus vel.', '(jp)Nam consequatur placeat ab qui minima.', NULL),
(9, 1, '(th)Cupiditate voluptate ipsum ratione accusantium autem totam ea.', '(th)In incidunt repellat aut facere in dolorum porro.', '(th)Cupiditate voluptate ipsum ratione accusantium autem totam ea.', '(th)In incidunt repellat aut facere in dolorum porro.', NULL),
(9, 2, '(en)Ducimus quo quis rem commodi explicabo molestias.', '(en)Alias qui ut quaerat quam maiores voluptatem sit.', '(en)Ducimus quo quis rem commodi explicabo molestias.', '(en)Alias qui ut quaerat quam maiores voluptatem sit.', NULL),
(9, 3, '(jp)Et dolor quibusdam et non.', '(jp)In omnis deserunt quo consequatur et provident reiciendis.', '(jp)Et dolor quibusdam et non.', '(jp)In omnis deserunt quo consequatur et provident reiciendis.', NULL),
(10, 1, '(th)Cum sapiente sed ea quia cumque.', '(th)Aspernatur omnis eos quidem possimus earum.', '(th)Cum sapiente sed ea quia cumque.', '(th)Aspernatur omnis eos quidem possimus earum.', NULL),
(10, 2, '(en)Ullam dicta sed numquam et.', '(en)Nisi voluptatum harum expedita voluptas ut et.', '(en)Ullam dicta sed numquam et.', '(en)Nisi voluptatum harum expedita voluptas ut et.', NULL),
(10, 3, '(jp)Doloribus iusto pariatur ratione sint nisi vel.', '(jp)Suscipit dolorum quia consequatur totam voluptatum voluptatum omnis.', '(jp)Doloribus iusto pariatur ratione sint nisi vel.', '(jp)Suscipit dolorum quia consequatur totam voluptatum voluptatum omnis.', NULL),
(11, 1, '(th)Minima et asperiores dolorum animi qui aut consequatur voluptatem.', '(th)Veniam placeat voluptates voluptatum aspernatur.', '(th)Minima et asperiores dolorum animi qui aut consequatur voluptatem.', '(th)Veniam placeat voluptates voluptatum aspernatur.', NULL),
(11, 2, '(en)Ea fuga quo atque exercitationem quis non.', '(en)Distinctio ut omnis atque in odio.', '(en)Ea fuga quo atque exercitationem quis non.', '(en)Distinctio ut omnis atque in odio.', NULL),
(11, 3, '(jp)A ex inventore omnis ipsa expedita velit dolorem.', '(jp)Natus molestiae saepe assumenda vero similique omnis.', '(jp)A ex inventore omnis ipsa expedita velit dolorem.', '(jp)Natus molestiae saepe assumenda vero similique omnis.', NULL),
(12, 1, '(th)Velit iste culpa quisquam ipsam et nam ut.', '(th)Voluptatibus est enim dolor natus aut laborum necessitatibus cum.', '(th)Velit iste culpa quisquam ipsam et nam ut.', '(th)Voluptatibus est enim dolor natus aut laborum necessitatibus cum.', NULL),
(12, 2, '(en)In ut tempora dolorem eum.', '(en)At nam consectetur rerum similique.', '(en)In ut tempora dolorem eum.', '(en)At nam consectetur rerum similique.', NULL),
(12, 3, '(jp)Doloremque asperiores et cumque ducimus.', '(jp)Hic asperiores illo explicabo ea possimus qui iusto.', '(jp)Doloremque asperiores et cumque ducimus.', '(jp)Hic asperiores illo explicabo ea possimus qui iusto.', NULL),
(13, 1, '(th)Ea voluptatem enim occaecati hic officia sed voluptas.', '(th)Veritatis sequi non accusamus tempore quisquam nam vitae.', '(th)Ea voluptatem enim occaecati hic officia sed voluptas.', '(th)Veritatis sequi non accusamus tempore quisquam nam vitae.', NULL),
(13, 2, '(en)Earum numquam facere aperiam ut.', '(en)Necessitatibus voluptatibus est quae et accusantium.', '(en)Earum numquam facere aperiam ut.', '(en)Necessitatibus voluptatibus est quae et accusantium.', NULL),
(13, 3, '(jp)Temporibus quos dolor sunt aut sit corrupti aut.', '(jp)Earum ipsam necessitatibus odit sunt consequatur nisi qui ut.', '(jp)Temporibus quos dolor sunt aut sit corrupti aut.', '(jp)Earum ipsam necessitatibus odit sunt consequatur nisi qui ut.', NULL),
(14, 1, '(th)Rerum et quae et enim.', '(th)Esse iusto cum est qui consequatur.', '(th)Rerum et quae et enim.', '(th)Esse iusto cum est qui consequatur.', NULL),
(14, 2, '(en)Ipsa iure unde iusto nulla voluptatem.', '(en)Voluptatibus quidem voluptas quia perspiciatis molestias et.', '(en)Ipsa iure unde iusto nulla voluptatem.', '(en)Voluptatibus quidem voluptas quia perspiciatis molestias et.', NULL),
(14, 3, '(jp)Consequuntur at praesentium quia cupiditate dolor.', '(jp)Dolorum illum eaque adipisci neque delectus.', '(jp)Consequuntur at praesentium quia cupiditate dolor.', '(jp)Dolorum illum eaque adipisci neque delectus.', NULL),
(15, 1, '(th)Qui aut aspernatur voluptatem delectus ipsa.', '(th)Fuga aut animi debitis sed blanditiis provident consequatur.', '(th)Qui aut aspernatur voluptatem delectus ipsa.', '(th)Fuga aut animi debitis sed blanditiis provident consequatur.', NULL),
(15, 2, '(en)Soluta perferendis voluptatem provident voluptas.', '(en)Voluptatum qui voluptatem dignissimos nesciunt aliquam eius vitae.', '(en)Soluta perferendis voluptatem provident voluptas.', '(en)Voluptatum qui voluptatem dignissimos nesciunt aliquam eius vitae.', NULL),
(15, 3, '(jp)Temporibus consequuntur quas ut repellat.', '(jp)Perspiciatis quia consequatur reprehenderit officiis qui omnis.', '(jp)Temporibus consequuntur quas ut repellat.', '(jp)Perspiciatis quia consequatur reprehenderit officiis qui omnis.', NULL),
(16, 1, '(th)Tempora rem dolores est repellendus velit eos.', '(th)Velit est et pariatur voluptates eos totam harum.', '(th)Tempora rem dolores est repellendus velit eos.', '(th)Velit est et pariatur voluptates eos totam harum.', NULL),
(16, 2, '(en)Nulla dolores eveniet vitae quo.', '(en)Tenetur nam eos aut voluptatum repellat.', '(en)Nulla dolores eveniet vitae quo.', '(en)Tenetur nam eos aut voluptatum repellat.', NULL),
(16, 3, '(jp)Quas eaque cumque placeat qui hic.', '(jp)Omnis magni ipsam vel perspiciatis officiis reiciendis placeat.', '(jp)Quas eaque cumque placeat qui hic.', '(jp)Omnis magni ipsam vel perspiciatis officiis reiciendis placeat.', NULL),
(17, 1, '(th)Commodi inventore cumque reiciendis autem sit quis.', '(th)Commodi omnis sed dignissimos perferendis in id.', '(th)Commodi inventore cumque reiciendis autem sit quis.', '(th)Commodi omnis sed dignissimos perferendis in id.', NULL),
(17, 2, '(en)Et quis et architecto non error molestiae consequatur excepturi.', '(en)Ab perferendis quo ut et ad qui.', '(en)Et quis et architecto non error molestiae consequatur excepturi.', '(en)Ab perferendis quo ut et ad qui.', NULL),
(17, 3, '(jp)Esse quia quibusdam magnam id.', '(jp)In maiores expedita magni nesciunt deleniti autem iusto rerum.', '(jp)Esse quia quibusdam magnam id.', '(jp)In maiores expedita magni nesciunt deleniti autem iusto rerum.', NULL),
(18, 1, '(th)Dolores qui nostrum reiciendis assumenda ut accusantium sint debitis.', '(th)Ex ullam et debitis quam.', '(th)Dolores qui nostrum reiciendis assumenda ut accusantium sint debitis.', '(th)Ex ullam et debitis quam.', NULL),
(18, 2, '(en)Quis vero dolores id molestiae officia ducimus.', '(en)Inventore corrupti sequi non voluptatibus dolorum atque occaecati.', '(en)Quis vero dolores id molestiae officia ducimus.', '(en)Inventore corrupti sequi non voluptatibus dolorum atque occaecati.', NULL),
(18, 3, '(jp)Asperiores quasi sit ex aut.', '(jp)Sed magni aut dolorum cupiditate voluptas.', '(jp)Asperiores quasi sit ex aut.', '(jp)Sed magni aut dolorum cupiditate voluptas.', NULL),
(19, 1, '(th)Repellendus incidunt nemo similique exercitationem.', '(th)Temporibus quasi temporibus aut autem voluptates dignissimos eveniet.', '(th)Repellendus incidunt nemo similique exercitationem.', '(th)Temporibus quasi temporibus aut autem voluptates dignissimos eveniet.', NULL),
(19, 2, '(en)Et dolorem et distinctio aliquam.', '(en)Quia eligendi voluptatem voluptates beatae eum voluptas.', '(en)Et dolorem et distinctio aliquam.', '(en)Quia eligendi voluptatem voluptates beatae eum voluptas.', NULL),
(19, 3, '(jp)Asperiores dolorem autem porro illum qui.', '(jp)Eius suscipit qui eos aspernatur et est voluptatem.', '(jp)Asperiores dolorem autem porro illum qui.', '(jp)Eius suscipit qui eos aspernatur et est voluptatem.', NULL),
(20, 1, '(th)Veritatis est hic repellat ex.', '(th)Tempora itaque perferendis ut voluptas.', '(th)Veritatis est hic repellat ex.', '(th)Tempora itaque perferendis ut voluptas.', NULL),
(20, 2, '(en)Quasi et dolor necessitatibus debitis.', '(en)Ipsam omnis assumenda occaecati corporis et cumque consequatur.', '(en)Quasi et dolor necessitatibus debitis.', '(en)Ipsam omnis assumenda occaecati corporis et cumque consequatur.', NULL),
(20, 3, '(jp)Harum necessitatibus voluptatum velit dolore consectetur dolores eos.', '(jp)Aliquid id nobis id voluptatem perferendis.', '(jp)Harum necessitatibus voluptatum velit dolore consectetur dolores eos.', '(jp)Aliquid id nobis id voluptatem perferendis.', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `article_descriptions`
--

CREATE TABLE `article_descriptions` (
  `article_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `tag` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article_descriptions`
--

INSERT INTO `article_descriptions` (`article_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 1, '(th)Eius incidunt perspiciatis et commodi est.', '(th)Facilis consectetur et quia.', NULL, '(th)Eius incidunt perspiciatis et commodi est.', '(th)Facilis consectetur et quia.', NULL),
(1, 2, '(en)Rerum autem qui voluptates.', '(en)Et excepturi ullam est adipisci et saepe.', NULL, '(en)Rerum autem qui voluptates.', '(en)Et excepturi ullam est adipisci et saepe.', NULL),
(1, 3, '(jp)Non architecto eaque at vitae cupiditate ut.', '(jp)Eum eos et deleniti cum et.', NULL, '(jp)Non architecto eaque at vitae cupiditate ut.', '(jp)Eum eos et deleniti cum et.', NULL),
(2, 1, '(th)Minus officia odio velit sit doloremque aliquid explicabo hic.', '(th)Sapiente illo iste maxime architecto vel et.', NULL, '(th)Minus officia odio velit sit doloremque aliquid explicabo hic.', '(th)Sapiente illo iste maxime architecto vel et.', NULL),
(2, 2, '(en)Quaerat quia optio pariatur et atque ut quia.', '(en)Vel quo ab est quia modi earum similique.', NULL, '(en)Quaerat quia optio pariatur et atque ut quia.', '(en)Vel quo ab est quia modi earum similique.', NULL),
(2, 3, '(jp)Ut dolor dolore ab ut.', '(jp)Fugit ab totam adipisci voluptas ratione.', NULL, '(jp)Ut dolor dolore ab ut.', '(jp)Fugit ab totam adipisci voluptas ratione.', NULL),
(3, 1, '(th)Fugit laborum sequi voluptates asperiores voluptas.', '(th)Consectetur tempora ullam nulla ut consequatur cumque.', NULL, '(th)Fugit laborum sequi voluptates asperiores voluptas.', '(th)Consectetur tempora ullam nulla ut consequatur cumque.', NULL),
(3, 2, '(en)Eos adipisci commodi cum et voluptas fugiat quo.', '(en)Et velit voluptas iure est atque.', NULL, '(en)Eos adipisci commodi cum et voluptas fugiat quo.', '(en)Et velit voluptas iure est atque.', NULL),
(3, 3, '(jp)Ad vel et natus.', '(jp)Numquam velit aut dolores suscipit voluptatem enim.', NULL, '(jp)Ad vel et natus.', '(jp)Numquam velit aut dolores suscipit voluptatem enim.', NULL),
(4, 1, '(th)Sint et quo eveniet.', '(th)Voluptas dolores mollitia nihil sint.', NULL, '(th)Sint et quo eveniet.', '(th)Voluptas dolores mollitia nihil sint.', NULL),
(4, 2, '(en)Suscipit aut cumque quibusdam non consequatur alias ut.', '(en)Enim eligendi vel iure commodi.', NULL, '(en)Suscipit aut cumque quibusdam non consequatur alias ut.', '(en)Enim eligendi vel iure commodi.', NULL),
(4, 3, '(jp)Voluptatibus iste qui consectetur fugit beatae consequatur error.', '(jp)Velit eum voluptas reprehenderit optio.', NULL, '(jp)Voluptatibus iste qui consectetur fugit beatae consequatur error.', '(jp)Velit eum voluptas reprehenderit optio.', NULL),
(5, 1, '(th)Temporibus ut occaecati dolorem at eum expedita.', '(th)Hic et non impedit enim accusamus voluptatem asperiores.', NULL, '(th)Temporibus ut occaecati dolorem at eum expedita.', '(th)Hic et non impedit enim accusamus voluptatem asperiores.', NULL),
(5, 2, '(en)Dolorem nulla autem est excepturi animi voluptas dolores.', '(en)Hic ut vel amet et doloribus.', NULL, '(en)Dolorem nulla autem est excepturi animi voluptas dolores.', '(en)Hic ut vel amet et doloribus.', NULL),
(5, 3, '(jp)Ut aspernatur blanditiis vitae quas voluptatem tempora tempora consequatur.', '(jp)Voluptatum necessitatibus sapiente et quo accusantium.', NULL, '(jp)Ut aspernatur blanditiis vitae quas voluptatem tempora tempora consequatur.', '(jp)Voluptatum necessitatibus sapiente et quo accusantium.', NULL),
(6, 1, '(th)Modi sunt libero aperiam aut est nisi sint.', '(th)Dicta est dolore nobis ut accusantium.', NULL, '(th)Modi sunt libero aperiam aut est nisi sint.', '(th)Dicta est dolore nobis ut accusantium.', NULL),
(6, 2, '(en)Et accusantium quia similique molestiae in eos.', '(en)Reprehenderit aliquid explicabo sequi.', NULL, '(en)Et accusantium quia similique molestiae in eos.', '(en)Reprehenderit aliquid explicabo sequi.', NULL),
(6, 3, '(jp)Dolorem illo suscipit id quos vero error voluptates.', '(jp)Dolorem cum inventore dolorem quia laudantium aut consequuntur.', NULL, '(jp)Dolorem illo suscipit id quos vero error voluptates.', '(jp)Dolorem cum inventore dolorem quia laudantium aut consequuntur.', NULL),
(7, 1, '(th)Incidunt minima magnam voluptate eum ducimus rem odit hic.', '(th)Animi aliquam minus ullam consequatur laudantium ut corrupti.', NULL, '(th)Incidunt minima magnam voluptate eum ducimus rem odit hic.', '(th)Animi aliquam minus ullam consequatur laudantium ut corrupti.', NULL),
(7, 2, '(en)Sequi ea officia sit voluptatibus ducimus iste sint sed.', '(en)Esse libero quibusdam velit reiciendis totam repellat fugiat.', NULL, '(en)Sequi ea officia sit voluptatibus ducimus iste sint sed.', '(en)Esse libero quibusdam velit reiciendis totam repellat fugiat.', NULL),
(7, 3, '(jp)Et cumque quis unde.', '(jp)Nihil sed quisquam pariatur et.', NULL, '(jp)Et cumque quis unde.', '(jp)Nihil sed quisquam pariatur et.', NULL),
(8, 1, '(th)Minus sed quaerat voluptatem qui asperiores.', '(th)Perferendis voluptatum animi suscipit sequi quis aliquid.', NULL, '(th)Minus sed quaerat voluptatem qui asperiores.', '(th)Perferendis voluptatum animi suscipit sequi quis aliquid.', NULL),
(8, 2, '(en)Iure nihil ad rem quae veritatis cum perferendis.', '(en)Soluta autem accusamus repellat nobis.', NULL, '(en)Iure nihil ad rem quae veritatis cum perferendis.', '(en)Soluta autem accusamus repellat nobis.', NULL),
(8, 3, '(jp)Beatae quae eligendi et est voluptates fuga.', '(jp)In aut error labore hic et inventore ut.', NULL, '(jp)Beatae quae eligendi et est voluptates fuga.', '(jp)In aut error labore hic et inventore ut.', NULL),
(9, 1, '(th)Et sit nihil rem minima quaerat ipsa.', '(th)Quidem aut voluptates doloremque recusandae.', NULL, '(th)Et sit nihil rem minima quaerat ipsa.', '(th)Quidem aut voluptates doloremque recusandae.', NULL),
(9, 2, '(en)Expedita placeat autem pariatur a consequatur dolorem et.', '(en)Eum eaque quis cum voluptate odio tenetur.', NULL, '(en)Expedita placeat autem pariatur a consequatur dolorem et.', '(en)Eum eaque quis cum voluptate odio tenetur.', NULL),
(9, 3, '(jp)Ullam in reprehenderit culpa et quod.', '(jp)Dolorem fugiat omnis vero aut officiis consequatur voluptatem.', NULL, '(jp)Ullam in reprehenderit culpa et quod.', '(jp)Dolorem fugiat omnis vero aut officiis consequatur voluptatem.', NULL),
(10, 1, '(th)Est nihil libero asperiores aut.', '(th)Qui excepturi repudiandae enim cumque ea itaque.', NULL, '(th)Est nihil libero asperiores aut.', '(th)Qui excepturi repudiandae enim cumque ea itaque.', NULL),
(10, 2, '(en)Cupiditate nemo consequatur enim aperiam reiciendis.', '(en)Delectus iure animi quo nihil possimus laudantium nam.', NULL, '(en)Cupiditate nemo consequatur enim aperiam reiciendis.', '(en)Delectus iure animi quo nihil possimus laudantium nam.', NULL),
(10, 3, '(jp)Et eaque consequatur ducimus culpa.', '(jp)Labore dolores minus iste ab placeat nesciunt mollitia.', NULL, '(jp)Et eaque consequatur ducimus culpa.', '(jp)Labore dolores minus iste ab placeat nesciunt mollitia.', NULL),
(11, 1, '(th)Ducimus qui non rem aut.', '(th)Officiis minima possimus fugiat ipsum impedit suscipit.', NULL, '(th)Ducimus qui non rem aut.', '(th)Officiis minima possimus fugiat ipsum impedit suscipit.', NULL),
(11, 2, '(en)Dolores minima dolor quia voluptatem.', '(en)Magnam molestias dolor illo qui et aut quo.', NULL, '(en)Dolores minima dolor quia voluptatem.', '(en)Magnam molestias dolor illo qui et aut quo.', NULL),
(11, 3, '(jp)Quo eos rerum cupiditate rerum aperiam.', '(jp)Magnam hic dolores quia amet accusamus veniam.', NULL, '(jp)Quo eos rerum cupiditate rerum aperiam.', '(jp)Magnam hic dolores quia amet accusamus veniam.', NULL),
(12, 1, '(th)Ratione eum ea et sit magnam natus.', '(th)Repudiandae eos et mollitia est illum fuga.', NULL, '(th)Ratione eum ea et sit magnam natus.', '(th)Repudiandae eos et mollitia est illum fuga.', NULL),
(12, 2, '(en)Dolorem dicta doloribus modi error.', '(en)Non voluptatem corporis dolorem qui maxime excepturi.', NULL, '(en)Dolorem dicta doloribus modi error.', '(en)Non voluptatem corporis dolorem qui maxime excepturi.', NULL),
(12, 3, '(jp)Quisquam non sapiente molestiae quam voluptate eligendi nesciunt molestiae.', '(jp)Cupiditate pariatur voluptas rem et quia.', NULL, '(jp)Quisquam non sapiente molestiae quam voluptate eligendi nesciunt molestiae.', '(jp)Cupiditate pariatur voluptas rem et quia.', NULL),
(13, 1, '(th)Odio dolores quasi sunt doloribus soluta ut quia.', '(th)Exercitationem similique fugit quas autem quo ducimus nobis.', NULL, '(th)Odio dolores quasi sunt doloribus soluta ut quia.', '(th)Exercitationem similique fugit quas autem quo ducimus nobis.', NULL),
(13, 2, '(en)Repellat pariatur beatae debitis ipsam sed molestias officiis odio.', '(en)Aliquid amet provident recusandae velit incidunt.', NULL, '(en)Repellat pariatur beatae debitis ipsam sed molestias officiis odio.', '(en)Aliquid amet provident recusandae velit incidunt.', NULL),
(13, 3, '(jp)Ipsam quos aut omnis porro est nisi illo.', '(jp)Consequatur exercitationem ea sit quia aut iure maiores.', NULL, '(jp)Ipsam quos aut omnis porro est nisi illo.', '(jp)Consequatur exercitationem ea sit quia aut iure maiores.', NULL),
(14, 1, '(th)Assumenda optio eum dolorum sit.', '(th)Culpa assumenda fugit veritatis aut quidem.', NULL, '(th)Assumenda optio eum dolorum sit.', '(th)Culpa assumenda fugit veritatis aut quidem.', NULL),
(14, 2, '(en)Accusantium eum sed laboriosam ipsa similique aut perspiciatis.', '(en)Ut vero aspernatur qui ducimus nulla et aperiam.', NULL, '(en)Accusantium eum sed laboriosam ipsa similique aut perspiciatis.', '(en)Ut vero aspernatur qui ducimus nulla et aperiam.', NULL),
(14, 3, '(jp)Consequatur est ut vel aut aliquam facere cupiditate est.', '(jp)Consequuntur eos quis repellat laudantium quas.', NULL, '(jp)Consequatur est ut vel aut aliquam facere cupiditate est.', '(jp)Consequuntur eos quis repellat laudantium quas.', NULL),
(15, 1, '(th)Nulla corrupti excepturi nisi et facere sint.', '(th)Et occaecati suscipit cum aliquam praesentium facilis et.', NULL, '(th)Nulla corrupti excepturi nisi et facere sint.', '(th)Et occaecati suscipit cum aliquam praesentium facilis et.', NULL),
(15, 2, '(en)Est veritatis et eum voluptatem sit.', '(en)Consequatur incidunt sed officia soluta necessitatibus.', NULL, '(en)Est veritatis et eum voluptatem sit.', '(en)Consequatur incidunt sed officia soluta necessitatibus.', NULL),
(15, 3, '(jp)Omnis et qui quo.', '(jp)Nihil blanditiis doloremque optio eos assumenda aliquid.', NULL, '(jp)Omnis et qui quo.', '(jp)Nihil blanditiis doloremque optio eos assumenda aliquid.', NULL),
(16, 1, '(th)Ea odio beatae quidem suscipit mollitia quia aut.', '(th)Et est quia iste praesentium eveniet animi tempore repellendus.', NULL, '(th)Ea odio beatae quidem suscipit mollitia quia aut.', '(th)Et est quia iste praesentium eveniet animi tempore repellendus.', NULL),
(16, 2, '(en)Minima qui fugiat adipisci laudantium.', '(en)Consequatur animi facilis quos dolorem eos aut.', NULL, '(en)Minima qui fugiat adipisci laudantium.', '(en)Consequatur animi facilis quos dolorem eos aut.', NULL),
(16, 3, '(jp)Omnis aut accusamus ex consequatur similique qui.', '(jp)Fugit libero minima ea.', NULL, '(jp)Omnis aut accusamus ex consequatur similique qui.', '(jp)Fugit libero minima ea.', NULL),
(17, 1, '(th)Officia aspernatur eveniet aut quia eum.', '(th)Modi quia quaerat quis voluptatum veniam qui.', NULL, '(th)Officia aspernatur eveniet aut quia eum.', '(th)Modi quia quaerat quis voluptatum veniam qui.', NULL),
(17, 2, '(en)Assumenda necessitatibus quam nihil qui.', '(en)Et labore voluptatem rerum temporibus.', NULL, '(en)Assumenda necessitatibus quam nihil qui.', '(en)Et labore voluptatem rerum temporibus.', NULL),
(17, 3, '(jp)Qui natus at et nihil et laudantium debitis.', '(jp)Voluptas incidunt quos earum.', NULL, '(jp)Qui natus at et nihil et laudantium debitis.', '(jp)Voluptas incidunt quos earum.', NULL),
(18, 1, '(th)Voluptas ut et labore error.', '(th)Ducimus quo ut labore nulla voluptatem.', NULL, '(th)Voluptas ut et labore error.', '(th)Ducimus quo ut labore nulla voluptatem.', NULL),
(18, 2, '(en)Aut eos consequatur minima.', '(en)Est alias voluptas a quis ea blanditiis.', NULL, '(en)Aut eos consequatur minima.', '(en)Est alias voluptas a quis ea blanditiis.', NULL),
(18, 3, '(jp)Sit ducimus modi et sapiente ipsam nesciunt quisquam.', '(jp)Vel molestiae minima beatae eos debitis eos.', NULL, '(jp)Sit ducimus modi et sapiente ipsam nesciunt quisquam.', '(jp)Vel molestiae minima beatae eos debitis eos.', NULL),
(19, 1, '(th)Voluptas in autem sit sunt provident pariatur.', '(th)Tempora aut quos recusandae architecto.', NULL, '(th)Voluptas in autem sit sunt provident pariatur.', '(th)Tempora aut quos recusandae architecto.', NULL),
(19, 2, '(en)Illo incidunt ullam et officia odit.', '(en)Modi cupiditate omnis totam voluptatem ducimus.', NULL, '(en)Illo incidunt ullam et officia odit.', '(en)Modi cupiditate omnis totam voluptatem ducimus.', NULL),
(19, 3, '(jp)Qui recusandae voluptates omnis est natus velit.', '(jp)Suscipit distinctio sed eum error.', NULL, '(jp)Qui recusandae voluptates omnis est natus velit.', '(jp)Suscipit distinctio sed eum error.', NULL),
(20, 1, '(th)Voluptatum aliquid aliquid iusto odit ab rerum esse.', '(th)Nulla est eos voluptatem doloribus quia fuga quia.', NULL, '(th)Voluptatum aliquid aliquid iusto odit ab rerum esse.', '(th)Nulla est eos voluptatem doloribus quia fuga quia.', NULL),
(20, 2, '(en)Molestiae pariatur ad ducimus ratione repellat.', '(en)Similique voluptas rem illo distinctio aliquam nobis.', NULL, '(en)Molestiae pariatur ad ducimus ratione repellat.', '(en)Similique voluptas rem illo distinctio aliquam nobis.', NULL),
(20, 3, '(jp)Dolores unde et itaque in.', '(jp)Blanditiis maiores tempore impedit ullam quis temporibus aliquid.', NULL, '(jp)Dolores unde et itaque in.', '(jp)Blanditiis maiores tempore impedit ullam quis temporibus aliquid.', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `article_images`
--

CREATE TABLE `article_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article_images`
--

INSERT INTO `article_images` (`id`, `article_id`, `image`, `sort_order`) VALUES
(1, 20, 'catalog/mockup/mockup-5.png', 1),
(2, 20, 'catalog/mockup/mockup-9.png', 2),
(3, 20, 'catalog/mockup/mockup-7.png', 3);

-- --------------------------------------------------------

--
-- Table structure for table `article_to_categories`
--

CREATE TABLE `article_to_categories` (
  `article_id` int(10) UNSIGNED NOT NULL,
  `article_category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article_to_categories`
--

INSERT INTO `article_to_categories` (`article_id`, `article_category_id`) VALUES
(1, 4),
(1, 5),
(1, 6),
(3, 6),
(3, 7),
(3, 8),
(4, 9),
(4, 10),
(4, 11),
(4, 12),
(4, 13),
(4, 14),
(4, 15),
(4, 16),
(4, 17),
(4, 18),
(4, 19),
(5, 9),
(5, 10),
(5, 11),
(5, 12),
(5, 13),
(5, 14),
(7, 13),
(7, 14),
(7, 15),
(7, 16),
(7, 17),
(7, 18),
(8, 9),
(8, 10),
(8, 11),
(8, 12),
(8, 13),
(8, 14),
(8, 15),
(8, 16),
(9, 18),
(10, 8),
(10, 9),
(10, 10),
(10, 11),
(10, 12),
(10, 13),
(10, 14),
(10, 15),
(10, 16),
(10, 17),
(10, 18),
(11, 11),
(11, 12),
(11, 13),
(11, 14),
(11, 15),
(11, 16),
(12, 1),
(12, 2),
(12, 3),
(12, 4),
(13, 15),
(13, 16),
(13, 17),
(13, 18),
(14, 6),
(14, 7),
(14, 8),
(14, 9),
(14, 10),
(15, 7),
(15, 8),
(15, 9),
(15, 10),
(15, 11),
(16, 18),
(16, 19),
(17, 7),
(18, 15),
(18, 16),
(18, 17),
(18, 18),
(18, 19),
(19, 2),
(19, 3),
(19, 4),
(19, 5),
(20, 12),
(20, 13),
(20, 14);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background_color` varchar(6) COLLATE utf8_unicode_ci DEFAULT 'FFFFFF',
  `publish_start` datetime NOT NULL,
  `publish_stop` datetime DEFAULT NULL,
  `sort_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `name`, `image`, `link`, `background_color`, `publish_start`, `publish_stop`, `sort_order`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Quis rerum accusamus pariatur qui et eum beatae.', 'catalog/mockup/mockup-banner-1.png', NULL, 'D81638', '2018-11-26 16:31:11', NULL, 1, 1, '2018-11-27 09:31:11', '2018-11-27 10:01:00'),
(2, 'Veritatis molestiae ad qui quas et eaque.', 'catalog/mockup/mockup-banner-2.png', NULL, 'ff5138', '2018-11-27 16:31:11', NULL, 2, 1, '2018-11-27 09:31:11', '2018-11-27 09:31:11'),
(3, 'Eum maxime assumenda nam aut aut velit quia qui.', 'catalog/mockup/mockup-banner-3.png', NULL, '28416a', '2018-11-27 16:31:11', NULL, 3, 1, '2018-11-27 09:31:11', '2018-11-27 09:31:11'),
(4, 'Mollitia sapiente eos blanditiis provident.', 'catalog/mockup/mockup-banner-4.png', NULL, '002a52', '2018-11-27 16:31:11', NULL, 4, 1, '2018-11-27 09:31:11', '2018-11-27 09:31:11'),
(5, 'Quaerat aut et facilis dolore vel veritatis.', 'catalog/mockup/mockup-banner-5.png', NULL, '4b73a7', '2018-11-27 16:31:11', NULL, 5, 1, '2018-11-27 09:31:11', '2018-11-27 09:31:11');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `directory` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ภาษาไทย', 'th', 'en_TH.UTF-8,en_TH,en-th,thai', 'th.png', 'th', 1, 1, '2018-11-27 09:31:03', '2018-11-27 09:31:03'),
(2, 'English', 'en', 'en-US,en_US.UTF-8,en_US,en-gb,english', 'en.png', 'en', 2, 1, '2018-11-27 09:31:03', '2018-11-27 09:31:03'),
(3, '日本人', 'jp', 'en-JP.UTF-8,en_JP,en-jp,japan', 'jp.png', 'jp', 3, 1, '2018-11-27 09:31:03', '2018-11-27 09:31:03');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(73, '2018_11_09_035740_create_password_resets_table', 1),
(74, '2018_11_09_035741_create_roles_table', 1),
(75, '2018_11_09_050505_create_users_table', 1),
(76, '2018_11_09_050506_create_role_user_table', 1),
(77, '2018_11_09_151410_create_languages_table', 1),
(78, '2018_11_09_155812_create_article_categories_table', 1),
(79, '2018_11_09_160343_create_article_category_descriptions_table', 1),
(80, '2018_11_14_111248_create_articles_table', 1),
(81, '2018_11_14_111334_create_article_descriptions_table', 1),
(82, '2018_11_14_111427_create_article_images_table', 1),
(83, '2018_11_14_111512_create_article_to_categories_table', 1),
(84, '2018_11_27_131253_create_banners_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, 'user/access', 'User Access', 1, '2018-11-27 09:31:04', '2018-11-27 09:31:04'),
(2, 'user/modify', 'User Modify', 2, '2018-11-27 09:31:04', '2018-11-27 09:31:04'),
(3, 'filemanager/access', 'Filemanager Access', 3, '2018-11-27 09:31:04', '2018-11-27 09:31:04'),
(4, 'filemanager/modify', 'Filemanager Modify', 4, '2018-11-27 09:31:04', '2018-11-27 09:31:04'),
(5, 'article_category/access', 'Article Caregory Access', 5, '2018-11-27 09:31:04', '2018-11-27 09:31:04'),
(6, 'article_category/modify', 'Article Caregory Modify', 6, '2018-11-27 09:31:04', '2018-11-27 09:31:04'),
(7, 'article/access', 'Article Access', 7, '2018-11-27 09:31:04', '2018-11-27 09:31:04'),
(8, 'article/modify', 'Article Modify', 8, '2018-11-27 09:31:04', '2018-11-27 09:31:04'),
(9, 'banner/access', 'Banner Access', 8, '2018-11-27 09:31:04', '2018-11-27 09:31:04'),
(10, 'banner/modify', 'Banner Modify', 9, '2018-11-27 09:31:04', '2018-11-27 09:31:04');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(4, 2),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(7, 1),
(7, 2),
(8, 1),
(8, 2),
(9, 1),
(9, 2),
(10, 1),
(10, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `image`, `last_login`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Korn', 'kornthebkk@gmail.com', '$2y$10$2XRRiT73k08iZW43u5tVS.JyyUTaJWYTc2gqSs1YpTKWrFd8tgkO6', 'catalog/users/David-Beckham.jpg', '2018-11-27 16:50:29', 1, 'NqW6HS67xDJEOhiYfT15NvCOx0qkbsUtv3kvgnlDMNsBEppbqDWkjMkOb7bx', '2018-11-27 09:31:04', '2018-11-27 09:51:57'),
(2, 'Webmaster', 'webmaster@tni.ac.th', '$2y$10$zU6HdREIZAeI3bRV7r7f3uALnPu.yiMn.rBSbefwibwXA5sT7GBni', 'catalog/users/Natalie-Portman.jpeg', '2018-11-28 10:34:18', 1, 'cBXnC1zz4wntFcQfiLB3OQvgEsOFzPR3gPxELcv5geU96xBdZvdx2hb1YBHX', '2018-11-27 09:31:05', '2018-11-28 03:34:18'),
(3, 'Sonya Wilkinson', 'arlie.barton@example.net', '$2y$10$lE2ntkHWtZwcXxLX3lH76eNTVW0HWog7hx3KUywy1nfh7Ttno3L9i', NULL, NULL, 0, 'KJmwLlEYdW', '2018-11-27 09:31:05', '2018-11-27 09:31:05'),
(4, 'Dr. Quincy Pfannerstill DDS', 'carroll.dedrick@example.net', '$2y$10$lE2ntkHWtZwcXxLX3lH76eNTVW0HWog7hx3KUywy1nfh7Ttno3L9i', NULL, NULL, 1, 'f3bVIA2679', '2018-11-27 09:31:05', '2018-11-27 09:31:05'),
(5, 'Sydnee Von', 'harmony95@example.com', '$2y$10$lE2ntkHWtZwcXxLX3lH76eNTVW0HWog7hx3KUywy1nfh7Ttno3L9i', NULL, NULL, 0, 'ztzFteUJh1', '2018-11-27 09:31:05', '2018-11-27 09:31:05'),
(6, 'Donna Schaefer MD', 'fmarks@example.com', '$2y$10$lE2ntkHWtZwcXxLX3lH76eNTVW0HWog7hx3KUywy1nfh7Ttno3L9i', NULL, NULL, 1, 'ZP6YhiyBMr', '2018-11-27 09:31:05', '2018-11-27 09:31:05'),
(7, 'Imogene Koss', 'abshire.rosalinda@example.com', '$2y$10$lE2ntkHWtZwcXxLX3lH76eNTVW0HWog7hx3KUywy1nfh7Ttno3L9i', NULL, NULL, 1, 'IDpjkgJFrD', '2018-11-27 09:31:05', '2018-11-27 09:31:05'),
(8, 'Prof. Corrine Kulas III', 'rowena.tillman@example.org', '$2y$10$lE2ntkHWtZwcXxLX3lH76eNTVW0HWog7hx3KUywy1nfh7Ttno3L9i', NULL, NULL, 1, 'xupUrfdU1n', '2018-11-27 09:31:05', '2018-11-27 09:31:05'),
(9, 'Prof. Laurence Rippin', 'dallin.mohr@example.org', '$2y$10$lE2ntkHWtZwcXxLX3lH76eNTVW0HWog7hx3KUywy1nfh7Ttno3L9i', NULL, NULL, 1, 'tsxqMeorKz', '2018-11-27 09:31:05', '2018-11-27 09:31:05'),
(10, 'Miss Thalia Batz DVM', 'garland82@example.net', '$2y$10$lE2ntkHWtZwcXxLX3lH76eNTVW0HWog7hx3KUywy1nfh7Ttno3L9i', NULL, NULL, 0, '20KFpooKqW', '2018-11-27 09:31:05', '2018-11-27 09:31:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_categories`
--
ALTER TABLE `article_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_category_descriptions`
--
ALTER TABLE `article_category_descriptions`
  ADD PRIMARY KEY (`article_category_id`,`language_id`),
  ADD KEY `article_category_descriptions_article_category_id_index` (`article_category_id`),
  ADD KEY `article_category_descriptions_language_id_index` (`language_id`);

--
-- Indexes for table `article_descriptions`
--
ALTER TABLE `article_descriptions`
  ADD PRIMARY KEY (`article_id`,`language_id`),
  ADD KEY `article_descriptions_article_id_index` (`article_id`),
  ADD KEY `article_descriptions_language_id_index` (`language_id`);

--
-- Indexes for table `article_images`
--
ALTER TABLE `article_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_images_article_id_index` (`article_id`);

--
-- Indexes for table `article_to_categories`
--
ALTER TABLE `article_to_categories`
  ADD PRIMARY KEY (`article_id`,`article_category_id`),
  ADD KEY `article_to_categories_article_category_id_foreign` (`article_category_id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `languages_code_index` (`code`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_password_index` (`password`),
  ADD KEY `users_status_index` (`status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `article_categories`
--
ALTER TABLE `article_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `article_images`
--
ALTER TABLE `article_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `article_category_descriptions`
--
ALTER TABLE `article_category_descriptions`
  ADD CONSTRAINT `article_category_descriptions_article_category_id_foreign` FOREIGN KEY (`article_category_id`) REFERENCES `article_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `article_category_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `article_descriptions`
--
ALTER TABLE `article_descriptions`
  ADD CONSTRAINT `article_descriptions_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `article_descriptions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `article_images`
--
ALTER TABLE `article_images`
  ADD CONSTRAINT `article_images_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `article_to_categories`
--
ALTER TABLE `article_to_categories`
  ADD CONSTRAINT `article_to_categories_article_category_id_foreign` FOREIGN KEY (`article_category_id`) REFERENCES `article_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `article_to_categories_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
