<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_descriptions', function (Blueprint $table) {
            $table->integer('event_id')->unsigned()->index();
            $table->integer('language_id')->unsigned()->index();
            $table->string('name', 255);
            $table->text('description')->nullable();
            $table->text('tag')->nullable();
            $table->string('meta_title', 255);
            $table->string('meta_description')->nullable();
            $table->string('meta_keyword')->nullable();
            //$table->timestamps();

            $table->primary(['event_id', 'language_id'], 'event_descriptions_primary');

            $table->foreign('language_id')
                ->references('id')->on('languages')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('event_id')
                ->references('id')->on('events')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_descriptions');
    }
}
