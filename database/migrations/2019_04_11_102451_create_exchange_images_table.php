<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exchange_program_id')->unsigned()->index();
            $table->string('image', 255)->nullable();
            $table->integer('sort_order')->unsigned();

            $table->foreign('exchange_program_id')
                ->references('id')->on('exchange_programs')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_images');
    }
}
