<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admission_to_categories', function (Blueprint $table) {
            $table->integer('admission_id')->unsigned();
            $table->integer('admission_category_id')->unsigned();

            $table->primary(['admission_id', 'admission_category_id'], 'admission_to_categories_primary');

            $table->foreign('admission_id')
                ->references('id')->on('admissions')
                ->onDelete('cascade');

            $table->foreign('admission_category_id')
                ->references('id')->on('admission_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_to_categories');
    }
}
