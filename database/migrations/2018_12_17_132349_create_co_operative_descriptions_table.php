<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoOperativeDescriptionsTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('co_operative_descriptions', function (Blueprint $table) {
            $table->integer('co_operative_id')->unsigned()->index();
            $table->integer('language_id')->unsigned()->index();
            $table->string('name', 255);
            $table->text('description')->nullable();
            $table->text('tag')->nullable();
            $table->string('meta_title', 255);
            $table->string('meta_description')->nullable();
            $table->string('meta_keyword')->nullable();
            

            $table->primary(['co_operative_id', 'language_id'], 'co_operative_descriptions_primary');

            $table->foreign('language_id')
                ->references('id')->on('languages')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('co_operative_id')
                ->references('id')->on('co_operatives')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('co_operative_descriptions');
    }
}