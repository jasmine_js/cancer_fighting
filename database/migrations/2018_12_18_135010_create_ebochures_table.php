<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbochuresTable extends Migration
{
        /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ebochures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image', 255)->nullable();
            $table->string('image_highlight', 255)->nullable();
            $table->string('pdf_link', 255);
            $table->dateTime('publish_start');
            $table->dateTime('publish_stop')->nullable();
            $table->integer('viewed')->unsigned()->default(0)->nullable();
            $table->boolean('status')->default(1)->nullable();
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ebochures');
    }
}
