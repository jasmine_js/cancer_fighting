<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admission_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admission_id')->unsigned()->index();
            $table->string('image', 255)->nullable();
            $table->integer('sort_order')->unsigned();

            $table->foreign('admission_id')
                ->references('id')->on('admissions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_images');
    }
}
