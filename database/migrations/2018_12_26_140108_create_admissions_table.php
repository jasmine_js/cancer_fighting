<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admissions' , function(Blueprint $table){
            $table->increments('id');
            $table->string('image',255)->nullable();
            $table->datetime('publish_start');
            $table->datetime('publish_stop')->nullable();
            $table->integer('viewed')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();//_create attribute create_at , update_up
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admissions');
    }
}
