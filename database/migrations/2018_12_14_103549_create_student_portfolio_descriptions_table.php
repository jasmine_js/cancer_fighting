<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentPortfolioDescriptionsTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('student_portfolio_descriptions', function (Blueprint $table) {
             $table->integer('student_portfolio_id')->unsigned()->index();
             $table->integer('language_id')->unsigned()->index();
             $table->string('name', 255);
             $table->text('description')->nullable();
             $table->text('tag')->nullable();
             $table->string('meta_title', 255);
             $table->string('meta_description')->nullable();
             $table->string('meta_keyword')->nullable();
             
 
             $table->primary(['student_portfolio_id', 'language_id'], 'student_portfolio_descriptions_primary');
 
             $table->foreign('language_id')
                 ->references('id')->on('languages')
                 ->onDelete('restrict')
                 ->onUpdate('cascade');
 
             $table->foreign('student_portfolio_id')
                 ->references('id')->on('student_portfolios')
                 ->onDelete('cascade');
 
         });
     }
 
     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('student_portfolio_descriptions');
     }
 }