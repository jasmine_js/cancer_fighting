<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::all();

        $user = new User();
        $user->name = 'Korn';
        $user->email = 'kornthebkk@gmail.com';
        $user->password = bcrypt('000000');
        $user->save();
        $user->roles()->attach($roles);

        $user = new User();
        $user->name = 'juntima';
        $user->email = 'juntima@tni.ac.th';
        $user->password = bcrypt('123456');
        $user->image ='catalog/users/38640375_298873737586663_7287199890759745536_n.jpg';
        $user->save();
        $user->roles()->attach($roles);

        $user = new User();
        $user->name = 'matha';
        $user->email = 'matha@tni.ac.th';
        $user->password = bcrypt('Tni12345');
        //$user->image ='catalog/users/38640375_298873737586663_7287199890759745536_n.jpg';
        $user->save();
        $user->roles()->attach($roles);

        $user = new User();
        $user->name = 'icc_tni';
        $user->email = 'icc@tni.ac.th';
        $user->password = bcrypt('Tni12345');
        //$user->image ='catalog/users/38640375_298873737586663_7287199890759745536_n.jpg';
        $user->save();
        $user->roles()->attach($roles);


    }
}
