<?php

use App\Admission;
use App\AdmissionCategory;
use App\AdmissionToCategory;
use Illuminate\Database\Seeder;

class AdmissionToCategoriesTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = AdmissionCategory::all();

        $items = Admission::all();

        foreach ($items as $item) {
            $related = $this->random_categories($categories);
            if ($related) {
                foreach ($related as $admission_category_id) {
                    $related_obj = new AdmissionToCategory();
                    $related_obj->admission_id = $item->id;
                    $related_obj->admission_category_id = $admission_category_id;
                    $related_obj->save();
                    unset($related_obj);
                }
            }
        }
    }

    private function random_categories($categories)
    {
        $index_last = count($categories) - 1;
        $random_begin = rand(0, $index_last);
        // echo $random_begin; die;
        $random_end = rand($random_begin, $index_last);
        // echo $random_begin . ', ' . $random_end; die;

        $arr_related = [];
        for ($i = $random_begin; $i < $random_end; $i++) {
            $arr_related[] = $categories[$i]->id;
        }
        return $arr_related;
    }
}
