<?php

use App\AdmissionCategory;
use App\AdmissionCategoryDescription;
use App\Language;
use Illuminate\Database\Seeder;

class AdmissionCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        // โหลดภาษาทั้งหมด
        $languages = Language::all();

        //จำลองข้อมูล
        $arrCategoriesName = ['ประกาศรับสมัครนักศึกษาใหม่','ประกาศผลสอบและผู้ได้รับทุน','ทุนการศึกษา','ค่าเทอม/อัตราค่าเรียน','สิทธิพิเศษและส่วนลด','รายละเอียดการรายงานตัว นศ.ใหม่'];

        for ($i = 1; $i <= 6; $i++) {

            $admission_category_id = $i;

            $category = new AdmissionCategory();
            //$category->image = $faker->imageUrl(300, 300);
            $category->sort_order = $admission_category_id;
            $category->created_at = date('Y-m-d H:i:s');
            $category->updated_at = date('Y-m-d H:i:s');
            $category->save();
            unset($category);

            //ลูปเพิ่มแถวตามจำนวนภาษาที่มี
            foreach ($languages as $language) {
                $k           = $i-1;
                $name        = $arrCategoriesName[$k];
                $description = $faker->sentence;

                $category_description                      = new AdmissionCategoryDescription();
                $category_description->admission_category_id = $admission_category_id;
                $category_description->language_id         = $language->id;
                $category_description->name                = '(' . $language->code . ')' . $name;
                $category_description->description         = '(' . $language->code . ')' . $description;
                $category_description->meta_title          = '(' . $language->code . ')' . $name;
                $category_description->meta_description    = '(' . $language->code . ')' . $description;
                $category_description->save();

                unset($category_description);
            }
        } //end for
    }
}
