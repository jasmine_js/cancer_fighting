<?php
use Illuminate\Database\Seeder;
use App\BannerHighlight;


class BannerHighlightsTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {

        $banners = [
            '1' => ['image' => 'catalog/mockup/mockup-banner-1.png'],
            '2' => ['image' => 'catalog/mockup/mockup-banner-2.png'],
            '3' => ['image' => 'catalog/mockup/mockup-banner-3.png'],
            '4' => ['image' => 'catalog/mockup/mockup-banner-4.png'],
            '5' => ['image' => 'catalog/mockup/mockup-banner-5.png'],
        ];

        //จำลองข้อมูล
        for ($i = 1; $i <= count($banners); $i++) {
            $item = new BannerHighlight();
            $item->name = $faker->sentence;
            $item->image = $banners[$i]['image'];
            // $item->background_color = $banners[$i]['background_color'];
            $item->publish_start = date('Y-m-d H:i:s');
            $item->sort_order = $i;
            $item->created_at = date('Y-m-d H:i:s');
            $item->updated_at = date('Y-m-d H:i:s');
            $item->save();
            unset($item);
        } //end for
    }
}
