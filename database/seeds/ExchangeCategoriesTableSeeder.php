<?php

use App\ExchangeCategory;
use App\ExchangeCategoryDescription;
use App\Language;
use Illuminate\Database\Seeder;

class ExchangeCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void 
     * }
   
     */

    public function run(Faker\Generator $faker)
    {
        // โหลดภาษาทั้งหมด
        $languages = Language::all();

        //จำลองข้อมูล
        $arrCategoriesName = ['รายละเอียดโครงการแลกเปลี่ยน','โครงการแลกเปลี่ยน',
        'บันทึกประสบการณ์นักศึกษาโครงการแลกเปลี่ยน ดูงานและทัศนศึกษา',
        'บันทึกประสบการณ์อาจารย์ / บุคลากรโครงการแลกเปลี่ยน ดูงานและทัศนศึกษา',
        'รายชื่อมหาวิทยาลัย / สถาบันต่างประเทศที่มีความร่วมมือทางวิชาการ'];

        for ($i = 1; $i <= 5; $i++) {

            $exchange_category_id = $i;

            $category = new ExchangeCategory();
            //$category->image = $faker->imageUrl(300, 300);
            $category->sort_order = $exchange_category_id;
            $category->created_at = date('Y-m-d H:i:s');
            $category->updated_at = date('Y-m-d H:i:s');
            $category->save();
            unset($category);

            //ลูปเพิ่มแถวตามจำนวนภาษาที่มี
            foreach ($languages as $language) {
                $k           = $i-1;
                $name        = $arrCategoriesName[$k];
                $description = $faker->sentence;

                $category_description                      = new ExchangeCategoryDescription();
                $category_description->exchange_category_id = $exchange_category_id;
                $category_description->language_id         = $language->id;
                $category_description->name                = '(' . $language->code . ')' . $name;
                $category_description->description         = '(' . $language->code . ')' . $description;
                $category_description->meta_title          = '(' . $language->code . ')' . $name;
                $category_description->meta_description    = '(' . $language->code . ')' . $description;
                $category_description->save();

                unset($category_description);
            }
        } //end for
    }
}
