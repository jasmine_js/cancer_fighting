<?php

use Illuminate\Database\Seeder;
use App\Ebochure;
use App\EbochureDescription;
use App\Language;

class EbochuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        // โหลดภาษาทั้งหมด
        $languages = Language::all();

        //จำลองข้อมูล
        for ($i = 1; $i <= 20; $i++) {

            $ebochure_id = $i;

            $item                = new Ebochure();
            $item->publish_start = date('Y-m-d H:i:s');
            $item->viewed        = $faker->numberBetween(100, 10000);
            $item->created_at    = date('Y-m-d H:i:s');
            $item->updated_at    = date('Y-m-d H:i:s');
            $item->pdf_link      = $faker->url;
            $item->save();
            unset($item);

            //ลูปเพิ่มแถวตามจำนวนภาษาที่มี
            foreach ($languages as $language) {
                $name        = $faker->sentence;
                $description = $faker->sentence;

                $item_description                   = new EbochureDescription();                   //Model
                $item_description->ebochure_id      = $ebochure_id;
                $item_description->language_id      = $language->id;
                $item_description->name             = '(' . $language->code . ')' . $name;
                $item_description->description      = '(' . $language->code . ')' . $description;
                $item_description->meta_title       = '(' . $language->code . ')' . $name;
                $item_description->meta_description = '(' . $language->code . ')' . $description;
                $item_description->link             = $faker->url;
                $item_description->save();

                unset($item_description);
            }
        } //end for
    }
}
