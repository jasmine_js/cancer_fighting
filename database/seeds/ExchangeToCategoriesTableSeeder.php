<?php

use App\ExchangeProgram;
use App\ExchangeCategory;
use App\ExchangeToCategory;
use Illuminate\Database\Seeder;

class ExchangeToCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ExchangeCategory::all();

        $items = ExchangeProgram::all();

        foreach ($items as $item) {
            $related = $this->random_categories($categories);
            if ($related) {
                foreach ($related as $exchange_category_id) {
                    $related_obj = new ExchangeToCategory();
                    $related_obj->exchange_program_id = $item->id;
                    $related_obj->exchange_category_id = $exchange_category_id;
                    $related_obj->save();
                    unset($related_obj);
                }
            }
        }
    }

    private function random_categories($categories)
    {
        $index_last = count($categories) - 1;
        $random_begin = rand(0, $index_last);
        // echo $random_begin; die;
        $random_end = rand($random_begin, $index_last);
        // echo $random_begin . ', ' . $random_end; die;

        $arr_related = [];
        for ($i = $random_begin; $i < $random_end; $i++) {
            $arr_related[] = $categories[$i]->id;
        }
        return $arr_related;
    }
}
