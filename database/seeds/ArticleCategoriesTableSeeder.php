<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

 
use App\ArticleCategory;
use App\ArticleCategoryDescription;
use App\Language;
use Illuminate\Database\Seeder;

class ArticleCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    /* public function run(Faker\Generator $faker)
    {
        // โหลดภาษาทั้งหมด
        $languages = Language::all();

        //จำลองข้อมูล
        for ($i = 1; $i <= 20; $i++) {

            $article_category_id = $i;

            $category = new ArticleCategory();
            //$category->image = $faker->imageUrl(300, 300);
            $category->sort_order = $article_category_id;
            $category->created_at = date('Y-m-d H:i:s');
            $category->updated_at = date('Y-m-d H:i:s');
            $category->save();
            unset($category);

            //ลูปเพิ่มแถวตามจำนวนภาษาที่มี
            foreach ($languages as $language) {
                $name = $faker->sentence;
                $description = $faker->sentence;

                $category_description = new ArticleCategoryDescription();
                $category_description->article_category_id = $article_category_id;
                $category_description->language_id = $language->id;
                $category_description->name = '(' . $language->code . ')' . $name;
                $category_description->description = '(' . $language->code . ')' . $description;
                $category_description->meta_title = '(' . $language->code . ')' . $name;
                $category_description->meta_description = '(' . $language->code . ')' . $description;
                $category_description->save();

                unset($category_description);
            }
        } //end for
    } */

    public function run(Faker\Generator $faker)
    {
        // โหลดภาษาทั้งหมด
        $languages = Language::all();

        //จำลองข้อมูล
        $arrCategoriesName = ['รับสมัครนักศึกษา','ผลงานนักศึกษา','สหกิจศึกษา','ทุน/โครงการแลกเปลี่ยน','งานวิจัย'];

        for ($i = 1; $i <= 5; $i++) {

            $article_category_id = $i;

            $category = new ArticleCategory();
            //$category->image = $faker->imageUrl(300, 300);
            $category->sort_order = $article_category_id;
            $category->created_at = date('Y-m-d H:i:s');
            $category->updated_at = date('Y-m-d H:i:s');
            $category->save();
            unset($category);

            //ลูปเพิ่มแถวตามจำนวนภาษาที่มี
            foreach ($languages as $language) {
                $k           = $i-1;
                $name        = $arrCategoriesName[$k];
                $description = $faker->sentence;

                $category_description                      = new ArticleCategoryDescription();
                $category_description->article_category_id = $article_category_id;
                $category_description->language_id         = $language->id;
                $category_description->name                = '(' . $language->code . ')' . $name;
                $category_description->description         = '(' . $language->code . ')' . $description;
                $category_description->meta_title          = '(' . $language->code . ')' . $name;
                $category_description->meta_description    = '(' . $language->code . ')' . $description;
                $category_description->save();

                unset($category_description);
            }
        } //end for
    }
}
