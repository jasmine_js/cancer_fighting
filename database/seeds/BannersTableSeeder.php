<?php

use App\Banner;
use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {

        $banners = [
            '1' => ['image' => 'catalog/mockup/mockup-banner-1.png', 'background_color' => 'd81638'],
            '2' => ['image' => 'catalog/mockup/mockup-banner-2.png', 'background_color' => 'ff5138'],
            '3' => ['image' => 'catalog/mockup/mockup-banner-3.png', 'background_color' => '28416a'],
            '4' => ['image' => 'catalog/mockup/mockup-banner-4.png', 'background_color' => '002a52'],
            '5' => ['image' => 'catalog/mockup/mockup-banner-5.png', 'background_color' => '4b73a7'],
        ];

        //จำลองข้อมูล
        for ($i = 1; $i <= count($banners); $i++) {
            $item = new Banner();
            $item->name = $faker->sentence;
            $item->image = $banners[$i]['image'];
            $item->background_color = $banners[$i]['background_color'];
            $item->publish_start = date('Y-m-d H:i:s');
            $item->sort_order = $i;
            $item->created_at = date('Y-m-d H:i:s');
            $item->updated_at = date('Y-m-d H:i:s');
            $item->save();
            unset($item);
        } //end for
    }
}
