<?php

use Illuminate\Database\Seeder;
use App\Finance;
use App\FinanceDescription;
use App\Language;

class FinancesTableSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        // โหลดภาษาทั้งหมด
        $languages = Language::all();

        //จำลองข้อมูล
        for ($i = 1; $i <= 20; $i++) {

            $finance_id = $i;

            $item = new Finance();
            $item->publish_start = date('Y-m-d H:i:s');
            $item->viewed = $faker->numberBetween(100, 10000);
            $item->created_at = date('Y-m-d H:i:s');
            $item->updated_at = date('Y-m-d H:i:s');
            $item->save();
            unset($item);

            //ลูปเพิ่มแถวตามจำนวนภาษาที่มี
            foreach ($languages as $language) {
                $name = $faker->sentence;
                $description = $faker->sentence;

                $item_description = new FinanceDescription(); //Model
                $item_description->finance_id = $finance_id;
                $item_description->language_id = $language->id;
                $item_description->name = '(' . $language->code . ')' . $name;
                $item_description->description = '(' . $language->code . ')' . $description;
                $item_description->meta_title = '(' . $language->code . ')' . $name;
                $item_description->meta_description = '(' . $language->code . ')' . $description;
                $item_description->save();

                unset($item_description);
            }
        } //end for
    }
}
