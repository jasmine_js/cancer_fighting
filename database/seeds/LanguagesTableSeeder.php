<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 */

use Illuminate\Database\Seeder;
use App\Language;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = new Language();
        $item->name = 'ภาษาไทย';
        $item->code = 'th';
        $item->locale = 'en_TH.UTF-8,en_TH,en-th,thai';
        $item->image = 'th.png';
        $item->directory = 'th';
        $item->sort_order = 1;
        $item->status = 1;
        $item->save();

        $item = new Language();
        $item->name = 'English';
        $item->code = 'en';
        $item->locale = 'en-US,en_US.UTF-8,en_US,en-gb,english';
        $item->image = 'en.png';
        $item->directory = 'en';
        $item->sort_order = 2;
        $item->status = 1;
        $item->save();

        $item = new Language();
        $item->name = '日本人';
        $item->code = 'jp';
        $item->locale = 'en-JP.UTF-8,en_JP,en-jp,japan';
        $item->image = 'jp.png';
        $item->directory = 'jp';
        $item->sort_order = 3;
        $item->status = 1;
        $item->save();
    }
}
