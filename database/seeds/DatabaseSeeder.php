<?php
/**
 * Nov 9, 2018, 9:16 AM
 * Developed by Korn <kornthebkk@gmail.com>
 * 
 * co-developer:
 * Metha <workersdontcry@gmail.com>
 * Juntima <>
 */
use Illuminate\Database\Seeder;
use App\BannerHighlight;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //เพิ่มข้อมูลภาษาสำหรับใช้งานจิรง
        $this->call(LanguagesTableSeeder::class);
        
        // เพิ่มข้อมูลสำหรับใช้งานจริงสำหรับ permission รายชื่อสิทธิ์การใช้งาน : ถ้ามีการเขียนโมดูลเพิ่ม ให้ไปเพิ่ม permission รายชื่อสิทธิ์ตาม pattern เดิมที่ RolesTableSeeder.php
        $this->call(RolesTableSeeder::class);

        // เพิ่มข้อมูลจำลองสำหรับผู้ใช้สูงสุดคนแรก
        $this->call(UsersTableSeeder::class);

        //เพิ่มข้อมูลจำลองผู้ใช้
        factory(App\User::class, 9)->create();

        //เพิ่มข้อมูลจำลองหมวดหมู่ข่าว
        $this->call(ArticleCategoriesTableSeeder::class);

        //เพิ่มข้อมูลจำลองข่าว
        $this->call(ArticlesTableSeeder::class);

        //เพิ่มข้อมูลจำลอง ความสัมพันธืแบบ many-to-many ของ ข่าว กับ หมวดหมู่
        $this->call(ArticleToCategoriesTable::class);

        //เพิ่มข้อมูลจำลอง home hero banner
       $this->call(BannersTableSeeder::class);

       //_added by Metha
       //เพิ่มข้อมูลจำลอง Events
       $this->call(EventsTableSeeder::class);

       //เพิ่มข้อมูลจำลอง รายการโครงการแลกเปลี่ยน 
       $this->call(ExchangeProgramsTableSeeder::class);

       //เพิ่มข้อมูลจำลองหมวดหมู่โครงการแลกเปลี่ยน 
       $this->call(ExchangeCategoriesTableSeeder::class);

       //เพิ่มข้อมูลจำลอง ความสัมพันธืแบบ many-to-many ของ โครงการแลกเปลี่ยน  กับ หมวดหมู่
       $this->call(ExchangeToCategoriesTableSeeder::class);


       //เพิ่มข้อมูลจำลอง สหกิจ
       $this->call(CoOperativesTableSeeder::class);
     
       //เพิ่มข้อมูลจำลอง ผลงานนักศึกษา
       $this->call(StudentPortfoliosTableSeeder::class);

       //เพิ่มข้อมูลจำลอง TNI Channel
       $this->call(VideosTableSeeder::class);

      //เพิ่มข้อมูลจำลอง Ebochure
      $this->call(EbochuresTableSeeder::class); 

      //เพิ่มข้อมูลจำลอง รับสมัคร
      $this->call(AdmissionsTableSeeder::class); 

      //เพิ่มข้อมูลจำลอง แบนเนอร์ไฮไลท์
      $this->call(BannerHighlightsTableSeeder::class); 

       //เพิ่มข้อมูลจำลองการเงิน
       $this->call(FinancesTableSeeder::class); 

    }
}
