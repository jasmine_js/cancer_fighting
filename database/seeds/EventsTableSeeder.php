
<?php

use Illuminate\Database\Seeder;
use App\Event;
use App\EventDescription;
use App\Language;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {

        /* $events = [
            '1' => ['image' => 'catalog/mockup/mockup-1.png'],
            '2' => ['image' => 'catalog/mockup/mockup-2.png'],
            '3' => ['image' => 'catalog/mockup/mockup-3.png'],
            '4' => ['image' => 'catalog/mockup/mockup-4.png'],
            '5' => ['image' => 'catalog/mockup/mockup-5.png'],
        ]; */

       // โหลดภาษาทั้งหมด
       $languages = Language::all();

       //จำลองข้อมูล
       //for ($i = 1; $i<= count($events); $i++) {
       for ($i = 1; $i<= 20; $i++) {

           $event_id = $i;

           $item = new Event();
           $item->event_date = date('Y-m-d');//date('d-m-Y');
           $item->publish_start = date('Y-m-d H:i:s');
           //$item->image = $events[$i]['image'];
           $item->viewed = $faker->numberBetween(100, 10000);
           $item->sort_order = $i;
           $item->created_at = date('Y-m-d H:i:s');
           $item->updated_at = date('Y-m-d H:i:s');
           $item->save();
           unset($item);

           //ลูปเพิ่มแถวตามจำนวนภาษาที่มี
           foreach ($languages as $language) {
               $name = $faker->sentence;
               $description = $faker->sentence;

               $item_description = new EventDescription();
               $item_description->event_id = $event_id;
               $item_description->language_id = $language->id;
               $item_description->name = '(' . $language->code . ')' . $name;
               $item_description->description = '(' . $language->code . ')' . $description;
               $item_description->meta_title = '(' . $language->code . ')' . $name;
               $item_description->meta_description = '(' . $language->code . ')' . $description;
               $item_description->save();
 
               unset($item_description);
           }
       } //end for
   }
}
