$(document).ready(function() {

    var insertImagePathReplace = 'storage'; // แก้ไขที่เก็บที่นี่
    var insertFilePathReplace = 'storage'; // แก้ไขที่เก็บที่นี่
    // console.log(site_url + '/backoffice/filemanager_doc');
    //_Added by Metha
    //_Create File Browser Button
        //_custom button for use in summernote's buttons
        var document_file_button = function (context) {
            
            var ui = $.summernote.ui;
            //_create button
            var button = ui.button({
                contents: '<i class="fa fa-file-o"/>',
                tooltip: 'File (pdf,docx,doc,xlsx,xls)',
                click: function () {
                    // invoke insertText method with 'hello' on editor module.
                    //context.invoke('editor.insertText', 'hello');
                    $.ajax({
                        //url:  "{{ url(Config::get('url.backoffice.filemanager_doc')) }}",
                        url:  site_url + '/backoffice/filemanager_doc',
                        dataType: 'html',
                        beforeSend: function() {
                            $('#button-image i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                            $('#button-image').prop('disabled', true);
                        },
                        complete: function() {
                            $('#button-image i').replaceWith('<i class="fa fa-upload"></i>');
                            $('#button-image').prop('disabled', false);
                        },
                        success: function(html) {
                            //alert(html);
                            $('body').append('<div id="modal-document" class="modal">' + html + '</div>');
                            $('#modal-document').modal('show');

                            //_click on file ,then create selected file link on editor
                            $('#modal-document').delegate('a.thumbnail', 'click', function(e) {

                                e.preventDefault();
                                var document_file_link = $(this).attr('href');
                                var document_file_name = $(this).find('input[type=hidden]').val();

                                $('[data-toggle=\'summernote\']').summernote('createLink', {
                                    text: document_file_name,
                                    url: document_file_link,
                                    isNewWindow: true //_whether link's target is new window or not
                                });
                                $('#modal-document').modal('hide');
                            });
                        },
                    });
                }
            });

            return button.render();   // return button as jquery object
        }
    //_End-Added by Metha


    // Override summernotes image manager
    $('[data-toggle=\'summernote\']').each(function() {
        var element = this;

        if ($(this).attr('data-lang')) {
            $('head').append('<script type="text/javascript" src="' + site_url + '/lib/summernote/lang/summernote-' + $(this).attr('data-lang') + '.js"></script>');
        }

        $(element).summernote({
            
            lang: $(this).attr('data-lang'),
            disableDragAndDrop: true,            
            height: 300,
            fontsize : 20,
            emptyPara: '',
            codemirror: { // codemirror options
                mode: 'text/html',
                htmlMode: true,
                lineNumbers: true,
                theme: 'monokai'
            },
            
            //fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '30', '36', '48', '64'],
            fontSizes: ['20','22','24', '26', '28','30', '36', '48', '64'],
            
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'image', 'video']],
                ['custom-button', ['document_file']],//_add by Metha
                ['view', ['fullscreen', 'codeview', 'help']]
            ],
            popover: {
                image: [
                    ['custom', ['imageAttributes']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ],
            },
            buttons: {
                image: function() {
                    var ui = $.summernote.ui;

                    // create button
                    var button = ui.button({
                        contents: '<i class="note-icon-picture" />',
                        tooltip: $.summernote.lang[$.summernote.options.lang].image.image,
                        click: function() {
                            $('#modal-image').remove();

                            $.ajax({
                                url: site_url + '/backoffice/filemanager',
                                dataType: 'html',
                                beforeSend: function() {
                                    $('#button-image i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                                    $('#button-image').prop('disabled', true);
                                },
                                complete: function() {
                                    $('#button-image i').replaceWith('<i class="fa fa-upload"></i>');
                                    $('#button-image').prop('disabled', false);
                                },
                                success: function(html) {
                                    $('body').append('<div id="modal-image" class="modal">' + html + '</div>');

                                    $('#modal-image').modal('show');

                                    $('#modal-image').delegate('a.thumbnail', 'click', function(e) {
                                        e.preventDefault();

                                        var image = $(this).attr('href');
                                        image = image.replace('backoffice', insertImagePathReplace);

                                        $(element).summernote('insertImage', image);

                                        $('#modal-image').modal('hide');
                                    });
                                }
                            });
                        }
                    });

                    return button.render();
                }

                ,document_file : document_file_button

            }

        });
       $(".note-current-fontsize").html('20');
       $('.note-editable').css('font-size','20px');
    });
});