@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')
<?php
//_ตัวแปรรับมาจากปุ่มในหน้าโฮม (ปุ่มใต้แบนเนอร์หลัก)
$faculty_clicked = isset($_GET['f']) ? $_GET['f'] : '' ;
$master_clicked = isset($_GET['m']) ? $_GET['m'] : '' ;
?>
    <div id="header" class="nav-item">
            <!-- <div class="container">
                <div class="second-header">{{ trans('main/home.text_faculty') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>
    </div><!-- End apply-now -->

    <div id="fac-course" class="container">
        <br>
        <div>
            <a href="#" class="shake" id='level_bachelor_fac'> {{ trans('main/home.text_bechelor') }}</a>
            <a href="#" class="shake" id='level_master_fac'>{{ trans('main/home.text_master') }}</a>
            <a href="http://inter.tni.ac.th/" class="shake" id='level_inter_fac' target="_blank">{{ trans('main/home.text_international_program') }}</a>

        </div>
    </div>



<div id='faculties2'>

    <div id="fac-nav-pec" class="hidden-sm hidden-xs">
        <div class="container">
            <div class="row" id='faculties' style="justify-content: center;">
                    <div class="home-nav-pec-item col-sm-6 col-md-3 col-lg-3 col-xl-3" id='fac_thumbnal_eng' >
                            <div id="fac_engineer">
                                <div style="background-image: #000;">
                                    {{-- @if($pec->link) --}}
                                    <a href="#" target="_blank"> <img src="img/faculty/eng/ENG_r.jpg" class="img-fluid" /></a>
                                    {{-- @else --}}
                                </div>

                            </div>
                            

                            <h1>{{ trans('main/faculty_level.text_ENG') }} </h1>
                            <h2 >{{ trans('main/faculty_level.text_eng') }}</h2>
                    </div>


                    <div class="home-nav-pec-item col-sm-6 col-md-3 col-lg-3 col-xl-3" id='fac_thumbnal_ba'>
                            <div id="ba">
                                <div style="background-image: #000;">
                                    {{-- @if($pec->link) --}}
                                    <a href="#" target="_blank"> <img src="img/faculty/ba/BA.jpg" class="img-fluid" /></a>
                                    {{-- @else --}}

                                </div>

                            </div>

                            <h1>{{ trans('main/faculty_level.text_BA') }} </h1>
                            <h2>{{ trans('main/faculty_level.text_ba') }} </h2>
                    </div>

                    <div class="home-nav-pec-item col-sm-6 col-md-3 col-lg-3 col-xl-3" id='fac_thumbnal_it'>
                            <div id="it">
                                <div style="background-image: #000;">
                                    {{-- @if($pec->link) --}}
                                    <a href="#" target="_blank"> <img src="img/faculty/inter/IT-INTER_r.jpg" class="img-fluid" /></a>
                                    {{-- @else --}}

                                </div>

                            </div>

                            <h1> {{ trans('main/faculty_level.text_IT') }}</h1>
                            <h2> {{trans('main/faculty_level.text_it') }} </h2>
                    </div>
                    <div class="home-nav-pec-item col-sm-6 col-md-3 col-lg-3 col-xl-3" id='fac_thumbnal_cgel'>
                        <div id="cgel">
                            <div style="background-image: #000;">
                              
                                <a href="#" target="_blank"> <img src="img/faculty/cgel/CGEL_r.jpg" class="img-fluid" /></a>
                             

                            </div>

                        </div>

                        <h1> {{ trans('main/faculty_level.text_CGEL') }} </h1>
                        <h2> {{ trans('main/faculty_level.text_cgel') }} </h2>
                </div>

                   

            </div>
        </div>
    </div>
</div>

@endsection

@section('main-content')


<?php

$text_more = trans('main/faculty.text_more');

$fac_template = <<<EOT
<div id="fac-line-%s" class="container">
    <h1>%s</h1>
        <h5 id="jp_style" style="color:#767676;"> %s </h5>
        <a href="%s" target="_blank"><h5 style="color:%s;font-weight: 600; text-align:center;"><b>{$text_more}</h5></a>

</div>
<div id="fac-line-%s">
    <div class="container">
        <div class="row" >
            <div class="col-xs-6 col-md-6 col-lg-6">
                <div class="fac-item">
                    <img src="%s" class="img-fluid" />
                </div>
            </div>
            <div class="col-xs-6  col-md-6 col-lg-6">
                <div class="fac-item">                                           
                    <h2 style="text-align:left;">%s</h2>
                    
                    <h4 style="text-align:left; ">%s</h4>
                    <h4 style="text-align:left;" >
                        <ul id="jp_style" style="list-style:circle;">
                            %s 
                        </ul>
                    </h4>

                    <h4 style="text-align:left; ">%s</h4>
                    <h4 style="text-align:left;" >
                        <ul id="jp_style" style="list-style:circle;">
                            %s 
                        </ul>
                    </h4>

                                             
                </div>
            </div>
        </div>
    </div> 
</div>
<div class="major-%s">
    <h1 >%s</h1> 
</div>

EOT;
$fac_template2 = <<<EOT

<div id="fac-line-%s">
    <div class="container">
        <div class="row" >
            <div class="col-xs-6 col-md-6 col-lg-6">
                <div class="fac-item">
                    <img src="%s" class="img-fluid" />
                </div>
            </div>
            <div class="col-xs-6  col-md-6 col-lg-6">
                <div class="fac-item" >
                    <h2>%s</h2>
                    <h5 id="jp_style_major" style="color:#767676; text-align:justify">
                         %s
                    </h5>
                    <a href="%s" target="_blank">
                        <h5 id="jp_style_major" style="color:%s;font-weight: 600; text-align:right;"><b>{$text_more}</h5>
                    </a>
                </div>
            </div>
        </div>
    </div> 
</div>

EOT;
$fac_template3 = <<<EOT

<div id="fac-line-%s">
<div class="container">
            <div class="row" >
                    <div class="col-xs-6 col-md-6 col-lg-6">
                         <div class="fac-item">
                            <h2>%s</h2>
                            <h5 id="jp_style_major" style="color:#767676; text-align:left">
                                    %s
                            </h5>
                            <a href="%s" target="_blank">
                                <h5 id="jp_style_major" style="color:%s;font-weight: 600;  text-align:right;">{$text_more}</h5>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-6 col-md-6 col-lg-6">
                        <div class="fac-item">
                            <img src="%s" class="img-fluid" />
                        </div>
                    </div>
            </div>
</div>
</div>

EOT;

$fac_template4 = <<<EOT

<div id="fac-line-%s">
<div class="container">
            <div class="row" >
                    <div class="col-xs-6 col-md-6 col-lg-6">
                         <div class="fac-item">
                            <h2>%s</h2>
                            
                            <h5 id="jp_style_major" style="color:#767676; text-align:left">
                                <ul id="jp_engul" style="padding-left: 17px;">
                                    %s 
                                </ul>
                             </h5>
                            <a href="%s" target="_blank">
                                <h5 id="jp_style_major" style="color:%s;font-weight: 600;  text-align:right;">{$text_more}</h5>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-6 col-md-6 col-lg-6">
                        <div class="fac-item">
                            <img src="%s" class="img-fluid" />
                        </div>
                    </div>
            </div>
</div>
</div>

EOT;

?>
<div id="fac_info"> 
    <?php
     
     $fac_info_result = sprintf($fac_template ,  'eng-white' 
                                ,trans('main/faculty_eng.text_name') 
                                ,trans('main/faculty_eng.text_detail')
                                // ,'https://eng.tni.ac.th/about-us/' 
                                ,'https://www.tni.ac.th/engineering/major'
                                ,'rgb(167, 0, 0)' ,'eng' //_id
                                ,'img/faculty/eng/ENG.jpg'
                                ,trans('main/faculty_eng.text_program') 
                                ,trans('main/faculty_eng.text_bachelor')
                                ,trans('main/faculty_eng.text_major')
                                ,trans('main/faculty_eng.text_bacherlor_inter')
                                ,trans('main/faculty_eng.text_inter_eng_major')                                  
                                ,'eng' ,trans('main/faculty.text_major') );

            $fac_info_result.= sprintf($fac_template2 ,'eng-white' 
                                ,'img/faculty/eng/AE.jpg'  //AE
                                ,trans('main/faculty_eng.text_major1')
                                ,trans('main/faculty_eng.text_detail_major1')
                                // ,'https://eng.tni.ac.th/education/automotive-engineering/'
                                ,'https://www.tni.ac.th/engineering/major_ae'
                                ,'rgb(167, 0, 0)' ); 

            $fac_info_result.= sprintf($fac_template4 ,'eng-gray'                                 
                                ,trans('main/faculty_eng.text_major2')
                                ,trans('main/faculty_eng.text_detail_major2')
                                // ,'https://eng.tni.ac.th/education/production-engineering/'
                                ,'https://www.tni.ac.th/engineering/major_re'
                                ,'rgb(167, 0, 0)' 
                                ,'img/faculty/eng/PE.jpg' );  //LE

            $fac_info_result.= sprintf($fac_template2 ,'eng-white' 
                                ,'img/faculty/eng/CE.jpg' //CE
                                ,trans('main/faculty_eng.text_major3')
                                ,trans('main/faculty_eng.text_detail_major3')
                                // ,'https://eng.tni.ac.th/education/computer-engineering/' 
                                ,'https://www.tni.ac.th/engineering/major_ce'
                                ,'rgb(167, 0, 0)' ); 

            $fac_info_result.= sprintf($fac_template3 ,'eng-gray'
                                 ,trans('main/faculty_eng.text_major4')
                                ,trans('main/faculty_eng.text_detail_major4')
                                // ,'https://eng.tni.ac.th/education/industrial-engineering/'
                                ,'https://www.tni.ac.th/engineering/major_ie'
                                ,'rgb(167, 0, 0)' 
                                ,'img/faculty/eng/IE.jpg');     //IE
            $fac_info_result.= sprintf($fac_template2 ,'eng-white' 
                                ,'img/faculty/eng/EE.jpg' //EE
                                ,trans('main/faculty_eng.text_major5')
                                ,trans('main/faculty_eng.text_detail_major5')
                                // ,'https://eng.tni.ac.th/education/electrical-engineering/'
                                ,'https://www.tni.ac.th/engineering/major_ee'
                                ,'rgb(167, 0, 0)' );
            $fac_info_result.= sprintf($fac_template3 ,'eng-gray' //inter DGE
                                ,trans('main/faculty_eng.text_inter_eng_major1')
                                ,trans('main/faculty_eng.text_detail_inter_eng_major1')
                                // ,'https://eng.tni.ac.th/education/digital-engineering/'
                                ,'https://www.tni.ac.th/engineering/major_dge'
                                ,'rgb(167, 0, 0)' 
                                ,'img/faculty/inter/ENG-INTER.jpg');                
                       
echo $fac_info_result;
    ?>
</div>

<div id="fac-line-footer">
<h2> {{ trans('main/faculty.text_alumni')}}</h2>
            <div class="swiper-container">
                <div class="swiper-wrapper" id="alumni">

                    <div class="swiper-slide" >
                        <img src="img/alumni/a1.jpg"  id ="img-graduate"  >
                        <h4 id="jp_alumni" style="color:#fff; font-weight: 600;">
                            <i class="fa fa-quote-left"></i>
                                 &nbsp;   {{ trans('main/faculty.text_comment1')}}  &nbsp;
                            <i class="fa fa-quote-right"></i>  <br><br>
                            {{-- <h5 style="color:#fff; font-weight: 600;"> {{ trans('main/faculty.text_job1')}} </h5> --}}
                        </h4>   <br/>
                        <h4 id="jp_alumni" style="color:#fff; font-weight: 600; ">{{ trans('main/faculty.text_major1')}} </h4> <br><br>
                        <h5 style="color:#fff; "> {{ trans('main/faculty.text_name1')}}   </h5>
                    </div>
                    
                    <div class="swiper-slide" >
                        <img src="img/alumni/a2.jpg"  id ="img-graduate"  >
                        <h4 id="jp_alumni" style="color:#fff; font-weight: 600;">
                            <i class="fa fa-quote-left"></i>
                                 &nbsp;  {{ trans('main/faculty.text_comment2')}}  &nbsp;
                            <i class="fa fa-quote-right"></i>  <br><br>
                            {{-- <h5 style="color:#fff; font-weight: 600;">{{ trans('main/faculty.text_job2')}}</h5> --}}
                        </h4>   <br/>
                        <h4 id="jp_alumni" style="color:#fff; font-weight: 600; ">{{ trans('main/faculty.text_major2')}} </h4> <br><br>
                        <h5 style="color:#fff; ">{{ trans('main/faculty.text_name2')}}</h5>
                    </div>

                    <div class="swiper-slide" >
                        <img src="img/alumni/a3.jpg"  id ="img-graduate"  >
                        <h4 id="jp_alumni" style="color:#fff; font-weight: 600;">
                            <i class="fa fa-quote-left"></i>
                                 &nbsp;  {{ trans('main/faculty.text_comment3')}}  &nbsp;
                            <i class="fa fa-quote-right"></i>  <br><br>
                            {{-- <h5 style="color:#fff; font-weight: 600;"> {{ trans('main/faculty.text_job3')}} </h5> --}}
                        </h4>   <br/>
                        <h4 id="jp_alumni" style="color:#fff; font-weight: 600; ">{{ trans('main/faculty.text_major3')}}</h4> <br><br>
                        <h5 style="color:#fff; "> {{ trans('main/faculty.text_name3')}}</h5>
                    </div>
                    <!-- add 19-12-2019 juntima -->
                    <div class="swiper-slide" >
                        <img src="img/alumni/a4.jpg"  id ="img-graduate"  >
                        <h4 id="jp_alumni" style="color:#fff; font-weight: 600;">
                            <i class="fa fa-quote-left"></i>
                                 &nbsp;  {{ trans('main/faculty.text_comment4')}}  &nbsp;
                            <i class="fa fa-quote-right"></i>  <br><br>
                            {{-- <h5 style="color:#fff; font-weight: 600;"> {{ trans('main/faculty.text_job3')}} </h5> --}}
                        </h4>   <br/>
                        <h4 id="jp_alumni" style="color:#fff; font-weight: 600; ">{{ trans('main/faculty.text_major4')}}</h4> <br><br>
                        <h5 style="color:#fff; "> {!! trans('main/faculty.text_name4')!!}</h5>
                    </div>

                    <div class="swiper-slide" >
                        <img src="img/alumni/a5.jpg"  id ="img-graduate"  >
                        <h4 id="jp_alumni" style="color:#fff; font-weight: 600;">
                            <i class="fa fa-quote-left"></i>
                                 &nbsp;  {{ trans('main/faculty.text_comment5')}}  &nbsp;
                            <i class="fa fa-quote-right"></i>  <br><br>
                            {{-- <h5 style="color:#fff; font-weight: 600;"> {{ trans('main/faculty.text_job3')}} </h5> --}}
                        </h4>   <br/>
                        <h4 id="jp_alumni" style="color:#fff; font-weight: 600; ">{{ trans('main/faculty.text_major5')}}</h4> <br><br>
                        <h5 style="color:#fff; "> {!!trans('main/faculty.text_name5')!!}</h5>
                    </div>

                    <div class="swiper-slide" >
                        <img src="img/alumni/a6.jpg"  id ="img-graduate"  >
                        <h4 id="jp_alumni" style="color:#fff; font-weight: 600;">
                            <i class="fa fa-quote-left"></i>
                                 &nbsp;  {{ trans('main/faculty.text_comment6')}}  &nbsp;
                            <i class="fa fa-quote-right"></i>  <br><br>
                            {{-- <h5 style="color:#fff; font-weight: 600;"> {{ trans('main/faculty.text_job3')}} </h5> --}}
                        </h4>   <br/>
                        <h4 id="jp_alumni" style="color:#fff; font-weight: 600; ">{{ trans('main/faculty.text_major6')}}</h4> <br><br>
                        <h5 style="color:#fff; "> {!! trans('main/faculty.text_name6')!!}</h5>
                    </div>
                    
                </div>
                <div class="swiper-pagination-graduate"></div>
            </div>
</div>    
 

@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')
<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script>
var homeHeroSwiper = new Swiper('#fac-line-footer .swiper-container', {
    loop: true,
    autoplay: {
        delay: 5000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination-graduate',
        clickable: true,
    },

});

</script>

<script>
    $(document).ready(function(){
         
          //_selects id that are preceded by 'fac_thumbnal'
        $("[id^='level_']").click(function(){
            
            level_selected = $(this).attr('id');//_ตัวแปรสำหรับส่งไปทำงานหน้าที่จะเรียกใน method Ajax
            
            $.ajax({
                url : '{{ url(Config::get('url.main.level_ajax')) }}'
               , type: 'get' //_เรียกหน้า fac_info_ajax.php แบบ post
               , data: {'level_selected_param':level_selected} //_ตัวแปรส่งไปหน้า level_ajax.php
            }).done(function(result){
              //_display faculty
                $('#faculties').html($.trim(result));
                    //_แสดงคณะ เมือคลิกเข้ามาครั้งแรก
                    if(level_selected=='level_bachelor_fac'){
                        faculty_selected = 'fac_thumbnal_eng';
                    }else if(level_selected=='level_master_fac'){
                        faculty_selected = 'fac_thumbnal_master1';
                     }//else if(level_selected=='level_inter_fac'){
                    //     faculty_selected = 'fac_thumbnal_inter1';
                    // }
                    //alert(level_selected);
                $.ajax({
                        url : '{{ url(Config::get('url.main.fac_ajax')) }}'
                    , type: 'get' //_เรียกหน้า fac_info_ajax.php แบบ post
                        , data: {'faculty_selected_param':faculty_selected} //_ตัวแปรส่งไปหน้า fac_info_ajax.php
                }).done(function(result){
                        //_display faculty info
                    $('#fac_info').html($.trim(result));
                });
      
            });
        });

            //_selects id that are preceded by 'fac_thumbnal'
        $("[id^='fac_thumbnal']").click(function(){
            
            faculty_selected = $(this).attr('id');//_ตัวแปรสำหรับส่งไปทำงานหน้าที่จะเรียกใน method Ajax
                //alert(faculty_selected);
            $.ajax({
                url : '{{ url(Config::get('url.main.fac_ajax')) }}'
                , type: 'get' //_เรียกหน้า fac_info_ajax.php แบบ post
                , data: {'faculty_selected_param':faculty_selected} //_ตัวแปรส่งไปหน้า fac_info_ajax.php
            }).done(function(result){
                    //_display faculty info
                $('#fac_info').html($.trim(result));
            });
          
        });

      
            

    });
</script>

<style>
    [id^='fac_thumbnal'],[id^='level_']{
          cursor:pointer;
    }   
</style>

<script>
        $(document).ready(function(){
          
            $('#level_bachelor_fac').addClass('active');
            
            $('[id^=level_]').click(function(){
          
                $('[id^=level_]').removeClass('active');
                $(this).addClass('active');
                
            });
            var faculty_click = "<?php echo $faculty_clicked;?>";
            var master_click = "<?php echo $master_clicked;?>";

            if(faculty_click=='master'){
                  
                $("#level_master_fac").trigger('click');

                if(master_click=='master_ba')
                    {
                        //   $("#level_master_fac").trigger('click');
                        $("#fac_thumbnal_master2").trigger('click');
                        
                        console.log("ba");
                    
                    }
                if(master_click=='master_en')
                    {
                        $("#fac_thumbnal_master1").trigger('click');
                        console.log("eng");
                    }
                if(master_click=='master_it')
                    {
                        $("#fac_thumbnal_master3").trigger('click');
                        console.log("IT");
                    }
            }
            
            // var faculty_click = "<?php echo $faculty_clicked;?>";
            // var master_click = "<?php echo $master_clicked;?>";

            // if(faculty_click=='master'){
                  
            //     $("#level_master_fac").trigger('click');

            //     if(master_click=='master_ba')
            //         {
            //             //   $("#level_master_fac").trigger('click');
            //               $("#fac_thumbnal_master2").trigger('click');
                        
            //             console.log("ba");
                    
            //         }
            //     if(master_click=='master_en')
            //         {
            //             $("#fac_thumbnal_master1").trigger('click');
            //             console.log("eng");
            //         }
            //     if(master_click=='master_it')
            //         {
            //             $("#fac_thumbnal_master3").trigger('click');
            //             console.log("IT");
            //         }
            // }
            
// alert(master_click);
             /*  if((faculty_click=='master') and (master_click=='master_en')){
                  $("#level_master_fac").trigger('click');
                  $("#fac_thumbnal_master1").trigger('click');
              }
              if((faculty_click=='master') and (master_click=='master_it')){
                  $("#level_master_fac").trigger('click');
                  $("#fac_thumbnal_master3").trigger('click');
              } */
            
             /*  if((faculty_click=='master') && (master_click=='master_ba')){
                  //$("#level_master_fac").trigger('click');
                  $("#fac_thumbnal_master2").trigger('click');
               */
            
        });
  
  </script>

@endsection

