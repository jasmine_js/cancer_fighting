@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')

    <div id="header" class="nav-item">

            <!-- <div class="container">
                <div class="second-header">{{ trans('main/contact.text_contact') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->
    <div id="home-course" class="container">

    </div>

   
@endsection


@section('main-content')



<div class="container">

    <div class="contact">
        <h3><b> {{ trans('main/policy.text_privacy_title') }} </b></h3>
        <h4 id="jp_policy" style="text-indent:1em;">
            {{ trans('main/policy.text_detail') }} <br>          
         
        </h4>  
      
        @if(count($policy_detail) > 0 )
            @for($i=0;$i<count($policy_detail);$i++)
                <h4 id="jp_policy" style="text-align:left;"><b>{{$policy_detail[$i]['list'] }}</b></h4>
                <h4 id="jp_policy" style="text-align:justify; text-indent:1em;" >{{$policy_detail[$i]['detail'] }}</h4>
         
            @endfor                             
        @endif

        <br>
        <h4 style="text-indent:1em;">{{ trans('main/policy.text_comment') }}</h4> 

        <br><br>
        <h4><b>{{ trans('main/policy.text_tni') }}</b></h4>
            <h4>{{ trans('main/policy.text_address') }}</h4> 
            <h4>{{ trans('main/policy.text_phone') }}</h4> 
            <h4> <a href="mailto:tniinfo@tni.ac.th">{{ trans('main/policy.text_email') }}</a></h4> 
    </div>

</div>{{-- end containner --}}
    
<br>
<br>
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">


@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

 <style>
        [id^='card'],[id^='accordion']{
          cursor:pointer;
        }
</style>



@endsection

