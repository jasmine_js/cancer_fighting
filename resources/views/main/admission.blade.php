@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')

    <div id="header" class="nav-item">

            <!-- <div class="container">
                <div class="second-header">{{ trans('main/admission.text_admission') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                
                                    <li class="active">{!! $key !!}</li>
                                @else
                               {{-- <i class="fas fa-align-center"></i> --}}
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->
    <div id="home-course" class="container">

    </div>

    <div class="container">
        <div class="row" id="card">
                <div class="col-xs-4 col-sm-12 col-md-8 col-lg-8" style="text-align:center;  ">

                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="history-item" >
                                <a href="#accordion">
                                    <div class="card text-white shake" >
                                        <div class="card-body" id="card_bec">
                                            <i class="fa fa-graduation-cap fa-3x"></i><br>{{ trans('main/home.text_bechelor') }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="history-item" >
                                <!-- <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=contents&category=75" target="_blank">}} -->
                                <a href="https://www.tni.ac.th/grad_programme/" target="_blank">
                                    <div class="card text-white shake" >
                                        <div class="card-body" id="card_mas">
                                            <i class="fa fa-graduation-cap fa-3x"></i><br>{{ trans('main/home.text_master') }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>                       
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="history-item" >
                                <a href="http://inter.tni.ac.th/page/Admission" target="_blank">
                                    <div class="card text-white shake">                                       
                                        <div class="card-body " id="card_inter">
                                            <i class="fa fa-graduation-cap fa-3x"></i><br>{{trans('main/home.text_international_program')}}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-xs-4 col-sm-14 col-md-4 col-lg-4" style="text-align:center">     

                    <div class="col-xs-14 col-sm-14 col-md-14 col-lg-14">
                        <div class="row">

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="history-item shake">
                                    <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=contents&category=70&id=61" target="_blank">
                                        <div class="card text-white" >
                                            <div class="card-body danger" >{{ trans('main/admission.text_scholarship') }}</div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="history-item shake">
                                    <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=news&categoryid=12&pagesize=20" target="_blank">
                                        <div class="card text-white" >
                                            <div class="card-body danger" >{{ trans('main/admission.text_benefit') }}</div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>

                    <br>

                    <div class="col-xs-14 col-sm-14 col-md-14 col-lg-14">
                      
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <div class="history-item shake">
                                        <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=contents&category=89&id=66" target="_blank">
                                            <div class="card text-white" >
                                                <div class="card-body danger" >                                                  
                                                    {{ trans('main/admission.text_fee') }} : {{ trans('main/admission.text_bechelor') }}                                                                                              
                                                </div>
                                            </div> 
                                        </a>                                   
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <div class="history-item shake">
                                        <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=contents&category=88&id=15" target="_blank">
                                            <div class="card text-white" >
                                                <div class="card-body danger" >                                                                                              
                                                    {{ trans('main/admission.text_fee') }} :  {{ trans('main/admission.text_master') }}                                                     
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>       
                    </div>

                </div>
        </div>   {{-- end row card --}}

    </div>{{-- end containner --}}
        
@endsection


@section('main-content')
<br>
<div class="container">
    <div class="row">

        <div class="col-xs-8  col-md-8 col-lg-8">
            <div id="accordion" role="tablist">
                                       
                <div id="dropdown-bachelor_1"  style="text-indent:10%;" data-toggle="collapse" href="#collapse1" >
                    {{ trans('main/home.text_bechelor') }}           
                </div>

                <!-- _SHOW Admisson Form Link DATA -->
                <div id="collapse1" class="collapse show" role="tabpanel" aria-labelledby="heading1" data-parent="#accordion">
                         <?php
                         print_r($admissions);
                         ?>
                         {{-- {{strtotime(date('Y-m-d H:i:s'))}} --}}
                        @if(count($admissions) > 0)
                            {{-- $admissions มาจากโมเดล admissiondescriptio หน้า controller --}}
                            
                            @foreach($admissions as $item)
                                <a href="{{$item->link}}" target="_blank" >
                                    <div class="card-body bacherlor ">
                                      {{ $item->name}} <span class="badge badge-pill badge-danger ml-2">{{trans('main/admission.text_click')}} &nbsp;</span>
                                    </div>
                                </a>
                            @endforeach
                    @else
                           <div style='text-align:center;margin-top:10px'><b>{{ trans('main/common.text_not_found_data') }}</b></div>
                    @endif
                </div> 
                
                {{-- <div id="collapse1" class="collapse show" role="tabpanel" aria-labelledby="heading1" data-parent="#accordion">
                
                    <div class="card-body bacherlor">
                         {{ trans('main/home.text_quaota') }}  
                    </div>
                     <div class="card-body bacherlor">
                        {{ trans('main/home.text_direct_exam') }}  
                    </div>
                    <div class="card-body bacherlor">
                        {{ trans('main/home.text_direct_admission') }}  
                    </div>
                    <div class="card-body bacherlor">
                        {{ trans('main/home.text_portfolio') }}  
                    </div>                     
              
                </div> end collapse 1 --}}

                <br>        

                {{-- <div id="dropdown-bachelor_2"  style="text-indent:10%;" data-toggle="collapse" href="#collapse2">
                    หลักสูตรปริญญา 2 ใบ   {{--  collaps bachelor 2                     
                </div>
           
                <div id="collapse2" class="collapse " role="tabpanel" aria-labelledby="heading2" data-parent="#accordion">

                    <div class="card-body bacherlor">
                        {{ trans('main/home.text_quaota') }}  
                    </div>
                    <div class="card-body bacherlor">
                        {{ trans('main/home.text_direct_exam') }}  
                    </div>
                    <div class="card-body bacherlor">
                        {{ trans('main/home.text_direct_admission') }}  
                    </div>
                    <div class="card-body bacherlor">
                        {{ trans('main/home.text_portfolio') }}  
                    </div>      

               </div>  end collapse 2 --}}

            </div> {{-- end accordion --}}

            <br>
           
                <div class="row">

                    <div class="col-sm-6 col-md-6 col-xs-6 col-xl-6"  id="question">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                        <i class="fa fa-edit" ></i>{{ trans('main/admission.text_faq') }}
                    </div>
                
                    {{-- <div class="col-sm-6 col-md-6 col-xs-6 col-xl-6" style="text-align:right">
                        <i class="fa fa-bars"></i> &nbsp;{{ trans('main/admission.text_faq_all') }}
                    </div>  --}}

                </div> 
                <br>

            <div id="accordion1">

                <div class="card-header" id="faq-1">
                    <h5 class="mb-0" >
                        <a class="collapsed" tyle="text-indent:10%;" data-toggle="collapse" href="#faq1" aria-expanded="true" aria-controls="faq1">
                            {{ trans('main/admission.text_question1') }}  
                        </a>
                    </h5>
                </div>
                <div id="faq1" class="collapse show" data-parent="#accordion1" aria-labelledby="faq-1">
                    <div class="card-body faq">
                        {{trans('main/admission.text_answer1') }} 
                      
                    </div>  
                </div>   {{-- faq1 --}}
                
                <br>        
    
                <div class="card-header" id="faq-2">
                    <h5 class="mb-0">
                        <a class="collapsed"  tyle="text-indent:10%;" data-toggle="collapse" href="#faq2" aria-expanded="false" aria-controls="faq2">
                            {{ trans('main/admission.text_question2') }}    
                        </a>
                    </h5>
                </div>
                <div id="faq2" class="collapse " data-parent="#accordion1" aria-labelledby="faq-2">   
                    <div class="card-body faq">
                        {{ trans('main/admission.text_answer2') }}  
                    </div>
                </div>  {{-- faq2 --}}           

                <br>   

                <div class="card-header" id="faq-3">
                    <h5 class="mb-0">
                        <a class="collapsed"  tyle="text-indent:10%;" data-toggle="collapse" href="#faq3" aria-expanded="false" aria-controls="faq3">
                            {{ trans('main/admission.text_question3') }}   
                        </a>
                    </h5>
                </div>
                <div id="faq3" class="collapse" data-parent="#accordion1" aria-labelledby="faq-3">
                    <div class="card-body faq">
                        {!! trans('main/admission.text_answer3')!!}  
                    </div>  
                </div> {{-- faq3 --}}
                 
                <br>        
    
                <div class="card-header" id="faq-4">
                    <h5 class="mb-0">
                        <a class="collapsed"  tyle="text-indent:10%;" data-toggle="collapse" href="#faq4" aria-expanded="false" aria-controls="faq4">
                            {{ trans('main/admission.text_question4') }}                    
                        </a>
                    </h5>
                </div>
                <div id="faq4" class="collapse " data-parent="#accordion1" aria-labelledby="faq-4">   
                    <div class="card-body faq">
                        {!! trans('main/admission.text_answer4')!!}  
                    </div>
                </div> {{-- faq4 --}}

                <br>
                <div class="card-header" id="faq-5">
                    <h5 class="mb-0">
                        <a class="collapsed"  tyle="text-indent:10%;" data-toggle="collapse" href="#faq5" aria-expanded="false" aria-controls="faq5">
                            {{ trans('main/admission.text_question5') }}                    
                        </a>
                    </h5>
                </div>
                <div id="faq5" class="collapse " data-parent="#accordion1" aria-labelledby="faq-5">   
                    <div class="card-body faq">
                        {!! trans('main/admission.text_answer5')!!}  
                    </div>
                </div> {{-- faq5 --}}

                <br>   
    
            </div> {{-- end accordion1 --}}

          
        </div> {{-- end col 8 --}}

        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" >  
           
            <div id="admiss-nav-pec" class="hidden-sm hidden-xs">                
                <div class="admiss-nav-pec-item" style="width:100%; hight:20%;">
                    <div>
                        <div style="background-image: #000;">                                   
                            <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=contents&category=90&id=5" target="_blank"> 
                                <img src="img/admid-center.jpg" class="img-fluid" /></a>                                  
                        </div>
                    </div>
                    <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=contents&category=90&id=5" target="_blank" >
                        <h1> {{ trans('main/admission.text_place_admission') }}</h1>
                    </a>  
                </div> 
            </div>   {{-- สถานที่รับสมัคร--}}

            <div id="admiss-nav-pec" class="hidden-sm hidden-xs">                
                <div class="admiss-nav-pec-item" style="width:100%; hight:20%;">
                    <div>
                        <div style="background-image: #000;">                                    
                            <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=news&categoryid=11&pagesize=20" target="_blank">
                                <img src="img/result.jpg" class="img-fluid" />
                            </a>                                    
                        </div>
                    </div>
                        <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=news&categoryid=11&pagesize=20" target="_blank" >
                            <h1>{{ trans('main/admission.text_name_result') }} </h1>
                        </a>                            
                </div>
            </div>   <br> {{-- ประกาศรายชื่อและผลสอบ --}}

            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                    <div id="admiss-nav-pec" class="hidden-sm hidden-xs">                
                        <div class="admiss-nav-pec-item" style="width:100%; hight:20%;">
                            <div>
                                <div style="background-image: #000;">                                    
                                    <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=news&categoryid=10&pagesize=20" target="_blank"> 
                                        <img src="img/step.jpg" class="img-fluid" />
                                    </a>                                    
                                </div>
                            </div>
                            <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=news&categoryid=10&pagesize=20" target="_blank" >
                                <h1> {{ trans('main/admission.text_apply_step') }} </h1>
                            </a>                            
                        </div>
                    </div>   <br> {{-- ขั้นตอนนการสมัคร --}}

                    <div id="admiss-nav-pec" class="hidden-sm hidden-xs">                
                        <div class="admiss-nav-pec-item" style="width:100%; hight:20%;">
                            <div>
                                <div style="background-image: #000;">                                    
                                    <a href="{{url('public/document/apply_step.pdf')}}" target="_blank"> <img src="img/student_transfers.jpg" class="img-fluid" /></a>                                    
                                </div>
                            </div>
                            <a href="{{url('public/document/apply_step.pdf')}}" target="_blank" ><h1> {{ trans('main/admission.text_student_transfers') }} </h1></a>                            
                        </div>
                    </div>   <br> {{-- นักศึกษาเทียบโอน --}}

                </div>   {{-- end col-6 --}} 
               
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">      

                    <div id="admiss-nav-pec" class="hidden-sm hidden-xs">                
                        <div class="admiss-nav-pec-item" style="width:100%; hight:20%;">
                            <div>
                                <div style="background-image: #000; ">                                    
                                    <a href="https://studentaffairs.tni.ac.th/home/?page_id=1336" target="_blank"> 
                                        <img src="img/fund.jpg" class="img-fluid" />
                                    </a>                                    
                                </div>
                            </div>
                            <a href="https://studentaffairs.tni.ac.th/home/?page_id=1336" target="_blank" >
                                <h1>{{ trans('main/admission.text_fund') }}</h1>
                            </a>                            
                        </div>
                    </div> <br> {{-- กยศ --}}
                    
                    <div id="admiss-nav-pec" class="hidden-sm hidden-xs">                
                        <div class="admiss-nav-pec-item" style="width:100%; hight:20%;">
                            {{-- <div>
                                <div style="background-image: #000;">                                    
                                    <a href="https://reg.tni.ac.th/registrar/calendar.asp?avs763750590=13" target="_blank"> 
                                        <img src="img/calendar.jpg" class="img-fluid" />
                                    </a>                                    
                                </div>
                            </div>
                            <a href="https://reg.tni.ac.th/registrar/calendar.asp?avs763750590=13" target="_blank" >
                                <h1> {{ trans('main/admission.text_calendar') }} </h1>
                            </a>   --}}                           
                             <div>
                                <div style="background-image: #000;">                                    
                                    <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=news&categoryid=13&pagesize=20" target="_blank"> 
                                        <img src="img/admission_calendar.jpg" class="img-fluid" />
                                    </a>                                    
                                </div>
                            </div>
                            <a href="https://admission.tni.ac.th/home/2017/main/index.php?option=news&categoryid=13&pagesize=20" target="_blank" >
                                <h1> {{ trans('main/admission.text_admission_calendar') }} </h1>
                            </a>      
                        </div>
                    </div>  {{-- ปฏิทิน --}}

                </div>  {{-- end col-6 --}}

            </div> {{-- end row col 4--}}
   
        </div> {{-- end col 4 --}}

    </div> {{-- end row  --}}
        
</div> {{-- end containner --}}

<br>
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">


@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

 <style>
        [id^='accordion']{
          cursor:pointer;
        }
</style>



<script>
      $(document).ready(function(){
        $('#dropdown-bachelor_1').addClass('active');
        $('[id^=dropdown-bachelor_]').click(function(){
        //alert('jjjj');
           $('[id^=dropdown-bachelor_]').removeClass('active');
            $(this).addClass('active');
        });

      });

</script>

<script>
        $(document).ready(function(){
          $('#faq-1').addClass('active');
          $('[id^=faq-]').click(function(){
       //   alert('jjjj');
             $('[id^=faq-]').removeClass('active');
              $(this).addClass('active');
          });
  
        });
  
</script>
  


@endsection

