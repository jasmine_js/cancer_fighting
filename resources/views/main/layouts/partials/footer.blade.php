<br />
<footer id="footer">
    <div class="container">
        <div class="row">
            <h1>{{ trans('main/footer.text_research_innovation') }}</h1>
            <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">
                <ul>
                    <li><a href="">{{ trans('main/footer.text_research_result') }}</a><li>
                    <li><a href="" target="_blank">{{ trans('main/footer.text_popular_research') }}</a><li> 
                    <li><a href="" target="_blank">{{ trans('main/footer.text_patent') }}</a><li>
                    <li><a href="" target="_blank">{{ trans('main/footer.text_innovation') }}</a><li>
                    <li><a href="" target="_blank">{{ trans('main/footer.text_award') }}</a><li>
                   
                </ul>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-2 col-xl-2">          
                <ul>
                    <li><a href=""  target="_blank">{{ trans('main/footer.text_rasearch_lab') }}</a><li>    
                    <li><a href="" target="_blank">{{ trans('main/footer.text_research_fund') }}</a><li>
                    <li><a href="" target="_blank">{{ trans('main/footer.text_research_management_system') }}</a><li>
                    <li><a href="" target="_blank">{{ trans('main/footer.text_about') }}</a><li>      
                </ul>

            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                <h6>{{ trans('main/footer.text_contact') }}</h6>
                <ul>
                    <li class="footer-fa footer-address">
                        {{ trans('main/footer.text_adress') }}
                    <li>
                    <li class="footer-fa footer-tel">
                        {!! trans('main/footer.text_tel') !!}
                    <li>
                    <li class="footer-fa footer-email">
                        <a href="mailto:research@tni.ac.th">{{ trans('main/footer.text_email') }}</a>
                    <li>
                    <li class="footer-fa footer-facebook">
                        <a href="https://www.facebook.com/Research-TNI-103552651592580/"  target="_blank">{{ trans('main/footer.text_fb') }}</a>
                    <li>
                </ul>
            </div>
            <div class="footer-map col-sm-12 col-md-6 col-lg-4 col-xl-4">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.6756048048237!2d100.62604996532494!3d13.738079340356332!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d61d1b7bec9cd%3A0xdecb451e7acbec94!2sThai-Nichi+Institute+of+Technology!5e0!3m2!1sen!2sth!4v1542862770412" frameborder="0" style="border:0" allowfullscreen></iframe>               
                {{-- <div class="footer-social">
                    <a href="https://www.facebook.com/ThaiNichi/" target="_blank" class="footer-social-facebook">Facebook</a>
                    <a href="https://twitter.com/tniadmissions" target="_blank" class="footer-social-twitter">Twitter</a>
                    {{-- <a href="https://plus.google.com/110037289499151553368" target="_blank" class="footer-social-google">Google+</a> 
                    <a href="https://www.youtube.com/user/tnipr" target="_blank" class="footer-social-youtube">Youtube</a>
                </div> --}}
            </div>
        </div>
    </div>

    <div class="footer-copyright">
        {{ trans('main/footer.text_copyright') }}
    </div>
</footer>

<a href="#top" id="go2top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>



    <?php //if(!isset($_COOKIE['cookie_set'])){ ?> <!-- ถ้า $_COOKIE['cookie_set'] ไม่ได้เซตค่า ให้แสดงปุ่ม ถ้ามีค่าให้ซ่อนปุ่ม -->
        {{-- <div class="cookie-policy row col-12 m-0 p-0" style="display: flex;" id="cookies">
            <div class="container-fluid align-items-center py-2 flex-column flex-md-row text-center ml-4">
                {{ trans('main/policy.text_popup_policy') }}
                <span class="ml-4 mr-4 "><a href="{{ url(Config::get('url.main.cookies')) }}" target="_blank">{{ trans('main/policy.text_link_more') }}</a></span>
                <!-- <a href="cookies_set.php"> -->
                <button id="btn-accept-cookie" class="btn btn-sm shake ml-4">{{ trans('main/policy.text_btn_accept') }}</button>
                <!-- //</a> -->
            </div>
        </div> --}}

    <?php //} ?>



 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $("#btn-accept-cookie").click(function(){  // When click btn-accept-cookie
            $.ajax({
                url : '{{ url(Config::get('url.main.ajax_cookies_set')) }}'	   ,     
                type: "get"	    // send post          
               
                // url : '{{ url(Config::get('url.main.fac_ajax')) }}'
                //url : "main.layouts.partials.cookies_set"      // go to cookies_set.php 
            }).done(function(data){	  // when cookies.php ทำงานเสร็จ หน้า cookies_set จะส่งค่าที่เสร็จแล้วมาไว้ในตัวแปร data เพื่อเอามาแสดงค่าหรือทำอย่างอื่นต่อไป 
              //  alert(status);
                $("#cookies").hide();             // hide all element id=cookies
            });
        });
    });
</script>

