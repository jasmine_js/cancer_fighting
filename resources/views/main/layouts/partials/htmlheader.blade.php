<head>
    <meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<title>@yield('htmlheader_title', 'วิจัยและนวัตกรรม สถาบันเทคโนโลยีไทย-ญี่ปุ่น : Research and Innovation, Thai-Nichi Institute of Technology')</title>

<meta name="title" content="@yield('htmlheader_meta_title' , 'วิจัยและนวัตกรรม สถาบันเทคโนโลยีไทย-ญี่ปุ่น : Research and Innovation, Thai-Nichi Institute of Technology')">

<meta name="description" content="@yield('htmlheader_description', 'วิจัยและนวัตกรรม สถาบันเทคโนโลยีไทย-ญี่ปุ่น: Research and Innovation, Thai-Nichi Institute of Technology ')" />

<meta name="keywords" content="@yield('htmlheader_keywords', 'วิจัยและนวัตกรรม,tni,วิจัยและนวัตกรรม-สถาบันเทคโนโลยีไทย-ญี่ปุ่น,thai-nichi institute of technology,คณะวิศวกรรมศาสตร์,คณะเทคโนโลยีสารสนเทศ,คณะบริหารธุรกิจ,วิศวกรรมคอมพิวเตอร์,วิศวกรรมการผลิต,วิศวกรรมยานยนต์,การจัดการอุตสาหกรรม,การจัดการอุตสาหกรรม,การจัดการวิสาหกิจสำหรับผู้บริหาร,เทคโนโลยีสารสนเทศ,เทคโนโลยี,it,information,thai-nichi,thai,nichi,engineering,programmer,อุตสาหกรรม,หลักสูตร,สาขาเทคโนโลยีสารสนเทศ,สาขาวิชาเทคโนโลยีมัลติมีเดีย,สาขาวิชาระบบสารสนเทศทางธุรกิจ,สารสนเทศ,มัลติมีเดีย,ระบบสนเทศทางธุรกิจ,ศึกษา,เรียน,ปริญญาตรี,ปริญญาโท,มหาวิทยาลัย,สถาบัน')" />


<meta property="og:url"           content="@yield('og_url' , 'https://www.tni.ac.th/home/news')" >
<meta property="og:type"          content="@yield('og_type' , 'website')" >
<meta property="og:title"         content="@yield('htmlheader_meta_title' , 'วิจัยและนวัตกรรม สถาบันเทคโนโลยีไทย-ญี่ปุ่น : Thai-Nichi Institute of Technology')">
<meta property="og:description"   content="@yield('htmlheader_description', 'วิจัยและนวัตกรรม สถาบันเทคโนโลยีไทย-ญี่ปุ่น:Thai-Nichi Institute of Technology มุ่งมั่นเป็นสถาบันอุดมศึกษาชั้นนำของประเทศที่เป็นศูนย์กลางทางวิชาการและ วิชาชีพเฉพาะทางชั้นสูง เพื่อเป็นแหล่งสร้างและพัฒนาบุคลากรในด้านเทคโนโลยีอุตสาหกรรมและเทคโนโลยี การบริหารจัดการที่ทันสมัย มีความเป็นเลิศทางวิชาการ การประยุกต์ และการเผยแพร่องค์ความรู้แก่สังคมโดยยึดมั่นในคุณธรรมและจิตสำนึกต่อสังคม')" />
<meta property="og:image"         content="@yield('og_image' ,'https://www.tni.ac.th/home/public/image/no_image.png')" >
   

    <link rel="icon" type="image/x-icon" href="{{ url('favicon.ico') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="FJgLohtByikAd2bciQ1MLxAq46GDNiB0Jzbwk9Gr">


  
    {{-- <link rel="stylesheet" href="{{ url('lib/bootstrap-4.0.0/css/bootstrap.min.css') }}"> --}}
    {{-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" > --}}
    {{-- <link rel="stylesheet" href="{{ url('lib/font-awesome-4.7.0/css/font-awesome.min.css') }}"> --}}

    <link rel="stylesheet" href="{{ url('lib/bootstrap-4.0.0/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" >
    <link rel="stylesheet" href="{{ url('lib/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    
    @yield('style')
    {{-- <link rel="stylesheet" href="{{ url('css/style.css?ver=1.0') }}">
    <link rel="stylesheet" href="{{ url('css/custom.css?ver=1.0') }}"> --}}
    <link rel="stylesheet" href="{{ url(trans("main/common.text_style_css")) }}">
    <link rel="stylesheet" href="{{ url(trans("main/common.text_custom_css")) }}">
    <script>var site_url = '{{ url("/") }}';</script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->




    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140716219-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-140716219-1');
    </script>
   

    <!-- Hotjar Tracking Code for https://www.tni.ac.th/home/ -->
<script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1332930,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>


</head>