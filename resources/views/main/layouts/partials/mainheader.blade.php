
<div id="header">
    <nav class="navbar navbar-expand-lg navbar-light bg-light container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{-- <img src="{{ url('img/logo.png') }}" class="img-fluid" /> --}}
            <img src="{{ url('img/logo_web.png') }}" class="img-fluid"/>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>        

        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto"></ul>
            <ul class="navbar-nav">
                <li class="nav-item d-block d-xl-none d-lg-none" hidden>
                    <div class="language" >
                    <ul>
                        <!-- @foreach($languages as $l)
                            @if($l->id == $language->id)
                                <li><a class="active" href="{{ url(Config::get('url.main.language') . '/?code=' . $l->code) }}">{{ $l->code }}</a></li>
                            @else
                                <li><a href="{{ url(Config::get('url.main.language') . '/?code=' . $l->code) }}">{{ $l->code }}</a></li>
                            @endif
                        @endforeach -->
                        @php
                            $active_class = "";
                            $url          = "";
                            $target       = "";
                        @endphp  

                        @foreach($languages as $l)
                        
                            @if($l->id == 1) {{-- if choose Thai language--}}
                                @php $url = url(Config::get('url.main.language') . '/?code=' . $l->code); @endphp
                            @elseif($l->id == 2) {{-- if choose Eng language--}}
                                {{-- @php 
                                    $url    = "https://admission.tni.ac.th/old/web/TNI2014-en/";
                                    $target = "target=_blank";
                                @endphp --}}
                                @php $url = url(Config::get('url.main.language') . '/?code=' . $l->code); @endphp
                            @elseif($l->id == 3) {{-- if choose Jap language--}}
                                {{-- @php
                                    $url    = "https://admission.tni.ac.th/old/web/TNI2014-jp/";
                                    $target = "target=_blank";
                                @endphp  --}}
                                @php $url = url(Config::get('url.main.language') . '/?code=' . $l->code); @endphp
                            @endif

                            @if($l->id == $language->id)
                                @php $active_class = "class=active"; @endphp
                            @else
                                @php $active_class = ""; @endphp
                            @endif
                            
                            <li><a href="{{$url}}" {{$active_class}} {{$target}}> {{ $l->code }}</a></li>
                        @endforeach
                        
                    </ul>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link" href="{{ url(Config::get('url.main.contact')) }}">
                        {{ trans('main/navbar.text_login') }}
                    </a>
                   
                </li>
              
                <li class="nav-item dropdown">
                    <a class="nav-link" href="{{ url(Config::get('url.main.contact')) }}"> 
                        {{ trans('main/navbar.text_register') }}
                    </a>                  
                </li>
             
             
            </ul>
        </div>
    </nav>
    
    <div class="language container d-none d-lg-block d-xl-block" >
        <ul hidden>
            <!-- @foreach($languages as $l)
                @if($l->id == $language->id)
                <li><a class="active" href="{{ url(Config::get('url.main.language') . '/?code=' . $l->code) }}">{{ $l->code }}</a></li>
                @else
                <li><a href="{{ url(Config::get('url.main.language') . '/?code=' . $l->code) }}">{{ $l->code }}</a></li>
                @endif
            @endforeach -->
                
                @php
                    $active_class = "";
                    $url          = "";
                    $target       = "";
                @endphp  

                @foreach($languages as $l)
                
                    @if($l->id == 1) {{-- if choose Thai language--}}
                        @php $url = url(Config::get('url.main.language') . '/?code=' . $l->code); @endphp
                    @elseif($l->id == 2) {{-- if choose Eng language--}}
                        {{-- @php 
                            $url    = "https://admission.tni.ac.th/old/web/TNI2014-en/";
                            $target = "target=_blank";
                        @endphp --}}
                        @php $url = url(Config::get('url.main.language') . '/?code=' . $l->code); @endphp
                    @elseif($l->id == 3) {{-- if choose Jap language--}}
                        {{-- @php
                            $url    = "https://admission.tni.ac.th/old/web/TNI2014-jp/";
                            $target = "target=_blank";
                        @endphp  --}}
                        @php $url = url(Config::get('url.main.language') . '/?code=' . $l->code); @endphp
                    @endif

                    @if($l->id == $language->id)
                        @php $active_class = "class=active"; @endphp
                    @else
                        @php $active_class = ""; @endphp
                    @endif
                    
                    <li><a href="{{$url}}" {{$active_class}} {{$target}}> {{ $l->code }}</a></li>
                @endforeach

        </ul>

        <!-- <div class="search-form">
            <input type="search" placeholder="{{ trans('main/common.text_search') }}" />
        </div> -->
    </div>
</div><!-- End header -->

<div id="apply-now" hidden>
    <div class="container">
        <div><a href="tel:+6627632605">0-2763-2605</a></div>
        
        <div class="shake">
            <a href="http://m.me/TNIadmissioncenter" target="_blank">
                <!-- <i class="fa fa-facebook-official"></i> -->
                <!-- <i class="fab fa-facebook-square"></i> -->
                <i class="fab fa-facebook-messenger"></i>
            </a>
        </div>
        
        <div class="shake">
            <a href="http://line.me/ti/p/@admission_tni" target="_blank" >
                <i class="fab fa-line"></i>
            </a>
        </div>
        <!-- <div class="shake"><a href="http://line.me/ti/p/@TNISAC" target="_blank" ><img src="{{ url('img/LINE_SOCIAL_Square_resize.png') }}"></a></div> -->
        
        {{-- <div><a href="{{ url(Config::get('url.main.admission'))}}" class="shake">{{ trans('main/common.text_apply_now') }}</a></div> --}}
        {{-- <div><a href="https://admission.tni.ac.th/home/2017/main/index.php" target="_blank" class="shake">{{ trans('main/common.text_apply_now') }}</a></div> --}}
        <div><a href=" https://reg.tni.ac.th/registrar/apphome.asp" target="_blank" class="shake">{{ trans('main/common.text_apply_now') }}</a></div>
       
        
    </div>
</div><!-- End apply-now -->