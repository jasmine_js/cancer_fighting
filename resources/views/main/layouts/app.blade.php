<!DOCTYPE html>
<html lang="{{ $language->code }}">
@include('main.layouts.partials.htmlheader')

<body style="background: linear-gradient(190deg, rgba(69,182,223,0.6) 32%, rgba(215,110,179,0.6) 88%);">
<div id="wrapper">
    
    @include('main.layouts.partials.mainheader')

    <div id="content-header">
    @yield('content-header')
    </div><!-- /.content-header -->

    <div id="content-wrapper">
    @yield('main-content')
    </div><!-- /.content-wrapper -->

    {{-- @include('main.layouts.partials.footer') --}}

</div><!-- ./wrapper -->

<script src="{{ url('lib/jquery-3.2.1.min.js') }}"></script>
<script src="{{ url('lib/popper.min.js') }}"></script>
<script src="{{ url('lib/bootstrap-4.0.0/js/bootstrap.min.js') }}"></script>
<script src="{{ url('js/front.js?ver=1.2') }}"></script>
@yield('script')

</body>
</html>

