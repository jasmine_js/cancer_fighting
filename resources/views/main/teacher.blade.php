@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')

    <div id="header" class="nav-item">

            <!-- <div class="container">
                <div class="second-header">{{ trans('main/history.text_teacher') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->
    <div id="home-course" class="container">

    </div>

    
@endsection


@section('main-content')
<br>
<div class="container">
        <div class="row" id="card-dep">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <div class="row" id="card-dep">
                        <div class="col-xs-3 col-sm-6 col-md-6 col-lg-3">
                            <div class="history-item" >
                                {{-- <a href="https://www.tni.ac.th/workload/teacher/eng.php" target="_blank"> --}}
                                <a href="https://workload.tni.ac.th/teacher/profiles.php?faculty=3" target="_blank">
                                    <div class="card text-white shake" >
                                        <div class="card-body t_eng" id="facilities">
                                            <i class="fas fa-chalkboard-teacher fa-3x"></i><br>{!!trans('main/history.text_eng')!!}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-6 col-md-6 col-lg-3">
                            <div class="history-item">
                                <a href="https://workload.tni.ac.th/teacher/profiles.php?faculty=2" target="_blank">
                                    <div class="card text-white shake" >
                                        <div class="card-body t_it" id="facilities">
                                            <i class="fas fa-chalkboard-teacher fa-3x"></i><br>{{trans('main/history.text_it')}}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>                       
                        <div class="col-xs-3 col-sm-6 col-md-6 col-lg-3">
                            <div class="history-item">
                                <a href="https://workload.tni.ac.th/teacher/profiles.php?faculty=1" target="_blank">
                                    <div class="card text-white shake">                                       
                                        <div class="card-body t_ba " id="facilities">
                                            <i class="fas fa-chalkboard-teacher fa-3x"></i><br>{{trans('main/history.text_ba') }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                         <div class="col-xs-3 col-sm-6 col-md-6 col-lg-3">
                            <div class="history-item">
                                <a href="https://workload.tni.ac.th/teacher/profiles.php?faculty=4" target="_blank">
                                    <div class="card text-white shake">                                       
                                        <div class="card-body t_cgel " id="facilities">
                                            <i class="fas fa-chalkboard-teacher fa-3x"></i><br>{{ trans('main/history.text_cgel') }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div> {{-- end div row 1 --}}

                </div> {{-- end div col-12 --}}   
        </div>   {{-- end row card --}}

</div>{{-- end containner --}}
    

<br>
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">


@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

 {{-- <style>
        [id^='card'],[id^='accordion']{
          cursor:pointer;
        }
</style> --}}



@endsection

