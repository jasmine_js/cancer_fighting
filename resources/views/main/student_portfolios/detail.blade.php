@extends('main.layouts.app')

@section('htmlheader_title'){!!@$item->description[$language->id]->name!!}:{{trans('main/home.text_htmltitle')}}@endsection

@section('htmlheader_meta_title'){!!@$item->description[$language->id]->meta_title!!}:{{trans('main/home.text_htmltitle')}}@endsection

@section('htmlheader_description')
@if(@$item->description[$language->id]->meta_description != ""){!!@$item->description[$language->id]->meta_description!!}@else{{trans('main/common.htmlheader_description')}}@endif
@endsection

@section('htmlheader_keywords')
@if(@$item->description[$language->id]->meta_keyword != ""){!!@$item->description[$language->id]->meta_keyword!!}@else{{trans('main/common.htmlheader_keywords')}}@endif
@endsection


@section('og_url'){!!'https://www.tni.ac.th/home/student_portfolios/'.@$item->id.'/detail'!!}@endsection  {{-- meta title --}}

@section('og_image'){!!@$item->thumb!!}@endsection  {{-- meta title --}}


@section('content-header')
<div id="background-new-detail">
    <div id="header" class="nav-item">

           <!-- <div class="container">
                <div class="second-header">{{ trans('main/home.text_student_portfolio') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>

                 <div class='poston'>
                    <i class='fa fa-clock-o fa'></i> {{date('d-M-Y H:m:s', strtotime(@$item->publish_start)) }}
                        | 
                    <i class="fa fa-eye"></i> {{ @$item->viewed }}
                 </div>   

            </div>

    </div><!-- End apply-now -->

</div>   
        
@endsection

@section('main-content')


<div id="background-new-detail"><br>
    <div class="container">

        <!--_SHOW Cover image -->
        <div class='row'>
            <div class='col-lg-12' >
                <!-- <div id="background-white">
                @if(@$item->thumb)
                    <img src="{{ $item->thumb }}" data-placeholder="{{ $item->description[$language->id]->name }}" class='img-thumbnail'/>
                @endif
                </div> -->
                @if(@$item->thumb)
                <div id="background-white">
                    <img src="{{ $item->thumb }}" data-placeholder="{{ url('storage/image/cache/no_image-100x100.png') }}"  class='img-thumbnail'/>
                </div>
                @endif
            </div>
        </div>
        <br>

        <!--_LEFT COLUMN -->
        <!--__SHOW DESC -->
        <div class="row">
            <div class="col-xs-8 col-md-8 col-lg-8">
                <div class='row'>
                    <div class='col-sm-12 col-md-12 col-lg-12 col-xl-12'>
                        <div id="background-white" style='padding:0 0.5em;' class='detail_content'>                
                            <h5>{!! @$item->description[$language->id]->description  !!}</h5>
                            <br>
                        </div> {{-- end div id bg white --}}
                    </div>
                </div>
                <br>

                <!-- GALLARY  -->
                @if(count($article_imgae) > 0)
                <div class='row'>
                    <div class='col-sm-12 col-md-12 col-lg-12 col-xl-12'>
                    <div id="background-white"> 
                        @if(count($article_imgae) > 0)
                            @foreach($article_imgae->chunk(4) as $chunk)
                            <div class='row'>
                                @foreach($chunk as $item_img)
                                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3"> <!-- col-md-6 col-lg-6 col-xl-6 -->
                                    <a class="fancybox-button" rel="item" href="{{ $item_img->image }}" title="">
                                        <img src='{{ $item_img->image }}' class='img-thumbnail' data-holder-rendered='true' alt=''>
                                    </a>  
                                </div>
                                @endforeach 
                            </div>
                            @endforeach      
                       <!--  @else
                        <div clas='row'>
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style="width:100%; hight:20%;">
                                {{ trans('main/common.text_not_found_data') }}
                            </div>
                        </div>
                        @endif -->
                        </div>
                    </div>
                </div>    
                <br>
                @endif

                
                <!-- SOCIAL  -->
                <div class='row'>
                    <div class='col-lg-12'>
                        <div id="social-share" >      
                            <h5 style="color:#767676"><b>SHARE :</b>   {{-- icon share --}}
                                <span> &nbsp;
                                    <a href="https://www.facebook.com/sharer.php?u={{ url(Config::get('url.main.student_portfolios').'/'.$item->id.'/detail') }}" target='_blank'>
                                        <i class="fa fa-facebook-square">     </i>
                                    </a>   &nbsp;
                                    <a href="https://twitter.com/share?url={{ url(Config::get('url.main.student_portfolios').'/'.$item->id.'/detail') }}" target='_blank'>
                                        <i class="fa fa-twitter-square">      </i>
                                    </a>   &nbsp;
                                    <!-- <a href="https://plus.google.com/share?url=https://ideagital.com/blog/howto_make_social_share_url">
                                        <i class="fab fa-google-plus-square">  </i>
                                    </a>   &nbsp; -->
                                    <!-- <a href="https://www.tumblr.com/share?url=https://ideagital.com/blog/howto_make_social_share_url">
                                        <i class="fab fa-tumblr-square">       </i>
                                    </a>   &nbsp; -->
                                    <!-- <a href="https://www.pinterest.com/share?url=https://ideagital.com/blog/howto_make_social_share_url">
                                        <i class="fab fa-pinterest-square">    </i>
                                    </a>   &nbsp; -->
                                    <!-- <a href="https://www.linkedin.com/share?url=https://ideagital.com/blog/howto_make_social_share_url">
                                        <i class="fab fa-linkedin-square">     </i>
                                    </a>   &nbsp; -->
                                    <!-- <a href="#">
                                    <i class="fas fa-layer-group">              </i>
                                    </a>   &nbsp;
                                    <a href="#">
                                        <i class="fa fa-envelope-square">      </i>
                                    </a>   &nbsp;
                                    <a href="#">
                                        <i class="fa fa-print">                </i>
                                    </a>   &nbsp; -->                    
                                </span>    
                                {{-- <span class="social-share"> &nbsp;    
                                            <a href="#" target="_blank" class="social-share-facebook">Facebook</a>
                                            <a href="#" target="_blank" class="social-share-twitter">Twitter</a>
                                            <a href="#" target="_blank" class="social-share-google">Google+</a>
                                            <a href="#" target="_blank" class="social-share-youtube">Youtube</a>
                                
                                </span> --}}
                            </h5>          
                        </div> {{-- end social share --}}
                    </div>
                </div>

                <br><br><br><br>
            </div> {{-- end div col-8 --}}

           <!-- _RIGHT COLUMN : 2 Tabs : Pouplar & Last posted -->                        
            <div class="col-xs-4  col-md-4 col-lg-4" > 
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-justified" id="tab-news">
                        <li class="nav-item">
                            <a class="nav-link active" id="popular-tab-just" data-toggle="tab" href="#popular" role="tab" aria-controls="popular-just" aria-selected="true">
                                <i class="fa fa-star"></i> {{ trans('main/allnews.text_popular') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="latest_post-tab-just" data-toggle="tab" href="#latest_post" role="tab" aria-controls="latest_post-just" aria-selected="false">
                                <i class="fa fa-star"></i> {{ trans('main/allnews.text_latest') }}
                               
                            </a>
                        </li>                     
                    </ul>

                    <div class="tab-content" id="tab-news-detail">
                        <div  class="tab-pane fade show active" id="popular" role="tabpanel" aria-labelledby="popular-tab-just">
                        <br>
                      
                        @if(!empty($item))
                            @foreach($populars as $popular) 
                                <div class="home-news-more">                 
                                    <div class="row">  
                                        <div class="col-md-4 col-sm-4 col-xl-4">  
                                            <div class="news-nav-pec-item" id="news-nav-pec" style="width:100%; hight:100%;">                                                
                                                <div style="background-image:#000;">
                                                    @if($popular->image)
                                                        <a href="{{ url(Config::get('url.main.news').'/'.$popular->id.'/detail') }}">                                
                                                            <img src="{{ $popular->image }}" class="img-thumbnail" />
                                                        </a>
                                                    @else
                                                        <a href="{{ url(Config::get('url.main.news').'/'.$popular->id.'/detail') }}">
                                                            <img src="{{ $article_images_blank }}" class="img-thumbnail" /> 
                                                        </a>
                                                    @endif                                                                                                                                                                           
                                                </div>                                
                                            </div>
                                        </div> 
                                      
                                        <div class="col-md-8 col-sm-8 col-xl-8">                                         
                                            <a href="{{ url(Config::get('url.main.news').'/'.$popular->id.'/detail') }}">                            
                                                <h5>{{ $popular->name }}<br>
                                                   <!--  <span style="color:#767676;">
                                                        <i class="far fa-clock"></i> {{date('d-m-Y H:m:s', strtotime($popular->publish_start)) }} 
                                                    </span> -->
                                                    <div class='poston2' style='font-size:0.75em;'>
                                                                <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$popular->publish_start)) }}
                                                            </div> 
                                                </h5>                                                     
                                            </a>                                                                                                                                     
                                        </div>                                       
                                   
                                    </div>     {{-- end row --}}                          
                                </div> <hr> {{-- end home-news-more --}}   
                            @endforeach 
                        @endif                                    
                        </div> {{-- end id poppula --}}

                        <div  class="tab-pane fade " id="latest_post" role="tabpanel" aria-labelledby="latest_post-tab-just">
                            <br>
                            {{--@if(count($item) > 0 )--}}
                            @if(!empty($item))
                                @foreach($lastest_posts as $lastest_post) 
                                    <div class="home-news-more">                 
                                        <div class="row">  
                                            <div class="col-md-4 col-sm-4 col-xl-4">  
                                                <div class="news-nav-pec-item" id="news-nav-pec" style="width:100%; hight:100%;">                                                
                                                    <div style="background-image:#000;">
                                                        @if($lastest_post->image)
                                                            <a href="{{ url(Config::get('url.main.news').'/'.$lastest_post->id.'/detail') }}">                                
                                                                <img src="{{ $lastest_post->image }}" class="img-thumbnail" />
                                                            </a>
                                                        @else
                                                            <a href="{{ url(Config::get('url.main.news').'/'.$lastest_post->id.'/detail') }}"> 
                                                                <img src="{{ $article_images_blank  }}" class="img-thumbnail" /> 
                                                            </a>
                                                        @endif                                                                                                                                                                           
                                                    </div>                                
                                                </div>
                                            </div> 
                                        
                                            <div class="col-md-8 col-sm-8 col-xl-8">                                             
                                                <a href="{{ url(Config::get('url.main.news').'/'.$lastest_post->id.'/detail') }}">                             
                                                    <h5>{{ $lastest_post->name }}<br>
                                                       <!--  <span style="color:#767676;">
                                                            <i class="far fa-clock"></i> {{date('d-m-Y H:m:s', strtotime($lastest_post->publish_start)) }} 
                                                        </span> -->
                                                        <div class='poston2' style='font-size:0.75em;'>
                                                            <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$lastest_post->publish_start)) }}
                                                        </div> 
                                                    </h5>                                                     
                                                </a>                                                                                                                                    
                                            </div>                                                                            
                                        </div>     {{-- end row --}}                          
                                    </div> <hr> {{-- end home-news-more --}}   
                                @endforeach 
                            @endif

                        </div> {{-- end id last post --}}
                    </div> {{-- end div id tab-news-detail--}}
                    <br><br><br><br>
            </div> {{-- end col-4 --}}

         </div> {{-- end row --}}


    </div> {{-- end containner --}}
</div> {{-- background-new-detail --}}
    
<br>

<div id="background-white">   
        <div class="container">   
            <h3><b>ข่าวที่เกี่ยวข้อง</b></h3>
                <!-- <div class="home-news-more">   --> 
                <div>
                    @if(count($articels_related) > 0 )
                        <div class='row'>
                        @foreach($articels_related as $item )
                            {{-- <div class="news-nav-pec-item col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3" id="news-nav-pec2" style="width:100%; hight:20%;">
                                <div style="background-image:#000;">                                                     
                                    <a href="{{ url(Config::get('url.main.news').'/'.$item->id.'/detail') }}">                                
                                            <img src="{{ $item->image }}" class="img-fluid" />
                                    </a>
                                </div> 
                                <a href="{{ url(Config::get('url.main.news').'/'.$item->id.'/detail') }}">                            
                                    <h1 class='truncate_text'>{{ $item->name }}<br><br>
                                        <!-- <span style="color:#767676;"> {{date('d-m-Y', strtotime($item->publish_start)) }} </span> -->
                                        <div class='poston' style='font-size:0.75em;'>
                                            <i class='far fa-clock'></i> {{ date('d-M-Y', strtotime(@$item->publish_start)) }}
                                        </div> 
                                    </h1>                                                     
                                </a>  
                            </div> end news-nav-pec-item --}}
                            <div class="home-news-item " >
                                <div>
                                    <a href="{{ url(Config::get('url.main.news').'/'.$item->id.'/detail') }}">
                                    
                                        <div style="background-image: url({{ $item->image }})">
                                            <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                        </div>
                                                                 
                                    </a> 
                                </div>
                                
                                <a href="{{ url(Config::get('url.main.news').'/'.$item->id.'/detail') }}">
                                    <h1 class='truncate_text'>{{ $item->name }}  </h1>
 
                                </a> 
                                <div class='poston' style='font-size:0.75em;'>
                                        
                                    <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$item->publish_start)) }}
                                    
                                </div>

                            </div> 
                        @endforeach
                    @else
                        <div class="news-nav-pec-item col-lg-12" id="news-nav-pec2" style="width:100%; hight:20%;">
                                {{ trans('main/common.text_not_found_data') }}
                        </div>
                    </div>
                    @endif 
                    
                </div> {{-- end div home-news-more --}}
           
        </div> 
</div> 

@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">



<!-- Add fancyBox main CSS files -->
<link rel="stylesheet"  href="{{ url('lib/fancybox/source/jquery.fancybox.css?v=2.1.5') }}" type="text/css" media="screen" />

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="{{ url('lib/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5') }}" type="text/css" media="screen" />
<link rel="stylesheet" href="{{ url('lib/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7') }}" type="text/css" media="screen" />
@endsection


@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<!-- _fancy box (simple) -->
    <!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="{{ url('lib/fancybox/lib/jquery.mousewheel-3.0.6.pack.js') }}"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="{{ url('lib/fancybox/source/jquery.fancybox.js?v=2.1.5') }}"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->

<script type="text/javascript" src="{{ url('lib/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5') }}"></script>
<script type="text/javascript" src="{{ url('lib/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6') }}"></script>

<script type="text/javascript" src="{{ url('lib/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7') }}"></script>


<script>
    $(document).ready(function() {
        
        //_Simple image gallery. Uses default settings
        //$('.fancybox').fancybox();

        $(".fancybox-button").fancybox({
            prevEffect		: 'none',
            nextEffect		: 'none',
            closeBtn		: false,
            helpers		: {
                title	: { type : 'inside' },
                buttons	: {}
            }
        });

    });

</script>

@endsection

    