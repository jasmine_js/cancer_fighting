@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')

    <div id="header" class="nav-item">

            <!-- <div class="container">
                <div class="second-header">{{ trans('main/home.text_career_tni') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->
    <div id="home-course" class="container">

    </div>

    
@endsection


@section('main-content')

<div class="container">
        <div id="home-course" class="container">
                <h3>{{ trans('main/home.text_application_form') }}</h3>
                <div id="download">
                    <br>                 
                    <a href="{{url('public/document/TNI-Application.doc')}}" class="shake">
                        <i class="fa fa-arrow-circle-o-down"></i> {{ trans('main/home.text_download') }} 
                    </a>
                </div>

                <div style="color:#555;">
                    <h4>
                        <b>{!! trans('main/home.text_send_application') !!}</b> <br>
                        <b>{!! trans('main/home.text_travel') !!}</b> <br><br>
                        <p style="text-align:left; font-size:24px;">{!! trans('main/home.text_travel_detail') !!}</p>
                    </h4>                            
                </div>
        </div>

        <h3 style="text-align:left;color:#555;">
            <b>{{trans('main/home.text_jobtni')}} :
                <a href="http://www.jobtni.com/home?l=th" target="_blank"> {{trans('main/home.text_link_jobtni')}} 
                </a>
            </b>
        </h3> 

</div>{{-- end containner --}}
    
<br>
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">


@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

 {{-- <style>
        [id^='card'],[id^='accordion']{
          cursor:pointer;
        }
</style> --}}



@endsection

