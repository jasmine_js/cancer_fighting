<meta charset="utf-8">
<?php
@require public_path() . '/lib/sendmail.class.php'; 


/*-------------------------
	GET-&-SET VARIABLES
---------------------------*/
/* $strInquirerName     = filtervar_custom($_POST['txtInquirerName']);
$strInquirerEmail    = filtervar_custom($_POST['txtInquirerEmail']);
$strInquirerPhone    = filtervar_custom($_POST['txtInquirerPhone']);
$strAreInquirerQuery = $_POST['txtAreInquirerQuery']; */

$strInquirerName     = filtervar_custom($_GET['txtInquirerName']);
$strInquirerEmail    = filtervar_custom($_GET['txtInquirerEmail']);
$strInquirerPhone    = filtervar_custom($_GET['txtInquirerPhone']);
$strAreInquirerQuery = $_GET['txtAreInquirerQuery'];


//_email account for send
$confEmailUn          = Config::get('mail.username');
$confEmailPw          = Config::get('mail.password');

$confEmail_sendername = 'tniinfo@tni.ac.th';
$confEmail_title      = 'สายตรงอธิการบดี';

//$email_reciever       =  $confEmailUn;
$email_reciever   = 'tniinfo@tni.ac.th';

//_set email content
$content_temp = <<<EOT
	<b>ชื่อ : </b>%s<br>
	<b>อีเมล์ : </b>%s<br>
	<b>เบอร์โทรศัพท์ : </b>%s<br>
	<b>สอบถามเรื่อง : </b>%s
EOT;

$email_content = sprintf($content_temp 
							,$strInquirerName,$strInquirerEmail
							,$strInquirerPhone,$strAreInquirerQuery);

/*===============
	SEND EMAIL
=================*/
$oMail = new sendmail();
$oMail->setMail($confEmailUn, base64_decode($confEmailPw)); 

$myBody    = $oMail->setBody_querySection($email_content);
$iSentMail = $oMail->sendContactGmail($confEmailUn, $confEmail_sendername
									,$confEmail_title ,$myBody, $email_reciever);

//_send to inquirer
$myBody     = $oMail->setBody_reply($strInquirerName , $strAreInquirerQuery);
$iSentMail2 = $oMail->sendContactGmail($confEmailUn , $confEmail_sendername
									,"re:".$confEmail_title , $myBody , $strInquirerEmail);
									

if($iSentMail){
	//echo 'ระบบได้ทำการส่งข้อความของคุณ ถึงอธิการบดีเรียบร้อยแล้วค่ะ';
	echo 'ระบบได้รับข้อความของท่านเรียบร้อยแล้ว จะทำการตรวจสอบและประสานงานไปยังหน่วยงานที่เกี่ยวข้องต่อไป';
}else{
	echo 'ไม่สามารถส่งข้อความของคุณ ถึงอธิการบดีได้ กรุณาลองอีกครั้งหรือติดต่อที่อีเมล์:'.$confEmail_sendername;
}

function filtervar_custom($strInput)
{
	$strInput = trim($strInput);
	$strInput = filter_var( $strInput , FILTER_SANITIZE_STRING);//_Removes tags/special characters from a string
	$strInput = htmlspecialchars( $strInput , ENT_QUOTES); //_convert special HTML entities back to characters
	return $strInput;
}

?>