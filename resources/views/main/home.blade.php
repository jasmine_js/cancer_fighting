@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')
<div id="hero-banner" hidden>
        <div class="container">
            
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    
                
                        <div class="swiper-slide" >
                         
                            <a href="#" target="_blank">
                                <img src="img/banner/banner.jpg" class="img-fluid" />     
                                {{-- <img src="img/faculty/eng/ENG_r.jpg" class="img-fluid" /> --}}
                            </a>
                          
                        </div>
                
    
                </div>
    
                <div class="swiper-pagination"></div>
            </div>
    
        </div>
    </div>
@endsection


@section('main-content')


<div id="home-course" class="container">
    {{-- <h3>{{ trans('main/home.text_reaserch_heading') }}</h3> --}}
    <br>
  
</div>

<div id="home-news-event" class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div id="home-news">
                <h1 class="text-center">{{ trans('main/home.text_article') }}</h1>           
                @if(count($articles) > 0 )
                                    @foreach($articles->chunk(4) as $chunk) {{-- method chunk : แบ่ง object เป็น row row ละ 4 คอลัมน์ --}}
                                        <div class='row'>
                                            @foreach($chunk as $item)                                           
                                                <div class="home-news-item" >
                                                   
                                                    
                                                        <div> 
                                                            @if($item->link )
                                                            <a href="{{ $item->link}}" target="_blank">
                                                            
                                                                <div style="background-image: url({{ $item->image }})">
                                                                    <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                </div>
                                                                                         
                                                            </a> 
                                                            @else
                                                            <a href="{{ url(Config::get('url.main.news').'/'.$item->id.'/detail') }}">
                                                            
                                                                    <div style="background-image: url({{ $item->image }})">
                                                                        <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                    </div>
                                                                                             
                                                            </a> 
                                                            @endif

                                                        </div>
                                                        @if($item->link )
                                                            <a href="{{ $item->link}}" target="_blank">
                                                                <h1 class='truncate_text'>{{ $item->name }}  </h1>
                            
                                                            </a> 
                                                        @else
                                                            <a href="{{ url(Config::get('url.main.news').'/'.$item->id.'/detail') }}">
                                                                <h1 class='truncate_text'>{{ $item->name }}  </h1>
                            
                                                            </a> 
                                                        @endif
                                                        <div class='poston' style='font-size:0.75em;'>
                                                                
                                                            <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$item->publish_start)) }}
                                                            
                                                        </div>
                        
                                                </div> 
                                            @endforeach 
                                        </div> <br> <br> <br>
                                       
                                    @endforeach 

                                  
                                @else 

                                    <div class='row'>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                            {{trans('main/common.text_not_found_data')}}
                                        </div>
                                    </div>
                                                                   
                                @endif {{-- @if($catagory[$i]->article_category_id == '1' ) --}}
                         
                <a href="{{ url(Config::get('url.main.news')) }}" class="btn-more pull-right">{{ trans('main/home.text_news_more' )}}</a>   {{--  ปุ่ม more  --}}
                
            </div>
        </div>
        {{-- <div class="col-sm-12 col-md-4 col-lg-3 col-xl-3">
            <div id="home-event">
                <h1>{{ trans('main/home.text_popular') }}</h1>
                @if(count($event_update) > 0)
                    <ul class="timeline">
                        @foreach($event_update as $event)
                        <li>
                            @if($event->link )
                                <a href="{{ $event->link }}" target="_blank">{{ $event->month }}</a>
                                <a href="{{ $event->link }}" target="_blank" class='truncate_text_event'>{{ $event->name }}<br>&nbsp;</a>
                            @else
                                <a href="{{ url(Config::get('url.main.events').'/'.$event->id.'/detail') }}">{{ $event->month }}</a>
                                <a href="{{ url(Config::get('url.main.events').'/'.$event->id.'/detail') }}" class='truncate_text_event'>{{ $event->name }}<br>&nbsp;</a>
                            @endif
                            <div class="timeline-day">{{ $event->day }}</div>
                        </li>
                        @endforeach
                    </ul>
                @else
                    <div class="data_notfound pt-2 pb-2"><b>{{ trans('main/common.text_not_found_data') }}</b></div>
                @endif
                <a href="{{ url(Config::get('url.main.events')) }}" class="btn-more pull-right">{{ trans('main/home.text_event_more' )}}</a>
            </div>
        </div> --}}
    </div>
</div>

<div id="home-nav-pec" hidden>
    <div class="container">
        <div class="row">
           
            @foreach($nav_pec as $pec)
            <div class="home-nav-pec-item col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div>
                    <!-- <div style="background-image: url({{ $pec->image }})">
                        @if($pec->link)
                        <a href="{{ $pec->link }}">
                            <img src="{{ $pec->image }}" class="img-fluid" style="visibility: hidden;"/>
                        </a>
                        @else
                        <img src="{{ $pec->image }}" class="img-fluid" />
                        @endif
                    </div> -->

                    @if($pec->link)
                        <a href="{{ $pec->link }}" >
                            <div style="background-image: url({{ $pec->image }})">
                                <img src="{{ $pec->image }}" class="img-fluid" style="visibility: hidden;"/>
                            </div>
                        </a>
                    @else
                        <div style="background-image: url({{ $pec->image }})">
                            <img src="{{ $pec->image }}" class="img-fluid" style="visibility: hidden;"/>
                        </div>
                    @endif


                </div>

                @if($pec->link)
                <a href="{{ $pec->link }}" ><h1>{{ $pec->name }}</h1></a>
                @else
                <h1>{{ $pec->name }}</h1>
                @endif
            </div>
            @endforeach
        </div>
    </div>
</div>


<div id="home-learn2know" hidden>
    <div class="container">
        <div>
            <h1>{{ trans('main/home.text_learn2know_heading') }}</h1>
            <h2>{{ trans('main/home.text_learn2know_subheading') }}</h2>
            <p>{{ trans('main/home.text_learn2know_p1') }}</p>
            <p>{{ trans('main/home.text_learn2know_p2') }}</p>
        </div>
        <a class="shake" href="http://learn2know.tni.ac.th/main/" target="_blank">{{ trans('main/home.text_click_here') }}</a>
    </div>
</div>



<div id="home-channel" hidden> 
        <div class="container">
            <h1>{{ trans('main/home.text_research_result') }}</h1>
            <div class="row">
                @if(count($articles) > 0)
                @for($i=0; $i < count($articles); $i++)
                        <div class="home-news-item" >
                            <div>
                                @if($articles[$i]->link )
                                    <a href="{{ $articles[$i]->link }}" target="_blank">
                                    
                                        <div style="background-image: url({{ $articles[$i]->image }})">
                                            <img src="{{ $articles[$i]->image }}" class="img-fluid" style="visibility: hidden" />
                                        </div>
                                                                
                                    </a> 
                                @else
                                    <a href="{{ url(Config::get('url.main.news').'/'.$articles[$i]->id.'/detail') }}">
                                    
                                        <div style="background-image: url({{ $articles[$i]->image }})">
                                            <img src="{{ $articles[$i]->image }}" class="img-fluid" style="visibility: hidden" />
                                        </div>
                                                                
                                    </a> 
                                @endif
                            </div>
                            @if($articles[$i]->link )
                                <a href="{{ $articles[$i]->link }}" target="_blank">
                                    <h1>{{ $articles[$i]->name }} <span class="badge badge-pill badge-info">NEW</span></h1>
                                </a>
                            @else
                                <a href="{{ url(Config::get('url.main.news').'/'.$articles[$i]->id.'/detail') }}">
                                    <h1>{{ $articles[$i]->name }} <span class="badge badge-pill badge-info">NEW</span></h1>
                                    {{-- <h1> <span style="color:red"><i class="fas fa-star"></i></span> {{ $articles[$i]->name }}</h1>  --}}

                                </a>
                            @endif
                        </div> 
                  

                    @endfor
                @else
                <div class="data_notfound pt-2">{{ trans('main/common.text_not_found_data') }}</div>
                @endif
            </div>

            <a href="{{ url(Config::get('url.main.news')) }}" class="btn-more pull-right">{{ trans('main/home.text_news_more' )}}</a>   {{--  ปุ่ม more  --}}
            </div> {{-- row --}}
    </div>
</div>

<br>


<div id="home-channel" hidden>
    <div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h1>{{ trans('main/home.text_other_web') }}</h1>
            <div id="hero-banner">
                <div class="container">
    
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
    
                            {{-- @foreach($home_hero_banner as $banner) --}}
                            @foreach($banner_image as $banner)
                                <div class="swiper-slide" data-background_color="{{ $banner->background_color }}">
                                    @if($banner->link)
                                    <a href="{{ $banner->link }}" target="_blank">
                                        <img src="{{ $banner->image }}" class="img-fluid" alt="{{ $banner->title }}" />
                                    </a>
                                    @else
                                    <img src="{{ $banner->image }}" class="img-fluid" alt="{{ $banner->title }}" />
                                    @endif
                                </div>
                            @endforeach
    
                        </div>
    
                        <div class="swiper-pagination"></div>
                    </div>
    
                </div>
        </div>
    

        </div>
      
    </div>
    </div>
</div>


<div id="home-channel" class="container" hidden>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h1>{{ trans('main/home.text_other_web') }}</h1>
            @if(count($latest_video) > 0)
                <div class="video-item" >
                        {{-- <div style="background: rgba(0,0,0,0.8); position:absolute;height:100%; width:100%;z-index:1"></div> --}}
                    @foreach($latest_video as $video)
                        @if($video->link)
                            <div class="embed-responsive embed-responsive-16by9" id="video" >                         
                                {!!$video->link !!}                           
                            </div>
                                {{-- <img src="{{ $video->image }}" class="img-fluid" />--}}
                            <h2>{{ $video->name }}</h2>  
                        @else                           
                            <img src="{{ url('storage/image/cache/no_image-100x100.png') }}"  class='img-thumbnail'/>                           
                        @endif                 
                    @endforeach
                </div>
                <a href="https://www.youtube.com/channel/UCOMVmuT0gRQxBGAFBDgqBLg" target="_blank"  class="btn-more pull-right">{{ trans('main/home.text_more') }}</a>                       
            @endif

        </div>
      
    </div>
</div>

<br><br><br> <br> <br> <br>

@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')
<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
<script>
var homeHeroSwiper = new Swiper('#hero-banner .swiper-container', {
    autoplay: {
        delay: 5000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    on: {
        init: function() {
            var background_color = $('#hero-banner .swiper-wrapper>div:nth-child(1)').data('background_color');
            $('#hero-banner').css('background', background_color).addClass('transition-none');
        },
        slideChange: function(e) {
            var background_color = $('#hero-banner .swiper-wrapper>div:nth-child(' + (homeHeroSwiper.activeIndex + 1) + ')').data('background_color');
            $('#hero-banner').css('background', background_color).removeClass('transition-none').addClass('transition-slow');
        }
    }
});

var homeNewsSwiper = new Swiper('#home-news .swiper-container', {
    loop: true,
    effect: 'fade',
    autoplay: {
        delay: 10000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
});
</script>


{{-- <script>
    $(document).ready(function() {
  $('#play-video').on('click', function(ev) {
    
    // var x = $("iframe").attr('src');
    // alert(x);
    
    
    $("iframe")[0].src += "&autoplay=1";
    ev.preventDefault();
 
  });
});
</script> --}}
@endsection