@extends('main.layouts.app')
@if(count($admissions_bycat_4['admissions']) > 0 )
    @for($i=0;$i<1;$i++)
        @section('htmlheader_title'){!!$admissions_bycat_4['admissions'][$i]['name']!!}:{{trans('main/home.text_htmltitle')}}@endsection  {{--  title --}} 

        @section('htmlheader_meta_title'){!!$admissions_bycat_4['admissions'][$i]['meta_title']!!}:{{trans('main/home.text_htmltitle')}}@endsection  {{-- meta title --}} 
        
        @section('htmlheader_description')
        @if($admissions_bycat_4['admissions'][$i]['meta_description'] != ""){!!@$admissions_bycat_4['admissions'][$i]['meta_description']!!}@else{{trans('main/common.htmlheader_description')}}@endif
        @endsection

        @section('htmlheader_keywords')
        @if($admissions_bycat_4['admissions'][$i]['meta_keyword'] != ""){!!$admissions_bycat_4['admissions'][$i]['meta_keyword']!!}@else{{trans('main/common.htmlheader_keywords')}}@endif
        @endsection

        @section('og_url'){!!'https://www.tni.ac.th/home/admissions/'.'student_fee'!!}@endsection  {{-- meta title --}}

        @section('og_image'){!!$admissions_bycat_4['admissions'][$i]['image'] !!}@endsection  {{-- meta title --}}
    @endfor
@endif

@section('content-header')
<div id="background-new-detail">
    <div id="header" class="nav-item">

           <!-- <div class="container">
                <div class="second-header">{{ trans('main/allnews.text_news_activity') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
                <br>
            </div>
    </div><!-- End apply-now -->
</div>   
        
@endsection


@section('main-content')
<div id="background-new-detail">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class='row'>
                    <div class='col-sm-12 col-md-12 col-lg-12 col-xl-12'>
                        @if(count($admissions_bycat_4['admissions']) > 0 )
                            @foreach($admissions_bycat_4['admissions'] as $item) {{-- admissions_bycat_3 คือ ทุนการศึกษา --}}
                                <div id="background-white" class='detail_content'>                                                 
                                    <h2 class="pt-3 pb-3"><b>{{ $item->name }}</b></h2>
                                    <h5>{!! $item->description !!}</h5><br> <br>                         
                                </div> {{-- end div id bg white --}}
                            @endforeach
                         @else
                            <div id="background-white" class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-2 detail_content" style='text-align:center'>
                                {{trans('main/common.text_not_found_data')}}
                            </div>
                        @endif
                    </div>
                </div>
                <br>
                <!-- SOCIAL  -->
                <div class='row'>
                    <div class='col-lg-12'>
                        <div id="social-share" >      
                            <h5 style="color:#767676"><b>SHARE :</b>   {{-- icon share --}}
                                <span> &nbsp;
                                   <a href="https://www.facebook.com/sharer.php?u={{ url(Config::get('url.main.admissions').'/student_fee') }}" target='_blank'>
                                        <i class="fa fa-facebook-square">     </i>
                                    </a>   &nbsp;
                                    <a href="https://twitter.com/share?url={{ url(Config::get('url.main.admissions').'/student_fee') }}" target='_blank'>
                                        <i class="fa fa-twitter-square">      </i>
                                    </a>   &nbsp;

                                </span>    
                               
                            </h5>          
                        </div> {{-- end social share --}}
                    </div>
                </div>

                <br><br><br><br>
            </div> {{-- end div col-8 --}}
        </div>
    </div>
</div>
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>


@endsection 

