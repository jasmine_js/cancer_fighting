@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection


@section('content-header')

@php
    {{--_รับค่าตัวแปรที่ส่งมาตอนกดเปลี่ยนหน้า ใช้ที่ Jquery ด้านล่าง --}}
    $admission_category_id = @$_GET['category'];
@endphp

    <div id="header" class="nav-item">

           <!-- <div class="container">
                <div class="second-header">{{ trans('main/allnews.text_news_activity') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->
    <div id="home-course" class="container">

    </div>

@endsection


@section('main-content')
 <div class="container ">

    @if(@count($admission_cat) > 0)
        @foreach($admission_cat as $item)
            {!! $item->name !!}
        @endforeach
    @endif 

    <div id="admission-menu" class="container">
      
        <div>
            <a href="{{ url(Config::get('url.main.faculty')) }}" class="shake">{{ trans('main/admission.text_bechelor') }}</a>
            <a href="https://www.tni.ac.th/bg_trans_credits/" class="shake" target="_blank">{{ trans('main/admission.text_bg_bechelor') }}</a>            
            <a href="http://inter.tni.ac.th/" class="shake" target="_blank">{{ trans('main/admission.text_international_program') }}</a>
            <a href="https://www.tni.ac.th/grad_programme/" class="shake" target="_blank">{{ trans('main/admission.text_master') }}</a>
            
        </div>
        <div >
            <a href="{{ url(Config::get('url.main.admissions').'/scholarship') }}" class="shake">{{ trans('main/admission.text_scholarship') }}</a>
            <a href="{{ url(Config::get('url.main.admissions').'/student_fee') }}" class="shake">{{ trans('main/admission.text_fee') }}</a>
            <a href="{{ url(Config::get('url.main.admissions').'/privilege') }}" class="shake">{{ trans('main/admission.text_privilege') }}</a>
            <a href="{{ url(Config::get('url.main.admissions').'/enrollment_confirmation') }}" class="shake">{{ trans('main/admission.text_regist_stdnew') }}</a> 
        </div>
    </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">   
                <div class="tabbable-line">
                    
                    <ul class="nav nav-tabs nav-justified " id="myTabJust" role="tablist">                            
                        <li class="nav-item">
                            <a class="nav-link active" id="admission-tab-just" data-toggle="tab" href="#admission" role="tab" aria-controls="admission-just" aria-selected="true">
                                 <span>{{ trans('main/admission.text_admission_announce') }} </span>
                            </a>
                        </li>
                       
                        <li class="nav-item">
                            <a class="nav-link " id="result_scholarship-tab-just" data-toggle="tab" href="#result_scholarship" role="tab" aria-controls="result_scholarship-just" aria-selected="false">
                                 <span>{{ trans('main/admission.text_result_scholarship_announce') }} </span>
                            </a>
                        </li>                  

                    </ul>

                    <div class="tab-content card pt-5" id="myTabContentJust" >
                            <div class="tab-pane fade show active" id="admission" role="tabpanel" aria-labelledby="admission-tab-just">
                                <h5></h5>
                                @if(count($admissions_bycat_1['admissions']) > 0 )
                                    @foreach($admissions_bycat_1['admissions']->chunk(4) as $chunk) {{-- method chunk : แบ่ง object เป็น row row ละ 4 คอลัมน์ --}}
                                        <div class='row'>
                                            @foreach($chunk as $item)
                                          
                                                <div class="home-news-item" >
                                                   
                                                    
                                                        <div> 
                                                            @if($item->link )
                                                            <a href="{{ $item->link}}" target="_blank">
                                                            
                                                                <div style="background-image: url({{ $item->image }})">
                                                                    <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                </div>
                                                                                         
                                                            </a> 
                                                            @else
                                                            <a href="{{ url(Config::get('url.main.admissions').'/'.$item->id.'/detail') }}">
                                                            
                                                                    <div style="background-image: url({{ $item->image }})">
                                                                        <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                    </div>
                                                                                             
                                                            </a> 
                                                            @endif

                                                        </div>
                                                        @if($item->link )
                                                            <a href="{{ $item->link}}" target="_blank">
                                                                <h1 class='truncate_text'>{{ $item->name }}  </h1>
                            
                                                            </a> 
                                                        @else
                                                            <a href="{{ url(Config::get('url.main.admissions').'/'.$item->id.'/detail') }}">
                                                                <h1 class='truncate_text'>{{ $item->name }}  </h1>
                            
                                                            </a> 
                                                        @endif
                                                        <div class='poston' style='font-size:0.75em;'>
                                                                
                                                            <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$item->publish_start)) }}
                                                            
                                                        </div>
                        
                                                </div> 
                                            @endforeach 
                                        </div> <br>
                                    @endforeach 

                                    <div class='row'>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:right'>
                                            <div class="pager">
                                                <div class="pageinfo">
                                                    <span>
                                                    {{ trans('backoffice/common.text_all_results') }} {{ $admissions_bycat_1['admissions']->total() }} {{ trans('backoffice/common.text_item(s)') }}
                                                    </span>
                                                    <span>
                                                    {{ trans('backoffice/common.text_page') }} {{ $admissions_bycat_1['admissions']->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $admissions_bycat_1['admissions']->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}
                                                    </span>
                                                </div>		
                                            </div>	      
                                        </div>
                                    </div>

                                    <div class='row'>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                            {{$admissions_bycat_1['admissions']->links('vendor.pagination.bootstrap-4')}}
                                            
                                            {{-- $admissions_bycat_2['admissions']->appends(array_except(Request::query(),'test_2'))->links('vendor.pagination.bootstrap-4') --}}
                                        </div>
                                    </div> 

                                @else 

                                    <div class='row'>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                            {{trans('main/common.text_not_found_data')}}
                                        </div>
                                    </div>
                                                                   
                                @endif {{-- @if($catagory[$i]->admission_category_id == '1' ) --}}
                            </div>{{-- end div admission  --}}

                        
                        {{--  ================================================================================================================================  --}}  

                            <div class="tab-pane fade" id="result_scholarship" role="tabpanel" aria-labelledby="result_scholarship-tab-just">
                                    <h5></h5>
                                @if(count($admissions_bycat_2['admissions']) > 0 )
                                    @foreach($admissions_bycat_2['admissions']->chunk(4) as $chunk) {{-- method chunk : แบ่ง object เป็น row row ละ 4 คอลัมน์ --}}
                                        <div class='row'>
                                            @foreach($chunk as $item)
                                          
                                                <div class="home-news-item" >
                                                   
                                                    
                                                        <div> 
                                                            @if($item->link )
                                                            <a href="{{ $item->link}}" target="_blank">
                                                            
                                                                <div style="background-image: url({{ $item->image }})">
                                                                    <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                </div>
                                                                                         
                                                            </a> 
                                                            @else
                                                            <a href="{{ url(Config::get('url.main.admissions').'/'.$item->id.'/detail') }}">
                                                            
                                                                    <div style="background-image: url({{ $item->image }})">
                                                                        <img src="{{ $item->image }}" class="img-fluid" style="visibility: hidden" />
                                                                    </div>
                                                                                             
                                                            </a> 
                                                            @endif

                                                        </div>
                                                        @if($item->link )
                                                            <a href="{{ $item->link}}" target="_blank">
                                                                <h1 class='truncate_text'>{{ $item->name }}  </h1>
                            
                                                            </a> 
                                                        @else
                                                            <a href="{{ url(Config::get('url.main.admissions').'/'.$item->id.'/detail') }}">
                                                                <h1 class='truncate_text'>{{ $item->name }}  </h1>
                            
                                                            </a> 
                                                        @endif
                                                        <div class='poston' style='font-size:0.75em;'>
                                                                
                                                            <i class='fa fa-clock-o fa'></i> {{ date('d-M-Y', strtotime(@$item->publish_start)) }}
                                                            
                                                        </div>
                        
                                                </div> 
                                            @endforeach 
                                        </div> <br>
                                    @endforeach 

                                    <div class='row'>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:right'>
                                            <div class="pager">
                                                <div class="pageinfo">
                                                    <span>
                                                    {{ trans('backoffice/common.text_all_results') }} {{ $admissions_bycat_2['admissions']->total() }} {{ trans('backoffice/common.text_item(s)') }}
                                                    </span>
                                                    <span>
                                                    {{ trans('backoffice/common.text_page') }} {{ $admissions_bycat_2['admissions']->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $admissions_bycat_2['admissions']->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}
                                                    </span>
                                                </div>		
                                            </div>	      
                                        </div>
                                    </div>

                                    <div class='row'>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                            {{$admissions_bycat_2['admissions']->links('vendor.pagination.bootstrap-4')}}
                                            
                                            {{-- $admissions_bycat_2['admissions']->appends(array_except(Request::query(),'test_2'))->links('vendor.pagination.bootstrap-4') --}}
                                        </div>
                                    </div> 

                                @else 

                                    <div class='row'>
                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style='text-align:center'>
                                            {{trans('main/common.text_not_found_data')}}
                                        </div>
                                    </div>
                                                                   
                                @endif {{-- @if($catagory[$i]->admission_category_id == '1' ) --}}
                            </div> {{-- end div result_scholarship  --}}

                        {{--  ================================================================================================================================  --}} 
                          

                    </div> {{-- end div myTabContentJust --}}

                </div> {{-- end div tabbable-line --}}
            </div>{{-- end div col-md-12 col-sm-12 col-xs-12 --}}
        </div> {{-- end div row--}}   
        
        <br><br>

        <div class="row">
            <div class="col-sm-6 col-md-6 col-xs-6 col-xl-6"  id="question">                
                <i class="fa fa-edit pl-2 text-danger" ></i>{{ trans('main/admission.text_faq') }}
            </div>

            <br><br>

            <div id="accordion1" class="col-12">
                @if(count($admissions_bycat_7['admissions']) > 0 )
                    {{-- @foreach($admissions_bycat_7['admissions'] as $item)  --}}
                    
                    @for($i=0;$i<count($admissions_bycat_7['admissions']);$i++)

                        <div class="card-header" id="faq-{{$i}}">
                            <h5 class="mb-0" >
                                @if($i =="0") {{-- // ถ้าเป็นตัวแรก ให้ครื่องหมาย > คว่ำลงเพื่อแสดงว่าเป็นการเปิดเนื้อหาด้านใน --}}
                                    <a class="collapsed" tyle="text-indent:10%;" data-toggle="collapse" href="#faq{{$i}}" aria-expanded="true" aria-controls="faq{{$i}}">
                                        {{ $admissions_bycat_7['admissions'][$i]['name'] }}
                                    </a>                       
                                @else
                                    <a class="collapsed" tyle="text-indent:10%;" data-toggle="collapse" href="#faq{{$i}}" aria-expanded="false" aria-controls="faq{{$i}}">
                                        {{ $admissions_bycat_7['admissions'][$i]['name'] }}
                                    </a>                         
                                @endif
                            </h5>{{-- faq-name --}}
                        </div>

                            @if($i =="0") {{-- // ถ้าเป็นตัวแรก ให้เปิดเนือหาด้านใน --}}
                                <div id="faq{{$i}}" class="collapse show" data-parent="#accordion1" aria-labelledby="faq-{{$i}}">
                                    <div class="card-body faq">
                                        {!! $admissions_bycat_7['admissions'][$i]['description']!!}      
                                    </div>  
                                </div>  
                            @else
                                <div id="faq{{$i}}" class="collapse" data-parent="#accordion1" aria-labelledby="faq-{{$i}}">
                                    <div class="card-body faq">
                                        {!! $admissions_bycat_7['admissions'][$i]['description']!!}      
                                    </div>  
                                </div>   {{-- faq-description --}}
                            @endif

                        <br>
                        
                    @endfor 
                 @else
                 
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-2" style='text-align:center'>
                        {{trans('main/common.text_not_found_data')}}
                    </div>
               
                @endif        
                <br>   
            </div> {{-- end accordion1 --}}

        </div> {{-- end row --}}

    </div>{{-- end containner --}}

<br>
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">

@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<script>

    $(document).ready(function(){
        
        //_รับค่าตัวแปรที่ส่งมาตอนกดเปลี่ยนหน้า เพื่อคง tab ที่เลือกไว้
        var cat_id = "<?php echo $admission_category_id;?>"

        //_ถ้าว่างแสดงว่าเลือก tab ข่าวทั้งหมดอยู่
        if(cat_id == ''){
            cat_id = 0;
        }  
       
        //console.log(cat_id);
        //_id tag ที่เกี่ยวข้อง ของ แต่ละ tab
        var aryCategory = ['#admission-tab-just,#admission'//1
                            ,'#student-tab-just,#student'//2
                        ];
        
        aryCategory.forEach(function(selector_item , index) {
            
            if(cat_id == index){
                $(selector_item).addClass('active show');
            }else{
                $(selector_item).removeClass('active show');
            }
        });
    });

</script> 

@endsection 

