@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')

    <div id="header" class="nav-item">

            <!-- <div class="container">
                <div class="second-header">{{ trans('main/footer.text_web_link') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->
    <div id="home-course" class="container">

    </div>

    
@endsection


@section('main-content')

<div class="container">
        <div id="home-course" class="container">
                <h3 style="text-align:left; color:#555 ; font-size:28px;"><b>{{ trans('main/home.text_organization') }}</b></h3> 
        </div>
            <div style="color:#555;">
                    <table class="table table-striped weblink ">
                        <tbody style="text-indent:1.0em; font-size:1.5rem" id="table" >
            
                                <tr >
                                    <td class="shake">
                                        <a href='http://www.tdmat.com/' target="_blank">3D Mat COMPANY LIMITED</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.allied-material.co.jp/english' target="_blank">A.L.M.T. (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.aphonda.co.th/honda2012/aphonda-home.asp' target="_blank">A.P. Honda Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.abk-aots.org/' target="_blank"><span data-scayt_word='ABK' data-scaytid='240' target="_blank">ABK</span>&amp;<span data-scayt_word='AOTS' data-scaytid='241' target="_blank">AOTS</span> ALUMNI ASSOCIATION (THAILAND)</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.advics.co.jp/eng' target="_blank"><span data-scayt_word='Advics' data-scaytid='242' target="_blank">Advics</span> Asia Pacific Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.aeon.co.th/aeon' target="_blank"><span data-scayt_word='AEON' data-scaytid='243' target="_blank">AEON</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.th.agc-group.com/' target="_blank"><span data-scayt_word='AGC' data-scaytid='244' target="_blank">AGC</span> Automotive (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.acth.co.th/' target="_blank"><span data-scayt_word='AGC' data-scaytid='245' target="_blank">AGC</span> Chemicals(Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.agc-flatglass.co.th/home.php' target="_blank"><span data-scayt_word='AGC' data-scaytid='246' target="_blank">AGC</span> Flat Glass (Thailand) Plc.Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.aisin.com/' target="_blank"><span data-scayt_word='Aisin' data-scaytid='247' target="_blank">Aisin</span> Ai (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.aisin.com/news/2014/index.html' target="_blank"><span data-scayt_word='Aisin' data-scaytid='248' target="_blank">Aisin</span> Asia Pacific Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.anritsu-industry.com/en' target="_blank"><span data-scayt_word='Anritsu' data-scaytid='249' target="_blank">Anritsu</span> Industrial Solutions (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.anritsu-industry.com/en' target="_blank"><span data-scayt_word='Aoyama' data-scaytid='250' target="_blank">Aoyama</span> Thai Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.aqua.co.th/' target="_blank">Aqua <span data-scayt_word='Nishihara' data-scaytid='251' target="_blank">Nishihara</span> Corporation Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.arrk.co.th/home/home_eng.html' target="_blank"><span data-scayt_word='Arrk' data-scaytid='252' target="_blank">Arrk</span> Corporation (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.asahitec.co.th/index.html' target="_blank">Asahi Tec <span data-scayt_word='Aluminium' data-scaytid='253' target="_blank">Aluminium</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.asahitec.co.th/index.html' target="_blank">Asia <span data-scayt_word='Cultral' data-scaytid='254' target="_blank">Cultral</span>&amp;Technological Promotion Foundation(<span data-scayt_word='HOAUMI' data-scaytid='255' target="_blank">HOAUMI</span>)</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.asahitec.co.th/index.html' target="_blank">Asia Metal Works Industry Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.asahitec.co.th/index.html' target="_blank">Asia <span data-scayt_word='Paiboon' data-scaytid='256' target="_blank">Paiboon</span> Wong Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ayw.co.th/' target="_blank">Asia <span data-scayt_word='Yamashita' data-scaytid='257' target="_blank">Yamashita</span> Works Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.asianhonda.com/' target="_blank">Asian Honda Motor Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.asimar.com/' target="_blank">Asian Marine Services Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.fts-com.co.jp/g/company/company-thailand2.php' target="_blank"><span data-scayt_word='Asno' data-scaytid='258' target="_blank">Asno</span> <span data-scayt_word='Horie' data-scaytid='259' target="_blank">Horie</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.atsumitec.co.jp/en/public/index' target="_blank"><span data-scayt_word='Atsumitec' data-scaytid='260' target="_blank">Atsumitec</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.autoalliance.co.th/' target="_blank">Auto Alliance (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.autoworldparts.com/index.php?lang=en' target="_blank">Auto Technic Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.avance-thai.com/index.php/th' target="_blank"><span data-scayt_word='Avance' data-scaytid='261' target="_blank">Avance</span> Recruitment (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://th.azbil.com/top.html' target="_blank"><span data-scayt_word='Azbil' data-scaytid='262' target="_blank">Azbil</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://th.azbil.com/top.html' target="_blank"><span data-scayt_word='Asno' data-scaytid='263' target="_blank">Asno</span> <span data-scayt_word='Horie' data-scaytid='264' target="_blank">Horie</span> (Thailand)</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.becl.co.th/index.php?lang=th' target="_blank">Bangkok Expressway Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.bangkokmetro.co.th/' target="_blank">Bangkok Metro Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.bpssteel.com/bpsEN/index1.html' target="_blank">Bangkok Pacific Steel Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.btssprings.com/' target="_blank">Bangkok <span data-scayt_word='Taiyo' data-scaytid='265' target="_blank">Taiyo</span> Springs Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.btssprings.com/' target="_blank">Bedazzles Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.btssprings.com/' target="_blank"><span data-scayt_word='Betagro' data-scaytid='266' target="_blank">Betagro</span> Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.big-daishowa.com/' target="_blank">Big <span data-scayt_word='Daishowa' data-scaytid='267' target="_blank">Daishowa</span> <span data-scayt_word='Seiki' data-scaytid='268' target="_blank">Seiki</span> Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.big-daishowa.com/' target="_blank"><span data-scayt_word='Bookbridges' data-scaytid='269' target="_blank">Bookbridges</span> Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.big-daishowa.com/' target="_blank"><span data-scayt_word='Boonrawd' data-scaytid='270' target="_blank">Boonrawd</span> Trading Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.bossprinting.net/index-en.html' target="_blank">Boss Printing Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='https://www.bol.co.th/en/home' target="_blank">Business Online Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.bky.co.th/company.php' target="_blank"><span data-scayt_word='BYK' data-scaytid='271' target="_blank">BYK</span> Engineering Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.calbee.co.th/' target="_blank"><span data-scayt_word='Calbee' data-scaytid='272' target="_blank">Calbee</span> <span data-scayt_word='Tanawat' data-scaytid='273' target="_blank">Tanawat</span> Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.canon.co.th/home' target="_blank">Canon Hi-Tech (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.cataler.co.th/' target="_blank"><span data-scayt_word='Cataler' data-scaytid='274' target="_blank">Cataler</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.cataler.co.th/' target="_blank">Central Motor Wheel (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ch-karnchang.co.th/' target="_blank">CH. <span data-scayt_word='Karnchang' data-scaytid='275' target="_blank">Karnchang</span> Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ch-karnchang.co.th/' target="_blank">CH. <span data-scayt_word='Karnchang-Tokyu' data-scaytid='276' target="_blank">Karnchang-Tokyu</span> Construction Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ch-karnchang.co.th/' target="_blank"><span data-scayt_word='Charoen' data-scaytid='277' target="_blank">Charoen</span> Sap Rice Mill Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ch-karnchang.co.th/' target="_blank"><span data-scayt_word='Chatporn' data-scaytid='278' target="_blank">Chatporn</span> Shop (2005) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.chugai.co.jp/index_e.html' target="_blank"><span data-scayt_word='Chugai' data-scaytid='279' target="_blank">Chugai</span> Ro (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.chugai.co.jp/index_e.html' target="_blank"><span data-scayt_word='Chunlee' data-scaytid='280' target="_blank">Chunlee</span> <span data-scayt_word='Padriew' data-scaytid='281' target="_blank">Padriew</span></a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.chugai.co.jp/index_e.html' target="_blank">Citizen Manufacturing Asia Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://atsmemall.com/shop/home.php?uid=34413' target="_blank">Comfort International Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.controlcomponentsco.com/' target="_blank">Control Component Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.controlcomponentsco.com/' target="_blank">Core Tech System Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.taiwantrade.com.tw/EP/moldex3d' target="_blank">CT Industry Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.daicelsst-stt.com/' target="_blank"><span data-scayt_word='Daicel' data-scaytid='282' target="_blank">Daicel</span> Safety Systems (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.daidosittipol.com/home.html' target="_blank"><span data-scayt_word='Daido' data-scaytid='283' target="_blank">Daido</span> <span data-scayt_word='Sittipol' data-scaytid='284' target="_blank">Sittipol</span> Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.daihen.co.th/' target="_blank"><span data-scayt_word='Daihen' data-scaytid='285' target="_blank">Daihen</span> Electric Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.daikinthai.com/daikinthai/index.php?lang=th' target="_blank"><span data-scayt_word='Daikin' data-scaytid='286' target="_blank">Daikin</span> Industries (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://dct.co.th/home.html' target="_blank"><span data-scayt_word='Dainichi' data-scaytid='287' target="_blank">Dainichi</span> Color (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.daishothai.com/index.php' target="_blank"><span data-scayt_word='Daisho' data-scaytid='288' target="_blank">Daisho</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.daiwa-dat.co.th/' target="_blank">Daiwa <span data-scayt_word='Kasei' data-scaytid='289' target="_blank">Kasei</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.denso.co.th/' target="_blank"><span data-scayt_word='DENSO' data-scaytid='290' target="_blank">DENSO</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.denso.co.th/' target="_blank"><span data-scayt_word='DENSO' data-scaytid='291' target="_blank">DENSO</span> International (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.denso.co.th/' target="_blank"><span data-scayt_word='DENSO' data-scaytid='292' target="_blank">DENSO</span> International Asia Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.diaresibon.com/?file=index' target="_blank"><span data-scayt_word='Dia' data-scaytid='293' target="_blank">Dia</span> <span data-scayt_word='Resibon' data-scaytid='294' target="_blank">Resibon</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.dynametal.co.th/default.htm' target="_blank"><span data-scayt_word='Dyna' data-scaytid='295' target="_blank">Dyna</span> Metal Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ebara.co.th/index.php/en' target="_blank"><span data-scayt_word='Ebara' data-scaytid='296' target="_blank">Ebara</span> (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.enkeithai.co.th/' target="_blank"><span data-scayt_word='Enkei' data-scaytid='297' target="_blank">Enkei</span> Thai Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.essom.com/' target="_blank"><span data-scayt_word='ESSOM' data-scaytid='298' target="_blank">ESSOM</span> Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.essom.com/' target="_blank">European Bakery Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.essom.com/' target="_blank">Excel Tool Partnership Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='https://www.fcc-net.co.jp/en/index.html' target="_blank">FCC (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='https://www.fcc-net.co.jp/en/index.html' target="_blank">February Thirty Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.fillpart.com/' target="_blank"><span data-scayt_word='Fillpart' data-scaytid='299' target="_blank">Fillpart</span> Engineering (1999) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.fujikura.co.th/' target="_blank"><span data-scayt_word='Fujikura' data-scaytid='300' target="_blank">Fujikura</span> Group in Thailand</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.fujikasui.co.th/' target="_blank"><span data-scayt_word='Fujikasui' data-scaytid='301' target="_blank">Fujikasui</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://fujimaki-web.co.jp/global/gi/offices/thailand' target="_blank"><span data-scayt_word='Fujimaki' data-scaytid='302' target="_blank">Fujimaki</span> Steel (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.fujitsu.com/th/th' target="_blank">Fujitsu (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.fujitsugeneral.com/' target="_blank">Fujitsu General (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.fujitsu.com/global' target="_blank">Fujitsu Systems Business (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.fujitsu-ten.com/' target="_blank">Fujitsu Ten (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.fujitsu-ten.com/' target="_blank">Full Moon Supply Partnership Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.gsc.co.th/' target="_blank">Grand Siam Composites Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.gsc.co.th/' target="_blank">Grand Dragoon Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hayatele.co.jp/tai' target="_blank"><span data-scayt_word='Hayashi' data-scaytid='303' target="_blank">Hayashi</span> <span data-scayt_word='Telempu' data-scaytid='304' target="_blank">Telempu</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hayatele.co.jp/tai' target="_blank">High Quality Garment Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hinomanufacturing.co.th/EN/Index.aspx' target="_blank"><span data-scayt_word='Hino' data-scaytid='305' target="_blank">Hino</span> Motors Manufacturing (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hinothailand.com/2014/landing.php' target="_blank"><span data-scayt_word='Hino' data-scaytid='306' target="_blank">Hino</span> Motors Sales (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi.co.th/' target="_blank">Hitachi Asia (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi.com.sg/' target="_blank">Hitachi Asia Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi-chem.co.jp/english/company/group_ap.html' target="_blank">Hitachi Chemical Asia-Pacific <span data-scayt_word='Pte' data-scaytid='307' target="_blank">Pte</span>. Limited (Bangkok Representative Office)</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi-chem.co.jp/english/company/group_thailand.html' target="_blank">Hitachi Chemical Automotive Products (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi-compressor.com/hctl2/index.php' target="_blank">Hitachi Compressor (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi-c-m.com/asia' target="_blank">Hitachi Construction Machinery (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi.co.th/index.html' target="_blank">Hitachi Global Storage Technologies (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi-hitec.com/thailand' target="_blank">Hitachi High-Technologies (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi.co.th/index.html' target="_blank">Hitachi Metals (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi-powertools.co.th/' target="_blank">Hitachi Power Tools (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi.com.sg/' target="_blank">Hitachi Rolls (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi-hb.co.jp/english/index.html' target="_blank">Hitachi Transport System (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.honda.co.th/th/home' target="_blank">Honda Automobile (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.h1-co.jp/' target="_blank">H-One Parts (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toyo-hymold.com/index.htm#' target="_blank"><span data-scayt_word='HYMOLD' data-scaytid='308' target="_blank">HYMOLD</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toyo-hymold.com/index.htm#' target="_blank"><span data-scayt_word='Ikura' data-scaytid='309' target="_blank">Ikura</span> <span data-scayt_word='Seiki' data-scaytid='310' target="_blank">Seiki</span> Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.inabata.co.jp/english/index.html' target="_blank"><span data-scayt_word='Inabata' data-scaytid='311' target="_blank">Inabata</span> Thai Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.inabata.co.jp/english/index.html' target="_blank"><span data-scayt_word='Intelnovation' data-scaytid='312' target="_blank">Intelnovation</span> Group Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.inabata.co.jp/english/index.html' target="_blank">Internet Foundation for The Development of Thailand</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.inabata.co.jp/english/index.html' target="_blank"><span data-scayt_word='Isuzu' data-scaytid='313' target="_blank">Isuzu</span> Engine Manufacturing (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.isz.co.jp/en' target="_blank"><span data-scayt_word='Isuzu' data-scaytid='314' target="_blank">Isuzu</span> Group Foundation</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.isuzu.com/index.jsp' target="_blank"><span data-scayt_word='Isuzu' data-scaytid='315' target="_blank">Isuzu</span> Motor Company (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.itochu.co.th/' target="_blank"><span data-scayt_word='Itochu' data-scaytid='316' target="_blank">Itochu</span> (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.itochu.co.th/' target="_blank"><span data-scayt_word='Itochu' data-scaytid='317' target="_blank">Itochu</span> Management (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.iwatani.com/' target="_blank"><span data-scayt_word='Iwatani' data-scaytid='318' target="_blank">Iwatani</span> Corporation (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.jac-recruitment.co.th/newweb/index.php' target="_blank"><span data-scayt_word='JAC' data-scaytid='319' target="_blank">JAC</span> Personnel Recruitment Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.jat.co.jp/english' target="_blank">Japan Techno Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.jtecs.or.jp/e' target="_blank">Japan Thailand and Economic Cooperation Society</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.jcc.or.th/th/site/index' target="_blank">Japanese Chamber of Commerce, Bangkok</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.jetro.go.jp/thailand' target="_blank"><span data-scayt_word='JETRO' data-scaytid='320' target="_blank">JETRO</span> Bangkok</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.jfe-eng.co.jp/en' target="_blank"><span data-scayt_word='JFE' data-scaytid='321' target="_blank">JFE</span> Engineering &amp; Construction (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.jfe-shoji.co.jp/english' target="_blank"><span data-scayt_word='JFE' data-scaytid='322' target="_blank">JFE</span> Shoji Trade (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.jfe-steel.co.jp/en' target="_blank"><span data-scayt_word='JFE' data-scaytid='323' target="_blank">JFE</span> Steel Corporation</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.jibuhin.co.th/' target="_blank"><span data-scayt_word='Jibuhin' data-scaytid='324' target="_blank">Jibuhin</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.jica.go.jp/english' target="_blank"><span data-scayt_word='JICA' data-scaytid='325' target="_blank">JICA</span> (Japan International Cooperation Agency)</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.jtekt.co.th/' target="_blank"><span data-scayt_word='JTEKT' data-scaytid='326' target="_blank">JTEKT</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.jvc.net/th' target="_blank">JVC Manufacturing (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kline.co.th/klinecoth/Default.aspx' target="_blank">K Line (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kue.co.th/kue/index.htm' target="_blank">K&amp;U Enterprise Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kawasaki.co.th/th/home.asp' target="_blank">Kawasaki Motors Enterprise (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kingfisher.co.th/' target="_blank">Kingfisher Holdings Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kingfisher.co.th/' target="_blank">Kobe Electronics Material (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaikobe.com/default.php' target="_blank">Kobe <span data-scayt_word='Mig' data-scaytid='327' target="_blank">Mig</span> Wire (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaikobe.com/default.php' target="_blank">Kobe Steel Limited (Bangkok Representative Office)</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kma.com.sg/' target="_blank"><span data-scayt_word='Kobelco' data-scaytid='328' target="_blank">Kobelco</span> Machinery Asia <span data-scayt_word='Pte' data-scaytid='329' target="_blank">Pte</span>. Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.koratmatsushita.com/web-en' target="_blank"><span data-scayt_word='Korat' data-scaytid='330' target="_blank">Korat</span> <span data-scayt_word='Matsushita' data-scaytid='331' target="_blank">Matsushita</span> Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.koratmatsushita.com/web-en' target="_blank"><span data-scayt_word='Krua' data-scaytid='332' target="_blank">Krua</span> <span data-scayt_word='Baan' data-scaytid='333' target="_blank">Baan</span> <span data-scayt_word='Sai' data-scaytid='334' target="_blank">Sai</span></a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kyocera.co.th/index.html' target="_blank"><span data-scayt_word='Kyocera' data-scaytid='335' target="_blank">Kyocera</span> Asia Pacific (Thailand) Co.,Ltd.</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kyocera.co.th/index.html' target="_blank"><span data-scayt_word='Kyodo' data-scaytid='336' target="_blank">Kyodo</span> Die-Works (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kyotoelectricwire.com/' target="_blank">Kyoto Electric Wire (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kyotoelectricwire.com/' target="_blank"><span data-scayt_word='Limphanich' data-scaytid='337' target="_blank">Limphanich</span> Limited Partnership</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kyotoelectricwire.com/' target="_blank">Machine Tech Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.mahle.com/' target="_blank"><span data-scayt_word='Mahle' data-scaytid='338' target="_blank">Mahle</span> Siam Filter Systems Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.mahle.com/' target="_blank"><span data-scayt_word='Makino' data-scaytid='339' target="_blank">Makino</span> Milling Machine Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.marubeni.co.th/index.html' target="_blank"><span data-scayt_word='Marubeni' data-scaytid='340' target="_blank">Marubeni</span> Thailand Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.marubeni.co.th/index.html' target="_blank"><span data-scayt_word='Marubeni-Itochu' data-scaytid='341' target="_blank">Marubeni-Itochu</span> Steel <span data-scayt_word='Pte' data-scaytid='342' target="_blank">Pte</span>. Limited, Bangkok Branch</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.marubeni.co.th/index.html' target="_blank">Max Value Technology Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://meiwa-thai.com/' target="_blank"><span data-scayt_word='Meiwa' data-scaytid='343' target="_blank">Meiwa</span> Enterprise (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://meiwa-thai.com/' target="_blank"><span data-scayt_word='Mikuni' data-scaytid='344' target="_blank">Mikuni</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://http//www.mitsubishicorp.com/th/en' target="_blank">Mitsubishi Company (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.makino.co.jp/en' target="_blank">Mitsubishi Electric Asia (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.marubeni.co.th/index.html' target="_blank">Mitsubishi Electric Automation (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.benichu.com/english' target="_blank">Mitsubishi Electric Corporation Regional Office</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.benichu.com/english' target="_blank">Mitsubishi Electric Corporation, Japan</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.fusotruck.co.th/' target="_blank">Mitsubishi <span data-scayt_word='Fuso' data-scaytid='345' target="_blank">Fuso</span> Truck (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://meiwa-thai.com/' target="_blank">Mitsubishi Heavy Industries (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.mikuni.co.th/thai/main_thai.php' target="_blank">Mitsubishi Motors (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.mitsubishicorp.com/th/en' target="_blank"><span data-scayt_word='Mitsui' data-scaytid='346' target="_blank">Mitsui</span> &amp; Co., (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.mitsubishielectric.com/company/about/locations/apac' target="_blank"><span data-scayt_word='Mitsui' data-scaytid='347' target="_blank">Mitsui</span> O.S.K. Lines (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.act.mitsui-kinzoku.co.jp/en/com_oversea.html' target="_blank"><span data-scayt_word='Mitsui' data-scaytid='348' target="_blank">Mitsui</span> Siam Components Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ms-ins.com/english' target="_blank"><span data-scayt_word='Mitsui' data-scaytid='349' target="_blank">Mitsui</span> <span data-scayt_word='Sumitomo' data-scaytid='350' target="_blank">Sumitomo</span> Insurance Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.mitsumi.co.jp/index_e.html' target="_blank"><span data-scayt_word='Mitsumi' data-scaytid='351' target="_blank">Mitsumi</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.mitutoyo.com/' target="_blank"><span data-scayt_word='Mitutoyo' data-scaytid='352' target="_blank">Mitutoyo</span> (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.mizuhobank.com/index.html' target="_blank">Mizuho Bank, Limited Bangkok Branch</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.mizuhobank.com/index.html' target="_blank">Mizuho Corporate Bank Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.mizuhobank.com/index.html' target="_blank">MMC Hardmetal (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.molten.co.jp/corporate/en/index.html' target="_blank">Molten Asia Polymer Products Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.moresco.co.th/Home.html' target="_blank">Moresco (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.moresco.co.th/Home.html' target="_blank">Mori Seiki Manufacturing (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.moresco.co.th/Home.html' target="_blank">Moriroku (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.moresco.co.th/Home.html' target="_blank">Muang-Max (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.moresco.co.th/Home.html' target="_blank">Murakami Ampas (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ntn.co.jp/' target="_blank">N.T.N Bearing Thailand Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ntn.co.jp/' target="_blank">N.Y.K. (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ntn.co.jp/' target="_blank">NDR Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ndr.co.jp/eng-default.htm' target="_blank">NEC Corporation (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://th.nec.com/' target="_blank">NEC Tokin Electronics (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nec-tokin.co.th/' target="_blank">NESIC (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.netmarks.co.th/' target="_blank">Netmarks (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nhkspg.co.th/th' target="_blank">NHK Spring (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nihonsuperior.co.jp/english' target="_blank">Nihon Superior (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nikkeisiam.com/index.html' target="_blank">Nikkei Siam Aluminium Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nikon.co.th/th_TH' target="_blank">Nikon (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nikon.co.th/th_TH' target="_blank">Nile (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://e-nippn.com/' target="_blank">Nippon Flour Mills (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thainippon.co.th/' target="_blank">Nippon Steel (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.e-nippn.com/' target="_blank">NIPRO (Thailand) Corporation Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tanchong.com/en/index.aspx' target="_blank">Nissan Diesel (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nissan.co.th/' target="_blank">Nissan Motor (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nct.co.th/' target="_blank">Nissen Chemitec (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nissin-thai.com/th/index.htm' target="_blank">Nissin Electric (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nissin-mfg.co.jp/thai-index.htm' target="_blank">Nissin Manufacturing (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nitto.com/' target="_blank">Nitto Denko Material (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.minebea.co.th/minebea/TH/' target="_blank">NMB-Minebea Thai Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nokpct.com/index1.html' target="_blank">NOK Precision Component (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nsk.co.th/index.php' target="_blank">NSK Asia Pacific Technology Centre (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nsk.co.th/index.php' target="_blank">NSK Bearings (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nsk.co.th/index.php' target="_blank">NSK Bearings Manufacturing (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.th.ntt.com/en/index.html' target="_blank">NTT Communication (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ocean.co.th/th/home/index.php' target="_blank">Ocean Life Insurance Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://inter.mua.go.th/main2/index.php' target="_blank">Office of the Higher Education Commission</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ogico.co.jp/eng/index.html' target="_blank">Ohgitani Kogyo (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ojipaper.co.th/' target="_blank">Oji Paper (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.okamotothai.com/' target="_blank">Okamoto (Thai) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.okamotothai.com/' target="_blank">Oki Data Manufacturing (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://omni-shop.com/index.htm' target="_blank">Omni Systems Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://omotenashi-a.com/t/index.html' target="_blank">Omotenashi Association Recruitment Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://omotenashi-a.com/t/index.html' target="_blank">Ooh Alai Bangkok Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.osg.co.th/' target="_blank">OSG (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.otcdaihenasia.com/th' target="_blank">OTC Daihen Asia Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://muramoto%20electron%20%28thailand%29%20public%20company%20limited/' target="_blank">Muramoto Electron (Thailand) Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.intelnovationgroup.com/P.V.T.Manufacturing_Co.,Ltd./Home.html' target="_blank">P.V.T. Manufacturing Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.panasonic.com/th/home.html' target="_blank">Panasonic (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.panasonic.com/th/home/' target="_blank">Panasonic Management (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.panus.co.th/evos/front/bin/home.phtml' target="_blank">Panus Assembly Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.panus.co.th/evos/front/bin/home.phtml' target="_blank">Pasona Employment Agency (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://personnelconsultant.co.th/home-th/' target="_blank">Personnel Consultant Manpower (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://pioneer-thailand.co.th/general/Homepage.asp' target="_blank">Pioneer Manufacturing (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://pioneer-thailand.co.th/general/Homepage.asp' target="_blank">Plaloc Asia (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.pollard.co.th/home.htm' target="_blank">Pollard Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.pollard.co.th/home.htm' target="_blank">Prapada Service Partnership Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.painet.org/index.html' target="_blank">Professionals&#39; Network in Advanced Instrument Society (PAI-NET)</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.pttplc.com/th/pages/home.aspx' target="_blank">PTT Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.qminvent.com/' target="_blank">QM Invent Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://qualityplus.co.th/' target="_blank">Quality Plus Aesthetic International Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.rotarybangrak.org/2013' target="_blank">Ray Business (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.rohm.com/' target="_blank">Rohm Integrated Systems (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.rondex.co.th/' target="_blank">Rondex (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.rotarybangrak.org/2013' target="_blank">Rotary Club of Bangrak</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ryouei-e.com/en/index.html' target="_blank">Ryoei Engineering Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ryouei-e.com/en/index.html' target="_blank">S.E.I. Group CSR Foundation</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sei.co.jp/' target="_blank">S.E.I. Thai Holding Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sei.co.jp/' target="_blank">SAEILO (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sei.co.jp/' target="_blank">Sakura Product (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sei.co.jp/' target="_blank">Sanno R &amp; D Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sei.co.jp/' target="_blank">Satake (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sei.co.jp/' target="_blank">SEI Break Systems (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sei.co.jp/' target="_blank">SEI Interconnect Products (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sei.co.jp/' target="_blank">Seiko Precision (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sei.co.jp/' target="_blank">Sews Asia Technical Center Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sews-ct.co.th/' target="_blank">SEWS Components (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sharpthai.co.th/' target="_blank">Sharp Appliances (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://sharp-world.com/index.html' target='_parent' target="_blank">Sharp Manufacturing (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://sharp-world.com/index.html' target='_parent' target="_blank">Shiroki Corporation (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sharpthai.co.th/' target="_blank">Sharp Thai Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.aisin.com/' target="_blank">Siam Aisin Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.aisin.com/' target="_blank">Siam Electronic Industries Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sgm.co.th/' target="_blank">Siam Goshi Manufacturing Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sgm.co.th/' target="_blank">Siam HPM Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://http//www.nissan-global.com/EN/' target="_blank">Siam Nissan Automobile Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://http//www.nissan-global.com/EN/' target="_blank">Siam NSK Steering Systems Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://http//www.nissan-global.com/EN/' target="_blank">Siam Toyodensan Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.skb-tech.com/' target="_blank">SKB Tech. (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.smthai.com/' target="_blank">SMCC (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sodick.co.th/' target="_blank">Sodick (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sojitz.com/jp/index.html' target="_blank">Sojitz (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sompojapanthai.com/' target="_blank">Sompo Japan Insurance (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sony.co.th/section/home?site=hp_en_TH_i' target="_blank">Sony Device Technology (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sony.co.th/section/home?site=hp_en_TH_i' target="_blank">Sritong Nameplate Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sumhitechs.co.th/' target="_blank">Sum Hitechs Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sumhitechs.co.th/' target="_blank">Sumiden Hyosung Steel Cord (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sumiden-thailand.com/index.php?sp=GMWEB-11112912184101003' target="_blank">Sumiden International Trading (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sumiden-tomita.co.jp/index.html' target="_blank">Sumiden Shoji (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sumipol.com/' target="_blank">Sumipol Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sumitomothailand.co.th/' target="_blank">Sumitomo Corporation Thailand Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sumitomothailand.co.th/' target="_blank">Sumitomo Electric (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sumitomothailand.co.th/' target="_blank">Sumitomo Electric Hardmetal Manufacturing (Thailand)</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sei.co.jp/' target="_blank">Sumitomo Electric Industries, Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sumitomothailand.co.th/' target="_blank">Sumitomo Electric Sintered Components (Thailand)</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.set-th.com/index.php' target="_blank">Sumitomo Electric Wintec (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sumitomothailand.co.th/' target="_blank">Sumitomo Electric Wiring Systems (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sumitomothailand.co.th/' target="_blank">Sumitomo Metal Industries, Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.smbc.co.jp/' target="_blank">Sumitomo Mitsui Banking Corporation</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.summitautobody.co.th/' target="_blank">Summit Auto Body Industry Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='https://www.suncall.co.jp/english/index.html' target="_blank">Suncall High Precision (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='https://www.suncall.co.jp/english/access/world/thailand.html' target="_blank">Super Urber Film Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='https://www.sws.co.jp/en/index.html' target="_blank">SWS Logistics &amp; Marketing (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='https://www.sws.co.jp/en/index.html' target="_blank">Sylvac Asia Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tj-earth.com/' target="_blank">T&amp;J Earth Mechatronics Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.trad.co.jp/' target="_blank">T. RAD (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.trad.co.jp/' target="_blank">T.S.B. Products Limited Partnership</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.taisei-thailand.co.th/' target="_blank">Taisei (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.taisei-thailand.co.th/' target="_blank">Takahashi Industrial And Economic Research Foundation</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.taisei-thailand.co.th/' target="_blank">Takao (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tnsc.com/' target="_blank">Takata-TOA Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tnsc.com/' target="_blank">Tanabe (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tbts.co.th/' target="_blank">TBTS (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tbts.co.th/' target="_blank">Teamwork Pacific Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tbts.co.th/' target="_blank">Teamwork Pacific Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.technoassocie.co.jp/' target="_blank">Techno Associe (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tpa.or.th/' target="_blank">Technology Promotion Association (Thailand-Japan)</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tpif.or.th/WebDev/index.php' target="_blank">Technology Promotion Institute Foundation</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.teijin.co.th/' target="_blank">Teijin Polyester (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tpc.co.th/' target="_blank">Textile Prestige Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.asahidenki-th.com/' target="_blank">Thai Asahi Denki Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.agc.com/english/index.html' target="_blank">Thai Asahi Glass Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.agc.com/english/index.html' target="_blank">Thai Aviation Refuelling Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.bridgestone.co.th/' target="_blank">Thai Bridgestone Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.cthaigroup.com/' target="_blank">Thai Chemical Corporation Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://world.honda.com/' target="_blank">Thai Honda Manufacturing Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://world.honda.com/' target="_blank">Thai International Die Making Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kajima.co.jp/' target="_blank">Thai Kajima Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kajima.co.jp/' target="_blank">Thai Kamaya Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kikuwa.net/eng/thai.htm' target="_blank">Thai Kikuwa Industries Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.koito.co.jp/' target="_blank">Thai Koito Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kurabo.co.jp/' target="_blank">Thai Kurabo Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kurabo.co.jp/' target="_blank">Thai Long Distance Telecommunication Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kurabo.co.jp/' target="_blank">Thai Mitchi Corporation Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kurabo.co.jp/' target="_blank">Thai Longdistance Telecommunication Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.kurabo.co.jp/' target="_blank">Thai MMA Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nippon-seiki.co.jp/' target="_blank">Thai Nippon Seiki Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.nisca.co.jp/' target="_blank">Thai Nisca Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thainishimatsu.com/' target="_blank">Thai Nishimatsu Construction Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thainishimatsu.com/' target="_blank">Thai Nissei Lamination Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaiobayashi.co.th/' target="_blank">Thai Obayashi Corporation Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaiobayashi.co.th/' target="_blank">Thai Otsuka Pharmaceutical Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaiobayashi.co.th/' target="_blank">Thai PET Resin Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaiobayashi.co.th/' target="_blank">Thai Polyacetal Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tpcc-tpac.com/' target="_blank">Thai Polycarbonate Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.secom.co.th/' target="_blank">Thai Secom Pitakkij Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.semcon.co.th/' target="_blank">Thai Semcon Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.semcon.co.th/' target="_blank">Thai Seng Trading Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.shimz-global.com/th/en/' target="_blank">Thai Shimizu Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi.co.th/eng/network/index.html' target="_blank">Thai Sintered Products Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.sinto.com/' target="_blank">Thai Sintokogio Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaistanley.com/' target="_blank">Thai Stanley Electric Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaistanley.com/' target="_blank">Thai Steel Pipe Industry Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tsmec.co.th/' target="_blank">Thai Summit Mitsuba Electric Manufacturing Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaitakeda.com/' target="_blank">Thai Takeda Lace Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.takenaka.co.th/' target="_blank">Thai Takenaka International Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.takenaka.co.th/' target="_blank">Thai Tap Water Supply Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaitinplate.co.th/' target="_blank">Thai Tinplate Manufacturing Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toshiba.co.th/' target="_blank">Thai Toshiba Electric Industries Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toshiba.co.th/' target="_blank">Thai Toshiba Lighting Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.wacoal.co.th/' target="_blank">Thai Wacoal Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.wacoal.co.th/' target="_blank">Thai Worth Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.yamaha-motor.co.th/' target="_blank">Thai Yamaha Motor Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.yazaki-group.com/' target="_blank">Thai Yazaki Corporation Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaikobe.com/corp/index.php?lang=en' target="_blank">Thai-Kobe Welding Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaikobe.com/corp/index.php?lang=en' target="_blank">Thailand Automotive Institute</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaioilmarine.com/index.html' target="_blank">Thaioil Marine Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thaioilgroup.com/en/' target="_blank">Thaioil Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thanachartbank.co.th/TbankCMSFrontend/defaultth.aspx' target="_blank">Thanachart Bank Public Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.thanachartbank.co.th/TbankCMSFrontend/defaultth.aspx' target="_blank">Thanya Ville Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.bot.or.th/Thai/Pages/BOTDefault.aspx' target="_blank">The Bank Of Thailand</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.bk.mufg.jp/english/' target="_blank">The Bank of Tokyo-Mitsubishi UFJ, Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hidajapan.or.jp/hida/en' target="_blank">The Overseas Human Resources and Industry Development Association (Hida)</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hidajapan.or.jp/hida/en' target="_blank">The Siam United Steel (1995) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.srimuang.co.th/' target="_blank">The Srimuang Insurance Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.srimuang.co.th/' target="_blank">Thumrongwat Company</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www1.tips.co.th/V2/company/profile.php#' target="_blank">Tips Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www1.tips.co.th/V2/company/profile.php#' target="_blank">TJ Prannarai Communication Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.togoh.co.jp/english/company/history.html' target="_blank">Togo Seisakusyo (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ter.co.th/' target="_blank">Tokai Eastern Rubber (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.topy-fas.co.jp/ja/default.htm' target="_blank">Topy Fasteners (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toray.co.th/' target="_blank">Toray Industries (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toray.co.th/' target="_blank">Toshiba Asia Pacific Pte. Limited (Bangkok Office)</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toshiba-aircon.in.th/profile' target="_blank">Toshiba Carrier (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.asia.toshiba.com/' target="_blank">Toshiba Asia Pacific Pte Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toshiba.co.th/' target="_blank">Toshiba Consumer Products (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hokuto.co.jp/' target="_blank">Toshiba Hokuto Electronic Device (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toshiba.co.th/' target="_blank">Toshiba Lighting Components (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.semicon.toshiba.co.jp/' target="_blank">Toshiba Semiconductor (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.semicon.toshiba.co.jp/' target="_blank">Toshiba Storage Devicen (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://lixil.tostem.co.th/' target="_blank">Tostem Thai Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toyo.co.th/th' target="_blank">Toyo Business Service Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toyoroki.co.th/ENG/TRE.html' target="_blank">Toyo Roki (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toyoda-gosei.com/' target="_blank">Toyoda Gosei (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toyoda-gosei.com/' target="_blank">Toyoda Gosei Asia Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toyoda-gosei.com/' target="_blank">Toyoda Gosei Rubber (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tmapem.com/' target="_blank">Toyota Motor Asia Pacific Engineering &amp; Manufacturing Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.toyota.co.th/' target="_blank">Toyota Motor Thailand Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.ttcap.co.th/' target="_blank">Toyota Technical Center Asia Pacific (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tttc.co.th/' target="_blank">Toyota Tsusho (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.fujitsu.com/' target="_blank">Transtron (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.trdasia.com/index.php?lang=th' target="_blank">TRD Asia Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.isuzu-tis.com/All-New/2012/home-2.html' target="_blank">Tripech Isuzu Sales (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.isuzu-tis.com/All-New/2012/home-2.html' target="_blank">Tripetch Isuzu Sales (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tsubaki.co.th/' target="_blank">Tsubakimoto Automotive (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tungaloy.co.jp/th/index.php' target="_blank">Tungaloy Cutting Tool (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.tungaloy.co.jp/th/index.php' target="_blank">Udom Niyomka Fund</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.excel.co.jp/cadmeister/e/index.shtml' target="_blank">UEL (Thailand )Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.uel.com.sg/index.htm' target="_blank">UEL Coporation</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.umc.co.jp/eng/index.html' target="_blank">UMC Electronics (Thailand) Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.unionzojirushi.com/' target="_blank">Union Chojirushi Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.hitachi.co.th/eng/network/index.html' target="_blank">Unisia Jecs (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://uaeconsultant.igetweb.com/index.php' target="_blank">United Analyst And Engineering Consultant Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.uvc.co.jp/english/index.php' target="_blank">Univance Corporation</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.uvc.co.jp/english/index.php' target="_blank">Uropien Bakery Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.uba.co.th/' target="_blank">Utility Business Alliance Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.vtgarment.com/' target="_blank">V.T. Garment Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.we-ef.com/' target="_blank">WE-EF Lighting Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='#' target="_blank"> Woraratyont</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.yspund.co.th/' target="_blank">Y S Pund Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://ymp-t.com/index_e.html' target="_blank">Y.M.P. (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://ymp-t.com/index_e.html' target="_blank">Yamasei Thai Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://ymp-t.com/index_e.html' target="_blank">Yamatake (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://yanmar.com/' target="_blank">Yanmar S.P. Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://ymp-t.com/index_t.htm' target="_blank">YMP Press &amp; Dies (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://ymp-t.com/index_t.htm' target="_blank">YN2-Tech (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.yea.co.th/aboutus.php' target="_blank">Yonezawa Engineering Asia (Thailand) Company Limited</a></td>
                                </tr>
                                <tr>
                                    <td class="shake">
                                        <a  href='http://www.yea.co.th/aboutus.php' target="_blank">Yorozu (Thailand) Company Limited</a></td>
                                </tr>
                            </tbody> 
                    </table>  
            </div>
     
</div>{{-- end containner --}}
    
<br>
@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">


@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

@endsection

