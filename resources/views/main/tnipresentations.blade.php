@extends('main.layouts.app')

@section('htmlheader_title'){{ trans('main/home.text_htmltitle') }}@endsection

@section('htmlheader_description'){{ trans('main/common.htmlheader_description') }}@endsection

@section('htmlheader_keywords'){{ trans('main/common.htmlheader_keywords') }}@endsection



@section('content-header')

    <div id="header" class="nav-item">

            <!-- <div class="container">
                <div class="second-header">{{ trans('main/home.text_tni_presentation') }}</div>
            </div> -->
            <div class="container">
                <div class="second-header">
                    @if(@$breadcrumb && count($breadcrumb) > 0)
                    <ul class="breadcrumb">
                        @if(@$breadcrumb)
                            @foreach(@$breadcrumb as $key => $value)
                                @if(@$value['active'] == 'active')
                                    <li class="active">{!! $key !!}</li>
                                @else
                                    <li><a href="{{ url(@$value['url']) }}">{!! $key !!}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    @endif
                </div>
            </div>

    </div><!-- End apply-now -->
    <div id="home-course" class="container">

    </div>

    
@endsection


@section('main-content')

<div class="container"> 
        <div class="row present">

            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">               
                <div class="video-item" id="th" >
                    <iframe width="100%" height="350" 
                        src="https://www.youtube.com/embed/Jf7zmZNsPuc" 
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>                   
                    </iframe>                          
                </div> 
            </div>
      
            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">               
                <div class="video-item" id="en">
                    <iframe width="100%" height="350" 
                        src="https://www.youtube.com/embed/RmXunf_wcGU" 
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>                   
                </div>    
            </div>  
      
            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">               
                <div class="video-item" id="jp">
                    <iframe width="100%" height="350" 
                        src="https://www.youtube.com/embed/beW2xALhFbs" 
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>                       
                </div> 
            </div> 

          
        </div>
      

        <div class="row present">

            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">    
                <h2>{{ trans('main/home.text_thai_version') }}</h2> 
            </div>

            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">    
                <h2>{{ trans('main/home.text_english_version') }}</h2> 
            </div>

            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">    
                <h2>{{ trans('main/home.text_japan_version') }}</h2> 
            </div>  
           

        </div>

        <div class="row present">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">               
                <div class="video-item" id="th" >
                    <iframe width="100%" height="450" 
                        src="https://www.youtube.com/embed/zK3e51DeE2g" 
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>                   
                    </iframe>             
                               
                </div> 
              
            </div>     
        </div>
        <div class="row present">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">    
                <h2>{{ trans('main/home.text_10year_tni') }}</h2> 
            </div>
        </div>
</div>

{{-- 
 
        <div class="container"> 
            <div class="row present">
                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">               
                    <div class="video-item" >
                        <iframe width="100%" height="350" 
                            src="https://www.youtube.com/embed/Jf7zmZNsPuc" 
                            frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>                   
                        </iframe>                          
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <h2>{{ trans('main/home.text_thai_version') }}</h2> 
                </div>
            </div>
      

            <div class="row present">
                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <h2>{{ trans('main/home.text_english_version') }}</h2> 
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">               
                    <div class="video-item" >
                        <iframe width="100%" height="350" 
                            src="https://www.youtube.com/embed/RmXunf_wcGU" 
                            frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                        </iframe>                   
                    </div>
                </div>  
            </div>
   

   
            <div class="row present">
                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">               
                    <div class="video-item" >
                        <iframe width="100%" height="350" 
                            src="https://www.youtube.com/embed/beW2xALhFbs" 
                            frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                        </iframe>                       
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <h2>{{ trans('main/home.text_japan_version') }}</h2> 
                </div>
            </div>
        </div>
   --}}

@endsection

@section('style')
<link rel="stylesheet" href="{{ url('lib/swiper-4.4.0/css/swiper.min.css') }}">
@endsection

@section('script')

<script src="{{ url('lib/swiper-4.4.0/js/swiper.min.js') }}"></script>
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

@endsection

