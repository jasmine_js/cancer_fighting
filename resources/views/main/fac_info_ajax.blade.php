<?php
    $selected_fac = $_GET['faculty_selected_param'];
    
    $fac_info = '';

    $text_more = trans('main/faculty.text_more');
    $text_major = trans('main/faculty.text_major');


//_เขียนแบบ heredoc ต้องเขียนชิด ห้ามมีย่อหน้า
//_%s คือ ค่าที่เราจะใส่ลง เรียวตามลำดับ ซึ่ง เราจะใช้ $fac_template กับฟังคชัน sprintf
//_ต.ย. sprintf($fac_template , ค่า สำหรับ %s ตัวที่1 ,  ค่า สำหรับ %s ตัวที่2 ,...)
//__* ถ้าจะใส่ % เช่น ข้างล่างนี้ เขียนเป็น 100%% ก็หมายถึง 100% นั้นแหละ

//head ป.ตรี
$fac_template = <<<EOT
<div id="fac-line-%s" class="container">
    <h1> %s </h1>
        <h5 id="jp_style" style="color:#767676;"> %s </h5>
        <a href="%s" target="_blank"><h5 style="color:%s;font-weight: 600; text-align:center;">{$text_more}</h5></a>

</div>
<div id="fac-line-%s">
    <div class="container">
        <div class="row" >
            <div class="col-xs-6 col-md-6 col-lg-6">
                <div class="fac-item">
                    <img src="%s" class="img-fluid" />
                </div>
            </div>
            <div class="col-xs-6  col-md-6 col-lg-6">
                <div class="fac-item">                                           
                    <h2 style="text-align:left;">%s</h2>
                    
                    <h4 style="text-align:left; ">%s</h4>
                    <h4 style="text-align:left;" >
                        <ul id="jp_style" style="list-style:circle;">
                            %s 
                        </ul>
                    </h4>

                    <h4 style="text-align:left; ">%s</h4>
                    <h4 style="text-align:left;" >
                        <ul  id="jp_style" style="list-style:circle;">
                            %s 
                        </ul>
                    </h4>
                    
                </div>
            </div>
        </div>
    </div> 
</div>
<div class="major-%s">
    <h1 >%s</h1> 
</div>

EOT;
$fac_template2 = <<<EOT

<div id="fac-line-%s">
    <div class="container">
        <div class="row" >
            <div class="col-xs-6 col-md-6 col-lg-6">
                <div class="fac-item">
                    <img src="%s" class="img-fluid" />
                </div>
            </div>
            <div class="col-xs-6  col-md-6 col-lg-6">
                <div class="fac-item" >
                    <h2>%s</h2>
                    <h5 id="jp_style_major" style="color:#767676; text-align:justify">
                         %s
                    </h5>
                    <a href="%s" target="_blank">
                        <h5 id="jp_style_major" style="color:%s;font-weight: 600; text-align:right;">{$text_more}</h5>
                    </a>
                </div>
            </div>
        </div>
    </div> 
</div>

EOT;
$fac_template3 = <<<EOT

<div id="fac-line-%s">
<div class="container">
            <div class="row" >
                    <div class="col-xs-6 col-md-6 col-lg-6">
                         <div class="fac-item">
                            <h2>%s</h2>
                            <h5 id="jp_style_major" style="color:#767676; text-align:justify">
                                    %s
                            </h5>
                            <a href="%s" target="_blank">
                                <h5 id="jp_style_major" style="color:%s;font-weight: 600;  text-align:right;">{$text_more}</h5>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-6 col-md-6 col-lg-6">
                        <div class="fac-item">
                            <img src="%s" class="img-fluid" />
                        </div>
                    </div>
            </div>
</div>
</div>

EOT;

//ใช้กับ RE

$fac_template4 = <<<EOT

<div id="fac-line-%s">
<div class="container">
            <div class="row" >
                    <div class="col-xs-6 col-md-6 col-lg-6">
                         <div class="fac-item">
                            <h2>%s</h2>
                            
                            <h5 id="jp_style_major" style="color:#767676; text-align:left">
                                <ul id="jp_engul" style="padding-left: 17px;">
                                    %s 
                                </ul>
                             </h5>
                            <a href="%s" target="_blank">
                                <h5 id="jp_style_major" style="color:%s;font-weight: 600;  text-align:right;">{$text_more}</h5>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-6 col-md-6 col-lg-6">
                        <div class="fac-item">
                            <img src="%s" class="img-fluid" />
                        </div>
                    </div>
            </div>
</div>
</div>

EOT;



// head ป.โท
$fac_template5 = <<<EOT
<div id="fac-line-%s" class="container">
    <h1>%s</h1>
        <h5 id="jp_style" style="color:#767676;"> %s </h5>
        <a href="%s" target="_blank"><h5 style="color:%s;font-weight: 600; text-align:center;">{$text_more}</h5></a>

</div>
<div id="fac-line-%s">
    <div class="container">
        <div class="row" >
            <div class="col-xs-6 col-md-6 col-lg-6">
                <div class="fac-item">
                    <img src="%s" class="img-fluid" />
                </div>
            </div>
            <div class="col-xs-6  col-md-6 col-lg-6">
                <div class="fac-item">                                           
                    <h2 style="text-align:left;">%s</h2>
                    
                    <h4 style="text-align:left; ">%s</h4>
                    <h4 id="jp_style_major" style="text-align:left;" >
                        <ul id="jp_style" style="list-style:circle;">
                            %s 
                        </ul>
                    </h4>
                    
                </div>
            </div>
        </div>
    </div> 
</div>
<div class="major-%s">
    <h1 >{$text_major}</h1> 
</div>   

EOT;



$fac_info_result = '';

    switch ($selected_fac) {
        case 'fac_thumbnal_eng':
        $fac_info_result = sprintf($fac_template ,  'eng-white' 
                                ,trans('main/faculty_eng.text_name') 
                                ,trans('main/faculty_eng.text_detail')
                                // ,'https://eng.tni.ac.th/about-us/' 
                                ,'https://www.tni.ac.th/engineering/major'
                                ,'rgb(167, 0, 0)' ,'eng' //_id
                                ,'img/faculty/eng/ENG.jpg'
                                ,trans('main/faculty_eng.text_program') 
                                ,trans('main/faculty_eng.text_bachelor')
                                ,trans('main/faculty_eng.text_major')
                                ,trans('main/faculty_eng.text_bacherlor_inter')
                                ,trans('main/faculty_eng.text_inter_eng_major')                                  
                                ,'eng' ,trans('main/faculty.text_major') );

            $fac_info_result.= sprintf($fac_template2 ,'eng-white' 
                                ,'img/faculty/eng/AE.jpg'  //AE
                                ,trans('main/faculty_eng.text_major1')
                                ,trans('main/faculty_eng.text_detail_major1')
                                // ,'https://eng.tni.ac.th/education/automotive-engineering/'
                                ,'https://www.tni.ac.th/engineering/major_ae'
                                ,'rgb(167, 0, 0)' ); 

            $fac_info_result.= sprintf($fac_template4 ,'eng-gray'                                 
                                ,trans('main/faculty_eng.text_major2')
                                ,trans('main/faculty_eng.text_detail_major2')
                                // ,'https://eng.tni.ac.th/education/production-engineering/'
                                ,'https://www.tni.ac.th/engineering/major_re'
                                ,'rgb(167, 0, 0)' 
                                ,'img/faculty/eng/PE.jpg' );  //LE

            $fac_info_result.= sprintf($fac_template2 ,'eng-white' 
                                ,'img/faculty/eng/CE.jpg' //CE
                                ,trans('main/faculty_eng.text_major3')
                                ,trans('main/faculty_eng.text_detail_major3')
                                // ,'https://eng.tni.ac.th/education/computer-engineering/' 
                                ,'https://www.tni.ac.th/engineering/major_ce'
                                ,'rgb(167, 0, 0)' ); 

            $fac_info_result.= sprintf($fac_template3 ,'eng-gray'
                                 ,trans('main/faculty_eng.text_major4')
                                ,trans('main/faculty_eng.text_detail_major4')
                                // ,'https://eng.tni.ac.th/education/industrial-engineering/'
                                ,'https://www.tni.ac.th/engineering/major_ie'
                                ,'rgb(167, 0, 0)' 
                                ,'img/faculty/eng/IE.jpg');     //IE
            $fac_info_result.= sprintf($fac_template2 ,'eng-white' 
                                ,'img/faculty/eng/EE.jpg' //EE
                                ,trans('main/faculty_eng.text_major5')
                                ,trans('main/faculty_eng.text_detail_major5')
                                // ,'https://eng.tni.ac.th/education/electrical-engineering/'
                                ,'https://www.tni.ac.th/engineering/major_ee'
                                ,'rgb(167, 0, 0)' );
            $fac_info_result.= sprintf($fac_template3 ,'eng-gray' //inter DGE
                                ,trans('main/faculty_eng.text_inter_eng_major1')
                                ,trans('main/faculty_eng.text_detail_inter_eng_major1')
                                // ,'https://eng.tni.ac.th/education/digital-engineering/'
                                ,'https://www.tni.ac.th/engineering/major_dge'
                                ,'rgb(167, 0, 0)' 
                                ,'img/faculty/inter/ENG-INTER.jpg');                
        break;

        case 'fac_thumbnal_ba':
            $fac_info_result = sprintf($fac_template , 'ba-white' 
                                , trans('main/faculty_ba.text_name')//'คณะบริหารธุรกิจ' 
                                , trans('main/faculty_ba.text_detail')
                                ,'https://ba.tni.ac.th/2018/?page_id=192' 
                                ,'#6EC8FF' 
                                ,'ba'
                                ,'img/faculty/ba/BA.jpg'
                                , trans('main/faculty_ba.text_program')//'หลักสูตร/สาขา'
                                , trans('main/faculty_ba.text_bachelor')//'ปริญญาตรี'
                                , trans('main/faculty_ba.text_major')
                                ,trans('main/faculty_ba.text_bacherlor_inter')
                                ,trans('main/faculty_ba.text_inter_ba_major')        
                                ,'ba' ,trans('main/faculty.text_major') );

            $fac_info_result.= sprintf($fac_template2 ,'ba-white' 
                                ,'img/faculty/ba/MI.jpg'
                                ,trans('main/faculty_ba.text_major1')//'การจัดการเทคโนโลยี และนวัตกรรมการผลิต'
                                ,trans('main/faculty_ba.text_detail_major1')
                                ,'https://ba.tni.ac.th/2018/?p=494'
                                ,'#6EC8FF' ,'+ ดูรายละเอียดเพิ่มเติม' ); 

            $fac_info_result.= sprintf($fac_template3 ,'ba-gray'
                                , trans('main/faculty_ba.text_major2')//'BJ บริหารธุรกิจญี่ปุ่น'
                                ,trans('main/faculty_ba.text_detail_major2')
                                ,'https://ba.tni.ac.th/2018/?p=496'                               
                                ,'#6EC8FF' 
                                ,'img/faculty/ba/BJ.jpg'); 

            $fac_info_result.= sprintf($fac_template2 ,'ba-white' 
                                ,'img/faculty/ba/IB.jpg'
                                , trans('main/faculty_ba.text_major3')//'การจัดการธุรกิจระหว่างประเทศ'
                                ,trans('main/faculty_ba.text_detail_major3')
                                ,'https://ba.tni.ac.th/2018/?p=503'                                                                      
                                ,'#6EC8FF' ); 

            $fac_info_result.= sprintf($fac_template3 ,'ba-gray'
                                ,trans('main/faculty_ba.text_major4')//'การบัญชี'
                                ,trans('main/faculty_ba.text_detail_major4')
                                ,'https://ba.tni.ac.th/2018/?p=505'
                                ,'#6EC8FF' 
                                ,'img/faculty/ba/AC.jpg');

            $fac_info_result.= sprintf($fac_template2  ,'ba-white' 
                                ,'img/faculty/ba/HR.jpg'
                                ,trans('main/faculty_ba.text_major5')//'การจัดการทรัพยากรมนุษย์แบบญี่ปุ่น'
                                ,trans('main/faculty_ba.text_detail_major5')
                                ,'https://ba.tni.ac.th/2018/?p=507'
                                ,'#6EC8FF' ); 

            $fac_info_result.= sprintf($fac_template3 ,'ba-gray'
                                ,trans('main/faculty_ba.text_major6')//'การจัดการโลจิสติกส์และโซ่อุปทาน'
                                ,trans('main/faculty_ba.text_detail_major6')
                                ,'https://ba.tni.ac.th/2018/?p=509'
                                ,'#6EC8FF' 
                                ,'img/faculty/ba/LM.jpg' );

            $fac_info_result.= sprintf($fac_template2   ,'ba-white' 
                                ,'img/faculty/ba/cm.jpg'
                                ,trans('main/faculty_ba.text_major7')//'การตลาดเชิงสร้างสรรค์'
                                ,trans('main/faculty_ba.text_detail_major7')
                                ,'https://ba.tni.ac.th/2018/?p=511'                                  
                                ,'#6EC8FF' ); 

            $fac_info_result.= sprintf($fac_template3 ,'ba-gray'
                                ,trans('main/faculty_ba.text_major8')//'การจัดการท่องเที่ยวและการบริการเชิงนวัตกรรม TH' 
                                ,trans('main/faculty_ba.text_detail_major8')
                                ,'https://ba.tni.ac.th/2018/?p=1487'
                                ,'#6EC8FF'
                                ,'img/faculty/ba/th.jpg' );

            // $fac_info_result.= sprintf($fac_template2 ,'ba-white' 
            //                     ,'image/catalog/mockup/mockup-1.png'
            //                     ,trans('main/faculty_ba.text_major9')
            //                     ,trans('main/faculty_ba.text_detail_major9')
            //                     ,'https://ba.tni.ac.th/2018/?p=1487'
            //                     ,'#6EC8FF' );
            $fac_info_result.= sprintf($fac_template2 ,'ba-white'  //IBM
                                ,'img/faculty/inter/BA-INTER.jpg'
                                ,trans('main/faculty_ba.text_inter_ba_major1')//inter
                                ,trans('main/faculty_ba.text_detail_inter_ba_major1')
                                ,'http://inter.tni.ac.th/page/INTERNATIONAL-BUSINESS-MANAGEMENT-IBM'
                                ,'#6EC8FF' );

        break;

        case 'fac_thumbnal_it':
            $fac_info_result = sprintf($fac_template , 'it-white' 
                                ,trans('main/faculty_it.text_name')
                                ,trans('main/faculty_it.text_detail')
                                ,'https://it.tni.ac.th/about-it/' 
                                ,'#302b82'  
                                ,'it' //_id
                                ,'img/faculty/inter/IT-INTER.jpg'
                                ,trans('main/faculty_it.text_program') 
                                ,trans('main/faculty_it.text_bachelor')
                                ,trans('main/faculty_it.text_major')  
                                ,trans('main/faculty_it.text_bacherlor_inter')
                                ,trans('main/faculty_it.text_inter_it_major')                                         
                                ,'it',trans('main/faculty.text_major') );

            $fac_info_result.= sprintf($fac_template2 ,'it-white' 
                                ,'img/faculty/it/it.jpg' //it
                                ,trans('main/faculty_it.text_major1')
                                ,trans('main/faculty_it.text_detail_major1')
                                ,'https://it.tni.ac.th/curriculum/bachelor-degree/it/'
                                ,'#302b82' ); 

            $fac_info_result.= sprintf($fac_template3 ,'it-gray'  //mt
                                ,trans('main/faculty_it.text_major2')
                                ,trans('main/faculty_it.text_detail_major2')
                                ,'https://it.tni.ac.th/curriculum/bachelor-degree/mt/'
                                ,'#302b82' 
                                ,'img/faculty/it/MT.jpg');    

            $fac_info_result.= sprintf($fac_template2 ,'it-white' 
                                ,'img/faculty/it/BI.jpg'  //bi
                                ,trans('main/faculty_it.text_major3')
                                ,trans('main/faculty_it.text_detail_major3')
                                ,'https://it.tni.ac.th/curriculum/bachelor-degree/bi/'
                                ,'#302b82' ); 

            $fac_info_result.= sprintf($fac_template3 ,'it-gray'
                                ,trans('main/faculty_it.text_major4') //dc
                                ,trans('main/faculty_it.text_detail_major4')
                                ,'https://it.tni.ac.th/curriculum/bachelor-degree/dc/'
                                ,'#302b82' 
                                ,'img/faculty/it/DC.jpg');    

            $fac_info_result.= sprintf($fac_template2 ,'it-white' //inter dsa
                                ,'img/faculty/inter/IT-INTER.jpg'
                                ,trans('main/faculty_it.text_inter_it_major1')
                                ,trans('main/faculty_it.text_detail_inter_it_major1')
                                ,'https://it.tni.ac.th/curriculum/bachelor-degree/dsa/'
                                ,'#302b82' );  

        break;    

        case 'fac_thumbnal_cgel':
            $fac_info_result = sprintf($fac_template , 'cgel-white' // fac_template = head ป.ตรี
                                , trans('main/faculty_cgel.text_name')//'สำนักวิชาพื้นฐานและภาษา' 
                                , trans('main/faculty_cgel.text_detail')
                                ,'https://cgel.tni.ac.th/2018/?page_id=1302' 
                                ,'#FFAB00'  
                                ,'cgel' //_id
                                ,'img/faculty/cgel/CGEL.jpg'
                                , trans('main/faculty_cgel.text_program')//'หลักสูตร/สาขา'
                                ,''//'ปริญญาตรี'
                                , trans('main/faculty_cgel.text_major')
                                ,''
                                ,''
                                ,'cgel',trans('main/faculty_cgel.text_subject') );

            $fac_info_result.= sprintf($fac_template2 ,'cgel-white' 
                                ,'img/faculty/cgel/CGEL-m.jpg'  //jp
                                ,trans('main/faculty_cgel.text_major1')
                                ,trans('main/faculty_cgel.text_detail_major1')
                                
                                ,'https://cgel.tni.ac.th/2018/?cat=19'
                                ,'#FFAB00' );

            $fac_info_result.= sprintf($fac_template3 ,'cgel-gray'
                                ,trans('main/faculty_cgel.text_major2') //en
                                ,trans('main/faculty_cgel.text_detail_major2')
                                ,'https://cgel.tni.ac.th/2018/?cat=20'
                                ,'#FFAB00' 
                                ,'img/faculty/cgel/eng.jpg');  

            $fac_info_result.= sprintf($fac_template2 ,'cgel-white' 
                                ,'img/faculty/cgel/socity.jpg'  //พื้นฐานและสังคม
                                ,trans('main/faculty_cgel.text_major3')
                                ,trans('main/faculty_cgel.text_detail_major3')
                                ,'https://cgel.tni.ac.th/2018/?cat=21'
                                ,'#FFAB00' );
            $fac_info_result.= sprintf($fac_template3 ,'cgel-gray'
                                ,trans('main/faculty_cgel.text_major4') //math
                                ,trans('main/faculty_cgel.text_detail_major4')
                                ,'#'
                                ,'#FFAB00' 
                                ,'img/faculty/cgel/science.jpg');  

        break; 
            
        case 'fac_thumbnal_master1':
            $fac_info_result = sprintf($fac_template5 ,  'eng-white'  // fac_template4 = head ป.โท 
                                ,trans('main/faculty_master.text_eng')
                                ,trans('main/faculty_master.text_eng_detail')
                                // ,'https://eng.tni.ac.th/education/m-eng-engineering-technology/'
                                ,'https://www.tni.ac.th/engineering/major'
                                ,'rgb(167, 0, 0)'  

                                ,'eng' //_id
                                // ,'img/faculty/master/ENG_f.jpg' 
                                ,'img/faculty/master/mmet.jpg'
                                ,trans('main/faculty_master.text_program')
                                ,trans('main/faculty_master.text_master')
                                ,trans('main/faculty_master.text_eng_major')
                                ,'eng');
            $fac_info_result.= sprintf($fac_template2 ,'eng-white' 
                                // ,'img/faculty/master/ENG_m.jpg'
                                ,'img/faculty/master/met.jpg'
                                ,trans('main/faculty_master.text_eng_major1')
                                ,trans('main/faculty_master.text_detail_eng_major1')
                                // ,'https://www.tni.ac.th/grad_programme/met_program/'
                                ,'https://www.tni.ac.th/engineering/major_met'
                                ,'rgb(167, 0, 0)' ); 

            
            break;

        case 'fac_thumbnal_master2':
            $fac_info_result = sprintf($fac_template5 , 'ba-white' // fac_template4 = head ป.โท 
                                ,trans('main/faculty_master.text_ba')
                                ,trans('main/faculty_master.text_ba_detail')
                                ,'https://grad.tni.ac.th/masterprogram/mba/'
                                ,'#6EC8FF' 
                               
                                ,'ba'
                                ,'img/faculty/master/BA.jpg'
                                ,trans('main/faculty_master.text_program')
                                ,trans('main/faculty_master.text_master')
                                ,trans('main/faculty_master.text_ba_major')                                   
                                ,'ba' );

            $fac_info_result.= sprintf($fac_template2 ,'ba-white' 
                                ,'img/faculty/master/MBA.jpg'
                                ,trans('main/faculty_master.text_ba_major1')
                                ,trans('main/faculty_master.text_detail_ba_major1')
                                ,'https://www.tni.ac.th/grad_programme/mbi_program/'
                                ,'#6EC8FF' ); 

            $fac_info_result.= sprintf($fac_template3 ,'ba-gray'
                                ,trans('main/faculty_master.text_ba_major2')
                                ,trans('main/faculty_master.text_detail_ba_major2')
                                ,'https://www.tni.ac.th/grad_programme/mbj_program/'
                                ,'#6EC8FF' 
                                ,'img/faculty/master/MBJ.jpg');   

            $fac_info_result.= sprintf($fac_template2 ,'ba-white' 
                                ,'img/faculty/master/lms.jpg'
                                ,trans('main/faculty_master.text_ba_major3')
                                ,trans('main/faculty_master.text_detail_ba_major3')
                                ,'https://www.tni.ac.th/grad_programme/lms_program/'
                                ,'#6EC8FF' ); 

            break;    

        case 'fac_thumbnal_master3':
        $fac_info_result = sprintf($fac_template5 , 'it-white' // fac_template4 = head ป.โท 
                                ,trans('main/faculty_master.text_it')
                                ,trans('main/faculty_master.text_it_detail')
                                ,'https://it.tni.ac.th/curriculum/masters-degree/mit/'
                                ,'#302b82'  

                                ,'it' //_id
                                ,'img/faculty/master/IT.jpg'
                                ,trans('main/faculty_master.text_program')
                                ,trans('main/faculty_master.text_master')
                                ,trans('main/faculty_master.text_it_major')          
                                ,'it');

            $fac_info_result.= sprintf($fac_template2 ,'it-white' 
                                ,'img/faculty/master/MIT.jpg'
                                ,trans('main/faculty_master.text_it_major1')
                                ,trans('main/faculty_master.text_detail_it_major1')
                                ,'https://www.tni.ac.th/grad_programme/mit_program/'
                                ,'#302b82' );
           

            break;
            
        // case 'fac_thumbnal_inter1':
        //     $fac_info_result = sprintf($fac_template ,  'ba-white' 
        //                         ,trans('main/faculty_master.text_inter_ba')
        //                         ,trans('main/faculty_master.text_inter_ba_detail')
        //                         ,'http://inter.tni.ac.th/page/INTERNATIONAL-BUSINESS-MANAGEMENT-IBM'
        //                         ,'#6EC8FF' 

        //                         ,'ba'
        //                         ,'image/catalog/mockup/mockup-1.png'
        //                         ,trans('main/faculty_master.text_program')
        //                         ,trans('main/faculty_master.text_bacherlor_inter')
        //                         ,trans('main/faculty_master.text_inter_ba_major')                                        
        //                         ,'ba' );

        //     $fac_info_result.= sprintf($fac_template2 ,'ba-white' 
        //                         ,'image/catalog/mockup/mockup-1.png'
        //                         ,trans('main/faculty_master.text_inter_ba_major1')
        //                         ,trans('main/faculty_master.text_detail_inter_ba_major1')
        //                         ,'http://inter.tni.ac.th/page/INTERNATIONAL-BUSINESS-MANAGEMENT-IBM'
        //                         ,'#6EC8FF '  ); 
            
        //     break;    

        // case 'fac_thumbnal_inter2':
        //     $fac_info_result = sprintf($fac_template ,  'eng-white' 
        //                         ,trans('main/faculty_master.text_inter_eng')
        //                         ,trans('main/faculty_master.text_inter_eng_detail')
        //                         ,'http://inter.tni.ac.th/page/DIGITAL-ENGINEERING'
        //                         ,'rgb(167, 0, 0)' 

        //                         ,'eng' //_id
        //                         ,'image/catalog/mockup/mockup-1.png'
        //                         ,trans('main/faculty_master.text_program')
        //                         ,trans('main/faculty_master.text_bacherlor_inter')
        //                         ,trans('main/faculty_master.text_inter_eng_major') 
        //                         ,'eng');

        //     $fac_info_result.= sprintf($fac_template2 , 'eng-white' 
        //                         ,'image/catalog/mockup/mockup-1.png'
        //                         ,trans('main/faculty_master.text_inter_eng_major1')
        //                         ,trans('main/faculty_master.text_detail_inter_eng_major1')
        //                         ,'http://inter.tni.ac.th/page/DIGITAL-ENGINEERING'
        //                         ,'rgb(167, 0, 0)'  ); 

        //     break;    
            
        // case 'fac_thumbnal_inter3':
        //     $fac_info_result = sprintf($fac_template , 'it-white' 
        //                         ,trans('main/faculty_master.text_inter_it')
        //                         ,trans('main/faculty_master.text_inter_it_detail')
        //                         ,'http://inter.tni.ac.th/page/DATA-SCIENCE-AND-ANALYTICS'
        //                         ,'#302b82'  

        //                         ,'it' //_id
        //                         ,'image/catalog/mockup/mockup-1.png'
        //                         ,trans('main/faculty_master.text_program')
        //                         ,trans('main/faculty_master.text_bacherlor_inter')
        //                         ,trans('main/faculty_master.text_inter_it_major')          
                                
        //                         ,'it');

        //     $fac_info_result.= sprintf($fac_template2 ,'it-white' 
        //                         ,'image/catalog/mockup/mockup-1.png'
        //                         ,trans('main/faculty_master.text_inter_it_major1')
        //                         ,trans('main/faculty_master.text_detail_inter_it_major1')
        //                         ,'http://inter.tni.ac.th/page/DATA-SCIENCE-AND-ANALYTICS'
        //                         ,'#302b82' );
                                               
        //     break;        

        default:
            $fac_info = '<h2>default text</h2>';           
            break;
    }

    echo $fac_info_result;
?>
