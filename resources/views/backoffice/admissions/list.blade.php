@extends('adminlte::page')

@section('htmlheader_title')
    {{ trans('backoffice/admissions.text_admission_lists') }}
@endsection

@section('contentheader_title')
<i class="fa fa-admissionpaper-o" aria-hidden="true"></i> {{ trans('backoffice/admissions.text_admission_lists') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                
                @if(in_array('admission/modify', Auth::user()->roles()->pluck('name')->toArray()))
                <div class="text-right">
                    <a href="{{ url(Config::get('url.backoffice.admissions_create')) }}" data-toggle="tooltip" title="{{ trans('backoffice/common.text_create') }}" class="btn btn-success"><i class="fa fa-plus"></i></a>
                </div>
                <br />
                @endif
                    
                {{ Form::open(['method' => 'GET']) }}
                <div class="row">
                    <div class="col-sm-6">                     
                        <select name="admission_category_id" id="admission_category_id" class="form-control">
                            <option value="">-- {{ trans('backoffice/admissions.text_category') }} --</option>
                            @foreach($categories as $category)
                            @if($category->id == $admission_category_id)
                            <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                            @else
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endif
                            @endforeach
                        </select>
                        <br />
                        <input type="text" name="find" id="find" value="{{ @$find }}" class="form-control" placeholder="{{ trans('backoffice/common.text_search') }}" maxlength="255" />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-primary" data-toggle="tooltip" title="{{ trans('backoffice/common.text_search') }}"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <a href="{{ url(Config::get('url.backoffice.admissions')) }}" data-toggle="tooltip" title="{{ trans('backoffice/common.text_clear_search') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
                    </div>
                </div>
                {{ Form::close() }}
                <br>
        
                <div class="panel panel-default">
        
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th width="5%">{{ trans('backoffice/common.text_id') }}</th>
                                <th width="10%">{{ trans('backoffice/common.text_image') }}</th>
                                <th width="25%">{{ trans('backoffice/common.text_name') }}</th>
                                <th width="10%" class="hidden-xs">{{ trans('backoffice/common.text_created_at') }}</th>
                                
                                <th width="10%" class="hidden-xs">{{ trans('backoffice/common.text_created_by') }}</th>
                                
                                <th width="10%" class="hidden-xs">{{ trans('backoffice/common.text_updated_at') }}</th>

                                <th width="10%" class="hidden-xs">{{ trans('backoffice/common.text_updated_by') }}</th>

                                <th width="15%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach ($items as $i => $item )
                            <tr class="{{ $item->admission->status ? '' : 'textDeactive' }}">
                                <td scope="row">{{ sprintf('%05d', $item->admission_id) }}</td>
                                <td><img src="{{ $item->image }}" class="img-thumbnail" /></td>
                                <td>{{ $item->name }}</td>
                                <td class="hidden-xs created-set">
                                    {{ $item->admission->created_at->format('d M Y') }}
                                </td>

                                <td class="hidden-xs created-set">
                                    {{$username[$i]['created_by']['name']}}
                                    {{--$item->user --}}
                                </td>

                                <td class="hidden-xs created-set">
                                   {{ $item->admission->updated_at->format('d M Y') }}
                            
                                </td>

                                <td class="hidden-xs created-set">
                                    {{--$username[$i]['updated_by']['name']--}}
                                    {{$username[$i]['updated_by']['name']}}
                                </td>

                                <td align="center">
                                    <div class="action">
                                        {{ Form::open(['url' => Config::get('url.backoffice.admissions') . '/' . $item->admission_id, 'method' => 'delete']) }}

                                        @if(in_array('admission/modify', Auth::user()->roles()->pluck('name')->toArray()))                                   
                                        <a data-toggle="tooltip" title="{{ trans('backoffice/common.text_edit') }}" href="{{ url(Config::get('url.backoffice.admissions') . '/' . $item->admission_id . '/edit') }}" class="btn btn-primary">
                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                        </a>

                                        {{-- <a data-toggle="tooltip" title="{{ trans('backoffice/common.text_touch') }}" href="{{ url(Config::get('url.backoffice.admissions') . '/touch/' . $item->admission_id . '/?admission_category_id=' . $admission_category_id) }}" class="btn btn-success">
                                            <i class="fa fa-hand-o-up" aria-hidden="true"></i>
                                        </a> --}}

                                        <button data-toggle="tooltip" title="{{ trans('backoffice/common.text_delete') }}" type="button" class="btn btn-danger btn-delete">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                        @endif

                                        {{ Form::close() }}
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                
                <div class="pager">
                    <div class="pageinfo">
                            <span>{{ trans('backoffice/common.text_all_results') }} {{ @$items->total() }} {{ trans('backoffice/common.text_item(s)') }}</span>
                            <span>{{ trans('backoffice/common.text_page') }} {{ $items->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $items->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}</span>
                        </div>
                    <div>
                    <div>
                        {{ $items->appends(['admission_category_id' => $admission_category_id, 'find' => $find])->links() }}
                    </div>					
                </div>	

            </div>
        </div>
	</div>
@endsection


@section('script')
<script>
@if(Session::has('success'))
    alertToat("{{ Session::get('success') }}");
@endif
@if(Session::has('error'))
    alertToat("{{ Session::get('error') }}", "red");
@endif
$('button.btn-search-reset').click(function(){
    location.href = '{{ url(Config::get("url.backoffice.admissions")) }}';
});

$('.btn-delete').click(function(){
    var _this = this;
    swalConfirm('{{ trans('backoffice/common.text_confirm_delete') }}', '{{ trans('backoffice/common.text_confirm_delete_message') }}')
    .then(function(result){
        if(result){
            $(_this).parent().submit();
        }
    });
});
</script>
@endsection