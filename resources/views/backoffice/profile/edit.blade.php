@extends('adminlte::page')

@section('htmlheader_title') 
	{{ trans('backoffice/common.text_profile') }} - {{ $item->name }}
@endsection

@section('contentheader_title')
<i class='fa fa-user'></i> {{ trans('backoffice/common.text_profile') }}
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">

			<div class="text-right">
				<button type="submit" form="submitForm" data-toggle="tooltip" title="{{ trans('backoffice/common.text_save') }}" class="btn btn-primary btn-save"><i class="fa fa-save"></i></button>
			</div>
			<br />

			{{ Form::model($item, ['method' => 'PUT', 'url' => Config::get('url.backoffice.profile') , 'files' => true, 'id' => 'submitForm']) }}
			
			<div class="panel panel-default">
				<div class="panel-body">
		
				   @include('backoffice.partials.error-lists')
		
					<div class="col-sm-12">
		
						<div class="form-group">
							<label for="name">{{ trans("backoffice/common.text_name") }}*</label>
							<div class="input-group">
								<i aria-hidden="true" class="input-group-addon fa fa-user"></i>
							{{ Form::text('name', null, ['maxlength' => '100', 'required', 'class' => 'form-control', 'placeholder' => 'Name', 'required']) }}
							</div>
						</div>
		
						<div class="form-group">
							<label for="name">{{ trans("backoffice/common.text_email") }}*</label>
							<div class="input-group">
								<i aria-hidden="true" class="input-group-addon fa fa-envelope"></i>
							{{ Form::text('email', null, ['maxlength' => '255', 'required', 'class' => 'form-control', 'placeholder' => 'E-Mail', 'required']) }}
							</div>
						</div>
		
						<div class="form-group">
							<label for="name">{{ trans("backoffice/common.text_password") }}*</label>
							<div class="input-group">
								<i aria-hidden="true" class="input-group-addon fa fa-key"></i>
								{{ Form::password('password', ['maxlength' => '50', 'class' => 'form-control', 'placeholder' => 'Password']) }}
							</div>
							
						</div>
		
						<div class="form-group">
								<label for="name">{{ trans("backoffice/common.text_password_confirmation") }}*</label>
							<div class="input-group">
								<i aria-hidden="true" class="input-group-addon fa fa-key"></i>
								{{ Form::password('password_confirmation', ['maxlength' => '50', 'class' => 'form-control', 'placeholder' => 'Password Confirmation']) }}
							</div>
						</div>

						<div class="form-group">
							<div style="margin-bottom: 5px">{{ trans('backoffice/common.text_image') }}</div>
							<a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
								@if(@$item->thumb)
								<img src="{{ $item->thumb }}" data-placeholder="{{ url('storage/image/cache/no_image-100x100.png') }}" />
								@else
								<img src="{{ url('storage/image/cache/no_image-100x100.png') }}" data-placeholder="{{ url('storage/image/cache/no_image-100x100.png') }}" />
								@endif
							</a>
							<input type="hidden" name="image" id="input-image" value="{{ @$item->image }}" />
						</div>
		
					</div>
				</div>

			</div>		
			
			{{ Form::close() }}
		</div>
	</div>
</div>
</div>
@endsection


@section('script')
<script>
	@if(Session::has('success'))
		alertToat("{{ Session::get('success') }}");
	@endif

	$('#privilegeAll').click(function () {
		// console.log($('#privilegeAll').prop('checked'));
		$('.privilege input:checkbox').prop('checked', $('#privilegeAll').prop('checked'));
	});

	$('#submitForm').submit(function(){
        $('.btn-save').prop('disabled', true);
        $('.btn-save').html('{{ trans("backoffice/common.text_saving") }}...');
        //return false;
    });
</script>
@endsection