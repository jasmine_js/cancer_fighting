@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('backoffice/ebochures.text_edit_ebochure') }}
@endsection

@section('contentheader_title')
<i class="fa fa-pencil-square-o" aria-hidden="true"></i> {{ trans('backoffice/ebochures.text_edit_ebochure') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">

    <div class="row">
            <div class="col-sm-12 col-xs-12">
            
                <div class="text-right">
                    <button type="submit" form="submitForm" data-toggle="tooltip" title="{{ trans('backoffice/common.text_save') }}" class="btn btn-primary btn-save"><i class="fa fa-save"></i></button>
                    <a href="{{ URL::to(Config::get('url.backoffice.ebochures')) }}" data-toggle="tooltip" title="{{ trans('backoffice/common.text_cancel') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
                </div>
                <br />
                
                <div class="panel panel-default">
                    <div class="panel-body">
                        
                        @include('backoffice.partials.error-lists')

                        {{ Form::model($item, ['url' => Config::get('url.backoffice.ebochures') . '/' . $item->id, 'method' => 'put', 'id' => 'submitForm'])}}
                            @include('backoffice.ebochures.form', [])
                        {{ Form::close() }}

                    </div>
                </div>

        </div>
    </div>
@endsection

