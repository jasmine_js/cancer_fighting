<div id="filemanager" class="modal-dialog modal-lg">
  <div class="modal-content">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">Document File Manager</h4>
    </div>
      
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-5">
            <a href="{{ @$parent }}" data-toggle="tooltip" title="Back to Root" id="button-parent_doc" class="btn btn-default"><i class="fa fa-level-up"></i></a> 
            <a href="{{ @$refresh }}" data-toggle="tooltip" title="Refresh" id="button-refresh_doc" class="btn btn-default"><i class="fa fa-refresh"></i></a>
            <button type="button" data-toggle="tooltip" title="Upload" id="button-upload_doc" class="btn btn-primary"><i class="fa fa-upload"></i></button>
            <button type="button" data-toggle="tooltip" title="New folder" id="button-folder_doc" class="btn btn-default"><i class="fa fa-folder"></i></button>
            <button type="button" data-toggle="tooltip" title="Delete" id="button-delete_doc" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
          </div>
          <div class="col-sm-7">
            <div class="input-group">
              <input type="text" name="search" value="{{ @$filter_name }}" placeholder="Search..." class="form-control">
              <span class="input-group-btn">
              <button type="button" data-toggle="tooltip" title="ค้นหา" id="button-search_doc" class="btn btn-primary"><i class="fa fa-search"></i></button>
              </span></div>
          </div>
       
          <div class="col-sm-5">
            <h5 style="color:red"> ** หมายเหตุ : ไฟล์ต้องมีขนาดไม่เกิน 100 MB **</h5>
          </div>
       
        </div>
         <hr />
        
        
        <!-- _Display files : 4 columns / 1 row -->
        
       <div class='row'>
          @foreach($documents as $document)
            <div class="col-sm-3 col-xs-6 text-center">
              @if($document['type'] == 'directory')
                <div class="text-center">
                  <a href="{{ $document['href'] }}" class="directory" style="vertical-align: middle;">
                    <i class="fa fa-folder fa-5x"></i>
                  </a>
                </div>
                <div>
                  <label>
                    <input type="checkbox" name="path[]" value="{{ $document['path'] }}" /> {{$document['name']}}
                  </label>
                </div>
              @endif

              @if($document['type'] != 'directory')
                  <div class="text-center">
                  <a href="{{ $document['href'] }}" class="thumbnail" style="vertical-align: middle;font-size:2em;border:0">
                    {!! $document['thumb'] !!} <input type='hidden' value="{{$document['name'] }}">
                  </a>
                  </div>
                  <div>
                    <label>
                      <input type="checkbox" name="path[]" value="{{ $document['path'] }}" /> {{ $document['name'] }}
                    </label>
                  </div>
              @endif
            </div>
          @endforeach  
        </div>
      </div> <!--_modal-body -->
    <div class="modal-footer">{!! $pagination !!}</div>
  </div> <!-- modal-content -->
</div>   


<script>
  @if($target)
    $('a.thumbnail').on('click', function(e) {

        e.preventDefault();
        @if($thumb)
          $('#{{ $thumb }}').find('img').attr('src', $(this).find('img').attr('src'));
        @endif
    
        $('#{{ $target }}').val($(this).parent().find('input').val());
        $('#modal-document').modal('hide');
    });
  @endif

  //_click on directory  
  $('a.directory').on('click', function(e) {
      e.preventDefault();
  
      $('#modal-document').load($(this).attr('href'));
  });

  //_click on next or previous page
  $('.pagination a').on('click', function(e) {
      e.preventDefault();
  
      $('#modal-document').load($(this).attr('href'));
  });

  //_Refresh Button CLICK event
  $('#button-refresh_doc').on('click', function(e) {
      e.preventDefault();
      $('#modal-document').load($(this).attr('href'));
     
  });

  //_Back to root Button : CLICK event
  $('#button-parent_doc').on('click', function(e) {
      e.preventDefault();
      $('#modal-document').load($(this).attr('href'));
  });

  //_SEARCH
  $('input[name=\'search\']').on('keydown', function(e) {
      if (e.which == 13) {//_enter
          $('#button-search_doc').trigger('click');
      }
  });
  $('#button-search_doc').on('click', function(e) {
      //var url = '{{ Config::get('app.url') }}backoffice/filemanager_doc/?directory={{ $directory }}';
      var url = "{{ url(Config::get('url.backoffice.filemanager_doc')) }}/?directory={{ $directory }}";
      var filter_name = $('input[name=\'search\']').val();
  
      if (filter_name) {
          url += '&filter_name=' + encodeURIComponent(filter_name);
      }
  
      @if($thumb)
        url += '&thumb=' + '{{ $thumb }}';
      @endif
  
      @if($target)
        url += '&target=' + '{{ $target }}';
      @endif
  
      $('#modal-document').load(url);
  });

  //_POPUP WINDOW : NEW FOLDER
  $('#button-folder_doc').popover({
      html: true,
      placement: 'bottom',
      trigger: 'click',
      title: 'Folder Name',
      content: function() {
          html  = '<div class="input-group">';
          html += '  <input type="text" name="folder" value="" placeholder="Enter 3 characters or more" class="form-control">';
          html += '  <span class="input-group-btn"><button type="button" title="button_folder" id="button-create" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></span>';
          html += '</div>';
  
          return html;
      }
  });

  //_When New Folder Shown this event is occur
  //_'shown.bs.popover' = The shown.bs.popover event occurs when the popover is fully shown.
  $('#button-folder_doc').on('shown.bs.popover', function() {
      
      $('#button-create').on('click', function() {
  
          //console.log("{{ url(Config::get('url.backoffice.filemanager_doc')) }}/folder/?directory={{ $directory }}");

          $.ajax({
              //url: '{{ Config::get('app.url') }}backoffice/filemanager/folder/?directory={{ $directory }}',
              url: "{{ url(Config::get('url.backoffice.filemanager_doc')) }}/folder/?directory={{ $directory }}",
              type: 'post',
              dataType: 'json',
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: 'folder=' + encodeURIComponent($('input[name=\'folder\']').val()),
              beforeSend: function() {
                  $('#button-create').prop('disabled', true);
              },
              complete: function() {
                  $('#button-create').prop('disabled', false);
              },
              success: function(json) {
                  if (json['error']) {
                      alert(json['error']);
                  }
  
                  if (json['success']) {
                      //alert(json['success']);
  
                      $('#button-refresh_doc').trigger('click');
                  }
              },
              error: function(xhr, ajaxOptions, thrownError) {
                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
              }
          });
      });
  });

  //_UPLOAD BUTTON CLICK
  $('#button-upload_doc').on('click', function() {

    $('#form-upload').remove();
    $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file[]" value="" multiple="multiple" accept=".pdf,.docx,doc,.xlsx,.xls"/></form>');
    $('#form-upload input[name=\'file[]\']').trigger('click');
  
    if (typeof timer != 'undefined') {
      clearInterval(timer);
    }

    //_Time delay set
    timer = setInterval(function(){

      //_if upload file input text not empty
      if ($('#form-upload input[name=\'file[]\']').val() != '')
      {

        clearInterval(timer);

        $.ajax({
            //url: '{{ Config::get('app.url') }}backoffice/filemanager/upload/?directory={{ $directory }}',
            url: "{{ url(Config::get('url.backoffice.filemanager_doc')) }}/upload/?directory={{ $directory }}",
            type: 'post',
            dataType: 'json',
            headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: new FormData($('#form-upload')[0]),
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('#button-upload_doc i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                $('#button-upload_doc').prop('disabled', true);
            },
            complete: function() {
                $('#button-upload_doc i').replaceWith('<i class="fa fa-upload"></i>');
                $('#button-upload_doc').prop('disabled', false);
            },
            success: function(json) {
                if (json['error']) {
                  alert(json['error']);
                }

                if (json['success']) {
                    $('#button-refresh_doc').trigger('click');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

      }//_endif : upload file input text not empty

    } , 500 ); //_end Timer
  
  });

  //_DELETE BUTTON CLICK
  $('#modal-document #button-delete_doc').on('click', function(e) {
      if (confirm('Confirm')) {
          $.ajax({
              //url: '{{ Config::get('app.url') }}backoffice/filemanager/delete',
              url: "{{ url(Config::get('url.backoffice.filemanager_doc')) }}/delete",
              type: 'post',
              dataType: 'json',
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: $('input[name^=\'path\']:checked'),
              beforeSend: function() {
                  $('#button-delete_doc').prop('disabled', true);
              },
              complete: function() {
                  $('#button-delete_doc').prop('disabled', false);
              },
              success: function(json) {
                  if (json['error']) {
                      alert(json['error']);
                  }
  
                  if (json['success']) {
                      //alert(json['success']);
  
                      $('#button-refresh_doc').trigger('click');
                  }
              },
              error: function(xhr, ajaxOptions, thrownError) {
                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
              }
          });
      }
  });

</script>