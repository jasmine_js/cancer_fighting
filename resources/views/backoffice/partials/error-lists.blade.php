@if($errors->any())
    {{-- <div class="alert alert-danger">
        <ul>
            @foreach($errors->keys() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>                           
    </div> --}}
    <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> แจ้งเตือน: พบข้อผิดพลาดคุณอาจกรอกข้อมูลที่จำเป็นไม่ครบ!
        <button type="button" class="close" data-dismiss="alert">×</button>
    </div>
@endif
        