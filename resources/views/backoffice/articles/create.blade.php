@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('backoffice/articles.text_create_news') }}
@endsection

@section('contentheader_title')
<i class="fa fa-plus" aria-hidden="true"></i> {{ trans('backoffice/articles.text_create_news') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-sm-12 col-xs-12">
            
            <div class="text-right">
                <button type="submit" form="submitForm" data-toggle="tooltip" title="{{ trans('backoffice/common.text_create') }}" class="btn btn-primary btn-save"><i class="fa fa-save"></i></button>
                <a href="{{ URL::to(Config::get('url.backoffice.articles')) }}" data-toggle="tooltip" title="{{ trans('backoffice/common.text_cancel') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <br />

            <div class="panel panel-default">
                <div class="panel-body">
                    
                    @include('backoffice.partials.error-lists')

                    {{ Form::open(['url' => Config::get('url.backoffice.articles'), 'id' => 'submitForm'])}}
                        @include('backoffice.articles.form')
                    {{ Form::close() }}

                </div>
              </div>

        </div>
    </div>
@endsection