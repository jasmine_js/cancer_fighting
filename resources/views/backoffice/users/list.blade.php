@extends('adminlte::page')

@section('htmlheader_title')
{{ trans('backoffice/users.text_user_lists') }}
@endsection

@section('contentheader_title')
<i class='fa fa-users'></i> {{ trans('backoffice/users.text_user_lists') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">                                   
		
				<div class="row">
					<div class="col-md-6 col-sm-6">
	
						<form method="GET">
							<div class="form-group">
								<div class="input-group">  
								<i class="input-group-addon fa fa-tag" aria-hidden="true"></i>  
									<input type="text" name="find" value="{{ $find }}" class="form-control" placeholder="{{ trans('backoffice/users.text_search') }}..." maxlength="255" />
								</div>
								<br />
								<button type="submit" class="btn btn-primary btn-search" data-toggle="tooltip" title="{{ trans('backoffice/users.text_search') }}">
										<i class="fa fa-search" aria-hidden="true"></i>
								</button>

								<a href="{{ url(Config::get('url.backoffice.users')) }}" data-toggle="tooltip" title="{{ trans('backoffice/users.text_clear_search') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
							</div>
						</form>
	
					</div><!-- /.col-md-6 -->
					
					<?php if(in_array('user/modify', Auth::user()->roles()->pluck('name')->toArray())): ?>
					<div class="col-md-6 col-sm-6 hidden-xs">
						<div class="item-create">
								<a href="{{ url(Config::get('url.backoffice.users_create')) }}" class="btn btn-success" data-toggle="tooltip" title="{{ trans('backoffice/users.text_create_user') }}">
									<i class="fa fa-plus" aria-hidden="true"></i>
								</a>
						</div>
						
					</div>
					<?php endif; ?>

				  </div><!-- /.row -->
				  	
				<div class="panel panel-default">
					<table class="table table-hover">
						<thead>
							<tr>
								<th width="6%">
									<a href="{{ $params }}&orderBy=id">
											{{ trans('backoffice/users.text_id') }}
										@if($orderBy == 'id')
											@if(!$asc)
											<i class="fa fa-caret-down" aria-hidden="true"></i>
											@else
											<i class="fa fa-caret-up" aria-hidden="true"></i>
											@endif
										@endif
									</a>
								</th>
								<th width="5%" class="hidden-xs">&nbsp;</th>
								<th width="29%">
									<a href="{{ $params }}&orderBy=name">
											{{ trans('backoffice/users.text_name') }}
										@if($orderBy == 'name')
											@if(!$asc)
											<i class="fa fa-caret-down" aria-hidden="true"></i>
											@else
											<i class="fa fa-caret-up" aria-hidden="true"></i>
											@endif
										@endif
									</a>
								</th>
								<th width="30%" class="hidden-xs">
									<a href="{{ $params }}&orderBy=email">
											{{ trans('backoffice/users.text_email') }}
										@if($orderBy == 'email')
											@if(!$asc)
											<i class="fa fa-caret-down" aria-hidden="true"></i>
											@else
											<i class="fa fa-caret-up" aria-hidden="true"></i>
											@endif
										@endif
									</a>
								</th>
								
								<th width="15%" class="hidden-xs">
									<a href="{{ $params }}&orderBy=last_login">
											{{ trans('backoffice/users.text_latest_access') }}
										@if($orderBy == 'last_login')
											@if(!$asc)
											<i class="fa fa-caret-down" aria-hidden="true"></i>
											@else
											<i class="fa fa-caret-up" aria-hidden="true"></i>
											@endif
										@endif
									</a>
								</th>
								<th width="15%">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							@foreach($items as $item)
							<tr class="{{ $item->status ? '' : 'textDeactive' }}">
								<td scope="row">{{ sprintf('%05d', $item->id) }}</td>
								<td scope="row" class="hidden-xs">
									@if($item->image)
									<img src="{{ url($item->image) }}" class="user-avatar img-circle" />
									@else
									<img src="{{ url('img/user-avatar.jpg') }}" class="user-avatar img-circle" />
									@endif
								</td>
								<td>
									{{ $item->name }}
								</td>
								<td class="hidden-xs">{{ $item->email }}</td>
								
								<td class="hidden-xs">
									@if($item->last_login)
									{{ $item->last_login->diffForHumans() }}
									@endif
								</td>
								<td align="right">
									<div class="action action-users">
											{{ Form::open(['url' => Config::get('url.backoffice.users').'/' . $item->id, 'method' => 'delete'])}}

											@if(in_array('user/modify', Auth::user()->roles()->pluck('name')->toArray()))
											<button type="button" class="btn btn-danger btn-delete" data-toggle="tooltip" title="{{ trans('backoffice/common.text_delete') }}">
												<i class="fa fa-trash" aria-hidden="true"></i>
											</button>
											@endif

											@if(in_array('user/modify', Auth::user()->roles()->pluck('name')->toArray()))
											<a href="{{ url(Config::get('url.backoffice.users') . '/' . $item->id . '/edit') }}" class="btn btn-primary" data-toggle="tooltip" title="{{ trans('backoffice/common.text_edit') }}">
												<i class="fa fa-cog" aria-hidden="true"></i>
											</a>
											@endif
											{{ Form::close() }}
									</div>
										
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				
				<div class="pager">
					<div class="pageinfo">
						<span>{{ trans('backoffice/common.text_all_results') }} {{ @$items->total() }} {{ trans('backoffice/common.text_item(s)') }}</span>
						<span>{{ trans('backoffice/common.text_page') }} {{ $items->currentPage () }} {{ trans('backoffice/common.text_from') }} {{ $items->lastPage() }} {{ trans('backoffice/common.text_page(s)') }}</span>
					</div>
					<div>
						{{ $items->appends(['find' => $find, 'asc' => $asc, 'orderBy' => $orderBy])->links() }}
					</div>					
				</div>				
			</div>
		</div>
	</div>
@endsection


@section('script')
<script>
@if(Session::has('success'))
	alertToat("{{ Session::get('success') }}");
@endif
@if(Session::has('error'))
	alertToat("{{ Session::get('error') }}", "red");
@endif

$('.btn-delete').click(function(){
	var _this = this;
    swalConfirm('{{ trans('backoffice/common.text_confirm_delete') }}', '{{ trans('backoffice/common.text_confirm_delete_message') }}')
    .then(function(result){
        if(result){
            $(_this).parent().submit();
        }
    });
});
</script>
@endsection