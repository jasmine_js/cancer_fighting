@extends('adminlte::page')

@section('htmlheader_title')
{{ trans('backoffice/common.text_edit') }}
@endsection

@section('contentheader_title')
<i class='fa fa-pencil-square-o'></i> {{ trans('backoffice/common.text_edit') }}
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">

			<div class="text-right">
				<button type="submit" form="formSubmit" data-toggle="tooltip" title="{{ trans('backoffice/common.text_save') }}" class="btn btn-primary btn-save"><i class="fa fa-save"></i></button>
				<a href="{{ URL::to(Config::get('url.backoffice.users')) }}" data-toggle="tooltip" title="{{ trans('backoffice/common.text_cancel') }}" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<br />

			{{ Form::model($item, ['method' => 'PUT', 'url' => Config::get('url.backoffice.users') . '/' . $item->id, 'id' =>'formSubmit', 'files' => true])}}
			
			@include('backoffice.users.form', ['submitText' => '{{ trans("backoffice/common.text_save") }}'])
			
			{{ Form::close() }}
		</div>

	</div>
</div>
</div>
@endsection