<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @yield('contentheader_title', 'Page Header here')
        <small>@yield('contentheader_description')</small>
    </h1>
    @if(@$breadcrumb && count($breadcrumb)>0)
    <ol class="breadcrumb">
        <li><a href="{{ url(Config::get('url.backoffice.dashboard')) }}"><i class="fa fa-dashboard"></i> {{ trans('backoffice/common.text_dashboard') }}</a></li>

        @if(@$breadcrumb)
            @foreach(@$breadcrumb as $key => $value)

                @if(@$value['active'] == 'active')
                <li class="active">{{ $key }}</li>
                @else
                <li><a href="{{ url(@$value['url']) }}">{{ $key }}</a></li>
                @endif

            @endforeach
        @endif

    </ol>
    @endif
</section>