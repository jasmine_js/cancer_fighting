<head>
    <!-- 
        TNI Backoffice version 1.0.0
        Copyright (C) 2018, Developed by Korn <kornthebkk@gmail.com>
    -->

    <meta charset="UTF-8">
    <title>@yield('htmlheader_title', 'Your title here') | TNI Back Office</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="icon" type="image/x-icon" href="{{ url('favicon.ico') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('/css/all.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/lib/jquery-toast/dist/jquery.toast.min.css') }}">
    <link href="{{ asset('/css/admin.css?ver=1.0.1') }}" rel="stylesheet" type="text/css" />

    @yield('style')

    <script>
        var site_url = '{{ URL::to("/") }}';
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
