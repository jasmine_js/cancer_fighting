<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Thai-Nichi Institute of Technology
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018-2029 ICC
</footer>

<!-- 
    TNI Backoffice version 1.0.0
    Copyright (C) 2018, Developed by Korn <kornthebkk@gmail.com>
-->
