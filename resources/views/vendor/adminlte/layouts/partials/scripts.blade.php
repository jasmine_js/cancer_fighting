<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('/lib/jquery-toast/dist/jquery.toast.min.js') }}"></script>
<script src="{{ url('js/image_manager.js') }}"></script>
<script src="{{ asset('/lib/sweetalert/dist/sweetalert.min.js') }}"></script>
<script src="{{ asset('/js/main.js?ver=' . time()) }}" type="text/javascript"></script>

@yield('script')
@yield('script2')
@yield('script3')

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>
