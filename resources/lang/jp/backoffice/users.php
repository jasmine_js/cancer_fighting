<?php

return [

    'text_user_lists' => 'รายการผู้ใช้',
    'text_user' => 'ผู้ใช้',
    'text_search' => 'ค้นหา',
    'text_clear_search' => 'เคลียร์การค้นหา',
    'text_create_user' => 'สร้างผู้ใช้ใหม่',
    'text_edit_user' => 'แก้ไขผู้ใช้',
    'text_delete' => 'ลบข้อมูล',
    'text_id' => 'รหัส',
    'text_name' => 'ชื่อ-สกุล',
    'text_email' => 'อีเมล์',
    'text_latest_access' => 'เข้าสู่ระบบล่าสุด',
    'text_password' => 'รหัสผ่าน (ต้องมากกว่า 5 ตัวอักษร)',
    'text_password_confirmation' => 'ยืนยันรหัสผ่าน (ต้องมากกว่า 5 ตัวอักษร)',
    'text_image_profile' => 'รูปโปรไฟล์',
    'text_user_privileges' => 'สิทธิ์การใช้งาน',
    'text_all_privileges' => 'ใช้งานได้ทั้งหมด',
];
