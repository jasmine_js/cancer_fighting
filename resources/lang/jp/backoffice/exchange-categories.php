<?php

return [

    'text_exchange_category_lists' => 'รายการหมวดหมู่โครงการแลกเปลี่ยน',
    'text_create_exchange_category' => 'สร้างหมวดหมู่โครงการแลกเปลี่ยน',
    'text_edit_exchange_category' => 'แก้ไขหมวดหมู่โครงการแลกเปลี่ยน',
    'text_id' => 'รหัส',
    'text_image' => 'รูป',
    'text_name' => 'ชื่อ',
    'text_edit_exchange_category' => 'แก้ไขหมวดหมู่โครงการแลกเปลี่ยน',
    
];
