<?php

    return [
        'text_htmltitle' => '泰日工業大学',
        'text_course_heading' => '泰日工業大学',

        'text_admission' => '入学',
        'text_bechelor' => '学士課程',
        'text_master' => '大学院課程',
        'text_international_program' => 'インターナショナルプログラム',
        'text_scholarship' => '新規入学生の奨学金',
        'text_exchange_program' => '交換留学プログラム',
        'text_education_fee' => '授業料',


        'text_exchange_program_news' => '交換留学プログラムへの応募',
        'text_highlight_activities' => '活動ハイライト',
        'text_news_more' => 'ニュース',
        'text_event_update' => 'イベントカレンダー',
        'text_event_more' => '全ての表示',
        'text_student_portfolio' => '学生',
        'text_co_operative' => 'インターンシップ＆就職支援センター',
        'text_click_here' => 'ここをクリック',
        'text_learn2know_heading' => 'Learn2know',
        'text_learn2know_subheading' => '実践的なリスニングを通して学習',
        'text_learn2know_p1' => '様々シチュエーションを想定して学習',
        'text_learn2know_p2' => '英語と日本語で様々なシチュエーションを練習',
        'text_tni_channel' => 'TNIチャンネル',
        'text_e_bochure' => '電子パンフレット',
        'text_more' => '全ての表示',
        'text_history' => 'TNIについて',
        'text_history_tni' => 'TNIの歴史',
        'text_philosophy' => 'TNIの理念',
        'text_around_tni' => 'キャンパス周辺',
        'text_resolution' => '決意',
        'text_vision' => 'ビジョン',
        'text_mission' => 'ミッション',
        'text_faculty' => '学部とプログラム',
        'text_home' => '<i class="fas fa-home fa"></i> ホーム',

        //ร่วมงานกับเรา
        'text_career_tni' => '一緒にやろう',
        'text_application_form' =>'アプリケーションダウンロード',
        'text_download' =>'ダウンロード',
        'text_send_application' =>' " 申込書の提出 <b style="color:#276fc1">hr@tni.ac.th</b> "',
        'text_travel' =>'<i style="color:red;">or</i> 直接の申し込み',

        'text_travel_detail' =>'TNIへの行き方 - BTSエカマイ駅からのアクセス
        トンロー寺前バス乗り場から133番バスに乗車しパタナカン37で下車 <br>
        - MRTペップリー駅からのアクセス
        ペップリー通りバス乗り場から11番バスに乗車しパタナカン37にて下車<br>
        - バスでのTNIのアクセス
        11番、133番、206番
        エアバス　92番、517番',

        'text_jobtni' => '求人情報',
        'text_link_jobtni' => 'www.jobtni.com',

        // web link 
        'text_organization' =>'TNIへの寄付金者' ,

        // TNI-Presentation 
        'text_thai_version' => 'タイ語版',
        'text_english_version' => '英語版',
        'text_japan_version' => '日本語版',
        'text_tni_presentation' =>'TNIの紹介',
        'text_10year_tni' =>'泰日工業大学10周年記念',

        //policy cookies 
        'text_privacy_policy' => '個人情報保護方針',
        'text_cookies_policy' => 'クッキーに関する方針',
    ];