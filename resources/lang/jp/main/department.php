<?php

    return [
        'text_department' => 'TNIの機​​能と学術サービス',
        'text_regiter_center' => '<br> アドミッションセンター',
        'text_public_relations' => '<br><br> 国際関係と奨学金' ,
        'text_adminsitrator' =>'<br><br> 経理部',
        'text_academic' =>'<br><br> 学務部',
        'text_studen_affairs' =>'<br><br> 学生部',
        'text_cooperative_career' =>'<br> コーオプ教育および職業紹介センター',
        'text_icc' =>'<br><br> 情報通信センター',
        'text_research_and_academic_services' =>'<br> リサーチ＆イノベーション部',      
        'text_journal_en' => 'TNIジャーナルオブエンジニアリングアンドテクノロジー',
        'text_journal_ba' => '<br> 経営学と言語のTNIジャーナル',
        'text_community_services' => '<br> コミュニティサービス',
        'text_library' => '<br><br> 図書館',
        'text_coe' => '<br> センターオブエクセレンス-ISI',
        'text_aca_training' => 'Academic Training and Research Services Center',

        //finance
        'text_finance' => '管理、アカウントおよび財務',
  
    ];