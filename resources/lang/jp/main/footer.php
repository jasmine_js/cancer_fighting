<?php

    return [

        'text_quick_link' => 'Quick Link',
        'text_apply_now' => 'オンライン申請',
        'text_calendar' => '学事カレンダー',
        'text_e_learning' => 'E-Learning',
        'text_e_dictionary' => 'E-Dictionary',
        'text_web_link' => 'TNI支援機関',
        'text_webmail' => 'TNI Webmail',
        'text_jobtni' => 'jobtni.com',
        'text_career_tni' => '参加する ',

        'text_journal_eng' => 'ニュースレター　工学　技術',
        'text_journal_ba' => 'ニュースレター　経営　語学',

        'text_faculty' => '学部',
        'text_engineering' => '工学部',
        'text_it' => '情報技術学部',
        'text_ba' => '経営学部',
        'text_cgel' => '語学教養課程',
        'text_grad' => '大学院',
        'text_internation_program' => '国際プログラム事務所',
        'text_reg' => '登録事務所',
        'text_studen_affairs' => '学生部',
        'text_research' => 'リサーチセンター',
        'text_information' => '国際協力・奨学金部',
        'text_co_operative' => 'Co-operative Education and Job Placement Center ',
        'text_qa' => 'QA',
        'text_account_finance' => '財務部',
        'text_icc' => '情報・コミュニケーションセンター',
        'text_alumni' => 'TNI卒業生',
        'text_library' => '図書館',
        'text_knowlege_management' => 'ナレッジマネジメント',
        'text_deparements' => '内部機能',
        'text_tni' => '泰日工業大学',
        'text_adress' => '1771/1 Pattanakarn Rd., Suan Luang, Bangkok 10250, Thailand',
        'text_tel' => '電話： 0-2763-2600 ファックス： 0-2763-2700',
        'text_email' => 'tniinfo@tni.ac.th',
        'text_direct_chancellor' => '学長直通',
        'text_donwload' => 'ダウンロード',
        'text_sigil_program' => 'Sigil プログラム',
        'text_map' => '地図',
        'text_copyright' => '© 2018-2019 THAI-NICHI INSTITUTE OF TECHNOLOGY (TNI) ALL RIGHTS RESERVED.',

        'text_other_link' => 'Related link',
        'text_privacy_policy' => 'Privacy Policy',
        'text_tpa' => 'Technology Promotion Association (Thailand-Japan)',


        
    ];