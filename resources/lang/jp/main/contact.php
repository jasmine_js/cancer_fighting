<?php

    return [
        'text_contact' => 'ติดต่อสถาบัน',
        'text_img_map' => 'image/catalog/mockup/mapth.png',
        'text_tni' => 'สถาบันเทคโนโลยีไทย-ญี่ปุ่น', 
        'text_address' => ' เลขที่ 1771/1 ซ.พัฒนาการ 37 ถนนพัฒนาการ แขวงสวนหลวง เขตสวนหลวง กรุงเทพฯ 10250 ',
        'text_phone' =>'โทรศัพท์ 0-2763-2600 &emsp; โทรสาร 0-2763-2700 ',
        'text_email' => 'E-Mail : tniinfo@tni.ac.th ',
        'text_website' => 'Website : http://www.tni.ac.th ',
        'text_tni_en' => 'Thai-Nichi Institute of Technology is located on Pattanakarn Road between Soi 37 and 39',

        'text_map' =>' It is easy to get to and from using the main roads such as Srinakarin, <br>
        Ram-inthra - Atnarong Expressway, Motorway (Bangkok - Chon Buri), and East Outer Ring Road.<br>
        There are buses number 11, 133, 206, Por-Or 517 and Por-Or 92, Airport Link (Ramkhamhaeng or Hua Mak stations), <br>
        BTS (Phra Khanong station), and MRT (Phetchaburi station) to the institute.<br>',
        'text_department_phonenumber' => ' เบอร์ติดต่อภายในของสถาบันฯ ',
        'text_tbody' => '
                            <tr>
                                <td>สำนักงานอธิการบดี</td>
                                <td>02-763-2748</td>                 
                            </tr>
                            <tr>
                                <td>ศูนย์รับสมัครนักศึกษา</td>
                                <td>02-763-2601-5</td>                
                            </tr>
                            <tr>
                                <td>ฝ่ายวิเทศสัมพันธ์ ประชาสัมพันธ์ และจัดหาทุน </td>
                                <td>02-763-2753 ,02-763-2780-3</td>
                            </tr>
                            <tr>
                                <td>ศูนย์สหกิจศึกษา </td>
                                <td>02-763-2750</td>
                            </tr>
                            <tr>
                                <td>ศูนย์วิทยบริการ (ห้องสมุด) </td>
                                <td>02-763-2626</td>
                            </tr>
                            <tr>
                                <td>ศูนย์สารสนเทศและการสื่อสาร (ศูนย์ ICC) </td>
                                <td>02-763-2643</td>
                            </tr>
                            <tr>
                                <td>ฝ่ายกิจการนักศึกษา </td>
                                <td>02-763-2620-4</td>
                            </tr>
                            <tr>
                                <td>ฝ่ายวิชาการ และฝ่ายทะเบียน </td>
                                <td>02-763-2501, 2504, 2505</td>
                            </tr>
                            <tr>
                                <td>ฝ่ายวิจัยและบริการวิชาการ </td>
                                <td>02-763-2704, 2752</td>
                            </tr>
                            <tr>
                                <td>บัณฑิตวิทยาลัย </td>
                                <td>02-763-2600  ต่อ 2402, 2403 </td>
                            </tr>
                            <tr>
                                <td>คณะวิศวกรรมศาสตร์	 </td>
                                <td>02-763-2910 </td>
                            </tr>
                            <tr>
                                <td>คณะเทคโนโลยีสารสนเทศ </td>
                                <td>02-763-2740 </td>
                            </tr>
                            <tr>
                                <td>คณะบริหารธุรกิจ </td>
                                <td>02-763-2712 </td>
                            </tr>
                            <tr>
                                <td>สำนักวิชาพื้นฐานและภาษา </td>
                                <td>02-763-2804 </td>
                            </tr>
                            <tr>
                                <td>ฝ่ายบริหาร</td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td>&emsp; - งานบริหารและพัฒนาทรัพยากรมนุษย์ </td>
                                <td>02-763-2615 </td>
                            </tr>
                            <tr>
                                <td>&emsp; - งานบัญชีและการเงิน</td>
                                <td>02-763-2610-3, 2629</td>
                            </tr>
                            <tr>
                                <td>&emsp; - งานธุรการและพัสดุ </td>
                                <td>02-763-2616 </td>
                            </tr>
                            <tr>
                                <td>&emsp; - งานจัดซื้อและอาคารสถานที่ </td>
                                <td>02-763-2618</td>
                            </tr> ' ,

                  
       
    ];