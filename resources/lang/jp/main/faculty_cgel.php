<?php

    return [
        'text_name'   => '一般教育言語学部',
        'text_detail' => 'ジェネレーション教育言語学部は、経済、社会、環境の急速な変化に社会と国家を意識して適応できる、英語と日本語のコミュニケーションスキルを備えたビジネスおよび業界の人材の育成と育成を目指しています。',
        'text_program'  => 'セクション',
        'text_bachelor' => '学士号',
        'text_major' => '<li>日本語</li>
                        <li>英語</li>
                        <li>社会科学と人文科学</li>
                        <li>科学と数学',

        'text_subject' => '主題' ,

        'text_major1' => '日本語', 
        'text_detail_major1' => '
        JPN-101 日本語経営学1 
        | JPN-102 日本語経営学2
        | JPN-201 日本語経営学3
        | JPN-202 日本語経営学4
        | JPN-301 日本語経営学5
        | JPN-302 日本語経営学6
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        JPN-407 日本語経営学1
        | JPN-408 日本語経営学2
        | JPN-427 社会人向け日本語
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        JPN-428 ITのための日本語
        | JPN-429 サラリーマンのための日本語
        | JPN-430 Listening to Voices from Japan
        | JPN-431 全体的な日本の話
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        JPN-432 日本語でのオンラインライティング 
        | JPN-433 日本語でのディスカッション
        | JPN-434 日本語でのプレゼンテーション
        | JPN-435 日本語での専門知識を高める
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        JPN-436 日本音楽の研究
        | JPN-437 自分自身で日本への旅行
        | JPN-491 日本語のエキストラカリキュラム1
        | JPN-492 日本語のエキストラカリキュラム2
        ',               

        'text_major2' => '英語', 
        'text_detail_major2' => '
        ENL-111 英語でのコミュニケーション
        | ENL-112 英語のスキルアップ
        | ENL-211 働くための英語
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        ENL-212 TOEICテストのプレゼンテーション
        | ENL-423 エンジニアのための英語
        | ENL-424 ITのための英語
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        ENL-425 ビジネスマンのための英語
        | ENL-426 産業人のための英語
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        ENL-427 ビジネスと産業のための翻訳の原則
        | ENL-428 プロの英語リスニングとスピーキング
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>
        ENL-429 プロの英語プレゼンテーション
        | ENL-430 プロの英語プレゼンテーション 
        | ENL-431 英語でのビジネス書簡
        | ENL-432 アドバンスTOEIC
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        ENL-433 IELTSプレゼンテーション
        | ENL-434 TOEFLプレゼンテーション
        | ENL-435 英語を通して西欧の文化の発見
        | ENL-436 タイ語文献の英語
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        ENL-436 課外活動　英語1 
        | ENL-492 課外活動　英語2
        ', 

        'text_major3' => '社会科学と人文科学', 
        'text_detail_major3' => '
        人文科学  
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>
        HUM-125 Arts of Life   
        | HUM-126 タイの伝統芸術 
        | HUM-127 世界的な大きなターニングポイント  
        | HUM-128 良く過ごす 
        | HUM-129 マスターアートを知る  
        | HUM-130 お金を持つ、お金を使う <br> <br>
      
        社会科学科目 
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>        
        SOC-125 キャリアの準備  
        | SOC-126 勇敢な人々  
        | SOC-127 政治意識  
        | SOC-128 生活法 
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>
        SOC-129 タイの社会と文化 
        | SOC-130 今日の日本  
        | SOC-131 革新的なソリューション管理 
        | SOC-132 アセアンー日本学
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>
        SOC-133  自然災害の回避  
        | SOC-134 タイ日社会  
        | SOC-135 メディア・リテラシー  
        | SOC-136 健康的な生活 
        | SOC-137 スタートアップ研究  
        | SOC-138 テクノロジーツアー 
        ',  

        'text_major4' => '科学と数学の科目', 
        'text_detail_major4' => ' 
          
        BUS-106 ビジネス・工業経営学
        | ENG-111 一般的な物理学
        | ENG-112 一般物理学研究室
        | ENG-121 一般化学
        | ENG-122 一般化学研究室
        | ENG-211 エンジニアリング統計 
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>  
        MSC-110 一般科学
        | MSC-111 数学と応用
        | MSC-112 科学技術 
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>  
        MSC-113 産業における一般科学
        | MSC-114 科学、技術、イノベーション
        | MSC-115 ビジネスにおける科学的思考 
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>
        MSC-202  統計と確率

        ',    
        
    ];
    ?>