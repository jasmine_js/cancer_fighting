<?php

    return [
        // * START Master **************************************** //

        // ENG -------------------------------------------------------------//
        'text_eng' => '工学部',
        'text_eng_detail' => '工学部は、ビジネスおよび産業部門のニーズに真に一致する実践的および理論的知識、英語と日本語のコミュニケーションスキル、および社会的意識に優れた資格のある工学労働力を提供するための学術知識の場所となることを約束しました ',
        'text_program' => 'プログラム',
        'text_master' => '修士課程',
        'text_eng_major' => '<li>工学技術学の修士課程 </li>
                            <p class="ml-3">プログラムは、設計、分析と技術開発、設計、製造とインテリジェントオートメーション、スマートエネルギーと環境システム、イノベーションとインテリジェント管理、インテリジェント電気およびコンピュータエンジニアリングシステムの幅広い知識を学生に教育します。 </p>
                           
                            ',
        // 'text_eng_major1' => 'สาขาวิชาเทคโนโลยีวิศวกรรม (MET)', 
        // 'text_detail_eng_major1' => ' มุ่งเน้นผลิตมหาบัณฑิตที่มีศักยภาพสูงในการทำงานด้านวิศวกรรมและการสร้างสรรค์เทคโนโลยี 
        // ควบคู่กับการพัฒนาความสามารถด้านภาษาอังกฤษ และภาษาญี่ปุ่น ตอบสนองความต้องการของภาคอุตสาหกรรม ตามแนวทางของญี่ปุ่น 
        // ที่เหมาะสมกับประเทศไทย สามารถเลือกเรียนวิชาเลือกได้จาก 4 กลุ่มวิชา ได้แก่ <br>
        //     - การออกแบบขั้นสูงและเทคโนโลยีการผลิต  (Advanced Design and Technology in Manufacturing) <br>
        //     - ระบบพลังงานและสิ่งแวดล้อม (Energy and Environmental System: EES) <br>
        //     - นวัตกรรมและการจัดการทางวิศวกรรม (Engineering Innovation and Management : EIM) <br>
        //     - ระบบวิศวกรรมไฟฟ้าอัจฉริยะ (Smart Electrical Engineering Systems: SES) ',
        'text_eng_major1' => 'MIT',
        'text_detail_eng_major1' => '
                                    <p>1. 高度な設計とロボットインテリジェントシステム：ADRS </p>
                                    <p>2. スマートエネルギーおよび環境システム：EES</p>
                                    <p>3. インテリジェントエンジニアリングの革新と管理：EIM </p>
                                    <p>4. スマート電気およびコンピュータ工学システム：SES </p>
                                ',               

        // BA -------------------------------------------------------------//
        'text_ba' => '経営学部',
        'text_ba_detail' => '経営学部は、学生や部外者が経営技術の専門知識を持ち、道徳と社会意識を持って業界と社会に利益をもたらすために現代の経営技術を適用できるようにするための優れた経営管理の学術センターになることを目指しています。 TNIは、あなたが知るほど成功する大きなチャンスがあると信じています。したがって、経験豊富な人々から学ぶ場合、知識は最も有益です。',
        'text_program' => 'プログラム',
        'text_master' => '修士課程',
        'text_ba_major' => '<li>ビジネスおよび産業管理（MBI）の革新</li>
                            <li>日本の経営学（MBJ）</li>
                            <li>リーン生産方式とロジスティクス管理（LMS）</li>',

        'text_ba_major1' => 'ビジネスと産業管理の革新', 
        'text_detail_ba_major1' =>'
        <p>- การจัดการอุตสาหกรรมเชิงนวัตกรรม (IIM) 
                มีความรู้ด้านการบริหารธุรกิจอุตสาหกรรมสมัยใหม่ผ่านนวัตกรรม สามารถยกระดับขีดความสามารถในการแข่งขันให้แก่องค์กรธุรกิจ 
                 ความรู้ในการสร้างมูลค่าเพิ่ม และนวัตกรรมทางการผลิตให้แก่ธุรกิจอุตสาหกรรม เพื่อพัฒนาตัวเองเป็นนักอุตสาหกรรมสมัยใหม่ นักการผลิตสมัยใหม่ 
                ที่เน้นการใช้นวัตกรรม นำระบบการผลิตแบบญี่ปุ่นไปสู่การพัฒนา ผลิตภาพทางการผลิตให้สามารถแข่งขันได้สูงขึ้น 
        </p>
        <p>- &nbsp;การวางแผนและการจัดการเชิงกลยุทธ์สำหรับผู้ประกอบการ (SME) 
             มีความรู้เฉพาะทาง ทั้งทางทฤษฏีและปฏิบัติ การวินิจฉัยปัญหา การแก้ไขปัญหาของธุรกิจ มีความสามารถในการกำหนดกลยุทธ์และจัดทำแผนธุรกิจ 
             ที่นำไปใช้ประโยชน์ได้จริง พร้อมทั้งได้รับทักษะการบริหารธุรกิจแบบเจ้าของกิจการ เพื่อสร้างและพัฒนาธุรกิจ พัฒนาองค์กรทั้งระดับกลางและระดับสูง 
        </p> 
       
        ', 

        'text_ba_major2' => '日本語・経営学', 
        'text_detail_ba_major2' => ' プログラムは、国際的および日本のビジネスの専門家であるタイ人と日本人の講師による教育で、日本のビジネスと経営のスタイルを提供します。これは、日本の経営管理とより高いレベルの日本語を学ぶことに対する学生のニーズをサポートするためです。
        ',  
        
        'text_ba_major3' => 'リーン製造システム・物流管理学', 
        'text_detail_ba_major3' => ' MBA-LMSは、技術経営と組織開発に精通したものづくりの原則に沿った新世代の幹部を育成することを目的とした修士課程です。技術と革新の急速な変化により、TNIは、日本式のスマート管理を通じてジョブトライアンのアクティブラーニングとコーチングを提供し、LMSソフトウェアとカイゼン、IOTのアプリケーションと実践を統合して効率的に使用します。',  


                                          
         // IT -------------------------------------------------------------//                       
        'text_it' => '情報技術学部',
        'text_it_detail' => '情報技術学部は、学術および技術開発人材の優れた中心となることを目指しました',
        'text_program' => 'プログラム',
        'text_master' => '修士課程',
        'text_it_major' => '<li>情報技術学</li>',

        'text_it_major1' => '情報技術学', 
        'text_detail_it_major1' => 'カリキュラムは、ものづくりと人づくりの原則を統合し、和風の商品と人材の創造に焦点を当てています。また、情報技術（特にビジネス情報システムとデータ分析、データマイニングとビッグデータ）に関する知識も統合しました。マルチメディアテクノロジー（特にネットワークテクノロジー、IoT、人工知能（AI）、および実践と研究による学習の混合。', 

        // * END Master ****************************************** //
       


        // // * STRAT Inter **************************************** //

        // // BA -------------------------------------------------------------//
        // 'text_inter_ba' => 'คณะบริหารธุรกิจ',
        // 'text_inter_ba_detail' => 'คณะบริหารธุรกิจ มุ่งมั่นเป็นศูนย์กลางทางวิชาการทางด้านการบริหาร การจัดการ ทั้งภายในและภายนอกสถาบันเทคโนโลยีไทย-ญี่ปุ่นเป็นแหล่งพัฒนาความเป็นเลิศให้แก่ 
        //                         นักศึกษา และบุคคลภายนอก ให้มีขีดความสามารถ (Ability) ทางด้านเทคโนโลยีการบริหารการ  จัดการ สามารถประยุกต์ใช้เทคโนโลยีการจัดการที่ทันสมัย ให้เป็นประโยชน์ต่ออุตสาหกรรม 
        //                         และสังคมอย่างมีคุณธรรมและจิตสำนึกต่อสังคม ',
        // 'text_program' => 'หลักสูตร/สาขา',
        // 'text_bacherlor_inter' => 'ปริญญาตรี',
        // 'text_inter_ba_major' => '<li>International Business Management (IBM)</li>',

        // 'text_inter_ba_major1' => 'International Business Management (IBM)', 
        // 'text_detail_inter_ba_major1' => '  In today\'s borderless world economy, both large and small companies must adapt to survive in highly competitive markets. 
        //                         The International Business Management program (IBM) endeavors to produce competent graduates who meet the ever-increasing standards 
        //                         to compete in the workforce market of tomorrow. Therefore, students will acquire invaluable knowledge from passionate and outstanding 
        //                         instructors who have a proven effective teaching from experience and academic distinction',           
                                
        // // ENG -------------------------------------------------------------//
        // 'text_inter_eng' => 'คณะวิศวกรรมศาสตร์',
        // 'text_inter_eng_detail' => 'คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีไทย-ญี่ปุ่น มุ่งมั่นเป็นแหล่งรวมวิชาการและผลิตวิศวกรที่มีคุณภาพ มีความเชี่ยวชาญ พร้อมทั้งสามารถประยุกต์ใช้เทคโนโลยีที่ทันสมัยให้เป็นประโยชน์ต่ออุตสาหกรรม 
        //                         และมีจิตสำนึกที่ดีต่อสังคม ผลิตบัณฑิตที่มีความรู้ความสามารถทั้งในด้านทฤษฎีและปฏิบัติ คิดเป็นทำเป็น และมีทักษะความสามารถในการสื่อสารภาษาญี่ปุ่นและภาษาอังกฤษ สู่ภาคอุตสาหกรรมและธุรกิจ 
        //                         ออกแบบหลักสูตรที่มีความเฉพาะและตอบสนองต่อความต้องการของผู้ใช้บัณฑิตอย่างแท้จริง',
        // 'text_program' => 'หลักสูตร/สาขา',
        // 'text_bacherlor_inter' => 'ปริญญาตรี',
        // 'text_inter_eng_major' => '<li>Digital Engineering (DGE)</li>',

        // 'text_inter_eng_major1' => 'Digital Engineering (DGE)', 
        // 'text_detail_inter_eng_major1' => 'The Digital Engineering program (DGE) will impart Digital Engineers students with the critical skills needed in the following areas: Economics, 
        //                         Engineering, Novel Business Model, Digital Engineering, and Artificial Intelligence. The program will prepare the students for Industry 4.0 
        //                         and equip them with the ability to design, fabricate, assemble, install, and integrate computer hardware, software, and control systems. 
        //                         They will acquire “Digital Literacy” skills needed to apply engineering technology to improve automation systems 
        //                         and IoT and study about Intelligent Software, Intelligent Embedded Systems, and Intelligent System Integration, including for first stage 
        //                         and emerging S-curve industries and enterprises. <br>
        //                         TNI is confident that Digital Engineers graduates will have the requisite skills desired by companies involved in Cloud Computing, Big Data, 
        //                         Mobile Application, and Business Intelligence Solutions. ',               

        // // IT -------------------------------------------------------------//
        // 'text_inter_it' => 'คณะเทคโนโลยีสารสนเทศ',
        // 'text_inter_it_detail' => 'คณะเทคโนโลยีสารสนเทศ สถาบันเทคโนโลยีไทย – ญี่ปุ่น มุ่งมั่นเป็นศูนย์กลางทางด้านวิชาการและเทคโนโลยีเพื่อเป็นแหล่งสร้างและพัฒนาบุคคลากรในด้านเทคโนโลยีสารสนเทศที่ทันสมัย  
        //                         มีความเป็นเลิศทางวิชาการ ทั้งการวิจัย การประยุกต์ และการเผยแพร่องค์ความรู้แก่สังคมโดยยึดมั่นในคุณธรรมและมีจริยธรรมในวิชาชีพ',
        // 'text_program' => 'หลักสูตร/สาขา',
        // 'text_bacherlor_inter' => 'ปริญญาตรี',
        // 'text_inter_it_major' => '<li>Data Science And Analytics (DSA)</li>',

        // 'text_inter_it_major1' => 'Data Science And Analytics (DSA)', 
        // 'text_detail_inter_it_major1' => 'The Data Science and Analytics program (DSA) represents a new breed of multi-disciplinary fields of study, combining the science of statistics 
        //                         and computers with the art of business and visualization, to make better informed strategic decisions. Our DSA program aims to produce graduates 
        //                         of the next generation with the knowledge and skill sets that can revolutionize many vital industries, such as manufacturing, finance, services, 
        //                         health care, or even agriculture under the Thailand 4.0 policy. DSA students will learn the best practices that can transform data to insight,
        //                         discover and harness hidden patterns, and predict or adapt to future trends to improve business competitiveness. ',               

        // // * END Inter ****************************************** //
      
    ];