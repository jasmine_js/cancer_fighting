<?php
    $arr_texts1[] = "実践および実習を重視し、優れた学術研究を通してビジネスおよび産業部門のニーズを満たす高等教育を提供する。";
    $arr_texts1[] = "実践的な知識・善良な精神・責任感・専門性の発揮・社会的意識、以上を備える学生を教育する 。";
    $arr_texts1[] = "ビジネスと産業に関する教育と学習に寄与することを目的とした優れた技術の創造および開発するための研究を行う。";
    $arr_texts1[] = "知識と高度な技術を提供し、ビジネスおよび産業部門の競争力を強化する";
    $arr_texts1[] = "タイの芸術、文化、叡智を育む";

    //_จุดเด่นและจุดเน้นของสถาบันฯ
    $arr_texts2[] = "タイと日本の関係";
    $arr_texts2[] = "ものづくりの原則に沿った学生の育成";

    //_ค่านิยมหลัก
    $arr_texts3[] = "ものづくり";
    $arr_texts3[] = "改善";
    $arr_texts3[] = "反省";
    $arr_texts3[] = "正直";
    $arr_texts3[] = "尊敬";
    $arr_texts3[] = "公益";

    $arr_symbolic_3[] = "大学のキャラクターである歯車";
    $arr_symbolic_3[] = "Aのシンボルは伝統的なタイ家屋の屋根に似ており、赤字で記された「TNI」という略語と共に表示されています。";
        
    $arr_symbolic_5[] = "この歯車は大学の継続的な発展を意味します。これは、大学を現代の革新的なテクノロジーと自己開発に反映し、テクノロジーの急速な変化に対応し、移動する世界のニーズに沿った人材を生み出します。";
    $arr_symbolic_5[] = "Aシンボルは、伝統的なタイ風の家の切妻屋根に似ており、タイの文化的アイデンティティを維持しているアジア大陸の学術機関を表しています。ギアの上と向こうに伸びる切妻屋根の上部は、固定されたパターンや物理的な境界のない知識と知恵を表しています。これは、研究所が境界の彼方を超えた知識と知恵の交換を通じて技術と革新を生み出すことを目指していることを意味します。";
        
    $arr_color_3[] = "青は、泰日工業大学の創設者であり、君主制を称える教育機関である技術振興協会（タイ-日本）の色です。さらに、色は知恵、ビジョン、貴族、名誉を表しており、タイの人材を先見の明があり、知識が豊富で、賢明であり、誇り、名誉、そして誇りを持って知識を応用できる研究所を設立するという創設者の意図を反映しています。尊厳。";

    $arr_color_3[] = "Resは太陽と東の色です。つまり、Insituteは、東半球、特に日本から知識、概念、哲学、技術を蓄積し、伝達することを目指しています。さらに、色は繁栄、成功、美徳を表しています。これらは、研究所の卒業生が自分自身、組織、社会、そして国のために繁栄を生み出すことができる善良で成功した人を望んでいる創設者の意志です";
    



    return [
        'text_intro'            => 'TNIについて',

        'text_history'                    => 'TNIの歴史と理念',                                           //_menu 
        'text_resolution_vision_mission' => '決意・ビジョン・ミッション',                                            //_menu
        'text_character'                  => '特徴',   //_menu
        'text_value'                      => '基本的価値観',                                                          //_menu
        'text_symbolic_color'             => '研究所のロゴと色',                                            //_menu
        'text_principal_buddha_image_tree' => '研究所の仏像',                               //_menu
        'text_monument_feature'           => 'TPA創設者の像',                             //_menu
        
        'text_history_tni' => 'TNIの歴史と理念',
        'text_philosophy'  => '理念',
        'text_around_tni'  => 'キャンパス周辺',
        'text_resolution'  => '決意 ',
        'text_vision'      => 'ビジョン ',
        'text_mission'     => 'ミッション ',

        'text_graduate_identity'         => 'アイデンティティ',
        'text_institute_identity'        => '独自性',
        'text_institute_highlight_focus' => '強みと焦点',
        
        'text_content_institution_history' => "泰日工業大学は2007年にTechnology Promotion Association（Thailand-Japan）（TPA）によって設立されました。 TPAは元大蔵大臣ソンマーイ・フントラクーン氏を初代代表とし、（社）日タイ経済協力協会（JTECS）の初代理事長である穂積五一氏の献身的な支援を受けて1973年1月に元日本留学生及び元研修生によって設立された非営利団体です。 TPAはタイの経済と産業の成長と進歩のために、タイへの知識の普及と技術移転を促進することを主な目的としています。 TPAの事業は日本語・タイ語などの語学学校、出版、産業セミナー、研修、工業計測機器校正、SME診断、IT、日系企業の進出相談など多岐に渡っております。過去40年近くのこれら活動を通して、タイでは一般の方から企業に至るまで広く知られています。TPAは2003年の創立30周年を契機に優秀な産業人の育成とタイ産業界への人材供給のために大学設立を目指すに至りました。2005年に泰日工業大学設立プロジェクトを開始し、2006年9月29日に文部省から公式ライセンスを取得し設立に至りました。現在は工学部・情報技術学部・経営学部の3つの学部と修士課程で構成されています。",

        'text_content_philosophy' => "泰日経済技術振興協会（TPA）は、日タイの技術普及の架け橋として47年以上にわたり、「知識の普及、経済基盤の構築」という哲学の下で運営されてきました。 TPAは、タイ王国経済の発展を支援するための知識とスキルを備えた人材の育成に大きな役割を果たしてきました。 TNI設立委員会は、TNIが創造と発展に焦点を当てた学術機関となることを目指して、活動とサービスの価値を拡大し、サービスの役割を拡大するために、組織運営に関するTPAとTNIの哲学を一致させる必要があると考えました。王国の人員、新しい知識を生み出すためのリソース、そして社会、特に産業部門に知識を広めるためのチャネル。したがって、TNIの運営哲学は、「知識を発展させ、経済と社会のために産業を豊かにする」ことです。" ,

        'text_content_resolution' => "泰日工業大学は、道徳と社会を遵守し、社会の技術と管理の分野で適切な人材の育成を支援するために、高等専門職の知識の創造と普及の中心となることを目的とした高等教育機関です。" ,//_ปณิธาน

        'text_content_vision' => "技術、管理、および個人間のスキルの専門分野で知識を提供し、強力な国内および内部のつながり、学術的卓越性を持ち、社会の知識の中心となるタイの主要な学術機関の1つになります" ,

        'text_content_mission'                   => $arr_texts1 ,
        'text_content_graduate_identity'         => '"これらの技術を誠実さと説明責任を持って社会に適切に適用し、普及させる。"',
        'text_content_institute_identity'        => '"人事育成の強化"',
        'text_content_institute_highlight_focus' => $arr_texts2,
        'text_content_value'                     => $arr_texts3,

        //_สัญลักษณ์และสีประจำสถาบัน
        'text_content_symbolic_1' => 'シンボル',
        'text_content_symbolic_2' => "シンボル要素",
        'text_content_symbolic_3' => $arr_symbolic_3,
        'text_content_symbolic_4' => "TNIのシンボルの意味",
        'text_content_symbolic_5' => $arr_symbolic_5,

        //_สีประจำสถาบันฯ
        'text_content_color_1' => '大学カラー',
        'text_content_color_2' => "TNIは大学のカラーとして青と赤を使っています。",
        'text_content_color_3' => $arr_color_3,

        //_พระประธานประจำสถาบัน
        'text_content_principal_buddha_image_1' => '研究所の仏像',
        'text_content_principal_buddha_image_2' => '"この仏像は2007年1月24日（水）に設置されました。仏の頭の中には、TNIの開会式の際にタイの最高総主教によって祝福され授けられた仏陀の遺物が含まれ、この仏像を「プラブッダマハモンコルシリパタナスタブン」と名付けました。教職員と学生に、以下に規定されているロードブッダの教えに従うように指導している。
            1.สพฺพปาปสฺสอกรณํ（罪を犯さない）
            2.กุสลสฺสูปสมฺปทา（生き方として善行をする）
            3.สจิตฺตปริโยทปนํ（心の安らぎと魂の純粋さを実現する）"',

        //_ต้นไม้ประจำสถาบัน
        'text_content_tree_1' => '大学の木',
        'text_content_tree_2' => '"TNIの大学の木は、タイの桜としても知られているモモイロノウゼンです。これはは中型の落葉樹で、高さ8メートルから12メートルに成長します。12cmほどの鋭い葉を5枚つける複合葉です。枝が密集した茂みに広がり、11月から1月頃の冬の時期に開花のために葉を落とします。通常2月から4月に明るいピンクの花が咲きます。花びらはブーケのように5〜8個の花弁がつき、朝顔の花や角のように見えます。花の根本は長いチューブの形をしており、5枚の薄い花びらが組み合わされているため簡単に散ります。花が散り積もる様子は、木で咲いているのと同じくらい美しいです。"',

        //_อนุสาวรีย์ผู้ก่อตั้ง
        'text_content_monument_feature'           => 'TPA創設者の記念碑
        ソンマーイ・フントラクーン氏と穂積五一氏は、Technology Promotion Association（Thailand-Japand）（TPA）の共同設立者であり、泰日工業大学を設立する際のコンセプトアイデアの創設者でした。この記念碑は、TNI設立5周年記念日（2012年7月28日）に正式に建立されました。',



        // Board -----------------------------------------------------
         'text_board_and_executives' => '幹部と職員',
         'text_board' => '役員',
         'text_executives' => '役員',

        // list board ================================================//
        'text_name1' => 'スポン理事長' ,
        'text_position1' => '理事会の議長' ,
        'text_graduate1' =>'Bangkok Expressway and Metro Public社上席役員 Phranakhon Rajabhat 大学',

        'text_name2' => 'スラパン副理事長' ,
        'text_position2' => 'TNI理事会副議長' ,
        'text_graduate2' =>'泰日経済技術振興協会（TPA）会長 Guru Square社　シニアアドバイザー',

        'text_name3' => 'クリサダー学長' ,
        'text_position3' => '泰日工業大学学長' ,
        'text_graduate3' =>'スチャリット教授',

        'text_name4' => 'Dr.Sumet Yamnoon' ,
        'text_position4' => '役員' ,
        'text_graduate4' =>'Ph.D. (Applied Statistics and Research Methods) University of Northern Colorado, USA',

        'text_name5' => 'ピチット教授' ,
        'text_position5' => '役員' ,
        'text_graduate5' =>'DBA.(Finance), Chulalongkorn University, MBA (MIS) University of Dallas B.A. (Quantitative Approach), Chulalongkorn University ',

        'text_name6' => 'ポーンアノン副学長' ,
        'text_position6' => '役員' ,
        'text_graduate6' =>'DBA.(Finance), Chulalongkorn University, MBA (MIS) University of Dallas B.A. (Quantitative Approach), Chulalongkorn University ',

        'text_name7' => 'スチャリット教授' ,
        'text_position7' => '役員 ' ,
        'text_graduate7' =>'D.Eng. (Agricultural Engineering) Kyoto University, Japan',

        'text_name8' => ' Assoc.Prof. Pranee Chongsucharittham' ,
        'text_position8' => '役員' ,
        'text_graduate8' =>'Former Vice Dean of Research and Academic Services Department, Kasetsart University M.I.A. (Japanese Literature) Tsukuba University, Japan',

        'text_name9' => 'Assoc.Prof. Dr. Virach Apimethithamrong ' ,
        'text_position9' => '役員' ,
        'text_graduate9' =>'Chairman of Dr. Virach and Associates Public Accounting Firm Ph.D. (Finance), University of Illinois, USA',

        'text_name10' => ' Dr. Krissanapong Kirtikara' ,
        'text_position10' => '役員 ' ,
        'text_graduate10' =>'Adviser to the University, King Mongkut\'s University of Technology Thonburi (KMUTT) Ph.D. (Electrical Engineering) University of Glasgow, UK',

        'text_name11' => 'Mr. Jiraphan Ulapathorn' ,
        'text_position11' => '役員' ,
        'text_graduate11' =>'President of the Sumipol Corporation Ltd. Master of Management, Sasin Graduate Institute of Business Administration of Chulalongkorn University',

        'text_name12' => 'Prof.Dr. Wiwut Tantapanichkul' ,
        'text_position12' => '役員' ,
        'text_graduate12' =>'',

        'text_name13' => 'Dr. Pasu Loharjun' ,
        'text_position13' => '役員' ,
        'text_graduate13' =>'',
        
        'text_name14' => 'ランサン経営学部長' ,
        'text_position14' => '理事会メンバー（教員の代表者）' ,
        'text_graduate14' =>'京都大学　MBA',

        'text_name15' => 'Mr.Chartchan Phodhiphad ' ,
        'text_position15' => '秘書' ,
        'text_graduate15' =>'M.Sc. (Human Resource and Organization Development), NIDA',



        // executive =========================================================================//
  
        'text_ename1' => 'クリサダー学長' ,
        'text_eposition1' => '<b style"font-size:22px;">学長</b> <br>Acting Vice President of Administration<br><br> ' ,
        'text_egraduate1' =>' 京都大学電気工学部',

        'text_ename2' => 'ピチット教授' ,
        'text_eposition2' => '学部副学長、大学院学部長 <br><br><br>' ,
        'text_egraduate2' =>'アジア工科大学',

        'text_ename5' => 'ポーンアノン副学長' ,
        'text_eposition5' => '学生活動担当 <br><br><br> ' ,
        'text_egraduate5' =>'一橋大学経済学部',

        'text_ename7' => 'チュムポン教授' ,
        'text_eposition7' => '工学部長 <br><br> ' ,
        'text_egraduate7' =>'Ph.D. (Electronics), INSA Touluse, France',

        'text_ename4' => 'ラティコン副学長' ,
        'text_eposition4' => ' 国際関係および奨学金担当副学長、情報技術学部長代理研究革新部副学長  <br><br>' ,
        'text_egraduate4' =>'京都大学MBA ',

        'text_ename3' => 'ランサン教授' ,
        'text_eposition3' => 'アカデミックサービスセンター副学長、アドミッションズアンドオーガニゼーションコミュニケーションセンター副学長代理、経営学部長' ,
        'text_egraduate3' =>'京都大学MBA',

        'text_ename8' => 'ワンウィモン教授' ,
        'text_eposition8' => '語学・教養課程部長<br><br><br>' ,
        'text_egraduate8' =>'大阪大学',

        'text_ename9' => 'Mr.Chartcharn Phodhiphad' ,
        'text_eposition9' => '学長室部長 <br><br><br> ' ,
        'text_egraduate9' =>'M.Sc.(Human Resource and Organization Development) , National Institute of  Development Administration',

        'text_ename10' => 'Ms.Suwannapa Ruengsilkonlakarn' ,
        'text_eposition10' => '総務部部長 <br><br><br> ' ,
        'text_egraduate10' =>'チュラロンコン大学政治学部',

        'text_ename11' => 'Mr.Narong Anankavanich' ,
        'text_eposition11' => '学務部部長<br><br><br> ' ,
        'text_egraduate11' =>'フロリダ大学機械工学部',

        'text_ename12' => 'Mr.Surawat Yingsawad' ,
        'text_eposition12' => '学生部部長<br><br><br>' ,
        'text_egraduate12' =>'カセサート大学体育学部',

        'text_ename13' => 'Ms.Supaporn Hempongsopa ' ,
        'text_eposition13' => '国際関係＆奨学金担当部長 <br><br><br>' ,
        'text_egraduate13' =>'ラムカムヘン大学経済学部',

        'text_ename6' => 'Assist.Prof Kasem Thiptarajan' ,
        'text_eposition6' => '情報技術部長補佐 <br><br>' ,
        'text_egraduate6' =>'<br>チュラロンコン大学',


        // facilities
        'text_facilities' => '支援施設',
        'text_eng_lab' =>'<br>工学部研究室',
        'text_it_lab' =>'<br>情報技術学部研究室',
        'text_ba_lab' =>'<br>経営学部研究室',
        'text_ba_classroom' =>'経営学部研究室',
        'text_cgel_lab' =>'一般教育言語学部研究室',
        'text_cgel_classroom' =>'一般教育言語学部研究室',
         'text_ba_detail_room' => '革新的な学習・研究・ビジネスサービスセンターは、TPS TPM TQMと新段である3T1Sの日本の製造コンセプトに基づいた経営管理、生産管理に関するコンサルティングを提供します。',
         'text_ba_room' => 'ものづくり研究センター',
         'text_cgel_detail_room' => '一般教育言語学部研究室',
         'text_library' =>'図書館',

         //teacher 
         'text_teacher' => '講師プロフィール',
         'text_eng' =>'工学部',
         'text_it' =>'情報技術学部',
         'text_ba' =>'経営学部',
         'text_cgel' =>'一般教育言語学部',

       
    ];
?>