<?php

    return [

        // privacy
        'text_privacy_title' => 'プライバシーポリシー：泰日工業大学',
        'text_detail' => 'TNIプライバシーポリシーは、個人データのプライバシー、学生の募集、教育と学習の管理、研究開発、人事、および学生、卒業生、訪問者などの個人が関与するさまざまな活動など、教育機関のすべての活動の基本的な運用を保護することを目的としています。したがって、TNIは、以下の法的な管理における実務と手順の一貫性を確保するために、ポリシーを規制します。',

        'text_list1' =>'1. 個人情報の利用',
        'text_list_detail1' => 'TNIは、マーケティングまたはその他の活動の目的で、名前、名前、写真などの必要な情報のみを使用して個人データを収集します。あなたの個人データは、あなたの許可がない限り、売りに出されたり、部外者に配布されたりすることはありません。',

        'text_list2' =>'2. 個人データを管理する権利',
        'text_list_detail2' => 'あなたのプライバシーを優先し、あなたの個人データを保護するために、あなたは私たちがあなたの必要に応じて処理するようにあなたの個人データの使用または共有のためにTNIを許可または禁止する権利を持っています。',

        'text_list3' =>'3. 個人データのセキュリティと取り扱い',
        'text_list_detail3' => '個人データの厳密な信頼性とセキュリティのために、TNIは、個人データにアクセスまたは使用する権利を決定するために、政府機関内にポリシー、規則、規制を定めています。',

        'text_list4' =>'4. クッキーの使い方',
        'text_list_detail4' => 'Cookieは、TNIがユーザーのWebブラウザを介して送信する小さなテキストファイルです。そのような情報がシステムにインストールされると、Cookieにより、TNIは、デバイスから除外されるまでユーザーの設定を記憶できるようになります。ただし、ユーザーはシステムからCookieを削除することでサービスを終了できます。クッキーは、あなたの好みに合った適切なコンテンツを配信するのに便利です。 Cookiesが記録および編集したデータについては、TNIが分析するか、サービスの向上を目的として他の活動に使用します。',
                        
        'text_list5' =>'5. 個人データ保護ポリシー',
        'text_list_detail5' => 'TNI has the rights to maintain or revise our privacy policy as appropriated  without prior notice. TNI requests users to read well 
                        the privacy policy every time when visit our website or use of our services.',

        'text_comment' => 'If you have any suggestion or iquiries regarding the compliance to our policy, you may contact us as follow:',
        
        'text_tni' => 'Thai -Nichi Institute of Technology ', 
        'text_address' => ' 1771/1 Pattanakarn 37, Pattanakarn Rd., Suan Luang, Bangkok 10250 ',
        'text_phone' =>'Tel 0-2-763-2600 Fax 02-763-2700',
        'text_email' => 'E-Mail : tniinfo@tni.ac.th ',
        
        
        
        
        
        // cookies 
        'text_cookies_title' => 'Cookie Policy : Thai-Nichi Institute of Technology ',

        'text_list_c1' =>'1. What is Cookie?',
        'text_list_detail_c1' => 'Cookie is a small text file that be saved in your PC or table or Mobile without dangerous to your devices or to be infected with Virus "......" .
                        While visiting the website, Cookie can track and keep information as well as remember you on how you use the website and activate again if you revisit.  
                        This will satify you for the online experiences.  Cookie is commonly used on website, social media, search engine, email etc. 
                        in remembering  your favorable registered login, theme selection, preferences, and other of your function settings.',

        'text_list_c2' =>'2.	Use of TNI Cookie',
        'text_list_detail_c2' => 'We utilize Google Analytics--a service from Google Inc. (Google) to analyze  website through cookies and disclose how you use our websit 
                        in order to improve our service and give your the good experience in your accessing on our systems.  
                        The alnalysis will be incognito mode and will not detect any of your persoanal data.  If you are several acessing 
                        "URL website" without any resetting your internet browser, therefore, your device will agree to accept cookie autonomuously 
                        for your next access.  You can reset it by selecting the " setting function" on your web browser to change on your setting, 
                        if needed be.   To do so, please be noted that you may not be able to use the full functions of our website.  However, 
                        for any visit to our website, we claim that you gree and allow Google to utilze your personal data for the said purpose.
        ',

        'text_list_c3' =>'3.	User Agreement ',
        'text_list_detail_c3' => 'If you are continued to use our website, it means you are agreeing to allow cookies to be installed on your device. 
        If you choose not to accept cookies We cannot guarantee that your experience will be as satisfying as it could be. ',


        // popup policy
        'text_popup_policy' => 'We use Cookie for our service , improvement and optimize your browsing experience. If you continue to use our website, you agree to the use of Cookie,',
        'text_link_more' =>'Click here for more informaition',
        'text_btn_accept' => 'ACCEPT',



    ]

?>