<?php 

    return [
        'text_name' => '名／姓',
        'text_email' => 'Eメール',
        'text_tel' => '電話番号',
        'text_message' => 'メッセージ',
        'text_sendbutton' => 'メッセージの送り先',
        'text_info' => 'メッセージの送信ができなかった場合は、以下のアドレスにご連絡ください。tniinfo@tni.ac.th',
       
    ];