<?php 

    return [
        'text_about' => 'About TNI',
        'text_faculty' => 'Faculties and Programs',
        'text_teacher' => 'Instructor\'s Profile',
        'text_department' => 'Department and Academic Services ',
        'text_contact' => 'Contact Us ',

        //about
        'text_history' => 'TNI at a Glance ',
        'text_personnel' => 'Executives and Staff',
        'text_learngin_support' => 'Facilities and Services',
        'text_popularity' => 'Core Value ',
    ];