<?php

    return [
        'text_event' => 'Events & Updates',
        'text_event_lists' => 'Events & Update Lists',
        'text_create_event' => 'Create Event & Update',
        'text_edit_event' => 'Edit Event & Update',
        'text_id' => 'ID',
        'text_image' => 'Image',
        'text_event_date' => 'Event Date',
        'text_name' => 'Name',

    ];