<?php

return [

    'text_news_lists' => 'News Lists',
    'text_create_news' => 'Create News',
    'text_edit_news' => 'Edit News',
    'text_category' => 'Category',

    'text_link' => 'Link (link to other website, if needed.)',
    'text_edit_news_category' => 'Edit News Category',
    
];
