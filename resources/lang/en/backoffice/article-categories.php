<?php

return [

    'text_news_category_lists' => 'News Category Lists',
    'text_create_news_category' => 'Create News Category',
    'text_edit_news_category' => 'Edit News Category',
    'text_id' => 'ID',
    'text_image' => 'Image',
    'text_name' => 'Name',

];
