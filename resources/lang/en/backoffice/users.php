<?php

return [

    'text_user_lists' => 'User Lists',
    'text_user' => 'User',
    'text_search' => 'Search',
    'text_clear_search' => 'Clear Search',
    'text_create_user' => 'Create New User',
    'text_edit_user' => 'Edit User',
    'text_delete' => 'Delete',
    'text_id' => 'ID',
    'text_name' => 'Name',
    'text_email' => 'E-Mail',
    'text_latest_access' => 'Last Online',
    'text_password' => 'Password (minimum 5 charecters)',
    'text_password_confirmation' => 'Confirm Password (minimum 5 charecters)',
    'text_image_profile' => 'Profile Image ',
    'text_user_privileges' => 'Role User',
    'text_all_privileges' => 'All',

];
