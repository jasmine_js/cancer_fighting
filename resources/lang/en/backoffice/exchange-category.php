<?php

return [
    'text_exchange_category_lists' => 'Exchange Program category Lists',
    'text_create_exchange_category' => 'Create Exchange Program Category',
    'text_edit_exchange_category' => 'Edit Exchange Program Category',
    'text_id' => 'ID',
    'text_image' => 'Image',
    'text_name' => 'Name',
    // 'text_edit_exchange_category' => 'แก้ไขหมวดหมู่โครงการแลกเปลี่ยน',
    
];