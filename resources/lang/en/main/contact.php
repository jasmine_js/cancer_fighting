<?php

    return [
        'text_contact' => 'Contact Us ',
        'text_img_map' => 'image/catalog/mockup/mapen.png',
        'text_tni' => 'Thai-Nichi Institute of Technology', 
        'text_address' => '1771/1 Pattanakarn Rd., Suan Luang, Bangkok 10250, Thailand',
        'text_phone' =>'Tel:0-0-2763-2600  &emsp; Fax:0-2763-2700 ',
        'text_email' => 'E-Mail : tniinfo@tni.ac.th ',
        'text_website' => 'Website : http://www.tni.ac.th ',
        'text_tni_en' => 'Thai-Nichi Institute of Technology is located on Pattanakarn Road between Soi 37 and 39',

        'text_map' =>' It is easy to get to and from using the main roads such as Srinakarin, <br>
        Ram-inthra - Atnarong Expressway, Motorway (Bangkok - Chon Buri), and East Outer Ring Road.<br>
        There are buses number 11, 133, 206, Por-Or 517 and Por-Or 92, Airport Link (Ramkhamhaeng or Hua Mak stations), <br>
        BTS (Phra Khanong station), and MRT (Phetchaburi station) to the institute.<br>',
        'text_department_phonenumber' => 'Telephone extension list',
        'text_tbody' => '
                           <tr>
                                <td>President Office</td>
                                <td>02-763-2748</td>                 
                            </tr>
                            <tr>
                                <td>Graduate School</td>
                                <td>02-763-2600 ต่อ 2402, 2403 </td>                
                            </tr>
                            <tr>
                                <td>Faculty of Engineering</td>
                                <td>02-763-2910</td>
                            </tr>
                            <tr>
                                <td>Faculty of Information Technology </td>
                                <td>02-763-2740</td>
                            </tr>
                            <tr>
                                <td>Faculty of Business Administration </td>
                                <td>02-763-2712</td>
                            </tr>
                            <tr>
                                <td>College of General Educaiton and Languages </td>
                                <td>02-763-2804</td>
                            </tr>
                            <tr>
                                <td>Office of International Program </td>
                                <td>02-763-2409, 2410</td>
                            </tr>
                            <tr>
                                <td>International Relations and Scholarship Affairs </td>
                                <td>02-763-2780-2</td>
                            </tr>
                            <tr>
                                <td>Admissions and Corporate Comminication Department </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>&emsp; - Admissions Center </td>
                                <td>02-763-2601-5</td>
                            </tr>
                            <tr>
                                <td>&emsp; - Corporate Communication Unit </td>
                                <td>02-763-2783</td>
                            </tr>
                            <tr>
                                <td>Administration Department </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>&emsp; - Human Resources Division</td>
                                <td>02-763-2615</td>
                            </tr>
                            <tr>
                                <td>&emsp; - Account and Finance Unit	</td>
                                <td>02-763-2610-3, 2629</td>
                            </tr>
                            <tr>
                                <td>&emsp; - General Administration and Supplies Unit	</td>
                                <td>02-763-2616</td>
                            </tr>
                            <tr>
                                <td>&emsp; - Purchasing /Building and Facilities Unit	</td>
                                <td>02-763-2618</td>
                            </tr>
                            <tr>
                                <td>Academic Affairs Department </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>&emsp; - Center of Excellence (COE)</td>
                                <td>02-763-2926</td>
                            </tr>
                            <tr>
                                <td>&emsp; - Academic Services Unit	</td>
                                <td>02-763-2704</td>
                            </tr>
                            <tr>
                                <td>Research and Innovation Department </td>
                                <td>02-763-2752</td>
                            </tr>
                             <tr>
                                <td>Student Affairs </td>
                                <td>02-763-2620-4</td>
                            </tr>
                             <tr>
                                <td>Academics Affairs </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>&emsp; - Registrar and Education Evaluation Unit</td>
                                <td>02-763-2505</td>
                            </tr>
                            <tr>
                                <td>&emsp; - Responsive Teaching Unit</td>
                                <td>02-763- 2504</td>
                            </tr>
                             <tr>
                                <td>&emsp; - Quality Assurance Unit</td>
                                <td>02-763-2503</td>
                            </tr>
                            <tr>
                                <td>Co-operative Education and Job Placement Center</td>
                                <td>02-763-2750</td>
                            </tr>
                            <tr>
                                <td>Information and Communication Center (ICC) </td>
                                <td>02-763-2643</td>
                            </tr>
                            <tr>
                                <td>Library</td>
                                <td>02-763-2626</td>
                            </tr>',
                         

                  
       
    ];