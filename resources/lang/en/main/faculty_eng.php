<?php

    return [
        'text_name' => 'Faculty of Engineering',
        'text_detail' => 'Engineering Faculty targets to be a hub of technical knowledge and produce of qualified engineer who are able to apply modernized technology for
            industrial usages with morality and consciousness. Graduate will have essential knowledge both theory and practice, competence in Japanese and English.
            Our programs are actually designed to meet the needs of business and industrial sectors.',

        'text_program' => 'Program',
        'text_bachelor' => 'Bachelor\'s Degree',
        'text_major' => '<li>Automotive Engineering (AE)</li>
                        <li>Robotics and Lean Automation Engineering (RE)</li>
                        <li>Computer Engineering (CE)</li>
                        <li>Industrial Engineering (IE)</li>
                        <li>Electrical Engineering (EE)</li>', 


        'text_major1' => 'Automotive Engineering  (AE)', 
        'text_detail_major1' => 'TNI aims to produce the graduate who are competence in design, material technology development,
            production process, production management, automatic control system, maintenance system, including analytical skills in solving automobile problem 
            and able to apply new technology to use in work. ',                                       

        'text_major2' => 'Robotics and Lean Automation Engineering (RE)', 
        'text_detail_major2' => '
            <li>Robotics and Lean Automation Engineering (RE) is the program highly in demand on employment by Japanese manufacturing in Thailand.</li>
            <li>Program\'s structures are designed by the cooperation of curriculum development committee who are leading industries related to automation production systems.
                Graduates of this program are competent in relevant automation system designs and be ready to work in this distinctive industry.</li>
            <li>Program integrats relevant knowledges of industrial robots, robotic automation system, technical analysis on waste management by LEAN. 
                TNI is also the first academic institution who offers this engineering course with important bridging on production management and innovation.</li>
            <li>Opportunity to work in the career path of engineering profession is more competitive than other engineering profession as students 
                also competent in production process, system design, electrical control system. programing and IT skills. </li>
            <li>This curriculum is in line with the new driving economic Policy that transforms the traditional economy into the new driving by automation production systems.</li>
                ',     

        'text_major3' => 'Computer Engineering (CE)', 
        'text_detail_major3' => 'the course focuses on hardware and software design, Embedded Systems, which are the heart of automatic controlling system for various industries.
            This course is also be regconized on its standard that equivalent to leading higher education institutions in Japan and the United States.',   

        'text_major4' => 'Industrial Engineering (IE)', 
        'text_detail_major4' => 'The course combines industrial management standard with Japanese industrial management that enable graduate to apply knowledge 
            from both ways in production planning,  production process improvement, quality control, including logistics management.',

        'text_major5' => 'Electrical Engineering: EE (EE)', 
        'text_detail_major5' => 'The course provides contents of electrical engineering covering 3 areas: power systems, smart systems, and mechatronics.
            All will focus on teaching and learning according to Japanese Monozukuri Principle', 


        //inter -----------------------------------------------------------------  

        'text_bacherlor_inter' => 'International Program',
        'text_inter_eng_major' => '<li>Digital Engineering (DGE)</li>' ,

        'text_inter_eng_major1' => 'Digital Engineering : International Program ', 
        'text_detail_inter_eng_major1' => '"DGE is the first course in Thailand that aims at producing graduates with  knowledge and expertise in Digital engineering 
            who can combine and integrate digital engineering with artificial intelligence knowledge,  develope of automation system and IOT as well as to be competent engineering
            workforce that can work internationally. 
        "',
                      


        
       
      
    ];