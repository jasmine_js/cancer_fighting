<?php

    return [


        'text_ENG' => 'ENGINEERING',
        'text_eng' => 'Faculty of Engineering',

        'text_BA' => 'BUSINESS ADMINISTRATOR',
        'text_ba' => 'Faculty of Business Administrator',

        'text_IT' => 'INFORMATION TECHNOLOGY',
        'text_it' => 'Faculty of Information Technology',

        'text_CGEL' => 'C-GEL',
        'text_cgel' => 'Collage of General Education and Language',

     
    ];