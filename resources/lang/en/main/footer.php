<?php

    return [

        'text_quick_link' => 'Quick Link',
        'text_apply_now' => 'Apply Now',
        'text_calendar' => 'Academic Calendar',
        'text_e_learning' => 'E-Learning',
        'text_e_dictionary' => 'E-Dictionary',
        'text_web_link' => 'TNI\'s Donors',
        'text_webmail' => 'TNI Webmail',
        'text_jobtni' => 'jobtni.com',
        'text_career_tni' => 'Join Us ',

        'text_journal_eng' => 'TNI Journal of Engineering and Technology',
        'text_journal_ba' => 'TNI Journal of Business Administration and Languages',

        'text_faculty' => 'Faculties',
        'text_engineering' => 'Faculty of Engineering',
        'text_it' => 'Faculty of Information Technology',
        'text_ba' => 'Faculty of Business Administration',
        'text_cgel' => 'College of General Education & Languages',
        'text_grad' => 'Graduate School',
        'text_internation_program' => 'Office of International Program ',
        'text_reg' => 'Registrar Office',
        'text_studen_affairs' => 'Student Affairs ',
        'text_research' => 'Research And Academic Services Center',
        'text_information' => 'International Relations and Scholarship Affairs ',
        'text_co_operative' => 'Co-operative Education and Job Placement Center ',
        'text_qa' => 'QA',
        'text_account_finance' => 'Account & Finance',
        'text_icc' => 'Information & Communication Center',
        'text_alumni' => 'TNI Alumni',
        'text_library' => 'Library',
        'text_knowlege_management' => 'Knowledge Management',
        'text_deparements' => 'Department',
        'text_tni' => 'Thai-Nichi Institute of Technology ',
        'text_adress' => '1771/1 Pattanakarn Rd., Suan Luang, Bangkok 10250, Thailand',
        'text_tel' => 'Tel. 0-2763-2600 Fax. 0-2763-2700',
        'text_email' => 'tniinfo@tni.ac.th',
        'text_direct_chancellor' => 'President Line ',
        'text_donwload' => 'Download',
        'text_sigil_program' => 'Sigil Program',
        'text_map' => 'Map',
        'text_copyright' => '© 2018-2019 THAI-NICHI INSTITUTE OF TECHNOLOGY (TNI) ALL RIGHTS RESERVED.',

        'text_other_link' => 'Related link',
        'text_privacy_policy' => 'Privacy Policy',
        'text_tpa' => 'Technology Promotion Association (Thailand-Japan)',


        
    ];