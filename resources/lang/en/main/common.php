<?php 

return [
    'htmlheader_description' => 'TNI aims to be the center of higher education on academic research and profession on human resources development as well as to incubate the professional manpower in cutting edge technology, excellence on modern efficiency management both academics and practices, and disseminate of knowledge inheriting on moral and conscience responsibility to the society.',
    'htmlheader_keywords' => 'Thai-Nichi Institute of Technology, Engineering Faculty, Information Technology Faculty, Business Administration Faculty, Computer Engineering, Robotic and Lean Automation Engineering, Automotive Engineering, Industrial Engineering, Management of Technology and Innovation, MBA in Innovation of Business and Industrial Management, Information Technology, Technology, IT, Information, Thai-Nichi, Thai, Nichi, Engineering, programmer, Industry, program, Information Technology Program, Multimedia Technology Program, Business Information Technology, Information Technology, Multimedia, Business Information, Education, study, Undergraduate, Graduate, University, Institute',
    'text_htmltitle' => 'Thai-Nichi Instutute of Technology',
    'text_apply_now' => 'Apply Now',
    'text_search' => 'Search',
    'text_not_found_data' => 'Not found', 

    // stylsheet
    'text_style_css' => 'css/style.css?ver=1.0',
    'text_custom_css' => 'css/custom.css?ver=1.0',

    
];