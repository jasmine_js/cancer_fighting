<?php

    return [
        'text_name'   => 'General Education and Language College',
        'text_detail' => 'College of Generation Education and Languages aims to build and develop business and industry personnel with English and Japanese communication skills 
            who enable to adapt into the rapid changes in economic, social and environmental conditions with a good consciousness for society and the Nation.',
        'text_program'  => 'Section',
        'text_bachelor' => 'Bachelor\'s Degree',
        'text_major' => '<li>Japanese Language</li>
                        <li>English Language</li>
                        <li>Social and Huminity </li>
                        <li>Sciences and Mathematics ',

        'text_subject' => 'Subject opened' ,

        'text_major1' => 'Japanese Language', 
        'text_detail_major1' => '
        JPN-101 Business Japanese 1 
        | JPN-102 Business Japanese 2
        | JPN-201 Business Japanese 3
        | JPN-202 Business Japanese 4 
        | JPN-301 Business Japanese 5
        | JPN-302 Business Japanese 6 
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        JPN-407 Japanese for Management 1
        | JPN-408 Japanese for Management 2
        | JPN-427 Japanese for Industrial Worker
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        JPN-428 Japanese for IT
        | JPN-429 Japanese for Salary Man
        | JPN-430 Listening to Voices from Japan
        | JPN-431 General Japanese stories
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        JPN-432 Online Writing in Japanese  
        | JPN-433 Discussion in Japanes 
        | JPN-434 Creative Presentation in Japanese 
        | JPN-435 Lift up more Expertise in Japanese 
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        JPN-436 J-pop study 
        | JPN-437 Travel in Japan by yourself 
        | JPN-491 Extra Curriculum Activity in Japanese 1
        | JPN-492 Extra Curriculum Activity in Japanese 2
        ',               

        'text_major2' => 'English', 
        'text_detail_major2' => '
        ENL-111 English for Communication
        | ENL-112 English for Skill Development
        | ENL-211 English for Work 
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        ENL-212 TOEIC Test Preparation
        | ENL-423 English for Engineer
        | ENL-424 English for IT
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        ENL-425 English for Businessmen
        | ENL-426 English for Industrial Worker
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        ENL-427 Principles of Translation for Business and Industry
        | ENL-428 Professional English Listening and Speaking
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>
        ENL-429 Professional English Presentation
        | ENL-430 Professional English Reading and Writing 
        | ENL-431 Business Correspondence in English
        | ENL-432 Advanced TOEIC
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        ENL-433 IELTS Preparation
        | ENL-434 TOEFL Preparation
        | ENL-435 Discovering English through Western Culture
        | ENL-436 English in Thai Context
        <i class="fab fa-diaspora" style="font-size:0.7em"></i> 
        ENL-436 Extra Curriculum Activity in English 1
        | ENL-492 Extra Curriculum Activity in English 2
        ', 

        'text_major3' => 'Social Sciences and Humanities', 
        'text_detail_major3' => '
        Humanity Subjects  
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>
        HUM-125 Arts of Life   
        | HUM-126 Thai Traditional Arts 
        | HUM-127 World\'s Great Turning Point  
        | HUM-128 Live Well
        | HUM-129 Knowing Master Arts  
        | HUM-130 Having Money, Spending Money <br> <br>
      
        Social Sciences Subjects 
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>        
        SOC-125 Career Preparation  
        | SOC-126 Lion Heart People  
        | SOC-127 Political Awareness  
        | SOC-128 Live Life Laws 
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>
        SOC-129 Thai Society and Culture 
        | SOC-130 Japan Today  
        | SOC-131 Innovative Solutions Management 
        | SOC-132 Asean-Japanology
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>
        SOC-133 "Avoiding Natural Disasters Avoiding Natural Disasters"
        | SOC-134 Thai-Nichi Society
        | SOC-135 Media Literacy  
        | SOC-136 Healthy living 
        | SOC-137 Startup Studies  
        | SOC-138 Technology Tour 
        ',  

        'text_major4' => 'Sciences and Mathematics Subjects', 
        'text_detail_major4' => ' 
          
        BUS-106 Business Mathematics
        | ENG-111 General Physics
        | ENG-112 General Physics Laboratory
        | ENG-121 General Chemistry
        | ENG-122 General Chemistry Laboratory
        | ENG-211 Engineering Statistics 
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>  
        MSC-110 General Science
        | MSC-111 Mathematics and Application
        | MSC-112 Science and Technology
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>  
        MSC-113 General Science in Industry
        | MSC-114 Science, Technology and Innovation
        | MSC-115 Scientific Thinking in Business 
        <i class="fab fa-diaspora" style="font-size:0.7em"></i>
        MSC-202 Statistics and Probability

        ',    
        
    ];
    ?>