<?php

    return [
        'text_name'   => 'Faculty of Business Administration',
        'text_detail' => 'Business Administration Faculty aims to be the excellence academic center of business management for students and outsiders to have expertise
             in management technology, and be able to apply modern management technology to benefit the industry and society with morality and social consciousness. ',
        'text_program'  => 'Program ',
        'text_bachelor' => ' Bachelor\'s Degree ',
        'text_major' => '<li>Management of Technology and Innovation (MI)</li>
                        <li>Business Japanese (BJ)</li>
                        <li>International Business Management  (IB)</li>
                        <li>Accountancy (AC)</li>
                        <li>Human Resources Management  (HR)</li>  
                        <li>Logistics and Supply Chain Management (LM)</li>
                        <li>Creative and Digital Marketing (CD)</li>
                        <li>Innovative Tourism and Hospitality Management (TH)</li>',

        'text_major1' => 'Management of Technology and Innovation (MI)', 
        'text_detail_major1' => 'Program is integrated between business, technology management and production innovation in line with Industry 4.0,  STEM Educational Learning Development of Thailand 4.0',                      

        'text_major2' => 'Business Japanese (BJ) ',    
        'text_detail_major2' => 'It is a practical business administration program, focusing on studying Japanese in various subjects in order to have a deep understanding 
            of Japanese administration and business contents. ',    

        'text_major3' => 'International Business Management (IB)', 
        'text_detail_major3' => 'This curriculum aims to create leadership for multinational companies and for those who want to enter business in overseas by themselves.
           The class conducts in English and aim to build global mindset which concerns the significance on cross culture.  ',

        'text_major4' => 'Accountancy (AC)', 
        'text_detail_major4' => 'This program aims to produce graduates who are competent in theoretical and practical in Accounting, Diagnose on Accounting as well as knowlege on Japanese Accounting which are the highlight and compulsory subjects of this program.',

        'text_major5' => 'Japanese Human Resources Management (HR)',   
        'text_detail_major5' => 'This curriculum focuses on  Human resources Management, Business management and Japanese corporate culture together with higher Japanese language proficiency that adequate for working ',
        
        'text_major6' => 'Logistics and Supply Chain Management (LM)', 
        'text_detail_major6' => 'The program aims to educate students both logistics and supply chains management by separating technical approach into operations to 
            support efficiency production, marketing activities, project management, the use of IT selection in order to deliver customers the optimization of satisfaction.
        ',

        'text_major7' => 'Creative and Digital Marketing (CD)', 
        'text_detail_major7' => 'This new curriculum aims to produce graduates and enable them with knowledge and ability in all aspects regarding to the creative marketing options,
             ready to work, carry out, and smart to define the best tools, innovation and marketing strategy that will be suitable for prospective customers. 
        ',

        'text_major8' => 'Innovative Tourism and Hospitality Management (TH)', 
        'text_detail_major8' => 'This program aims to produce graduates with quality knowledge, language and IT competency to serve the tourism industry, especially in specific inbound 
            and outbound Japanese tourist targeted.',

        //Inter
        'text_bacherlor_inter' => 'International Program',
        // 'text_inter_ba_major' => '<li>สาขาวิชาการจัดการธุรกิจระหว่างประเทศ <br>(International Business Management : IBM)</li>',
        // 'text_inter_ba_major1' => 'การจัดการธุรกิจระหว่างประเทศ (IBM : International Program)', 
        'text_inter_ba_major' => '<li>Global Business Management (GBM)</li>',
        'text_inter_ba_major1' => 'GBM: International Program', 
        // 'text_detail_inter_ba_major1' => 'หลักสูตร International Business Management (IBM) นานาชาติ เรียนเป็นภาษาอังกฤษและเสริมด้วยการเรียนรู้ภาษาญี่ปุ่นแบบเข้มข้น เหมาะสำหรับคนที่คิดการใหญ่ มีวิสัยทัศน์ไปถึงโลกอนาคตที่ภาษามีส่วนสำคัญในการทำงาน
        // หลักสูตร IBM มีความเข้มข้นไปด้วยกรณีศึกษาที่เป็น บริษัทขนาดใหญ่ มีเครือข่ายทั่วโลก เช่น Google Coca-Cola Toyota Apple ผสมผสานกับหลัก Monozukuri ที่สอนโดยการลงมือทำจริงๆ
        // เราจึงส่งเสริมให้นักศึกษาได้ Start-Up Business ของตัวเองตั้งแต่ปี 1',    
        'text_detail_inter_ba_major1' => 'International Program, Blobal Business Management : GBM) aims to edcating student the academic knowledge and higher experience in
            global business context,  learning by Monozukuri Principle through  hand-on experiences in many aspects of Startup, Data Anlaysis as well as conducting varities acvtivities
            through reality events/exhibitions which will be gained or lost from the performance.  Students have opportunity to join domestics and overseas study trips to learn
            global business and discuss with professionals Japanese or other foreign businessmen.    Programs covers not only class leaning,  but TNI also encouraging students 
            to learn from real world contents,  preparing students proficiency in Thai, English and Japanese Language, have skills and knowledge to utilize various tools 
            such as Automation, Content Gerating Tool, Social Media Technology and Business Management Applications etc.  GBM also provides 3+1 program as the alternative choice 
            to take one-year studying in Southern New Hampshire University, U.S.A.  in order to earns 2 Bachelor\'s Degrees. ',        
    
    ];
    ?>