<?php

$arr_texts1[] = "Providing higher education in specific fields that meet the needs of business and industrail sectors by focusing on principles of academic excellence, practice and practical training";
$arr_texts1[] = "Educating students to have qualified knowledge, virtues thinking, responsibility,  work professionally and be socially consciousness";
$arr_texts1[] = "Undertaking research to create and develop technologies for greater knowledge which aims to support teaching and learning on business and industry";
$arr_texts1[] = "Transferring of knowledge and high technology to strengthen the competitiveness in business and industrial sectors";
$arr_texts1[] = "Nurturing to promote, disseminate and exchange of Thai arts, culture and wisdom";

//_จุดเด่นและจุดเน้นของสถาบันฯ
$arr_texts2[] = "Thailand-Japan Relations";
$arr_texts2[] = "Student Development in line with Monozukuri Principle";

//_ค่านิยมหลัก
$arr_texts3[] = "Monozukuri (Dedication, Creativity and Development)";
$arr_texts3[] = "Kaizen (Continually Improve)";
$arr_texts3[] = "Hansei (Accept and Learn from Mistakes) ";
$arr_texts3[] = "Honest (Be honest)";
$arr_texts3[] = "Respect (Respect Yourself and Others)";
$arr_texts3[] = "Public Interest (Consider Public Interest)  ";

$arr_symbolic_3[] = "<b>The Gear</b> which is the character of the Institue";
$arr_symbolic_3[] = "<b>The A symbol</b> resembles a gable roof of traditional Thai-style house with the abbreviation word \"TNI\" in red. ";
    
$arr_symbolic_5[] = "the Cog means non-stop continuing development of the Institute which reflects the Institute in modern innovative technolgy and 
                self-development to capture with the rapid change of technology as well as to produce personnel in line with the needs of the moving World.";
$arr_symbolic_5[] = "The A Symbol resembles a gable roof of traditional Thai-style house, portrays an academic institution in the Asian continent that 
                maintains its Thai cultural identity.  The top of the gable roof reaching out above and beyond the gear represents knowledge and wisdom without 
                fixed pattern or physical boundary. This connotes that the institute aims to create technology and innovation through the exchange of 
                knowledge and wisdom that goes beyond the boundary ";
    
$arr_color_3[] = "<b>Blue</b> is the color of Technology Promotion Association (Thailand-Japan) which is the founder of Thai-Nichi Institute of Technology and
                being an educational institution that glorifies the monarchy. Moreover, the color depicts wisdom, vision, nobility, and honor, which reflects the 
                intention of the founder in establishing the institute that can help develop Thai personnel to be visionary, knowledgeable, wise, and can apply 
                their knowledge with pride, honor, and dignity.";

$arr_color_3[] = "<b>Red</b> is the color of the Sun and the East which means that the Insitute aims to accumulate and transfer knowledge, concepts, 
                philosophy and technology from the Eastern hemisphere, especially from Japan.  Additionally, the color represents prosperity, 
                successs, and virtue, which are the will of the founders who want the graduates of the Institute a virtuous and successful person that
                can create prosperity for themselves, organizations, society, and the nation.";
    



    return [
        'text_intro'            => 'About TNI',

        'text_history'                    => 'History of TNI, Philosophy',                                           //_menu 
        'text_resolution_vision_mission' => 'Resolution, Vision, Mission ',                                            //_menu
        'text_character'                  => 'Characteristic',   //_menu
        'text_value'                      => 'Core Value ',                                                          //_menu
        'text_symbolic_color'             => 'Logo and Colour of the Institute',                                            //_menu
        'text_principal_buddha_image_tree' => 'Principle Buddha Image of the Institute,The Institute tree',                               //_menu
        'text_monument_feature'           => 'Statue of TPA Founder ',                             //_menu
        
        'text_history_tni' => 'History of TNI',
        'text_philosophy'  => 'Philosophy',
        'text_around_tni'  => 'Around the Campus ',
        'text_resolution'  => 'Resolution ',
        'text_vision'      => 'Vision ',
        'text_mission'     => 'Mission ',

        'text_graduate_identity'         => 'Identity',
        'text_institute_identity'        => 'Uniqueness',
        'text_institute_highlight_focus' => 'Strengths and Focus',
        
        'text_content_institution_history' => "Thai-Nichi Institute of Technology was founded by Technology Promotion Association (Thailand-Japan) or TPA in 2007.  
            TPA was established on the 24th of January 1973 by a group of well-wishes composed of those who used to study or receive training in Japan, 
            with the strong support of Japanese government and private sectors.  Mr. Sommai Hoontrakul, the former Finance Minister, was the leader of 
            Thai side and Mr. Goichi Hozumi, the former president of Japan-Thailand Economic Cooperation Society of JTECS, have played an important 
            role in establishing TPA.  TPA’s objective is to be the center of promoting knowledge disseminating and technology transfer to Thai personnel for
            the growth and advancement of Thai economy and industry.  TPA has gradually become well-known by accumulating expertise and providing adequate services 
            to industrial sector.  These services include Training, Consulting, Instruments Calibration Center, Publishing and Language School.  
            Over the past four decades, TPA has continually introduced new technologies from overseas to Thai Industrial sector and has increasingly gained wide
            recognition from Thai public TPA started the Thai-Nichi Institute of Technology Project in 2005 and receive the official license from the Ministry of
            Education on the 29th September 2006.  In 2007, TNI started providing higher education for both undergraduate as well as Master’s degree in 3 faculties,
            namely, Engineering Faculty, Information Technology Faculty, Business Administration Faculty.",

        'text_content_philosophy' => "Technology Promotion Association (Thailand-Japan), or TPA has been operate under the philosophy of \"disseminating knowledge,
            building economic base\"  reflecting the clear direction and goal in operation and services of TPA throughly over 47 years which as  as a bridge of technology
            dissemination on Management and Engineering from Japan to the Thai personnel widely, TPA has taken a major part in cultivating human resources to be competent
            in knowledge and skills in orer to help develop of the Kingdom's economy.  TNI establishing comittee comtempted that the philosophy of TPA and TNI
            regarding organization operation must be in accordance with each other in order to proliferate the value of activities and services and expand the servicing role
            by aiming TNI to be an academic institute focusing on creating and developing the Kingdom's personnel, a resource for creating new knowledge and a channel 
            for disseminating knowledge to sociey, especially industrial sector.  Therefore, TNI philosphy for operation is for \"developing knowledge,
            enriching industry for economy and society\" " ,

        'text_content_resolution' => "Thai-Nichi Instutue of Technology is a higher education institute that aims to become a center knowledge creatiion and dissemination of
            high-level professions in order to help develop adequate human resources in the fields of technology and management for the society with adherence to morality and
            social conscience" ,//_ปณิธาน

        'text_content_vision' => "Become one of the leading academic institutions in Thailand which provides knowledge in specialized fields of technology, management and  
            interpesonal skills,  has strong  domestics and internaltional linkage,  academic excellence and be the center of knowledge for the society" ,

        'text_content_mission'                   => $arr_texts1 ,
        'text_content_graduate_identity'         => '" Excellence in theoretical and practical knowledge, profession and creativity in Thai-Japanese accomplishment,
            competence in communication skill, morality and social consciousness. "',
        'text_content_institute_identity'        => '" Be a hub of academics and human resources development for business and industry in line with Thai-Japanese accomplishment. "',
        'text_content_institute_highlight_focus' => $arr_texts2,
        'text_content_value'                     => $arr_texts3,

        //_สัญลักษณ์และสีประจำสถาบัน
        'text_content_symbolic_1' => 'Symbol',
        'text_content_symbolic_2' => "Symbol Eliment",
        'text_content_symbolic_3' => $arr_symbolic_3,
        'text_content_symbolic_4' => "Meaning of TNI symbol",
        'text_content_symbolic_5' => $arr_symbolic_5,

        //_สีประจำสถาบันฯ
        'text_content_color_1' => 'Institute Colours',
        'text_content_color_2' => " TNI uses blue and red as Intitute colours",
        'text_content_color_3' => $arr_color_3,

        //_พระประธานประจำสถาบัน
        'text_content_principal_buddha_image_1' => 'Principal Buddha Image of the Institute ',
        'text_content_principal_buddha_image_2' => '"This This walking Buddha Image was installed on Wednesday 24 January B.E. 2550. Inside the Buddha Head contained the relics of the Lord Buddha which was 
                blessed and bestowed by the Supreme Patriarch of Thailand on the occasion of the opening ceremony of TNI and named this Buddha Image as “ Phra Buddha Mahamongkol Siripattana Stabun,
                The objective is to remind faculties and students to follow the teachings of the Load Buddha as stipulated below: 
                1.	สพฺพปาปสฺส อกรณํ (Committing no sin)
                2.	กุสลสฺสูปสมฺปทา (Doing good deeds as a way of life) 
                3.	สจิตฺตปริโยทปนํ (Achieve peace of mind and purity of soul)"',

        //_ต้นไม้ประจำสถาบัน
        'text_content_tree_1' => 'The Institute tree',
        'text_content_tree_2' => '"The Institute tree of TNI is Tabebuia Rosea (Bertal), which is also known as Thai Cherry Blossom. It is a medium-sized deciduous tree, 
        growing from 8 to 12 meters in height, pentafolioate palmately compound leaf with acute leaf tip 12 cm. in length, branch spreading out into densely bush, 
        shedding its leave in winter around November – January for flower bloom. The color of its petals is usually bright pink in February to April.  
        Flower bloom likes a bouquet bush of 5-8 pieces each and look like morning glory flower or horns. The flower base shaped in long tube and compounded with
         5 thin petals so it fell down easily.  We can see this flower scatter around the tree as beautiful as it get on the tree."',

        //_อนุสาวรีย์ผู้ก่อตั้ง
        'text_content_monument_feature'           => 'TPA Founder Momument-- H.E. Sommai Hoontrakul and Prof. Goichi Hozumi were co-founder of Technology Promotion Association 
            (Thailand-Japand) and a founder of concept idea in establishing Thai-Nichi-Institute of Technology.  This monument was officially inaugurated on the 5th anniversary of
            TNI establishment (28 July 2012).',



        // Board -----------------------------------------------------
         'text_board_and_executives' => 'Executives and Personnels',
         'text_board' => 'Board Member',
         'text_executives' => 'Executives',

        // list board ================================================//
        'text_name1' => '<b style="font-size:18px;">Mr.Supong Chayutsahakij</b>' ,
        'text_position1' => 'Chairman of Board of Council' ,
        'text_graduate1' =>'Vice Chairman of The Executive Board Bangkok Expressway and Metro Public Co., Ltd. D.B.A (Management) Phranakhon Rajabhat University, Thailand',

        'text_name2' => '<b style="font-size:18px;">Dr. Dr. Surapan  Meknavin</b>' ,
        'text_position2' => 'Vice Chairman of TNI Council' ,
        'text_graduate2' =>'President  of Technology Promotion Association (Thailand-Japan) Managing Director of the Guru Square Co.,Ltd.',

        'text_name3' => '<b style="font-size:18px;">Assoc.Prof.Krisada Visavateeranon</b>' ,
        'text_position3' => 'Board Member and President of TNI' ,
        'text_graduate3' =>'M.Eng. (Electrical Engineering), Kyoto University, Japan',

        'text_name4' => '<b style="font-size:18px;">Dr.Sumet Yamnoon</b>' ,
        'text_position4' => 'Board Member' ,
        'text_graduate4' =>'Ph.D. (Applied Statistics and Research Methods) University of Northern Colorado, USA',

        'text_name5' => '<b style="font-size:18px;">Assoc.Prof.Pichit Lumyong</b>' ,
        'text_position5' => 'Board Member' ,
        'text_graduate5' =>'DBA.(Finance), Chulalongkorn University, MBA (MIS) University of Dallas B.A. (Quantitative Approach), Chulalongkorn University ',

        'text_name6' => '<b style="font-size:18px;">Assoc.Prof.Dr. Pornanong Budsaratragoon</b>' ,
        'text_position6' => 'Board Member' ,
        'text_graduate6' =>'DBA.(Finance), Chulalongkorn University, MBA (MIS) University of Dallas B.A. (Quantitative Approach), Chulalongkorn University ',

        'text_name7' => '<b style="font-size:18px;">Assoc.Prof.Dr. Sucharit Koontanakulvong </b>' ,
        'text_position7' => 'Board Member ' ,
        'text_graduate7' =>'D.Eng. (Agricultural Engineering) Kyoto University, Japan',

        'text_name8' => '<b style="font-size:18px;"> Assoc.Prof. Pranee Chongsucharittham</b>' ,
        'text_position8' => 'Board Member' ,
        'text_graduate8' =>'Former Vice Dean of Research and Academic Services Department, Kasetsart University M.I.A. (Japanese Literature) Tsukuba University, Japan',

        'text_name9' => '<b style="font-size:18px;">Assoc.Prof. Dr. Virach Apimethithamrong</b> ' ,
        'text_position9' => 'Board Member' ,
        'text_graduate9' =>'Chairman of Dr. Virach and Associates Public Accounting Firm Ph.D. (Finance), University of Illinois, USA',

        'text_name10' => '<b style="font-size:18px;"> Dr. Krissanapong Kirtikara</b>' ,
        'text_position10' => 'Board Member ' ,
        'text_graduate10' =>'Adviser to the University, King Mongkut\'s University of Technology Thonburi (KMUTT) Ph.D. (Electrical Engineering) University of Glasgow, UK',

        'text_name11' => '<b style="font-size:18px;">Mr. Jiraphan Ulapathorn</b>' ,
        'text_position11' => 'Board Member' ,
        'text_graduate11' =>'President of the Sumipol Corporation Ltd. Master of Management, Sasin Graduate Institute of Business Administration of Chulalongkorn University',

        'text_name12' => '<b style="font-size:18px;">Prof.Dr. Wiwut Tantapanichkul</b>' ,
        'text_position12' => 'Board Member' ,
        'text_graduate12' =>'',

        'text_name13' => '<b style="font-size:18px;">Dr. Pasu Loharjun</b>' ,
        'text_position13' => 'Board Member' ,
        'text_graduate13' =>'',
        
        'text_name14' => '<b style="font-size:18px;">Asst.Prof.Rungsun Lertnaisat</b>' ,
        'text_position14' => 'Board Member <br> (Representatives of Faculty Members)' ,
        'text_graduate14' =>'MBA (Marketing), Kyoto University, Japan',

        'text_name15' => '<b style="font-size:18px;">Mr.Chartchan Phodhiphad</b>' ,
        'text_position15' => 'Secretary' ,
        'text_graduate15' =>'Director of Office of the President M.Sc. (Human Resource and Organization Development), NIDA',

        // 'text_name15' => '<br style="font-size:18px;">Mr.Chartchan Phodhiphad</br> ' ,
        // 'text_position15' => 'Secretary' ,
        // 'text_graduate15' =>'M.Sc. (Human Resource and Organization Development), NIDA',



        // executive =========================================================================//
  
        'text_ename1' => 'Assoc.Prof.Krisada  Visavateeranon' ,
        'text_eposition1' => '<b style"font-size:22px;">President</b> <br>Acting Vice President of Administration<br><br> ' ,
        'text_egraduate1' =>' M.Eng. (Electrical Engineering), Kyoto University, Japan',

        'text_ename2' => 'Assoc.Prof.Dr.Pichit Sukcharoenpong' ,
        'text_eposition2' => 'Vice President of Ademic Affairs Department,<br> Dean of Graduate School <br>  ' ,
        'text_egraduate2' =>'Ph.D. (Industrial Engineering and Management), Asian Institute of Technology, Thailand',

        'text_ename5' => 'Mrs.Pornanong Niyomka Horikawa' ,
        'text_eposition5' => 'Vice President of Student Affairs <br><br><br> ' ,
        'text_egraduate5' =>'M.Econ. (Economics), Hitotsubashi University, Japan',

        'text_ename7' => 'Assoc.Prof.Dr.Choompol Antarasena' ,
        'text_eposition7' => 'Dean of  Engineering Faculty <br><br><br> ' ,
        'text_egraduate7' =>'Ph.D. (Electronics), INSA Touluse, France',

        'text_ename4' => 'Assoc.Prof.Dr.Ruttikorn Varakulsiripunth' ,
        'text_eposition4' => 'Vice President of International Relations and Scholarship Affairs <br> Acting Vice President of Research and Innovation Department <br> Dean of Information Technology Faculty' ,
        'text_egraduate4' =>'Ph.D. (Electrical and Communications Engineering), Tohoku University, Japan <br> <br><br>',

        'text_ename3' => 'Asst.Prof.Rungsun Lertnaisat' ,
        'text_eposition3' => 'Vice President of Academic Services Center<br> Acting Vice President of Admissions and Organization Communications Center<br>Dean of Business Administration Faculty' ,
        'text_egraduate3' =>'MBA (Marketing), Kyoto University, Japan',

        'text_ename8' => 'Asst.Prof.Dr.Wanwimon Roungtheera' ,
        'text_eposition8' => 'Dean of College of General Education and Languages <br><br>' ,
        'text_egraduate8' =>'Ph.D.(Japanese Language and Culture), Osaka University of Foreign Studies, Japan',

        'text_ename9' => 'Mr.Chartcharn Phodhiphad' ,
        'text_eposition9' => 'Director of the Office of the President <br><br><br> ' ,
        'text_egraduate9' =>'M.Sc.(Human Resource and Organization Development) , National Institute of  Development Administration',

        'text_ename10' => 'Ms.Suwannapa Ruengsilkonlakarn' ,
        'text_eposition10' => 'Director of the Administration Department <br><br><br> ' ,
        'text_egraduate10' =>'B.A.(Political Science), Chulalongkorn University',

        'text_ename11' => 'Mr.Narong Anankavanich' ,
        'text_eposition11' => 'Director of Academic Affairs Department <br><br><br> ' ,
        'text_egraduate11' =>'MS. (Mechanical Engineering), University of Florida, U.S.A.',

        'text_ename12' => 'Mr.Surawat Yingsawad' ,
        'text_eposition12' => 'Director of Student Affairs Department <br><br><br>' ,
        'text_egraduate12' =>'M.A.(Physical Education), Kasetsart University',

        'text_ename13' => 'Ms.Supaporn Hempongsopa ' ,
        'text_eposition13' => 'Director of International Relations <br> and Scholarship Affairs <br><br>' ,
        'text_egraduate13' =>'M.Econ.(Economics), Ramkhamhaeng University',

        'text_ename6' => 'Assist.Prof Kasem Thiptarajan' ,
        'text_eposition6' => 'Assistant to President, Information Technology <br>' ,
        'text_egraduate6' =>'<br>Master of Science, Business Information Technology, Chulalongkorn Universiy',


        // facilities
        'text_facilities' => 'Supporting  Facilities',
        'text_eng_lab' =>'Labs, <br> Faculty of Engineeering  ',
        'text_it_lab' =>'Labs,  Faculty of <br> Information Technology',
        'text_ba_lab' =>'Labs, Faculty of <br> Business Administration',
        'text_ba_classroom' =>'Labs, Faculty of Business Administration ',
        'text_cgel_lab' =>'Lab,  College of  General <br> Education and Languages',
        'text_cgel_classroom' =>'Lab, College of General Education and Languages ',
         'text_ba_detail_room' => 'Innovative Learning, Research and Business Services Center provides consultation on  
         business management, production management based on Japanese manufacturing concept of 3T1S which are TPS TPM TQM and Shindan',
         'text_ba_room' => 'Monozukuri Research Center ',
         'text_cgel_detail_room' => 'Lab, College of General Education and Languages ',
         'text_library' =>'Library',

         //teacher 
         'text_teacher' => 'Lecturers\' profiles',
         'text_eng' =>'<br>Faculty of Engineering',
         'text_it' =>'Faculty of information Technology ',
         'text_ba' =>'Faculty of Business Administration ',
         'text_cgel' =>'College of General Education and Languages ',

       
    ];
?>