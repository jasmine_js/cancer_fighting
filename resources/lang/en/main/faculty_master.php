<?php

    return [
        // * START Master **************************************** //

        // ENG -------------------------------------------------------------//
        'text_eng' => 'Faculty of Engineering',
        'text_eng_detail' => 'Faculty of Engineering committed to be the place of academic knowledges to supply the qualified engineering workforce who are competent in practical 
            and theoretical knowledge, English and Japanese communication skill,  and social consciousness which are truly in line with the needs of business and industrial sector',
        'text_program' => 'Program',
        'text_master' => 'Master\'s Degree ',
        'text_eng_major' => '<li>Master\'s Degree of Engineering Technology  (MET) </li>
                            <p class="ml-3">program will educate students in extensive knowledge of Design, Analysis and Technology Development, Design, Manufacturing 
                            and Intelligent Automation, Smart Energy and Environment Systems, Innovation and Intelligent Management, and Intelligent Electrical 
                            and Computer Engineering Systems  (AI, IIoT) </p>',
      
        'text_eng_major1' => 'MIT',
        'text_detail_eng_major1' => '
                                    <p>1. Advanced Design and Robotic Intelligent Systems: ADRS </p>
                                    <p>2. Smart Energy and Environmental Systems: EES</p>
                                    <p>3. Intelligent Engineering Innovation and Management:EIM</p>
                                    <p>4. Smart Electrical and Computer Engineering System: SES</p>
                                ',               

        // BA -------------------------------------------------------------//
        'text_ba' => 'Business Administration Faculty',
        'text_ba_detail' => 'Business Administration Faculty aims to be the excellence academic center of business management for students and outsiders to have expertise 
            in management technology, and be able to apply modern management technology to benefit the industry and society with morality and social consciousness.
            TNI believes that the more you know the large chance to get succeeding.  Therefore, knowledge will be the most benefit if you learn from experienced people.',
        'text_program' => 'Program',
        'text_master' => 'Master\'s Degree',
        'text_ba_major' => '<li>Innovation of Business and Industrial Management (MBI)</li>
                            <li>Japanese Business Administration (MBJ)</li>
                            <li>Lean manufacturing system and logistics management (LMS)</li>',

        'text_ba_major1' => 'Innovation of Business and Industrial Management (MBI)', 
        'text_detail_ba_major1' =>'The Innovation of Business and Industrial Management program aims to develop abilities of entrepreneurs,middle and top-level executives to enhance their professionalism a
        nd to successfully manage businesses worldwide. The uniqueness of this program is that it offers East-Meets-West business management theories in order to prepare entrepreneurs 
        for the multicultural business model.  This course will also help graduates to create achievable business plans and allow entrepreneurs to have the opportunity 
        to learn from real leaders in the ASEAN business community.  This program focuses on the organizational diagnosis and business development, venture business planning,
        applications of advanced management tools and techniques for holistic problem solving, in order to reach business strategic decisions, which will create alignments 
        and synergies within the enterprise’s organizations and sustain advantages and successes for their businesses.
        ', 
        // 'text_detail_ba_major1' =>'
        // <p>- การจัดการอุตสาหกรรมเชิงนวัตกรรม (IIM) 
        //         มีความรู้ด้านการบริหารธุรกิจอุตสาหกรรมสมัยใหม่ผ่านนวัตกรรม สามารถยกระดับขีดความสามารถในการแข่งขันให้แก่องค์กรธุรกิจ 
        //          ความรู้ในการสร้างมูลค่าเพิ่ม และนวัตกรรมทางการผลิตให้แก่ธุรกิจอุตสาหกรรม เพื่อพัฒนาตัวเองเป็นนักอุตสาหกรรมสมัยใหม่ นักการผลิตสมัยใหม่ 
        //         ที่เน้นการใช้นวัตกรรม นำระบบการผลิตแบบญี่ปุ่นไปสู่การพัฒนา ผลิตภาพทางการผลิตให้สามารถแข่งขันได้สูงขึ้น 
        // </p>
        // <p>- &nbsp;การวางแผนและการจัดการเชิงกลยุทธ์สำหรับผู้ประกอบการ (SME) 
        //      มีความรู้เฉพาะทาง ทั้งทางทฤษฏีและปฏิบัติ การวินิจฉัยปัญหา การแก้ไขปัญหาของธุรกิจ มีความสามารถในการกำหนดกลยุทธ์และจัดทำแผนธุรกิจ 
        //      ที่นำไปใช้ประโยชน์ได้จริง พร้อมทั้งได้รับทักษะการบริหารธุรกิจแบบเจ้าของกิจการ เพื่อสร้างและพัฒนาธุรกิจ พัฒนาองค์กรทั้งระดับกลางและระดับสูง 
        // </p> 
       
        // ', 

        'text_ba_major2' => 'Japanese Business Administration (MBJ)', 
        'text_detail_ba_major2' => ' Program provides Japanese style of business and management, teaching by Thai and Japanese lecturers who are
         profession in international and Japanese business. This is to support students needs on learning Japanese business management and 
         higher level of Japanese Language. 
        ',  
        
        'text_ba_major3' => 'Lean manufacturing system and logistics management (LMS)', 
        'text_detail_ba_major3' => 'MBA-LMS is the Master\'s Degree Program that aims to create new generation of executives in line with Monozukuri 
            Principle who are smart in technology management and organizational development. Due to the rapid change of technology and innovation,
            TNI offers the Active Learning & Coaching on the Job Trianing through smart management in Japanese style as well as integrates the application 
            and practice on LMS software and Kaizen, IOT to be used efficiently. ',  


                                          
         // IT -------------------------------------------------------------//                       
        'text_it' => 'Faculty of Information Technology  ',
        'text_it_detail' => 'Information Technology Faculty aims to be the academics center on information technology to create and develop the personnel in hi-end information technology,
             excellent IT both research, application and to circulate knowledge to the society based on morality and ethics in profession ',
        'text_program' => 'Program',
        'text_master' => 'Master\'s Degree',
        'text_it_major' => '<li>Master of Information Technology (MIT)</li>',

        'text_it_major1' => 'Master of Information Technology (MIT)', 
        'text_detail_it_major1' => 'The curriculum is integrated Monozukuri and Hitozukuri Principles that focuses on creating Japanese style products and human resources.
         It also integrated knowledge on Information Technology ( especially in Business Information System and Data Analytics, Data Mining and Big Data);
          Multimedia Technology (especially in network technology, IoT, Artificial Intelligence (AI); and the mixed of learning by Practice and Research.', 

        // * END Master ****************************************** //
       
    ];