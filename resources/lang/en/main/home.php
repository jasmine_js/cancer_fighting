<?php

    return [
        'text_htmltitle' => 'Thai-Nichi Instutute of Technology',
        'text_course_heading' => 'THAI-NICHI INSTITUTE OF TECHNOLOGY',
        'text_bechelor' => 'Bechelor\'s Degree',
        'text_master' => 'Master\'s Degree',
        'text_international_program' => 'International Program',
        'text_scholarship' => 'Scholarship',
        'text_exchange_program' => 'Exchange Program',
        'text_education_fee' => 'Tuition Fees',
        'text_exchange_program_news' => 'Student Exchange Program Application',
        'text_highlight_activities' => 'HIGHLIGHT & ACTIVITIES',
        'text_news_more' => 'All News ',
        'text_event_update' => 'EVENT UPDATE',
        'text_event_more' => 'View All',
        'text_student_portfolio' => 'Student',
        'text_co_operative' => 'Co-operative Education & Job Placement Center',
        'text_click_here' => 'Click Here',
        'text_learn2know_heading' => 'Learn2know',
        'text_learn2know_subheading' => 'Learning from Integrating Listening into Practice',
        'text_learn2know_p1' => 'Learning from Listening through Situations',
        'text_learn2know_p2' => 'Variety of situations to practice in English & Japanese',
        'text_tni_channel' => 'TNI CHANNEL',
        'text_e_bochure' => 'E-BROCHURE',
        'text_more' => 'View All ',
        'text_history' => 'About TNI',
        'text_history_tni' => 'TNI History',
        'text_philosophy' => 'TNI Philosophy',
        'text_around_tni' => 'Around the Campus',
        'text_resolution' => 'Resolution',
        'text_vision' => 'Vision',
        'text_mission' => 'Mission',
        'text_faculty' => 'Faculties & Programs',
        'text_home' => '<i class="fas fa-home fa"></i> Home',

        //ร่วมงานกับเรา
        'text_career_tni' => 'Join Us',
        'text_application_form' =>'Application Download ',
        'text_download' =>'Download',
        'text_send_application' =>' " Application Submission <b style="color:#276fc1">hr@tni.ac.th</b> "',
        'text_travel' =>'<i style="color:red;">or</i> Apply in Person',

        'text_travel_detail' =>'- Getting to TNI:  - By BTS transfer to Ekkamai Station, then take the bus no.133 from That Thong Temple to TNI, Pattanakarn Soi 37    <br>
        - By MRT transfer to Phetchaburi Station, then take the bus no. 11 from New Pethchaburi Rd., to TNI Pattanakarn Soi 37   <br>
        - By Bus direct to TNI, No. 11, 133, 206 Air Bus :no 92, 517 ',

        'text_jobtni' => 'Job Vacancy Information ',
        'text_link_jobtni' => 'www.jobtni.com',

        // web link 
        'text_organization' =>'TNI\'s Donors ' ,

        // TNI-Presentation 
        'text_thai_version' => 'Thai Version',
        'text_english_version' => 'English Version',
        'text_japan_version' => 'Japanese Version',
        'text_tni_presentation' =>'TNI PRESENTATION',
        'text_10year_tni' =>'10th Anniversary of Thai-Nichi Institute of Technology (English Version) ',

        //policy cookies 
        'text_privacy_policy' => 'Privacy Policy ',
        'text_cookies_policy' => 'Cookies Policy ',




    ];