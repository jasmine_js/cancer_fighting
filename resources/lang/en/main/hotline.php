<?php 

    return [
        'text_name' => 'Name/Surname',
        'text_email' => 'Email',
        'text_tel' => 'Tel ',
        'text_message' => 'Message',
        'text_sendbutton' => 'Send Message to',
        'text_info' => 'If message failed to send, please email to tniinfo@tni.ac.th',
    ];