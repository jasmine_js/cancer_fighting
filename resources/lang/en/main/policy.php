<?php

    return [

        // privacy
        'text_privacy_title' => 'Privacy Policy : Thai-Nichi Institute of Technology',
        'text_detail' => 'TNI Privacy Policy aims to protect the privacy on personal data, fundamental operation of all activities of the institution,
                        such as student recruitment, teaching and learning management, research and development, HR, 
                        and various activities Involving individuals including  students, alumni, visitors, etc.  Therefore, 
                        TNI will regulate the policy in order to ensure the consistency in practice and procedure in administering legally below:  ',

        'text_list1' =>'1. The use of Personal Data  ',
        'text_list_detail1' => 'TNI will use and collect your personal data only necessary information such as name, surname, photo for the purpose of marketing 
                        or other activities. Your personal data will not be taken for sale or distributed to outsider unless have your permission.',

        'text_list2' =>'2. The Rights to control your Personal data',
        'text_list_detail2' => 'For prioritizing your privacy and safeguarding your personal data, 
                        you have the rights to permit or prohibit TNI for any using or sharing of your personal data so that we will process as your needs.',

        'text_list3' =>'3. Security and Handle of Personal Data ',
        'text_list_detail3' => 'For strictly confidence and security of your personal data, TNI has set out policies, rules and regulations within the agencies to 
                        determine the rights to access or use your personal data.',

        'text_list4' =>'4. How to use Cookie',
        'text_list_detail4' => 'Cookies is the small text file that TNI sent through web browser of the users.  
                        When such information is installed into your system,  Cookies will enable TNI to remember your preferences until your left out from your device.  
                        However, user can terminate the service by deleting Cookies out of your system. Cookies will convenience you to deliver the appropriate content that 
                        match your preferred interest. For data which Cookies has recorded and compiled, TNI will analyze or use it for other activities with an aim to 
                        our service improvement.',
                        
        'text_list5' =>'5. Personal Data Protection Policy',
        'text_list_detail5' => 'TNI has the rights to maintain or revise our privacy policy as appropriated  without prior notice. TNI requests users to read well 
                        the privacy policy every time when visit our website or use of our services.',

        'text_comment' => 'If you have any suggestion or iquiries regarding the compliance to our policy, you may contact us as follow:',
        
        'text_tni' => 'Thai -Nichi Institute of Technology ', 
        'text_address' => ' 1771/1 Pattanakarn 37, Pattanakarn Rd., Suan Luang, Bangkok 10250 ',
        'text_phone' =>'Tel 0-2-763-2600 Fax 02-763-2700',
        'text_email' => 'E-Mail : tniinfo@tni.ac.th ',
        
        
        
        
        
        // cookies 
        'text_cookies_title' => 'Cookie Policy : Thai-Nichi Institute of Technology ',

        'text_list_c1' =>'1. What is Cookie?',
        'text_list_detail_c1' => 'Cookie is a small text file that be saved in your PC or table or Mobile without dangerous to your devices or to be infected with Virus "......" .
                        While visiting the website, Cookie can track and keep information as well as remember you on how you use the website and activate again if you revisit.  
                        This will satify you for the online experiences.  Cookie is commonly used on website, social media, search engine, email etc. 
                        in remembering  your favorable registered login, theme selection, preferences, and other of your function settings.',

        'text_list_c2' =>'2.	Use of TNI Cookie',
        'text_list_detail_c2' => 'We utilize Google Analytics--a service from Google Inc. (Google) to analyze  website through cookies and disclose how you use our websit 
                        in order to improve our service and give your the good experience in your accessing on our systems.  
                        The alnalysis will be incognito mode and will not detect any of your persoanal data.  If you are several acessing 
                        "URL website" without any resetting your internet browser, therefore, your device will agree to accept cookie autonomuously 
                        for your next access.  You can reset it by selecting the " setting function" on your web browser to change on your setting, 
                        if needed be.   To do so, please be noted that you may not be able to use the full functions of our website.  However, 
                        for any visit to our website, we claim that you gree and allow Google to utilze your personal data for the said purpose.
        ',

        'text_list_c3' =>'3.	User Agreement ',
        'text_list_detail_c3' => 'If you are continued to use our website, it means you are agreeing to allow cookies to be installed on your device. 
        If you choose not to accept cookies We cannot guarantee that your experience will be as satisfying as it could be. ',


        // popup policy
        'text_popup_policy' => 'We use Cookie for our service , improvement and optimize your browsing experience. If you continue to use our website, you agree to the use of Cookie,',
        'text_link_more' =>'Click here for more informaition',
        'text_btn_accept' => 'ACCEPT',



    ]

?>