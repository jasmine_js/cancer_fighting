<?php

    return [
        
        'text_more' => '+ See More Information',
        'text_major' => 'Available Programs',

        'text_alumni' => 'TNI Alumni ',

        'text_major1' => ' " Mutimedia Technology  (MT) " ',
        // 'text_job1' => 'นักวิชาการคอมพิวเตอร์',
        'text_comment1' => 'Student life in TNI experienced me to work with my friend, surrounded in better environment,
         learnt with young generation teachers who can advice you and able to apply in work and lives. ',
        'text_name1' => 'Miss Rarinda Panichsook',

        'text_major2' => ' " International Business Management (IB) " ',
        // 'text_job2' => '  นักวิชาการอุตสากรรม  ',
        'text_comment2' => '  I got an opportunity to visit company in Japan through JTECS internship scholarships. Knowledge obtained from TNI is worth for my life.',
        'text_name2' => 'Ms.Preriya Tae-mee',

        'text_major3' => ' " Automotive Engineering  (AE) " ',
        // 'text_job3' => 'นักธุรกิจ',
        'text_comment3' => ' When I was young, playing toys like small car was my favor and happiness so I dreamed to do anything regarding to automobile and engine ',
        'text_name3' => 'Mr.Patchongt Plian-sakul ',

        // add 19-12-2019 juntima // 
        'text_comment4' => 'Research has its value than degree but the worthiest is how people understand our research.',
        'text_major4' => ' " Master\'s Degree (Engineering)" ',
        'text_name4' => 'Mr.Krit Chaisuriyakul (MET Batch 06 ID Code 59)<br> now working at Makino Milling Machine Co.,Ltd., Japan ',

        'text_comment5' => 'MET provided us not only the excellence on academic and research skills, but also the wonderful experience on publishing research in international Journal',
        'text_major5' => ' " Master\'s Degree (Engineering) " ',
        'text_name5' => 'Acting Sub Lt. Anond Panich-chiva (MET Batch  ID Code 59)<br> now working at Toyota Boshoku Asia Co., Ltd. ',

        'text_comment6' => ' Learning is part of MET Program as the curriculum provides us not only knowledge but also enable the creativity thinking on research and innovation.   ',
        'text_major6' => ' " Master\'s Degree (Engineering)" ',
        'text_name6' => 'Mr.Weerachat Wait-kama (MET Batch 07, ID Code 60)<br> now working at Thai Industrial Standards Institute, Ministry of Industry',
        
      
    ];