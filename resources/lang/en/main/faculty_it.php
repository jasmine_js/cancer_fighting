<?php

    return [
        'text_name' => 'Faculty of Information Technology',
        'text_detail' => 'Information Technology Faculty aims to be the academics center on information technology to create 
            and develop the personnel in hi-end information technology, excellent IT both research, application and to circulate knowledge 
            to the society based on morality and ethics in profession ',
        'text_program' => 'Program',
        'text_bachelor' => 'Bachelor\'s Degree',
        'text_major' => '<li>Information Technology (IT)</li>
                        <li>Mutimedia Technology (MT)</li>
                        <li>Business Information Technology (BI)</li>
                        <li>Digtital Communication (DC)</li>',
                       

        'text_major1' => 'Information Technology (IT)', 
        // 'text_detail_major1' => ' หลักสูตรวิทยาศาสตรบัณฑิต สาขาวิชาเทคโนโลยีสารสนเทศ เป็นหลักสูตรหนึ่งที่สอดคล้องกับปรัชญาของสถาบันฯ และ สภาพแวดล้อมทางธุรกิจที่ความก้าวหน้าด้านเทคโนโลยีสารสนเทศ
        //                         เข้ามามีบทบาทในการบริหารจัดการในองค์การสมัยใหม่มากยิ่งขึ้น จนถือเป็นทรัพยากรและปัจจัยเชิงกลยุทธ์ทีสําคัญหนึ่ง สถาบันฯ ได้เล็งเห็นความสําคัญของกระแสและการมีอิทธิพลของ
        //                         เทคโนโลยีด้านนี่ จึงได้ออกแบบหลักสูตรเทคโนโลยีสารสนเทศนี่ขึ้น โดยอิงมาตรฐานหลักสูตรของ The Association for Computing Machinery (ACM) โดยเป็นหลักสูตรที่มุ่งผลิต
        //                         บัณฑิตที่มีความรู้ชํานาญทางเทคโนโลยีสารสนเทศสําหรับธุรกิจภาคอุตสาหกรรมที่มีความสามารถในการออกแบบ เขียน วิเคราะห์ และบริหารจัดการระบบสารสนเทศได้เป็นอย่างดี และมีความ
        //                         พร้อมที่จะพัฒนาตนเองให้ทันกับการเปลี่ยนแปลงเทคโนโลยีด้านนี่ที่มีการพัฒนาเป็นไปอย่างรวดเร็ว',
        'text_detail_major1' => 'Students with the knowledge of information technology or IT are required by various companies as an essential human resources for
            economic development and society because IT is neccessary and can help carrying various activities.  TNI’s IT offers students both the roles of developer 
            and users through the hands-on practice an well as the internship at real workplace situation.',               

        'text_major2' => 'Multimedia Technology (MT)', 
        // 'text_detail_major2' => ' หลักสูตรวิทยาศาสตรบัณฑิต สาขาวิชาเทคโนโลยีมัลติมีเดีย เป็นหลักสูตรหนึ่งที่สอดคล้องกับปรัชญาของสถาบันฯ โดยเน้นการศึกษาควบคู่กับการฝึกปฏิบัติจริง เทคโนโลยีมัลติมีเดีย
        //                         มีลักษณะเป็นการผสมผสานกันขององค์ความรู้ทางด้านคอมพิวเตอร์การสื่อสาร และความรู้ทางด้านศิลปะเข้าด้วยกัน โดยนำเสนอผลงานผ่านวิทยาการ ด้านเทคโนโลยีมัลติมีเดียและการสื่อสาร
        //                         ซึ่งจะต้องมีพื้นฐานความรู้ทั้งทางด้านเทคโนโลยีและศิลปะเป็นสำคัญ บุคลากรในสายอาชีพมัลติมีเดียจึงเป็นที่ต้องการสูงของวงการอุตสาหกรรมซอฟต์แวร์แอนิเมชั่นและมัลติมีเดีย 
        //                         หลักสูตรเทคโนโลยีมัลติมีเดียนี้ มุ่งผลิตบัณฑิตที่มีความรู้ ความชำนาญ เสริมสร้างประสบการณ์ พัฒนาบัณฑิตที่มีศักยภาพ ทางเทคโนโลยีมัลติมีเดียสำหรับธุรกิจอุตสาหกรรมซอฟต์แวร์มัลติมีเดีย
        //                         เป็นนักพัฒนา/ออกแบบผลิต และบริหารโครงงานมัลติมีเดีย คอมพิวเตอร์แอนิเมชั่นและกราฟฟิก โดยเน้นการศึกษาควบคู่กับฝึกปฏิบัติจริง สามารถทำงานด้านแอนิเมชั่น เช่น ธุรกิจการค้าทาง
        //                         อินเตอร์เนต ธุรกิจที่ใช้โทรศัพท์มือถือ ธุรกิจภาพยนตร์ ธุรกิจโฆษณา หรือทำงานในสายงานคอมพิวเตอร์กราฟฟิก การพัฒนาเว็บไซต์ ตลอดจนงานอื่นๆ และสามารถศึกษาต่อในระดับปริญญาโทสาขาต่างๆ
        //                         ได้อย่างกว้างขวางทั้งในประเทศและต่างประเทศ',
        'text_detail_major2' => 'Focus on theory and practice learning that enable students to create quality works with modern technology in accordance with international standards.
          The program produce graduates with the knowledge on product design and analytical skills in multimedia technology as well as competent in self-learning and developing 
          and be able to catching up  the changes of current technology with the sense of creativity, morality and ethics.  Student works have been accepted by various outsiders 
          and organizations. 
                                ', 

        'text_major3' => 'Business Information (BI)', 
        // 'text_detail_major3' => ' หลักสูตรที่มุ่งผลิตบัณฑิตที่มีความรู้ ความชํานาญด้านเทคโนโลยีสารสนเทศสําหรับงานธุรกิจอุตสาหกรรม ที่มีความสามารถในการออกแบบ วิเคราะห์ ดําเนินการ 
        //                         และบริหารจัดการระบบสารสนเทศในองค์กร สามารถสื่อสารและทํางานร่วมกับผู้อื่นในองค์กรได้เป็นอย่างดี 
        //                         และมีความพร้อมที่จะพัฒนาตนเองให้ทันกับการเปลี่ยนแปลงเทคโนโลยีด้านนี้ที่มีการพัฒนาเป็นไปอย่างรวดเร็ว',    
        'text_detail_major3' => 'This program equips students a good skill in Information Technology while get better understanding about Business, enabling students to develop 
            or applying IT to be used efficiently in companies’ various functions.  The program also offers students learning on the most popular software such as SAP, Oracle 
            and Google Applications.  According to the business and industrial survey, the demand of graduates in this field are still higher.',    

        'text_major4' => 'Digital Technology (DC)', 
        // 'text_detail_major4' => ' หลักสูตรวิศวกรรมศาสตรบัณฑิต สาขาวิชาวิศวกรรมอุตสาหการ เป็นหลักสูตรที่มุ่งผลิตบัณฑิตให้เป็นผู้มีความรู้ทางด้านวิศวกรรมอุตสาหการ
        //                         ด้านการออกแบบพัฒนา ด้านกระบวนการผลิต ด้านการบริหารจัดการระบบการผลิต ด้านการจัดการโลจิสติกส์และโซ่อุปทาน
        //                         เพื่อให้เป็นวิศวกรที่มีความสามารถในการประยุกต์ความรู้ทางด้านวิศวกรรมศาสตร์ สามารถวิเคราะห์ สังเคราะห์
        //                         การแก้ปัญหาระบบการผลิตตลอดจนการปรับปรุงผลิตภาพในอุตสาหกรรม เป็นผู้มีทักษะภาษาไทย ภาษาอังกฤษและภาษาญี่ปุ่นเพื่อการสื่อสารได้เป็นอย่างดี
        //                         รวมทั้งเป็นผู้มีคุณธรรมและจริยธรรม มีจิตสำนึกสาธารณะในการรักษาทรัพยากรธรรมชาติและสิ่งแวดล้อมอย่างยั่งยืน
        //                         มีความตระหนักถึงความจำเป็นในการเรียนรู้ตลอดชีวิต เป็นวิศวกรที่มีขีดความสามารถทัดเทียมและสามารถทำงานได้ในระดับสากล',
        'text_detail_major4' => 'Digital Technology in Mass Communication Program has integrated both teaching and learning on communication and digital technology.
                Program offers up-to-date contents in line with the creative industrial driving plan and national development strategic plan "Thailand 4.0".
                It also focuses on academics and hand-on experiences according to the Monozukuri Principle that help lifting up students\' skills on analysis,
                design and work on master piece evaluation.  Students have the opportunities to learn from industrial profession, especially from Japan—the country 
                that being recognized in international level as a role model on mass communication industry. In addition, students can learn and practice with tools 
                and equipment at same standard on mass communication & media industry being used. 
        ',

        // 'text_major5' => 'Data Science And Analytics (International Program)', 
        // 'text_detail_major5' => ' หลักสูตรวิศวกรรมศาสตรบัณฑิต สาขาวิชาวิศวกรรมคอมพิวเตอร์ เป็นหลักสูตรหนึ่งที่สอดคล้องกับปรัชญาสถาบันโดยเป็นหลักสูตรที่มุ่งผลิตบัณฑิต
        //                         ที่มีความรู้ชำนาญทางวิศวกรรมคอมพิวเตอร์ที่มีความรู้ในการออกแบบ การสร้าง การติดตั้ง
        //                         และการบำรุงรักษาระบบฮาร์ดแวร์และซอฟท์แวร์ของระบบคอมพิวเตอร์และอุปกรณ์ที่ควบคุมด้วยคอมพิวเตอร์
        //                         และมุ่งผลิตบัณฑิตให้สามารถประยุกต์ความรู้ในการออกแบบด้านฮาร์ดแวร์ ซอฟ์ทแวร์ ระบบเครือข่าย
        //                         และเครื่องมืออุปกรณ์ที่ใช้คอมพิวเตอร์ควบคุมอยู่ภายในได้เป็นอย่างดี',                  
        
        // Inter
        'text_bacherlor_inter' => 'International Program',
        'text_inter_it_major' => '<li>Data Science and Analytics: DSA</li>',
        'text_inter_it_major1' => ' DSA: International Program ', 
        'text_detail_inter_it_major1' => 'In the new era of diverse information, the demand on data and immensely on data analysis are important and more valuable than ever.
            Someone said that “Data is the new oil”, it is the future career of young generation.  Likewise, the data analysis become business strategies in Thailand 4.0 
            and huge rewards with the competence in Japanese. ',
        
       
      
    ];