<?php

    return [
        // * START Master **************************************** //

        // ENG -------------------------------------------------------------//
        'text_eng' => 'คณะวิศวกรรมศาสตร์',
        'text_eng_detail' => 'คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีไทย-ญี่ปุ่น มุ่งมั่นเป็นแหล่งรวมวิชาการและผลิตวิศวกรที่มีคุณภาพ มีความเชี่ยวชาญ พร้อมทั้งสามารถประยุกต์ใช้เทคโนโลยีที่ทันสมัยให้เป็นประโยชน์ต่ออุตสาหกรรม 
                            และมีจิตสำนึกที่ดีต่อสังคม ผลิตบัณฑิตที่มีความรู้ความสามารถทั้งในด้านทฤษฎีและปฏิบัติ คิดเป็นทำเป็น และมีทักษะความสามารถในการสื่อสารภาษาญี่ปุ่นและภาษาอังกฤษ สู่ภาคอุตสาหกรรมและธุรกิจ 
                            ออกแบบหลักสูตรที่มีความเฉพาะและตอบสนองต่อความต้องการของผู้ใช้บัณฑิตอย่างแท้จริง',
        'text_program' => 'หลักสูตร/สาขา',
        'text_master' => 'ปริญญาโท',
        'text_eng_major' => '<li>วิศวกรรมศาสตรมหาบัณฑิต สาขาวิชาเทคโนโลยีวิศวกรรม (MET) </li>
                            <p class="ml-3">มหาบัณฑิตที่มีความรู้ ทักษะ ความเชี่ยวชาญในการออกแบบ วิเคราะห์ และพัฒนาเทคโนโลยีวิศวกรรม 
                            การออกแบบการผลิตและหุ่นยนต์อัตโนมัติอัจฉริยะ  ระบบพลังงานและสิ่งแวดล้อมอัจฉริยะ  นวัตกรรมและการจัดการอัจฉริยะ 
                            และระบบวิศวกรรมไฟฟ้าและคอมพิวเตอร์อัจฉริยะ (AI, IIoT) </p>
                           
                            ',
        // 'text_eng_major1' => 'สาขาวิชาเทคโนโลยีวิศวกรรม (MET)', 
        // 'text_detail_eng_major1' => ' มุ่งเน้นผลิตมหาบัณฑิตที่มีศักยภาพสูงในการทำงานด้านวิศวกรรมและการสร้างสรรค์เทคโนโลยี 
        // ควบคู่กับการพัฒนาความสามารถด้านภาษาอังกฤษ และภาษาญี่ปุ่น ตอบสนองความต้องการของภาคอุตสาหกรรม ตามแนวทางของญี่ปุ่น 
        // ที่เหมาะสมกับประเทศไทย สามารถเลือกเรียนวิชาเลือกได้จาก 4 กลุ่มวิชา ได้แก่ <br>
        //     - การออกแบบขั้นสูงและเทคโนโลยีการผลิต  (Advanced Design and Technology in Manufacturing) <br>
        //     - ระบบพลังงานและสิ่งแวดล้อม (Energy and Environmental System: EES) <br>
        //     - นวัตกรรมและการจัดการทางวิศวกรรม (Engineering Innovation and Management : EIM) <br>
        //     - ระบบวิศวกรรมไฟฟ้าอัจฉริยะ (Smart Electrical Engineering Systems: SES) ',
        'text_eng_major1' => 'วศ.ม. (เทคโนโลยีวิศวกรรม) ',
        'text_detail_eng_major1' => '
                                    <p>1. การออกแบบการผลิตและระบบหุ่นยนต์อัจฉริยะ <br> (Advanced Design and Robotic Intelligent Systems: ADRS) </p>
                                    <p>2. ระบบพลังงานและสิ่งแวดล้อมอัจฉริยะ <br> (Smart Energy and Environmental Systems: EES)</p>
                                    <p>3. นวัตกรรมและการจัดการทางวิศวกรรมอัจฉริยะ <br>(Intelligent Engineering Innovation and Management : EIM)</p>
                                    <p>4. ระบบวิศวกรรมไฟฟ้าและคอมพิวเตอร์อัจฉริยะ <br>(Smart Electrical and Computer Engineering Systems: SES)</p>
                                ',               

        // BA -------------------------------------------------------------//
        'text_ba' => 'คณะบริหารธุรกิจ',
        'text_ba_detail' => 'คณะบริหารธุรกิจ มุ่งมั่นเป็นศูนย์กลางทางวิชาการทางด้านการบริหาร การจัดการ ทั้งภายในและภายนอกสถาบันเทคโนโลยีไทย-ญี่ปุ่นเป็นแหล่งพัฒนาความเป็นเลิศให้แก่ 
                            นักศึกษา และบุคคลภายนอก ให้มีขีดความสามารถ (Ability) ทางด้านเทคโนโลยีการบริหารการจัดการ สามารถประยุกต์ใช้เทคโนโลยีการจัดการที่ทันสมัย ให้เป็นประโยชน์ต่ออุตสาหกรรม 
                            และสังคมอย่างมีคุณธรรมและจิตสำนึกต่อสังคม เพราะ TNI เชื่อว่าถ้ารู้มากกว่า โอกาสความสำเร็จก็เพิ่มขึ้น เพราะความรู้จะเกิดประโยชน์สูงสุดเมื่อมีการเสริมทักษะจากผู้มีประสบการณ์จริง ',
        'text_program' => 'หลักสูตร/สาขา',
        'text_master' => 'ปริญญาโท',
        'text_ba_major' => '<li>สาขาวิชานวัตกรรมการจัดการธุรกิจและอุตสาหกรรม (MBI)</li>
                            <li>สาขาวิชาบริหารธุรกิจญี่ปุ่น (MBJ)</li>
                            <li>สาขาวิชาการจัดการระบบการผลิตและโลจิสติกส์แบบลีน (LMS)</li>',

        'text_ba_major1' => 'นวัตกรรมการจัดการธุรกิจและอุตสาหกรรม (MBI)', 
        'text_detail_ba_major1' =>'
        <p>- การจัดการอุตสาหกรรมเชิงนวัตกรรม (IIM) 
                มีความรู้ด้านการบริหารธุรกิจอุตสาหกรรมสมัยใหม่ผ่านนวัตกรรม สามารถยกระดับขีดความสามารถในการแข่งขันให้แก่องค์กรธุรกิจ 
                 ความรู้ในการสร้างมูลค่าเพิ่ม และนวัตกรรมทางการผลิตให้แก่ธุรกิจอุตสาหกรรม เพื่อพัฒนาตัวเองเป็นนักอุตสาหกรรมสมัยใหม่ นักการผลิตสมัยใหม่ 
                ที่เน้นการใช้นวัตกรรม นำระบบการผลิตแบบญี่ปุ่นไปสู่การพัฒนา ผลิตภาพทางการผลิตให้สามารถแข่งขันได้สูงขึ้น 
        </p>
        <p>- &nbsp;การวางแผนและการจัดการเชิงกลยุทธ์สำหรับผู้ประกอบการ (SME) 
             มีความรู้เฉพาะทาง ทั้งทางทฤษฏีและปฏิบัติ การวินิจฉัยปัญหา การแก้ไขปัญหาของธุรกิจ มีความสามารถในการกำหนดกลยุทธ์และจัดทำแผนธุรกิจ 
             ที่นำไปใช้ประโยชน์ได้จริง พร้อมทั้งได้รับทักษะการบริหารธุรกิจแบบเจ้าของกิจการ เพื่อสร้างและพัฒนาธุรกิจ พัฒนาองค์กรทั้งระดับกลางและระดับสูง 
        </p> 
       
        ', 

        'text_ba_major2' => 'บริหารธุรกิจญี่ปุ่น (MBJ)', 
        'text_detail_ba_major2' => ' ผู้เรียนจะได้ศึกษา ศาสตร์การบริหารธุรกิจและธุรกิจแบบญี่ปุ่น สอนโดยคณาจารย์ผู้มีประสบการณ์ทางการบริหารธุรกิจสากล และแบบญี่ปุ่น
        ทั้งอาจารย์ชาวไทยและอาจารย์ชาวญี่ปุ่นที่มีความเชี่ยวชาญและมีประสบการณ์สูง ตอบสนองความต้องการของผู้ที่ต้องการเรียนรู้การบริหารแบบญี่ปุ่น และภาษาญี่ปุ่นระดับสูง
        ',  
        
        'text_ba_major3' => 'การจัดการระบบการผลิตและโลจิสติกส์แบบลีน (LMS)', 
        'text_detail_ba_major3' => ' MBA-LMS หลักสูตรปริญญาโทที่มุ่งสร้างผู้บริหารยุคใหม่สไตล์ Smart Monozukuri ท่ามกลางการเปลี่ยนแปลงของธุรกิจที่รวดเร็ว บวกกับความก้าวหน้าทางเทคโนโลยี
        และนวัตกรรมใหม่ๆที่ถูกพัฒนาขึ้น TNI จึงเปิดหลักสูตร MBA-LMS เพื่อสร้างผู้บริหารยุคใหม่สไตล์ Smart Monozukuri ด้วยเทคโนโลยีการจัดการเพื่อพัฒนาองค์กร 
        ด้วยเทคนิคการสอนแบบ Active Learning & Coaching on the Job Training และการใช้ศาสตร์การบริหารผ่านเครื่องมือสไตล์ญี่ปุ่น 
        พร้อมทั้งผสมผสานการปฏิบัติและการประยุกต์ใช้ LMS Software and Kaizen IOT ให้มีประสิทธิภาพสูงสุด ',  


                                          
         // IT -------------------------------------------------------------//                       
        'text_it' => 'คณะเทคโนโลยีสารสนเทศ',
        'text_it_detail' => 'คณะเทคโนโลยีสารสนเทศ สถาบันเทคโนโลยีไทย – ญี่ปุ่น มุ่งมั่นเป็นศูนย์กลางทางด้านวิชาการและเทคโนโลยีเพื่อเป็นแหล่งสร้างและพัฒนาบุคคลากรในด้านเทคโนโลยีสารสนเทศที่ทันสมัย  
                            มีความเป็นเลิศทางวิชาการ ทั้งการวิจัย การประยุกต์ และการเผยแพร่องค์ความรู้แก่สังคมโดยยึดมั่นในคุณธรรมและมีจริยธรรมในวิชาชีพ ',
        'text_program' => 'หลักสูตร/สาขา',
        'text_master' => 'ปริญญาโท',
        'text_it_major' => '<li>สาขาวิชาเทคโนโลยีสารสนเทศ (MIT)</li>',

        'text_it_major1' => 'เทคโนโลยีสารสนเทศ (MIT)', 
        'text_detail_it_major1' => 'หลักสูตรถูกออกแบบมาโดยการบูรณาการหลักการของ Monozukuri และ Hitozukuri ที่เน้นการสร้างของและสร้างคนในรูปแบบของญี่ปุ่น 
        โดยรวมทั้ง 3 สาขาด้วยกัน เพื่อความสมบูรณ์ขององค์ความรู้ทางเทคโนโลยีสารสนเทศ คือ ด้านสารสนเทศทางธุรกิจและการวิเคราะห์ข้อมูล (Business Information Systems and Data Analytics)
        การทำเหมืองข้อมูล (Data Mining and Big Data), ด้านเทคโนโลยีมัลติมีเดีย (Multimedia Technology),
        ด้านเทคโนโลยีระบบ เครือข่าย (Network Technology),  Internet of Things (IoT) และ Artificial  Intelligence (AI) 
        นอกจากนี้ยังเป็นการผสมผสานการเรียน แบบ Practical and Research ', 

        // * END Master ****************************************** //
       


        // // * STRAT Inter **************************************** //

        // // BA -------------------------------------------------------------//
        // 'text_inter_ba' => 'คณะบริหารธุรกิจ',
        // 'text_inter_ba_detail' => 'คณะบริหารธุรกิจ มุ่งมั่นเป็นศูนย์กลางทางวิชาการทางด้านการบริหาร การจัดการ ทั้งภายในและภายนอกสถาบันเทคโนโลยีไทย-ญี่ปุ่นเป็นแหล่งพัฒนาความเป็นเลิศให้แก่ 
        //                         นักศึกษา และบุคคลภายนอก ให้มีขีดความสามารถ (Ability) ทางด้านเทคโนโลยีการบริหารการ  จัดการ สามารถประยุกต์ใช้เทคโนโลยีการจัดการที่ทันสมัย ให้เป็นประโยชน์ต่ออุตสาหกรรม 
        //                         และสังคมอย่างมีคุณธรรมและจิตสำนึกต่อสังคม ',
        // 'text_program' => 'หลักสูตร/สาขา',
        // 'text_bacherlor_inter' => 'ปริญญาตรี',
        // 'text_inter_ba_major' => '<li>International Business Management (IBM)</li>',

        // 'text_inter_ba_major1' => 'International Business Management (IBM)', 
        // 'text_detail_inter_ba_major1' => '  In today\'s borderless world economy, both large and small companies must adapt to survive in highly competitive markets. 
        //                         The International Business Management program (IBM) endeavors to produce competent graduates who meet the ever-increasing standards 
        //                         to compete in the workforce market of tomorrow. Therefore, students will acquire invaluable knowledge from passionate and outstanding 
        //                         instructors who have a proven effective teaching from experience and academic distinction',           
                                
        // // ENG -------------------------------------------------------------//
        // 'text_inter_eng' => 'คณะวิศวกรรมศาสตร์',
        // 'text_inter_eng_detail' => 'คณะวิศวกรรมศาสตร์ สถาบันเทคโนโลยีไทย-ญี่ปุ่น มุ่งมั่นเป็นแหล่งรวมวิชาการและผลิตวิศวกรที่มีคุณภาพ มีความเชี่ยวชาญ พร้อมทั้งสามารถประยุกต์ใช้เทคโนโลยีที่ทันสมัยให้เป็นประโยชน์ต่ออุตสาหกรรม 
        //                         และมีจิตสำนึกที่ดีต่อสังคม ผลิตบัณฑิตที่มีความรู้ความสามารถทั้งในด้านทฤษฎีและปฏิบัติ คิดเป็นทำเป็น และมีทักษะความสามารถในการสื่อสารภาษาญี่ปุ่นและภาษาอังกฤษ สู่ภาคอุตสาหกรรมและธุรกิจ 
        //                         ออกแบบหลักสูตรที่มีความเฉพาะและตอบสนองต่อความต้องการของผู้ใช้บัณฑิตอย่างแท้จริง',
        // 'text_program' => 'หลักสูตร/สาขา',
        // 'text_bacherlor_inter' => 'ปริญญาตรี',
        // 'text_inter_eng_major' => '<li>Digital Engineering (DGE)</li>',

        // 'text_inter_eng_major1' => 'Digital Engineering (DGE)', 
        // 'text_detail_inter_eng_major1' => 'The Digital Engineering program (DGE) will impart Digital Engineers students with the critical skills needed in the following areas: Economics, 
        //                         Engineering, Novel Business Model, Digital Engineering, and Artificial Intelligence. The program will prepare the students for Industry 4.0 
        //                         and equip them with the ability to design, fabricate, assemble, install, and integrate computer hardware, software, and control systems. 
        //                         They will acquire “Digital Literacy” skills needed to apply engineering technology to improve automation systems 
        //                         and IoT and study about Intelligent Software, Intelligent Embedded Systems, and Intelligent System Integration, including for first stage 
        //                         and emerging S-curve industries and enterprises. <br>
        //                         TNI is confident that Digital Engineers graduates will have the requisite skills desired by companies involved in Cloud Computing, Big Data, 
        //                         Mobile Application, and Business Intelligence Solutions. ',               

        // // IT -------------------------------------------------------------//
        // 'text_inter_it' => 'คณะเทคโนโลยีสารสนเทศ',
        // 'text_inter_it_detail' => 'คณะเทคโนโลยีสารสนเทศ สถาบันเทคโนโลยีไทย – ญี่ปุ่น มุ่งมั่นเป็นศูนย์กลางทางด้านวิชาการและเทคโนโลยีเพื่อเป็นแหล่งสร้างและพัฒนาบุคคลากรในด้านเทคโนโลยีสารสนเทศที่ทันสมัย  
        //                         มีความเป็นเลิศทางวิชาการ ทั้งการวิจัย การประยุกต์ และการเผยแพร่องค์ความรู้แก่สังคมโดยยึดมั่นในคุณธรรมและมีจริยธรรมในวิชาชีพ',
        // 'text_program' => 'หลักสูตร/สาขา',
        // 'text_bacherlor_inter' => 'ปริญญาตรี',
        // 'text_inter_it_major' => '<li>Data Science And Analytics (DSA)</li>',

        // 'text_inter_it_major1' => 'Data Science And Analytics (DSA)', 
        // 'text_detail_inter_it_major1' => 'The Data Science and Analytics program (DSA) represents a new breed of multi-disciplinary fields of study, combining the science of statistics 
        //                         and computers with the art of business and visualization, to make better informed strategic decisions. Our DSA program aims to produce graduates 
        //                         of the next generation with the knowledge and skill sets that can revolutionize many vital industries, such as manufacturing, finance, services, 
        //                         health care, or even agriculture under the Thailand 4.0 policy. DSA students will learn the best practices that can transform data to insight,
        //                         discover and harness hidden patterns, and predict or adapt to future trends to improve business competitiveness. ',               

        // // * END Inter ****************************************** //
      
    ];