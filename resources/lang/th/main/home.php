<?php

    return [
        'text_htmltitle' => 'วิจัยและนวัตกรรม สถาบันเทคโนโลยีไทย-ญี่ปุ่น',
        'text_reaserch_heading' => 'RESEARCH AND INNOVATION THAI-NICHI INSTITUTE OF TECHNOLOGY',
        'text_article' => '- บทความแนะนำ -',
        'text_event_more' => 'ดูทั้งหมด',
        'text_news_more' => 'ข่าวทั้งหมด',
        'text_popular' => 'POPULAR',
        'text_more' => 'ดูทั้งหมด',
        'text_research_result' => 'ผลงานวิจัย',
        'text_patent' => 'สิทธิบัตร/อนุสิทธิบัตร',
        'text_other_web' => 'เว็บไซต์ที่เกี่ยวข้อง',


        // 'text_admission' => 'รับสมัครนักศึกษาใหม่',
        // 'text_bechelor' => 'ปริญญาตรี',
        // 'text_master' => 'ปริญญาโท',
        // 'text_international_program' => 'International Program',
        // 'text_scholarship' => 'ทุนการศึกษา นศ.ใหม่',
        // 'text_exchange_program' => 'ทุนและโครงการแลกเปลี่ยน',
        // 'text_education_fee' => 'ค่าเทอม',


        // 'text_exchange_program_news' => 'ประกาศรับสมัครโครงการแลกเปลี่ยน',
        // 'text_highlight_activities' => 'HIGHLIGHT & ACTIVITIES',
        // 'text_news_more' => 'ข่าวทั้งหมด',
        // 'text_event_update' => 'EVENT UPDATE',
        // 'text_event_more' => 'ดูทั้งหมด',
        // 'text_student_portfolio' => 'ผลงานนักศึกษา',
        // 'text_co_operative' => 'สหกิจศึกษาและฝึกงาน',
        // 'text_click_here' => 'Click Here',
        // 'text_learn2know_heading' => 'เลียนให้รู้ : Learn2know',
        // 'text_learn2know_subheading' => 'เรียนจากการฟัง รู้จากการฝึกฝน',
        // 'text_learn2know_p1' => 'Learning from listening Through Situations',
        // 'text_learn2know_p2' => 'Variety of situations to practice in English & Japanese',
        // 'text_tni_channel' => 'TNI CHANNEL',
        // 'text_e_bochure' => 'E-BROCHURE',
        // 'text_more' => 'ดูทั้งหมด',
        // 'text_history' => 'แนะนำสถาบันเทคโนโลยีไทย-ญี่ปุ่น',
        // 'text_history_tni' => 'ความเป็นมาของสถาบัน',
        // 'text_philosophy' => 'ปรัชญา',
        // 'text_around_tni' => 'รอบรั้วสถาบัน',
        // 'text_resolution' => 'ปณิธาน',
        // 'text_vision' => 'วิสัยทัศน์',
        // 'text_mission' => 'พันธกิจ',
        // 'text_faculty' => 'คณะและสาขา',
        'text_home' => '<i class="fas fa-home fa"></i> หน้าหลัก',

        // //ร่วมงานกับเรา
        // 'text_career_tni' => 'ร่วมงานกับเรา',
        // 'text_application_form' =>'ดาวน์โหลดใบสมัคร',
        // 'text_download' =>'Download',
        // 'text_send_application' =>' " เมื่อกรอกใบสมัครเรียบร้อยแล้ว สามารถส่งใบสมัครมาได้ที่ <b style="color:#276fc1">hr@tni.ac.th</b> "',
        // 'text_travel' =>'<i style="color:red;">หรือ</i> สามารถเดินทางมาสมัครด้วยตนเอง ดังนี้ ',

        // 'text_travel_detail' =>'- เดินทางด้วยรถไฟฟ้า (BTS) ลงที่สถานีเอกมัย ต่อรถประจำทางสาย 133 จากหน้าวัดธาตุทอง ลงรถที่ป้ายหน้าซอยพัฒนาการ 37 <br>
        // - เดินทางด้วยรถไฟฟ้ามหานคร (รถไฟใต้ดิน) ลงที่สถานีเพชรบุรี ต่อรถประจำทางสาย 11 บนถนนเพชรบุรีตัดใหม่ ลงรถที่ป้ายหน้าซอยพัฒนาการ 37 <br>
        // - รถประจำทางที่ผ่านสถาบันฯ ได้แก่ สาย 11, 133, 206, ปอ.92 และ ปอ.517',

        // 'text_jobtni' => 'ข้อมูลตำแหน่งงาน',
        // 'text_link_jobtni' => 'www.jobtni.com',

        // // web link 
        // 'text_organization' =>'องค์กรผู้ให้การสนับสนุนสถาบันเทคโนโลยีไทย-ญี่ปุ่น' ,

        // // TNI-Presentation 
        // 'text_thai_version' => 'Thai Version',
        // 'text_english_version' => 'English Version',
        // 'text_japan_version' => 'Japanese Version',
        // 'text_tni_presentation' =>'TNI PRESENTATION',
        // 'text_10year_tni' =>'10th Anniversary of Thai-Nichi Institute of Technology (English Version) ',

        // //policy cookies 
        // 'text_privacy_policy' => 'นโยบายส่วนบุคคล',
        // 'text_cookies_policy' => 'นโยบายคุกกี้',
    ];