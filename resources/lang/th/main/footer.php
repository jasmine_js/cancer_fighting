<?php

    return [
        'text_research_innovation' => 'วิจัยและนวัตกรรม',
        'text_research_result' => 'ผลงานวิจัย',
        'text_popular_research' => 'งานวิจัยเด่น',
        'text_patent' => 'สิทธิบัตร/อนุสิทธิบัตร',
        'text_innovation' => 'นวัตกรรม',
        'text_award' => 'รางวัล',
        'text_rasearch_lab' => 'ห้องปฏิบัติการวิจัย',
        'text_research_fund' => 'แหล่งทุนวิจัย',
        'text_research_management_system' => 'ระบบบริหารงานวิจัย',
        'text_about' => 'แนะนำหน่วยงาน',
       
        'text_contact' => 'ติดต่อเรา',
        'text_adress' => 'วิจัยและนวัตกรรม สถาบันเทคโนโลยี ไทย-ญี่ปุ่น 1771/1 ซ.พัฒนาการ 37 ถนนพัฒนาการ แขวงสวนหลวง เขตสวนหลวง กรุงเทพฯ 10250',
        'text_tel' => 'Tel. 0-2763-2600 ต่อ 2752 ',
        'text_email' => 'research@tni.ac.th',
        'text_fb' => 'Research TNI',

        'text_map' => 'แผนที่',
        'text_copyright' => '© 2021 RESEARCH AND INNOVATION THAI-NICHI INSTITUTE OF TECHNOLOGY (TNI) ALL RIGHTS RESERVED.',

        // 'text_other_link' => 'ลิงก์ที่เกี่ยวข้อง',
        // 'text_privacy_policy' => 'นโยบายส่วนบุคคล',
        // 'text_tpa' => 'สมาคมส่งเสริมเทคโนโลยี (ไทย-ญี่ปุ่น)',

       
    ];