<?php

    return [


        'text_ENG' => 'ENGINEERING',
        'text_eng' => 'คณะวิศวกรรมศาสตร์',

        'text_BA' => 'BUSINESS ADMINISTRATION',
        'text_ba' => 'คณะบริหารธุรกิจ',

        'text_IT' => 'INFORMATION TECHNOLOGY',
        'text_it' => 'คณะเทคโนโลยีสารสนเทศ',

        'text_CGEL' => 'C-GEL',
        'text_cgel' => 'สำนักวิชาพื้นฐานและภาษา',

     
    ];