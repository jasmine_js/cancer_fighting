<?php 

    return [
        'text_name' => 'ชื่อ/สกุล',
        'text_email' => 'อีเมล์',
        'text_tel' => 'โทรศัพท์',
        'text_message' => 'ข้อความ',
        'text_sendbutton' => 'ส่งข้อความ',
        'text_info' => 'หากส่งข้อความไม่สำเร็จสามารถส่งเมล์มาได้ที่ tniinfo@tni.ac.th',
       
    ];