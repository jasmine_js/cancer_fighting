<?php

    return [
        'text_department' => 'หน่วยงานและบริการทางวิชาการ ',
        'text_regiter_center' => 'ศูนย์รับสมัครนักศึกษา <br> สถาบันเทคโนโลยีไทย-ญี่ปุ่น',
        'text_public_relations' => 'ฝ่ายความร่วมมือระหว่างประเทศและทุนการศึกษา' ,
        'text_adminsitrator' =>'ฝ่ายบริหาร<br>งานบัญชีและการเงิน',
        'text_academic' =>'<br>ฝ่ายวิชาการ',
        'text_studen_affairs' =>'<br>ฝ่ายกิจการนักศึกษา',
        'text_cooperative_career' =>'<br>ศูนย์สหกิจศึกษาและจัดหางาน',
        'text_icc' =>'<br>ศูนย์สารสนเทศและการสื่อสาร',
        'text_research_and_academic_services' =>'<br>ฝ่ายวิจัยและบริการวิชาการ',      
        'text_journal_en' => 'TNI Journal of Engineering and Technology ',
        'text_journal_ba' => 'TNI Journal of Business Administration and Languages ',
        'text_community_services' => '<br>บริการวิชาการเพื่อสังคม',
        'text_library' => '<br>ศูนย์วิทยบริการ',
        'text_coe' => 'ศูนย์ความเชี่ยวชาญการบูรณาการระบบอัจฉริยะ (COE-ISI)',
        'text_aca_training' => 'ศูนย์บริการวิชาการ<br>และการวิจัย',




        //finance
        'text_finance' => 'ฝ่ายบริหาร งานบัญชีและการเงิน',
  
    ];