<?php 

    return [

        'text_login' => 'เข้าสู่ระบบ',
        'text_register' => 'สมัครสมาชิก',
        
        'text_research_result' => 'ผลงานวิจัย',
        'text_popular_research' => 'ผลงานวิจัยเด่น',
        'text_patent' => 'สิทธิบัตร/อนุสิทธิบัตร',
        'text_innovation' => 'หน่วยงานและบริการทางวิชาการ',
        'text_award' => 'รางวัล',

        //menu
        'text_rasearch_lab' => 'ห้องปฏิบัติการวิจัย',
        'text_research_fund' => 'แหล่งทุนวิจัย',
        'text_research_management_system' => 'ระบบบริหารงานวิจัย',
        'text_about' => 'แนะนำหน่วยงาน',
        'text_contact' => 'ติดต่อ',
        
       
    ];