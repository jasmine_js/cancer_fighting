<?php

return [

    'text_news_category_lists' => 'รายการหมวดหมู่ข่าว',
    'text_create_news_category' => 'สร้างหมวดหมู่ข่าว',
    'text_edit_news_category' => 'แก้ไขหมวดหมู่ข่าว',
    'text_id' => 'รหัส',
    'text_image' => 'รูป',
    'text_name' => 'ชื่อ',
    'text_edit_news_category' => 'แก้ไขหมวดหมู่ข่าว',
    
];
